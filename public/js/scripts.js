!function (s, d, p) {
	function f(e, t) {
		return typeof e === t
	}

	function h(e) {
		return e.replace(/([a-z])-([a-z])/g, function (e, t, n) {
			return t + n.toUpperCase()
		}).replace(/^-/, "")
	}

	function g(e) {
		return "function" != typeof d.createElement ? d.createElement(e) : y ? d.createElementNS.call(d, "http://www.w3.org/2000/svg", e) : d.createElement.apply(d, arguments)
	}

	function o(e, t, n, i) {
		var o, r, s, a, l = "modernizr", c = g("div"), u = function () {
			var e = d.body;
			return e || ((e = g(y ? "svg" : "body")).fake = !0), e
		}();
		if (parseInt(n, 10)) for (; n--;) (s = g("div")).id = i ? i[n] : l + (n + 1), c.appendChild(s);
		return (o = g("style")).type = "text/css", o.id = "s" + l, (u.fake ? u : c).appendChild(o), u.appendChild(c), o.styleSheet ? o.styleSheet.cssText = e : o.appendChild(d.createTextNode(e)), c.id = l, u.fake && (u.style.background = "", u.style.overflow = "hidden", a = m.style.overflow, m.style.overflow = "hidden", m.appendChild(u)), r = t(c, e), u.fake ? (u.parentNode.removeChild(u), m.style.overflow = a, m.offsetHeight) : c.parentNode.removeChild(c), !!r
	}

	function a(e, t) {
		return function () {
			return e.apply(t, arguments)
		}
	}

	function r(e) {
		return e.replace(/([A-Z])/g, function (e, t) {
			return "-" + t.toLowerCase()
		}).replace(/^ms-/, "-ms-")
	}

	function v(e, t) {
		var n = e.length;
		if ("CSS" in s && "supports" in s.CSS) {
			for (; n--;) if (s.CSS.supports(r(e[n]), t)) return !0;
			return !1
		}
		if ("CSSSupportsRule" in s) {
			for (var i = []; n--;) i.push("(" + r(e[n]) + ":" + t + ")");
			return o("@supports (" + (i = i.join(" or ")) + ") { #modernizr { position: absolute; } }", function (e) {
				return "absolute" == function (e, t, n) {
					var i;
					if ("getComputedStyle" in s) {
						i = getComputedStyle.call(s, e, t);
						var o = s.console;
						if (null !== i) n && (i = i.getPropertyValue(n)); else if (o) {
							o[o.error ? "error" : "log"].call(o, "getComputedStyle returning null, its possible modernizr test results are inaccurate")
						}
					} else i = !t && e.currentStyle && e.currentStyle[n];
					return i
				}(e, null, "position")
			})
		}
		return p
	}

	function i(e, t, n, i, o) {
		var r = e.charAt(0).toUpperCase() + e.slice(1), s = (e + " " + x.join(r + " ") + r).split(" ");
		return f(t, "string") || f(t, "undefined") ? function (e, t, n, i) {
			function o() {
				s && (delete C.style, delete C.modElem)
			}

			if (i = !f(i, "undefined") && i, !f(n, "undefined")) {
				var r = v(e, n);
				if (!f(r, "undefined")) return r
			}
			for (var s, a, l, c, u, d = ["modernizr", "tspan", "samp"]; !C.style && d.length;) s = !0, C.modElem = g(d.shift()), C.style = C.modElem.style;
			for (l = e.length, a = 0; a < l; a++) if (c = e[a], u = C.style[c], !~("" + c).indexOf("-") || (c = h(c)), C.style[c] !== p) {
				if (i || f(n, "undefined")) return o(), "pfx" != t || c;
				try {
					C.style[c] = n
				} catch (e) {
				}
				if (C.style[c] != u) return o(), "pfx" != t || c
			}
			return o(), !1
		}(s, t, i, o) : function (e, t, n) {
			var i;
			for (var o in e) if (e[o] in t) return !1 === n ? e[o] : f(i = t[e[o]], "function") ? a(i, n || t) : i;
			return !1
		}(s = (e + " " + k.join(r + " ") + r).split(" "), t, n)
	}

	var l = [], e = {
		_version: "3.5.0",
		_config: {classPrefix: "", enableClasses: !0, enableJSClass: !0, usePrefixes: !0},
		_q: [],
		on: function (e, t) {
			var n = this;
			setTimeout(function () {
				t(n[e])
			}, 0)
		},
		addTest: function (e, t, n) {
			l.push({name: e, fn: t, options: n})
		},
		addAsyncTest: function (e) {
			l.push({name: null, fn: e})
		}
	}, c = function () {
	};
	c.prototype = e, c = new c;
	var n, u = [], m = d.documentElement, y = "svg" === m.nodeName.toLowerCase(),
		t = (n = s.matchMedia || s.msMatchMedia) ? function (e) {
			var t = n(e);
			return t && t.matches || !1
		} : function (e) {
			var t = !1;
			return o("@media " + e + " { #modernizr { position: absolute; } }", function (e) {
				t = "absolute" == (s.getComputedStyle ? s.getComputedStyle(e, null) : e.currentStyle).position
			}), t
		};
	e.mq = t;
	var b = "Moz O ms Webkit", x = e._config.usePrefixes ? b.split(" ") : [];
	e._cssomPrefixes = x;

	function w(e) {
		var t, n = prefixes.length, i = s.CSSRule;
		if (void 0 === i) return p;
		if (!e) return !1;
		if ((t = (e = e.replace(/^@/, "")).replace(/-/g, "_").toUpperCase() + "_RULE") in i) return "@" + e;
		for (var o = 0; o < n; o++) {
			var r = prefixes[o];
			if (r.toUpperCase() + "_" + t in i) return "@-" + r.toLowerCase() + "-" + e
		}
		return !1
	}

	e.atRule = w;
	var k = e._config.usePrefixes ? b.toLowerCase().split(" ") : [];
	e._domPrefixes = k;
	var T = {elem: g("modernizr")};
	c._q.push(function () {
		delete T.elem
	});
	var C = {style: T.elem.style};
	c._q.unshift(function () {
		delete C.style
	}), e.testAllProps = i;
	var S = e.prefixed = function (e, t, n) {
		return 0 === e.indexOf("@") ? w(e) : (-1 != e.indexOf("-") && (e = h(e)), t ? i(e, t, n) : i(e, "pfx"))
	};
	c.addTest("objectfit", !!S("objectFit"), {aliases: ["object-fit"]}), function () {
		var e, t, n, i, o, r;
		for (var s in l) if (l.hasOwnProperty(s)) {
			if (e = [], (t = l[s]).name && (e.push(t.name.toLowerCase()), t.options && t.options.aliases && t.options.aliases.length)) for (n = 0; n < t.options.aliases.length; n++) e.push(t.options.aliases[n].toLowerCase());
			for (i = f(t.fn, "function") ? t.fn() : t.fn, o = 0; o < e.length; o++) 1 === (r = e[o].split(".")).length ? c[r[0]] = i : (!c[r[0]] || c[r[0]] instanceof Boolean || (c[r[0]] = new Boolean(c[r[0]])), c[r[0]][r[1]] = i), u.push((i ? "" : "no-") + r.join("-"))
		}
	}(), delete e.addTest, delete e.addAsyncTest;
	for (var $ = 0; $ < c._q.length; $++) c._q[$]();
	s.Modernizr = c
}(window, document), function (e, t) {
	"use strict";
	"object" == typeof module && "object" == typeof module.exports ? module.exports = e.document ? t(e, !0) : function (e) {
		if (!e.document) throw new Error("jQuery requires a window with a document");
		return t(e)
	} : t(e)
}("undefined" != typeof window ? window : this, function (T, e) {
	"use strict";
	var t = [], C = T.document, i = Object.getPrototypeOf, a = t.slice, g = t.concat, l = t.push, o = t.indexOf, n = {},
		r = n.toString, h = n.hasOwnProperty, s = h.toString, c = s.call(Object), v = {};

	function m(e, t) {
		var n = (t = t || C).createElement("script");
		n.text = e, t.head.appendChild(n).parentNode.removeChild(n)
	}

	function u(e, t) {
		return t.toUpperCase()
	}

	var S = function (e, t) {
		return new S.fn.init(e, t)
	}, d = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, p = /^-ms-/, f = /-([a-z])/g;

	function y(e) {
		var t = !!e && "length" in e && e.length, n = S.type(e);
		return "function" !== n && !S.isWindow(e) && ("array" === n || 0 === t || "number" == typeof t && 0 < t && t - 1 in e)
	}

	S.fn = S.prototype = {
		jquery: "3.1.1", constructor: S, length: 0, toArray: function () {
			return a.call(this)
		}, get: function (e) {
			return null == e ? a.call(this) : e < 0 ? this[e + this.length] : this[e]
		}, pushStack: function (e) {
			var t = S.merge(this.constructor(), e);
			return t.prevObject = this, t
		}, each: function (e) {
			return S.each(this, e)
		}, map: function (n) {
			return this.pushStack(S.map(this, function (e, t) {
				return n.call(e, t, e)
			}))
		}, slice: function () {
			return this.pushStack(a.apply(this, arguments))
		}, first: function () {
			return this.eq(0)
		}, last: function () {
			return this.eq(-1)
		}, eq: function (e) {
			var t = this.length, n = +e + (e < 0 ? t : 0);
			return this.pushStack(0 <= n && n < t ? [this[n]] : [])
		}, end: function () {
			return this.prevObject || this.constructor()
		}, push: l, sort: t.sort, splice: t.splice
	}, S.extend = S.fn.extend = function () {
		var e, t, n, i, o, r, s = arguments[0] || {}, a = 1, l = arguments.length, c = !1;
		for ("boolean" == typeof s && (c = s, s = arguments[a] || {}, a++), "object" == typeof s || S.isFunction(s) || (s = {}), a === l && (s = this, a--); a < l; a++) if (null != (e = arguments[a])) for (t in e) n = s[t], s !== (i = e[t]) && (c && i && (S.isPlainObject(i) || (o = S.isArray(i))) ? (r = o ? (o = !1, n && S.isArray(n) ? n : []) : n && S.isPlainObject(n) ? n : {}, s[t] = S.extend(c, r, i)) : void 0 !== i && (s[t] = i));
		return s
	}, S.extend({
		expando: "jQuery" + ("3.1.1" + Math.random()).replace(/\D/g, ""), isReady: !0, error: function (e) {
			throw new Error(e)
		}, noop: function () {
		}, isFunction: function (e) {
			return "function" === S.type(e)
		}, isArray: Array.isArray, isWindow: function (e) {
			return null != e && e === e.window
		}, isNumeric: function (e) {
			var t = S.type(e);
			return ("number" === t || "string" === t) && !isNaN(e - parseFloat(e))
		}, isPlainObject: function (e) {
			var t, n;
			return !(!e || "[object Object]" !== r.call(e) || (t = i(e)) && ("function" != typeof (n = h.call(t, "constructor") && t.constructor) || s.call(n) !== c))
		}, isEmptyObject: function (e) {
			var t;
			for (t in e) return !1;
			return !0
		}, type: function (e) {
			return null == e ? e + "" : "object" == typeof e || "function" == typeof e ? n[r.call(e)] || "object" : typeof e
		}, globalEval: function (e) {
			m(e)
		}, camelCase: function (e) {
			return e.replace(p, "ms-").replace(f, u)
		}, nodeName: function (e, t) {
			return e.nodeName && e.nodeName.toLowerCase() === t.toLowerCase()
		}, each: function (e, t) {
			var n, i = 0;
			if (y(e)) for (n = e.length; i < n && !1 !== t.call(e[i], i, e[i]); i++) ; else for (i in e) if (!1 === t.call(e[i], i, e[i])) break;
			return e
		}, trim: function (e) {
			return null == e ? "" : (e + "").replace(d, "")
		}, makeArray: function (e, t) {
			var n = t || [];
			return null != e && (y(Object(e)) ? S.merge(n, "string" == typeof e ? [e] : e) : l.call(n, e)), n
		}, inArray: function (e, t, n) {
			return null == t ? -1 : o.call(t, e, n)
		}, merge: function (e, t) {
			for (var n = +t.length, i = 0, o = e.length; i < n; i++) e[o++] = t[i];
			return e.length = o, e
		}, grep: function (e, t, n) {
			for (var i = [], o = 0, r = e.length, s = !n; o < r; o++) !t(e[o], o) != s && i.push(e[o]);
			return i
		}, map: function (e, t, n) {
			var i, o, r = 0, s = [];
			if (y(e)) for (i = e.length; r < i; r++) null != (o = t(e[r], r, n)) && s.push(o); else for (r in e) null != (o = t(e[r], r, n)) && s.push(o);
			return g.apply([], s)
		}, guid: 1, proxy: function (e, t) {
			var n, i, o;
			if ("string" == typeof t && (n = e[t], t = e, e = n), S.isFunction(e)) return i = a.call(arguments, 2), (o = function () {
				return e.apply(t || this, i.concat(a.call(arguments)))
			}).guid = e.guid = e.guid || S.guid++, o
		}, now: Date.now, support: v
	}), "function" == typeof Symbol && (S.fn[Symbol.iterator] = t[Symbol.iterator]), S.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), function (e, t) {
		n["[object " + t + "]"] = t.toLowerCase()
	});
	var b = function (n) {
		function d(e, t, n) {
			var i = "0x" + t - 65536;
			return i != i || n ? t : i < 0 ? String.fromCharCode(65536 + i) : String.fromCharCode(i >> 10 | 55296, 1023 & i | 56320)
		}

		function o() {
			k()
		}

		var e, f, x, r, s, h, p, g, w, l, c, k, T, a, C, v, u, m, y, S = "sizzle" + 1 * new Date, b = n.document, $ = 0,
			i = 0, E = se(), A = se(), D = se(), j = function (e, t) {
				return e === t && (c = !0), 0
			}, N = {}.hasOwnProperty, t = [], L = t.pop, O = t.push, q = t.push, H = t.slice, I = function (e, t) {
				for (var n = 0, i = e.length; n < i; n++) if (e[n] === t) return n;
				return -1
			},
			P = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
			F = "[\\x20\\t\\r\\n\\f]", _ = "(?:\\\\.|[\\w-]|[^\0-\\xa0])+",
			M = "\\[" + F + "*(" + _ + ")(?:" + F + "*([*^$|!~]?=)" + F + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + _ + "))|)" + F + "*\\]",
			z = ":(" + _ + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + M + ")*)|.*)\\)|)",
			R = new RegExp(F + "+", "g"), W = new RegExp("^" + F + "+|((?:^|[^\\\\])(?:\\\\.)*)" + F + "+$", "g"),
			B = new RegExp("^" + F + "*," + F + "*"), U = new RegExp("^" + F + "*([>+~]|" + F + ")" + F + "*"),
			X = new RegExp("=" + F + "*([^\\]'\"]*?)" + F + "*\\]", "g"), K = new RegExp(z),
			V = new RegExp("^" + _ + "$"), Y = {
				ID: new RegExp("^#(" + _ + ")"),
				CLASS: new RegExp("^\\.(" + _ + ")"),
				TAG: new RegExp("^(" + _ + "|[*])"),
				ATTR: new RegExp("^" + M),
				PSEUDO: new RegExp("^" + z),
				CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + F + "*(even|odd|(([+-]|)(\\d*)n|)" + F + "*(?:([+-]|)" + F + "*(\\d+)|))" + F + "*\\)|)", "i"),
				bool: new RegExp("^(?:" + P + ")$", "i"),
				needsContext: new RegExp("^" + F + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + F + "*((?:-\\d)?\\d*)" + F + "*\\)|)(?=[^-]|$)", "i")
			}, G = /^(?:input|select|textarea|button)$/i, J = /^h\d$/i, Q = /^[^{]+\{\s*\[native \w/,
			Z = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/, ee = /[+~]/,
			te = new RegExp("\\\\([\\da-f]{1,6}" + F + "?|(" + F + ")|.)", "ig"),
			ne = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g, ie = function (e, t) {
				return t ? "\0" === e ? "�" : e.slice(0, -1) + "\\" + e.charCodeAt(e.length - 1).toString(16) + " " : "\\" + e
			}, oe = ye(function (e) {
				return !0 === e.disabled && ("form" in e || "label" in e)
			}, {dir: "parentNode", next: "legend"});
		try {
			q.apply(t = H.call(b.childNodes), b.childNodes), t[b.childNodes.length].nodeType
		} catch (e) {
			q = {
				apply: t.length ? function (e, t) {
					O.apply(e, H.call(t))
				} : function (e, t) {
					for (var n = e.length, i = 0; e[n++] = t[i++];) ;
					e.length = n - 1
				}
			}
		}

		function re(e, t, n, i) {
			var o, r, s, a, l, c, u, d = t && t.ownerDocument, p = t ? t.nodeType : 9;
			if (n = n || [], "string" != typeof e || !e || 1 !== p && 9 !== p && 11 !== p) return n;
			if (!i && ((t ? t.ownerDocument || t : b) !== T && k(t), t = t || T, C)) {
				if (11 !== p && (l = Z.exec(e))) if (o = l[1]) {
					if (9 === p) {
						if (!(s = t.getElementById(o))) return n;
						if (s.id === o) return n.push(s), n
					} else if (d && (s = d.getElementById(o)) && y(t, s) && s.id === o) return n.push(s), n
				} else {
					if (l[2]) return q.apply(n, t.getElementsByTagName(e)), n;
					if ((o = l[3]) && f.getElementsByClassName && t.getElementsByClassName) return q.apply(n, t.getElementsByClassName(o)), n
				}
				if (f.qsa && !D[e + " "] && (!v || !v.test(e))) {
					if (1 !== p) d = t, u = e; else if ("object" !== t.nodeName.toLowerCase()) {
						for ((a = t.getAttribute("id")) ? a = a.replace(ne, ie) : t.setAttribute("id", a = S), r = (c = h(e)).length; r--;) c[r] = "#" + a + " " + me(c[r]);
						u = c.join(","), d = ee.test(e) && ge(t.parentNode) || t
					}
					if (u) try {
						return q.apply(n, d.querySelectorAll(u)), n
					} catch (e) {
					} finally {
						a === S && t.removeAttribute("id")
					}
				}
			}
			return g(e.replace(W, "$1"), t, n, i)
		}

		function se() {
			var i = [];
			return function e(t, n) {
				return i.push(t + " ") > x.cacheLength && delete e[i.shift()], e[t + " "] = n
			}
		}

		function ae(e) {
			return e[S] = !0, e
		}

		function le(e) {
			var t = T.createElement("fieldset");
			try {
				return !!e(t)
			} catch (e) {
				return !1
			} finally {
				t.parentNode && t.parentNode.removeChild(t), t = null
			}
		}

		function ce(e, t) {
			for (var n = e.split("|"), i = n.length; i--;) x.attrHandle[n[i]] = t
		}

		function ue(e, t) {
			var n = t && e, i = n && 1 === e.nodeType && 1 === t.nodeType && e.sourceIndex - t.sourceIndex;
			if (i) return i;
			if (n) for (; n = n.nextSibling;) if (n === t) return -1;
			return e ? 1 : -1
		}

		function de(t) {
			return function (e) {
				return "input" === e.nodeName.toLowerCase() && e.type === t
			}
		}

		function pe(n) {
			return function (e) {
				var t = e.nodeName.toLowerCase();
				return ("input" === t || "button" === t) && e.type === n
			}
		}

		function fe(t) {
			return function (e) {
				return "form" in e ? e.parentNode && !1 === e.disabled ? "label" in e ? "label" in e.parentNode ? e.parentNode.disabled === t : e.disabled === t : e.isDisabled === t || e.isDisabled !== !t && oe(e) === t : e.disabled === t : "label" in e && e.disabled === t
			}
		}

		function he(s) {
			return ae(function (r) {
				return r = +r, ae(function (e, t) {
					for (var n, i = s([], e.length, r), o = i.length; o--;) e[n = i[o]] && (e[n] = !(t[n] = e[n]))
				})
			})
		}

		function ge(e) {
			return e && void 0 !== e.getElementsByTagName && e
		}

		for (e in f = re.support = {}, s = re.isXML = function (e) {
			var t = e && (e.ownerDocument || e).documentElement;
			return !!t && "HTML" !== t.nodeName
		}, k = re.setDocument = function (e) {
			var t, n, i = e ? e.ownerDocument || e : b;
			return i !== T && 9 === i.nodeType && i.documentElement && (a = (T = i).documentElement, C = !s(T), b !== T && (n = T.defaultView) && n.top !== n && (n.addEventListener ? n.addEventListener("unload", o, !1) : n.attachEvent && n.attachEvent("onunload", o)), f.attributes = le(function (e) {
				return e.className = "i", !e.getAttribute("className")
			}), f.getElementsByTagName = le(function (e) {
				return e.appendChild(T.createComment("")), !e.getElementsByTagName("*").length
			}), f.getElementsByClassName = Q.test(T.getElementsByClassName), f.getById = le(function (e) {
				return a.appendChild(e).id = S, !T.getElementsByName || !T.getElementsByName(S).length
			}), f.getById ? (x.filter.ID = function (e) {
				var t = e.replace(te, d);
				return function (e) {
					return e.getAttribute("id") === t
				}
			}, x.find.ID = function (e, t) {
				if (void 0 !== t.getElementById && C) {
					var n = t.getElementById(e);
					return n ? [n] : []
				}
			}) : (x.filter.ID = function (e) {
				var n = e.replace(te, d);
				return function (e) {
					var t = void 0 !== e.getAttributeNode && e.getAttributeNode("id");
					return t && t.value === n
				}
			}, x.find.ID = function (e, t) {
				if (void 0 !== t.getElementById && C) {
					var n, i, o, r = t.getElementById(e);
					if (r) {
						if ((n = r.getAttributeNode("id")) && n.value === e) return [r];
						for (o = t.getElementsByName(e), i = 0; r = o[i++];) if ((n = r.getAttributeNode("id")) && n.value === e) return [r]
					}
					return []
				}
			}), x.find.TAG = f.getElementsByTagName ? function (e, t) {
				return void 0 !== t.getElementsByTagName ? t.getElementsByTagName(e) : f.qsa ? t.querySelectorAll(e) : void 0
			} : function (e, t) {
				var n, i = [], o = 0, r = t.getElementsByTagName(e);
				if ("*" !== e) return r;
				for (; n = r[o++];) 1 === n.nodeType && i.push(n);
				return i
			}, x.find.CLASS = f.getElementsByClassName && function (e, t) {
				if (void 0 !== t.getElementsByClassName && C) return t.getElementsByClassName(e)
			}, u = [], v = [], (f.qsa = Q.test(T.querySelectorAll)) && (le(function (e) {
				a.appendChild(e).innerHTML = "<a id='" + S + "'></a><select id='" + S + "-\r\\' msallowcapture=''><option selected=''></option></select>", e.querySelectorAll("[msallowcapture^='']").length && v.push("[*^$]=" + F + "*(?:''|\"\")"), e.querySelectorAll("[selected]").length || v.push("\\[" + F + "*(?:value|" + P + ")"), e.querySelectorAll("[id~=" + S + "-]").length || v.push("~="), e.querySelectorAll(":checked").length || v.push(":checked"), e.querySelectorAll("a#" + S + "+*").length || v.push(".#.+[+~]")
			}), le(function (e) {
				e.innerHTML = "<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";
				var t = T.createElement("input");
				t.setAttribute("type", "hidden"), e.appendChild(t).setAttribute("name", "D"), e.querySelectorAll("[name=d]").length && v.push("name" + F + "*[*^$|!~]?="), 2 !== e.querySelectorAll(":enabled").length && v.push(":enabled", ":disabled"), a.appendChild(e).disabled = !0, 2 !== e.querySelectorAll(":disabled").length && v.push(":enabled", ":disabled"), e.querySelectorAll("*,:x"), v.push(",.*:")
			})), (f.matchesSelector = Q.test(m = a.matches || a.webkitMatchesSelector || a.mozMatchesSelector || a.oMatchesSelector || a.msMatchesSelector)) && le(function (e) {
				f.disconnectedMatch = m.call(e, "*"), m.call(e, "[s!='']:x"), u.push("!=", z)
			}), v = v.length && new RegExp(v.join("|")), u = u.length && new RegExp(u.join("|")), t = Q.test(a.compareDocumentPosition), y = t || Q.test(a.contains) ? function (e, t) {
				var n = 9 === e.nodeType ? e.documentElement : e, i = t && t.parentNode;
				return e === i || !(!i || 1 !== i.nodeType || !(n.contains ? n.contains(i) : e.compareDocumentPosition && 16 & e.compareDocumentPosition(i)))
			} : function (e, t) {
				if (t) for (; t = t.parentNode;) if (t === e) return !0;
				return !1
			}, j = t ? function (e, t) {
				if (e === t) return c = !0, 0;
				var n = !e.compareDocumentPosition - !t.compareDocumentPosition;
				return n || (1 & (n = (e.ownerDocument || e) === (t.ownerDocument || t) ? e.compareDocumentPosition(t) : 1) || !f.sortDetached && t.compareDocumentPosition(e) === n ? e === T || e.ownerDocument === b && y(b, e) ? -1 : t === T || t.ownerDocument === b && y(b, t) ? 1 : l ? I(l, e) - I(l, t) : 0 : 4 & n ? -1 : 1)
			} : function (e, t) {
				if (e === t) return c = !0, 0;
				var n, i = 0, o = e.parentNode, r = t.parentNode, s = [e], a = [t];
				if (!o || !r) return e === T ? -1 : t === T ? 1 : o ? -1 : r ? 1 : l ? I(l, e) - I(l, t) : 0;
				if (o === r) return ue(e, t);
				for (n = e; n = n.parentNode;) s.unshift(n);
				for (n = t; n = n.parentNode;) a.unshift(n);
				for (; s[i] === a[i];) i++;
				return i ? ue(s[i], a[i]) : s[i] === b ? -1 : a[i] === b ? 1 : 0
			}), T
		}, re.matches = function (e, t) {
			return re(e, null, null, t)
		}, re.matchesSelector = function (e, t) {
			if ((e.ownerDocument || e) !== T && k(e), t = t.replace(X, "='$1']"), f.matchesSelector && C && !D[t + " "] && (!u || !u.test(t)) && (!v || !v.test(t))) try {
				var n = m.call(e, t);
				if (n || f.disconnectedMatch || e.document && 11 !== e.document.nodeType) return n
			} catch (e) {
			}
			return 0 < re(t, T, null, [e]).length
		}, re.contains = function (e, t) {
			return (e.ownerDocument || e) !== T && k(e), y(e, t)
		}, re.attr = function (e, t) {
			(e.ownerDocument || e) !== T && k(e);
			var n = x.attrHandle[t.toLowerCase()],
				i = n && N.call(x.attrHandle, t.toLowerCase()) ? n(e, t, !C) : void 0;
			return void 0 !== i ? i : f.attributes || !C ? e.getAttribute(t) : (i = e.getAttributeNode(t)) && i.specified ? i.value : null
		}, re.escape = function (e) {
			return (e + "").replace(ne, ie)
		}, re.error = function (e) {
			throw new Error("Syntax error, unrecognized expression: " + e)
		}, re.uniqueSort = function (e) {
			var t, n = [], i = 0, o = 0;
			if (c = !f.detectDuplicates, l = !f.sortStable && e.slice(0), e.sort(j), c) {
				for (; t = e[o++];) t === e[o] && (i = n.push(o));
				for (; i--;) e.splice(n[i], 1)
			}
			return l = null, e
		}, r = re.getText = function (e) {
			var t, n = "", i = 0, o = e.nodeType;
			if (o) {
				if (1 === o || 9 === o || 11 === o) {
					if ("string" == typeof e.textContent) return e.textContent;
					for (e = e.firstChild; e; e = e.nextSibling) n += r(e)
				} else if (3 === o || 4 === o) return e.nodeValue
			} else for (; t = e[i++];) n += r(t);
			return n
		}, (x = re.selectors = {
			cacheLength: 50,
			createPseudo: ae,
			match: Y,
			attrHandle: {},
			find: {},
			relative: {
				">": {dir: "parentNode", first: !0},
				" ": {dir: "parentNode"},
				"+": {dir: "previousSibling", first: !0},
				"~": {dir: "previousSibling"}
			},
			preFilter: {
				ATTR: function (e) {
					return e[1] = e[1].replace(te, d), e[3] = (e[3] || e[4] || e[5] || "").replace(te, d), "~=" === e[2] && (e[3] = " " + e[3] + " "), e.slice(0, 4)
				}, CHILD: function (e) {
					return e[1] = e[1].toLowerCase(), "nth" === e[1].slice(0, 3) ? (e[3] || re.error(e[0]), e[4] = +(e[4] ? e[5] + (e[6] || 1) : 2 * ("even" === e[3] || "odd" === e[3])), e[5] = +(e[7] + e[8] || "odd" === e[3])) : e[3] && re.error(e[0]), e
				}, PSEUDO: function (e) {
					var t, n = !e[6] && e[2];
					return Y.CHILD.test(e[0]) ? null : (e[3] ? e[2] = e[4] || e[5] || "" : n && K.test(n) && (t = h(n, !0)) && (t = n.indexOf(")", n.length - t) - n.length) && (e[0] = e[0].slice(0, t), e[2] = n.slice(0, t)), e.slice(0, 3))
				}
			},
			filter: {
				TAG: function (e) {
					var t = e.replace(te, d).toLowerCase();
					return "*" === e ? function () {
						return !0
					} : function (e) {
						return e.nodeName && e.nodeName.toLowerCase() === t
					}
				}, CLASS: function (e) {
					var t = E[e + " "];
					return t || (t = new RegExp("(^|" + F + ")" + e + "(" + F + "|$)")) && E(e, function (e) {
						return t.test("string" == typeof e.className && e.className || void 0 !== e.getAttribute && e.getAttribute("class") || "")
					})
				}, ATTR: function (n, i, o) {
					return function (e) {
						var t = re.attr(e, n);
						return null == t ? "!=" === i : !i || (t += "", "=" === i ? t === o : "!=" === i ? t !== o : "^=" === i ? o && 0 === t.indexOf(o) : "*=" === i ? o && -1 < t.indexOf(o) : "$=" === i ? o && t.slice(-o.length) === o : "~=" === i ? -1 < (" " + t.replace(R, " ") + " ").indexOf(o) : "|=" === i && (t === o || t.slice(0, o.length + 1) === o + "-"))
					}
				}, CHILD: function (h, e, t, g, v) {
					var m = "nth" !== h.slice(0, 3), y = "last" !== h.slice(-4), b = "of-type" === e;
					return 1 === g && 0 === v ? function (e) {
						return !!e.parentNode
					} : function (e, t, n) {
						var i, o, r, s, a, l, c = m != y ? "nextSibling" : "previousSibling", u = e.parentNode,
							d = b && e.nodeName.toLowerCase(), p = !n && !b, f = !1;
						if (u) {
							if (m) {
								for (; c;) {
									for (s = e; s = s[c];) if (b ? s.nodeName.toLowerCase() === d : 1 === s.nodeType) return !1;
									l = c = "only" === h && !l && "nextSibling"
								}
								return !0
							}
							if (l = [y ? u.firstChild : u.lastChild], y && p) {
								for (f = (a = (i = (o = (r = (s = u)[S] || (s[S] = {}))[s.uniqueID] || (r[s.uniqueID] = {}))[h] || [])[0] === $ && i[1]) && i[2], s = a && u.childNodes[a]; s = ++a && s && s[c] || (f = a = 0) || l.pop();) if (1 === s.nodeType && ++f && s === e) {
									o[h] = [$, a, f];
									break
								}
							} else if (p && (f = a = (i = (o = (r = (s = e)[S] || (s[S] = {}))[s.uniqueID] || (r[s.uniqueID] = {}))[h] || [])[0] === $ && i[1]), !1 === f) for (; (s = ++a && s && s[c] || (f = a = 0) || l.pop()) && ((b ? s.nodeName.toLowerCase() !== d : 1 !== s.nodeType) || !++f || (p && ((o = (r = s[S] || (s[S] = {}))[s.uniqueID] || (r[s.uniqueID] = {}))[h] = [$, f]), s !== e));) ;
							return (f -= v) === g || f % g == 0 && 0 <= f / g
						}
					}
				}, PSEUDO: function (e, r) {
					var t, s = x.pseudos[e] || x.setFilters[e.toLowerCase()] || re.error("unsupported pseudo: " + e);
					return s[S] ? s(r) : 1 < s.length ? (t = [e, e, "", r], x.setFilters.hasOwnProperty(e.toLowerCase()) ? ae(function (e, t) {
						for (var n, i = s(e, r), o = i.length; o--;) e[n = I(e, i[o])] = !(t[n] = i[o])
					}) : function (e) {
						return s(e, 0, t)
					}) : s
				}
			},
			pseudos: {
				not: ae(function (e) {
					var i = [], o = [], a = p(e.replace(W, "$1"));
					return a[S] ? ae(function (e, t, n, i) {
						for (var o, r = a(e, null, i, []), s = e.length; s--;) (o = r[s]) && (e[s] = !(t[s] = o))
					}) : function (e, t, n) {
						return i[0] = e, a(i, null, n, o), i[0] = null, !o.pop()
					}
				}), has: ae(function (t) {
					return function (e) {
						return 0 < re(t, e).length
					}
				}), contains: ae(function (t) {
					return t = t.replace(te, d), function (e) {
						return -1 < (e.textContent || e.innerText || r(e)).indexOf(t)
					}
				}), lang: ae(function (n) {
					return V.test(n || "") || re.error("unsupported lang: " + n), n = n.replace(te, d).toLowerCase(), function (e) {
						var t;
						do {
							if (t = C ? e.lang : e.getAttribute("xml:lang") || e.getAttribute("lang")) return (t = t.toLowerCase()) === n || 0 === t.indexOf(n + "-")
						} while ((e = e.parentNode) && 1 === e.nodeType);
						return !1
					}
				}), target: function (e) {
					var t = n.location && n.location.hash;
					return t && t.slice(1) === e.id
				}, root: function (e) {
					return e === a
				}, focus: function (e) {
					return e === T.activeElement && (!T.hasFocus || T.hasFocus()) && !!(e.type || e.href || ~e.tabIndex)
				}, enabled: fe(!1), disabled: fe(!0), checked: function (e) {
					var t = e.nodeName.toLowerCase();
					return "input" === t && !!e.checked || "option" === t && !!e.selected
				}, selected: function (e) {
					return e.parentNode && e.parentNode.selectedIndex, !0 === e.selected
				}, empty: function (e) {
					for (e = e.firstChild; e; e = e.nextSibling) if (e.nodeType < 6) return !1;
					return !0
				}, parent: function (e) {
					return !x.pseudos.empty(e)
				}, header: function (e) {
					return J.test(e.nodeName)
				}, input: function (e) {
					return G.test(e.nodeName)
				}, button: function (e) {
					var t = e.nodeName.toLowerCase();
					return "input" === t && "button" === e.type || "button" === t
				}, text: function (e) {
					var t;
					return "input" === e.nodeName.toLowerCase() && "text" === e.type && (null == (t = e.getAttribute("type")) || "text" === t.toLowerCase())
				}, first: he(function () {
					return [0]
				}), last: he(function (e, t) {
					return [t - 1]
				}), eq: he(function (e, t, n) {
					return [n < 0 ? n + t : n]
				}), even: he(function (e, t) {
					for (var n = 0; n < t; n += 2) e.push(n);
					return e
				}), odd: he(function (e, t) {
					for (var n = 1; n < t; n += 2) e.push(n);
					return e
				}), lt: he(function (e, t, n) {
					for (var i = n < 0 ? n + t : n; 0 <= --i;) e.push(i);
					return e
				}), gt: he(function (e, t, n) {
					for (var i = n < 0 ? n + t : n; ++i < t;) e.push(i);
					return e
				})
			}
		}).pseudos.nth = x.pseudos.eq, {
			radio: !0,
			checkbox: !0,
			file: !0,
			password: !0,
			image: !0
		}) x.pseudos[e] = de(e);
		for (e in {submit: !0, reset: !0}) x.pseudos[e] = pe(e);

		function ve() {
		}

		function me(e) {
			for (var t = 0, n = e.length, i = ""; t < n; t++) i += e[t].value;
			return i
		}

		function ye(a, e, t) {
			var l = e.dir, c = e.next, u = c || l, d = t && "parentNode" === u, p = i++;
			return e.first ? function (e, t, n) {
				for (; e = e[l];) if (1 === e.nodeType || d) return a(e, t, n);
				return !1
			} : function (e, t, n) {
				var i, o, r, s = [$, p];
				if (n) {
					for (; e = e[l];) if ((1 === e.nodeType || d) && a(e, t, n)) return !0
				} else for (; e = e[l];) if (1 === e.nodeType || d) if (o = (r = e[S] || (e[S] = {}))[e.uniqueID] || (r[e.uniqueID] = {}), c && c === e.nodeName.toLowerCase()) e = e[l] || e; else {
					if ((i = o[u]) && i[0] === $ && i[1] === p) return s[2] = i[2];
					if ((o[u] = s)[2] = a(e, t, n)) return !0
				}
				return !1
			}
		}

		function be(o) {
			return 1 < o.length ? function (e, t, n) {
				for (var i = o.length; i--;) if (!o[i](e, t, n)) return !1;
				return !0
			} : o[0]
		}

		function xe(e, t, n, i, o) {
			for (var r, s = [], a = 0, l = e.length, c = null != t; a < l; a++) (r = e[a]) && (n && !n(r, i, o) || (s.push(r), c && t.push(a)));
			return s
		}

		function we(f, h, g, v, m, e) {
			return v && !v[S] && (v = we(v)), m && !m[S] && (m = we(m, e)), ae(function (e, t, n, i) {
				var o, r, s, a = [], l = [], c = t.length, u = e || function (e, t, n) {
						for (var i = 0, o = t.length; i < o; i++) re(e, t[i], n);
						return n
					}(h || "*", n.nodeType ? [n] : n, []), d = !f || !e && h ? u : xe(u, a, f, n, i),
					p = g ? m || (e ? f : c || v) ? [] : t : d;
				if (g && g(d, p, n, i), v) for (o = xe(p, l), v(o, [], n, i), r = o.length; r--;) (s = o[r]) && (p[l[r]] = !(d[l[r]] = s));
				if (e) {
					if (m || f) {
						if (m) {
							for (o = [], r = p.length; r--;) (s = p[r]) && o.push(d[r] = s);
							m(null, p = [], o, i)
						}
						for (r = p.length; r--;) (s = p[r]) && -1 < (o = m ? I(e, s) : a[r]) && (e[o] = !(t[o] = s))
					}
				} else p = xe(p === t ? p.splice(c, p.length) : p), m ? m(null, t, p, i) : q.apply(t, p)
			})
		}

		function ke(e) {
			for (var o, t, n, i = e.length, r = x.relative[e[0].type], s = r || x.relative[" "], a = r ? 1 : 0, l = ye(function (e) {
				return e === o
			}, s, !0), c = ye(function (e) {
				return -1 < I(o, e)
			}, s, !0), u = [function (e, t, n) {
				var i = !r && (n || t !== w) || ((o = t).nodeType ? l(e, t, n) : c(e, t, n));
				return o = null, i
			}]; a < i; a++) if (t = x.relative[e[a].type]) u = [ye(be(u), t)]; else {
				if ((t = x.filter[e[a].type].apply(null, e[a].matches))[S]) {
					for (n = ++a; n < i && !x.relative[e[n].type]; n++) ;
					return we(1 < a && be(u), 1 < a && me(e.slice(0, a - 1).concat({value: " " === e[a - 2].type ? "*" : ""})).replace(W, "$1"), t, a < n && ke(e.slice(a, n)), n < i && ke(e = e.slice(n)), n < i && me(e))
				}
				u.push(t)
			}
			return be(u)
		}

		function Te(v, m) {
			function e(e, t, n, i, o) {
				var r, s, a, l = 0, c = "0", u = e && [], d = [], p = w, f = e || b && x.find.TAG("*", o),
					h = $ += null == p ? 1 : Math.random() || .1, g = f.length;
				for (o && (w = t === T || t || o); c !== g && null != (r = f[c]); c++) {
					if (b && r) {
						for (s = 0, t || r.ownerDocument === T || (k(r), n = !C); a = v[s++];) if (a(r, t || T, n)) {
							i.push(r);
							break
						}
						o && ($ = h)
					}
					y && ((r = !a && r) && l--, e && u.push(r))
				}
				if (l += c, y && c !== l) {
					for (s = 0; a = m[s++];) a(u, d, t, n);
					if (e) {
						if (0 < l) for (; c--;) u[c] || d[c] || (d[c] = L.call(i));
						d = xe(d)
					}
					q.apply(i, d), o && !e && 0 < d.length && 1 < l + m.length && re.uniqueSort(i)
				}
				return o && ($ = h, w = p), u
			}

			var y = 0 < m.length, b = 0 < v.length;
			return y ? ae(e) : e
		}

		return ve.prototype = x.filters = x.pseudos, x.setFilters = new ve, h = re.tokenize = function (e, t) {
			var n, i, o, r, s, a, l, c = A[e + " "];
			if (c) return t ? 0 : c.slice(0);
			for (s = e, a = [], l = x.preFilter; s;) {
				for (r in n && !(i = B.exec(s)) || (i && (s = s.slice(i[0].length) || s), a.push(o = [])), n = !1, (i = U.exec(s)) && (n = i.shift(), o.push({
					value: n,
					type: i[0].replace(W, " ")
				}), s = s.slice(n.length)), x.filter) !(i = Y[r].exec(s)) || l[r] && !(i = l[r](i)) || (n = i.shift(), o.push({
					value: n,
					type: r,
					matches: i
				}), s = s.slice(n.length));
				if (!n) break
			}
			return t ? s.length : s ? re.error(e) : A(e, a).slice(0)
		}, p = re.compile = function (e, t) {
			var n, i = [], o = [], r = D[e + " "];
			if (!r) {
				for (n = (t = t || h(e)).length; n--;) (r = ke(t[n]))[S] ? i.push(r) : o.push(r);
				(r = D(e, Te(o, i))).selector = e
			}
			return r
		}, g = re.select = function (e, t, n, i) {
			var o, r, s, a, l, c = "function" == typeof e && e, u = !i && h(e = c.selector || e);
			if (n = n || [], 1 === u.length) {
				if (2 < (r = u[0] = u[0].slice(0)).length && "ID" === (s = r[0]).type && 9 === t.nodeType && C && x.relative[r[1].type]) {
					if (!(t = (x.find.ID(s.matches[0].replace(te, d), t) || [])[0])) return n;
					c && (t = t.parentNode), e = e.slice(r.shift().value.length)
				}
				for (o = Y.needsContext.test(e) ? 0 : r.length; o-- && (s = r[o], !x.relative[a = s.type]);) if ((l = x.find[a]) && (i = l(s.matches[0].replace(te, d), ee.test(r[0].type) && ge(t.parentNode) || t))) {
					if (r.splice(o, 1), !(e = i.length && me(r))) return q.apply(n, i), n;
					break
				}
			}
			return (c || p(e, u))(i, t, !C, n, !t || ee.test(e) && ge(t.parentNode) || t), n
		}, f.sortStable = S.split("").sort(j).join("") === S, f.detectDuplicates = !!c, k(), f.sortDetached = le(function (e) {
			return 1 & e.compareDocumentPosition(T.createElement("fieldset"))
		}), le(function (e) {
			return e.innerHTML = "<a href='#'></a>", "#" === e.firstChild.getAttribute("href")
		}) || ce("type|href|height|width", function (e, t, n) {
			if (!n) return e.getAttribute(t, "type" === t.toLowerCase() ? 1 : 2)
		}), f.attributes && le(function (e) {
			return e.innerHTML = "<input/>", e.firstChild.setAttribute("value", ""), "" === e.firstChild.getAttribute("value")
		}) || ce("value", function (e, t, n) {
			if (!n && "input" === e.nodeName.toLowerCase()) return e.defaultValue
		}), le(function (e) {
			return null == e.getAttribute("disabled")
		}) || ce(P, function (e, t, n) {
			var i;
			if (!n) return !0 === e[t] ? t.toLowerCase() : (i = e.getAttributeNode(t)) && i.specified ? i.value : null
		}), re
	}(T);
	S.find = b, S.expr = b.selectors, S.expr[":"] = S.expr.pseudos, S.uniqueSort = S.unique = b.uniqueSort, S.text = b.getText, S.isXMLDoc = b.isXML, S.contains = b.contains, S.escapeSelector = b.escape;

	function x(e, t, n) {
		for (var i = [], o = void 0 !== n; (e = e[t]) && 9 !== e.nodeType;) if (1 === e.nodeType) {
			if (o && S(e).is(n)) break;
			i.push(e)
		}
		return i
	}

	function w(e, t) {
		for (var n = []; e; e = e.nextSibling) 1 === e.nodeType && e !== t && n.push(e);
		return n
	}

	var k = S.expr.match.needsContext, $ = /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i,
		E = /^.[^:#\[\.,]*$/;

	function A(e, n, i) {
		return S.isFunction(n) ? S.grep(e, function (e, t) {
			return !!n.call(e, t, e) !== i
		}) : n.nodeType ? S.grep(e, function (e) {
			return e === n !== i
		}) : "string" != typeof n ? S.grep(e, function (e) {
			return -1 < o.call(n, e) !== i
		}) : E.test(n) ? S.filter(n, e, i) : (n = S.filter(n, e), S.grep(e, function (e) {
			return -1 < o.call(n, e) !== i && 1 === e.nodeType
		}))
	}

	S.filter = function (e, t, n) {
		var i = t[0];
		return n && (e = ":not(" + e + ")"), 1 === t.length && 1 === i.nodeType ? S.find.matchesSelector(i, e) ? [i] : [] : S.find.matches(e, S.grep(t, function (e) {
			return 1 === e.nodeType
		}))
	}, S.fn.extend({
		find: function (e) {
			var t, n, i = this.length, o = this;
			if ("string" != typeof e) return this.pushStack(S(e).filter(function () {
				for (t = 0; t < i; t++) if (S.contains(o[t], this)) return !0
			}));
			for (n = this.pushStack([]), t = 0; t < i; t++) S.find(e, o[t], n);
			return 1 < i ? S.uniqueSort(n) : n
		}, filter: function (e) {
			return this.pushStack(A(this, e || [], !1))
		}, not: function (e) {
			return this.pushStack(A(this, e || [], !0))
		}, is: function (e) {
			return !!A(this, "string" == typeof e && k.test(e) ? S(e) : e || [], !1).length
		}
	});
	var D, j = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/;
	(S.fn.init = function (e, t, n) {
		var i, o;
		if (!e) return this;
		if (n = n || D, "string" != typeof e) return e.nodeType ? (this[0] = e, this.length = 1, this) : S.isFunction(e) ? void 0 !== n.ready ? n.ready(e) : e(S) : S.makeArray(e, this);
		if (!(i = "<" === e[0] && ">" === e[e.length - 1] && 3 <= e.length ? [null, e, null] : j.exec(e)) || !i[1] && t) return !t || t.jquery ? (t || n).find(e) : this.constructor(t).find(e);
		if (i[1]) {
			if (t = t instanceof S ? t[0] : t, S.merge(this, S.parseHTML(i[1], t && t.nodeType ? t.ownerDocument || t : C, !0)), $.test(i[1]) && S.isPlainObject(t)) for (i in t) S.isFunction(this[i]) ? this[i](t[i]) : this.attr(i, t[i]);
			return this
		}
		return (o = C.getElementById(i[2])) && (this[0] = o, this.length = 1), this
	}).prototype = S.fn, D = S(C);
	var N = /^(?:parents|prev(?:Until|All))/, L = {children: !0, contents: !0, next: !0, prev: !0};

	function O(e, t) {
		for (; (e = e[t]) && 1 !== e.nodeType;) ;
		return e
	}

	S.fn.extend({
		has: function (e) {
			var t = S(e, this), n = t.length;
			return this.filter(function () {
				for (var e = 0; e < n; e++) if (S.contains(this, t[e])) return !0
			})
		}, closest: function (e, t) {
			var n, i = 0, o = this.length, r = [], s = "string" != typeof e && S(e);
			if (!k.test(e)) for (; i < o; i++) for (n = this[i]; n && n !== t; n = n.parentNode) if (n.nodeType < 11 && (s ? -1 < s.index(n) : 1 === n.nodeType && S.find.matchesSelector(n, e))) {
				r.push(n);
				break
			}
			return this.pushStack(1 < r.length ? S.uniqueSort(r) : r)
		}, index: function (e) {
			return e ? "string" == typeof e ? o.call(S(e), this[0]) : o.call(this, e.jquery ? e[0] : e) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
		}, add: function (e, t) {
			return this.pushStack(S.uniqueSort(S.merge(this.get(), S(e, t))))
		}, addBack: function (e) {
			return this.add(null == e ? this.prevObject : this.prevObject.filter(e))
		}
	}), S.each({
		parent: function (e) {
			var t = e.parentNode;
			return t && 11 !== t.nodeType ? t : null
		}, parents: function (e) {
			return x(e, "parentNode")
		}, parentsUntil: function (e, t, n) {
			return x(e, "parentNode", n)
		}, next: function (e) {
			return O(e, "nextSibling")
		}, prev: function (e) {
			return O(e, "previousSibling")
		}, nextAll: function (e) {
			return x(e, "nextSibling")
		}, prevAll: function (e) {
			return x(e, "previousSibling")
		}, nextUntil: function (e, t, n) {
			return x(e, "nextSibling", n)
		}, prevUntil: function (e, t, n) {
			return x(e, "previousSibling", n)
		}, siblings: function (e) {
			return w((e.parentNode || {}).firstChild, e)
		}, children: function (e) {
			return w(e.firstChild)
		}, contents: function (e) {
			return e.contentDocument || S.merge([], e.childNodes)
		}
	}, function (i, o) {
		S.fn[i] = function (e, t) {
			var n = S.map(this, o, e);
			return "Until" !== i.slice(-5) && (t = e), t && "string" == typeof t && (n = S.filter(t, n)), 1 < this.length && (L[i] || S.uniqueSort(n), N.test(i) && n.reverse()), this.pushStack(n)
		}
	});
	var q = /[^\x20\t\r\n\f]+/g;

	function H(e) {
		return e
	}

	function I(e) {
		throw e
	}

	function P(e, t, n) {
		var i;
		try {
			e && S.isFunction(i = e.promise) ? i.call(e).done(t).fail(n) : e && S.isFunction(i = e.then) ? i.call(e, t, n) : t.call(void 0, e)
		} catch (e) {
			n.call(void 0, e)
		}
	}

	S.Callbacks = function (i) {
		i = "string" == typeof i ? function (e) {
			var n = {};
			return S.each(e.match(q) || [], function (e, t) {
				n[t] = !0
			}), n
		}(i) : S.extend({}, i);

		function n() {
			for (r = i.once, t = o = !0; a.length; l = -1) for (e = a.shift(); ++l < s.length;) !1 === s[l].apply(e[0], e[1]) && i.stopOnFalse && (l = s.length, e = !1);
			i.memory || (e = !1), o = !1, r && (s = e ? [] : "")
		}

		var o, e, t, r, s = [], a = [], l = -1, c = {
			add: function () {
				return s && (e && !o && (l = s.length - 1, a.push(e)), function n(e) {
					S.each(e, function (e, t) {
						S.isFunction(t) ? i.unique && c.has(t) || s.push(t) : t && t.length && "string" !== S.type(t) && n(t)
					})
				}(arguments), e && !o && n()), this
			}, remove: function () {
				return S.each(arguments, function (e, t) {
					for (var n; -1 < (n = S.inArray(t, s, n));) s.splice(n, 1), n <= l && l--
				}), this
			}, has: function (e) {
				return e ? -1 < S.inArray(e, s) : 0 < s.length
			}, empty: function () {
				return s = s && [], this
			}, disable: function () {
				return r = a = [], s = e = "", this
			}, disabled: function () {
				return !s
			}, lock: function () {
				return r = a = [], e || o || (s = e = ""), this
			}, locked: function () {
				return !!r
			}, fireWith: function (e, t) {
				return r || (t = [e, (t = t || []).slice ? t.slice() : t], a.push(t), o || n()), this
			}, fire: function () {
				return c.fireWith(this, arguments), this
			}, fired: function () {
				return !!t
			}
		};
		return c
	}, S.extend({
		Deferred: function (e) {
			var r = [["notify", "progress", S.Callbacks("memory"), S.Callbacks("memory"), 2], ["resolve", "done", S.Callbacks("once memory"), S.Callbacks("once memory"), 0, "resolved"], ["reject", "fail", S.Callbacks("once memory"), S.Callbacks("once memory"), 1, "rejected"]],
				o = "pending", s = {
					state: function () {
						return o
					}, always: function () {
						return a.done(arguments).fail(arguments), this
					}, catch: function (e) {
						return s.then(null, e)
					}, pipe: function () {
						var o = arguments;
						return S.Deferred(function (i) {
							S.each(r, function (e, t) {
								var n = S.isFunction(o[t[4]]) && o[t[4]];
								a[t[1]](function () {
									var e = n && n.apply(this, arguments);
									e && S.isFunction(e.promise) ? e.promise().progress(i.notify).done(i.resolve).fail(i.reject) : i[t[0] + "With"](this, n ? [e] : arguments)
								})
							}), o = null
						}).promise()
					}, then: function (t, n, i) {
						var l = 0;

						function c(o, r, s, a) {
							return function () {
								function e() {
									var e, t;
									if (!(o < l)) {
										if ((e = s.apply(n, i)) === r.promise()) throw new TypeError("Thenable self-resolution");
										t = e && ("object" == typeof e || "function" == typeof e) && e.then, S.isFunction(t) ? a ? t.call(e, c(l, r, H, a), c(l, r, I, a)) : (l++, t.call(e, c(l, r, H, a), c(l, r, I, a), c(l, r, H, r.notifyWith))) : (s !== H && (n = void 0, i = [e]), (a || r.resolveWith)(n, i))
									}
								}

								var n = this, i = arguments, t = a ? e : function () {
									try {
										e()
									} catch (e) {
										S.Deferred.exceptionHook && S.Deferred.exceptionHook(e, t.stackTrace), l <= o + 1 && (s !== I && (n = void 0, i = [e]), r.rejectWith(n, i))
									}
								};
								o ? t() : (S.Deferred.getStackHook && (t.stackTrace = S.Deferred.getStackHook()), T.setTimeout(t))
							}
						}

						return S.Deferred(function (e) {
							r[0][3].add(c(0, e, S.isFunction(i) ? i : H, e.notifyWith)), r[1][3].add(c(0, e, S.isFunction(t) ? t : H)), r[2][3].add(c(0, e, S.isFunction(n) ? n : I))
						}).promise()
					}, promise: function (e) {
						return null != e ? S.extend(e, s) : s
					}
				}, a = {};
			return S.each(r, function (e, t) {
				var n = t[2], i = t[5];
				s[t[1]] = n.add, i && n.add(function () {
					o = i
				}, r[3 - e][2].disable, r[0][2].lock), n.add(t[3].fire), a[t[0]] = function () {
					return a[t[0] + "With"](this === a ? void 0 : this, arguments), this
				}, a[t[0] + "With"] = n.fireWith
			}), s.promise(a), e && e.call(a, a), a
		}, when: function (e) {
			function t(t) {
				return function (e) {
					o[t] = this, r[t] = 1 < arguments.length ? a.call(arguments) : e, --n || s.resolveWith(o, r)
				}
			}

			var n = arguments.length, i = n, o = Array(i), r = a.call(arguments), s = S.Deferred();
			if (n <= 1 && (P(e, s.done(t(i)).resolve, s.reject), "pending" === s.state() || S.isFunction(r[i] && r[i].then))) return s.then();
			for (; i--;) P(r[i], t(i), s.reject);
			return s.promise()
		}
	});
	var F = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;
	S.Deferred.exceptionHook = function (e, t) {
		T.console && T.console.warn && e && F.test(e.name) && T.console.warn("jQuery.Deferred exception: " + e.message, e.stack, t)
	}, S.readyException = function (e) {
		T.setTimeout(function () {
			throw e
		})
	};
	var _ = S.Deferred();

	function M() {
		C.removeEventListener("DOMContentLoaded", M), T.removeEventListener("load", M), S.ready()
	}

	S.fn.ready = function (e) {
		return _.then(e).catch(function (e) {
			S.readyException(e)
		}), this
	}, S.extend({
		isReady: !1, readyWait: 1, holdReady: function (e) {
			e ? S.readyWait++ : S.ready(!0)
		}, ready: function (e) {
			(!0 === e ? --S.readyWait : S.isReady) || ((S.isReady = !0) !== e && 0 < --S.readyWait || _.resolveWith(C, [S]))
		}
	}), S.ready.then = _.then, "complete" === C.readyState || "loading" !== C.readyState && !C.documentElement.doScroll ? T.setTimeout(S.ready) : (C.addEventListener("DOMContentLoaded", M), T.addEventListener("load", M));

	function z(e) {
		return 1 === e.nodeType || 9 === e.nodeType || !+e.nodeType
	}

	var R = function (e, t, n, i, o, r, s) {
		var a = 0, l = e.length, c = null == n;
		if ("object" === S.type(n)) for (a in o = !0, n) R(e, t, a, n[a], !0, r, s); else if (void 0 !== i && (o = !0, S.isFunction(i) || (s = !0), c && (t = s ? (t.call(e, i), null) : (c = t, function (e, t, n) {
			return c.call(S(e), n)
		})), t)) for (; a < l; a++) t(e[a], n, s ? i : i.call(e[a], a, t(e[a], n)));
		return o ? e : c ? t.call(e) : l ? t(e[0], n) : r
	};

	function W() {
		this.expando = S.expando + W.uid++
	}

	W.uid = 1, W.prototype = {
		cache: function (e) {
			var t = e[this.expando];
			return t || (t = {}, z(e) && (e.nodeType ? e[this.expando] = t : Object.defineProperty(e, this.expando, {
				value: t,
				configurable: !0
			}))), t
		}, set: function (e, t, n) {
			var i, o = this.cache(e);
			if ("string" == typeof t) o[S.camelCase(t)] = n; else for (i in t) o[S.camelCase(i)] = t[i];
			return o
		}, get: function (e, t) {
			return void 0 === t ? this.cache(e) : e[this.expando] && e[this.expando][S.camelCase(t)]
		}, access: function (e, t, n) {
			return void 0 === t || t && "string" == typeof t && void 0 === n ? this.get(e, t) : (this.set(e, t, n), void 0 !== n ? n : t)
		}, remove: function (e, t) {
			var n, i = e[this.expando];
			if (void 0 !== i) {
				if (void 0 !== t) {
					n = (t = S.isArray(t) ? t.map(S.camelCase) : (t = S.camelCase(t)) in i ? [t] : t.match(q) || []).length;
					for (; n--;) delete i[t[n]]
				}
				void 0 !== t && !S.isEmptyObject(i) || (e.nodeType ? e[this.expando] = void 0 : delete e[this.expando])
			}
		}, hasData: function (e) {
			var t = e[this.expando];
			return void 0 !== t && !S.isEmptyObject(t)
		}
	};
	var B = new W, U = new W, X = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/, K = /[A-Z]/g;

	function V(e, t, n) {
		var i;
		if (void 0 === n && 1 === e.nodeType) if (i = "data-" + t.replace(K, "-$&").toLowerCase(), "string" == typeof (n = e.getAttribute(i))) {
			try {
				n = function (e) {
					return "true" === e || "false" !== e && ("null" === e ? null : e === +e + "" ? +e : X.test(e) ? JSON.parse(e) : e)
				}(n)
			} catch (e) {
			}
			U.set(e, t, n)
		} else n = void 0;
		return n
	}

	S.extend({
		hasData: function (e) {
			return U.hasData(e) || B.hasData(e)
		}, data: function (e, t, n) {
			return U.access(e, t, n)
		}, removeData: function (e, t) {
			U.remove(e, t)
		}, _data: function (e, t, n) {
			return B.access(e, t, n)
		}, _removeData: function (e, t) {
			B.remove(e, t)
		}
	}), S.fn.extend({
		data: function (n, e) {
			var t, i, o, r = this[0], s = r && r.attributes;
			if (void 0 !== n) return "object" == typeof n ? this.each(function () {
				U.set(this, n)
			}) : R(this, function (e) {
				var t;
				if (r && void 0 === e) {
					if (void 0 !== (t = U.get(r, n))) return t;
					if (void 0 !== (t = V(r, n))) return t
				} else this.each(function () {
					U.set(this, n, e)
				})
			}, null, e, 1 < arguments.length, null, !0);
			if (this.length && (o = U.get(r), 1 === r.nodeType && !B.get(r, "hasDataAttrs"))) {
				for (t = s.length; t--;) s[t] && (0 === (i = s[t].name).indexOf("data-") && (i = S.camelCase(i.slice(5)), V(r, i, o[i])));
				B.set(r, "hasDataAttrs", !0)
			}
			return o
		}, removeData: function (e) {
			return this.each(function () {
				U.remove(this, e)
			})
		}
	}), S.extend({
		queue: function (e, t, n) {
			var i;
			if (e) return t = (t || "fx") + "queue", i = B.get(e, t), n && (!i || S.isArray(n) ? i = B.access(e, t, S.makeArray(n)) : i.push(n)), i || []
		}, dequeue: function (e, t) {
			t = t || "fx";
			var n = S.queue(e, t), i = n.length, o = n.shift(), r = S._queueHooks(e, t);
			"inprogress" === o && (o = n.shift(), i--), o && ("fx" === t && n.unshift("inprogress"), delete r.stop, o.call(e, function () {
				S.dequeue(e, t)
			}, r)), !i && r && r.empty.fire()
		}, _queueHooks: function (e, t) {
			var n = t + "queueHooks";
			return B.get(e, n) || B.access(e, n, {
				empty: S.Callbacks("once memory").add(function () {
					B.remove(e, [t + "queue", n])
				})
			})
		}
	}), S.fn.extend({
		queue: function (t, n) {
			var e = 2;
			return "string" != typeof t && (n = t, t = "fx", e--), arguments.length < e ? S.queue(this[0], t) : void 0 === n ? this : this.each(function () {
				var e = S.queue(this, t, n);
				S._queueHooks(this, t), "fx" === t && "inprogress" !== e[0] && S.dequeue(this, t)
			})
		}, dequeue: function (e) {
			return this.each(function () {
				S.dequeue(this, e)
			})
		}, clearQueue: function (e) {
			return this.queue(e || "fx", [])
		}, promise: function (e, t) {
			function n() {
				--o || r.resolveWith(s, [s])
			}

			var i, o = 1, r = S.Deferred(), s = this, a = this.length;
			for ("string" != typeof e && (t = e, e = void 0), e = e || "fx"; a--;) (i = B.get(s[a], e + "queueHooks")) && i.empty && (o++, i.empty.add(n));
			return n(), r.promise(t)
		}
	});

	function Y(e, t, n, i) {
		var o, r, s = {};
		for (r in t) s[r] = e.style[r], e.style[r] = t[r];
		for (r in o = n.apply(e, i || []), t) e.style[r] = s[r];
		return o
	}

	var G = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source, J = new RegExp("^(?:([+-])=|)(" + G + ")([a-z%]*)$", "i"),
		Q = ["Top", "Right", "Bottom", "Left"], Z = function (e, t) {
			return "none" === (e = t || e).style.display || "" === e.style.display && S.contains(e.ownerDocument, e) && "none" === S.css(e, "display")
		};

	function ee(e, t, n, i) {
		var o, r = 1, s = 20, a = i ? function () {
				return i.cur()
			} : function () {
				return S.css(e, t, "")
			}, l = a(), c = n && n[3] || (S.cssNumber[t] ? "" : "px"),
			u = (S.cssNumber[t] || "px" !== c && +l) && J.exec(S.css(e, t));
		if (u && u[3] !== c) for (c = c || u[3], n = n || [], u = +l || 1; u /= r = r || ".5", S.style(e, t, u + c), r !== (r = a() / l) && 1 !== r && --s;) ;
		return n && (u = +u || +l || 0, o = n[1] ? u + (n[1] + 1) * n[2] : +n[2], i && (i.unit = c, i.start = u, i.end = o)), o
	}

	var te = {};

	function ne(e, t) {
		for (var n, i, o = [], r = 0, s = e.length; r < s; r++) (i = e[r]).style && (n = i.style.display, t ? ("none" === n && (o[r] = B.get(i, "display") || null, o[r] || (i.style.display = "")), "" === i.style.display && Z(i) && (o[r] = (d = c = l = void 0, c = (a = i).ownerDocument, u = a.nodeName, (d = te[u]) || (l = c.body.appendChild(c.createElement(u)), d = S.css(l, "display"), l.parentNode.removeChild(l), "none" === d && (d = "block"), te[u] = d)))) : "none" !== n && (o[r] = "none", B.set(i, "display", n)));
		var a, l, c, u, d;
		for (r = 0; r < s; r++) null != o[r] && (e[r].style.display = o[r]);
		return e
	}

	S.fn.extend({
		show: function () {
			return ne(this, !0)
		}, hide: function () {
			return ne(this)
		}, toggle: function (e) {
			return "boolean" == typeof e ? e ? this.show() : this.hide() : this.each(function () {
				Z(this) ? S(this).show() : S(this).hide()
			})
		}
	});
	var ie = /^(?:checkbox|radio)$/i, oe = /<([a-z][^\/\0>\x20\t\r\n\f]+)/i, re = /^$|\/(?:java|ecma)script/i, se = {
		option: [1, "<select multiple='multiple'>", "</select>"],
		thead: [1, "<table>", "</table>"],
		col: [2, "<table><colgroup>", "</colgroup></table>"],
		tr: [2, "<table><tbody>", "</tbody></table>"],
		td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
		_default: [0, "", ""]
	};

	function ae(e, t) {
		var n;
		return n = void 0 !== e.getElementsByTagName ? e.getElementsByTagName(t || "*") : void 0 !== e.querySelectorAll ? e.querySelectorAll(t || "*") : [], void 0 === t || t && S.nodeName(e, t) ? S.merge([e], n) : n
	}

	function le(e, t) {
		for (var n = 0, i = e.length; n < i; n++) B.set(e[n], "globalEval", !t || B.get(t[n], "globalEval"))
	}

	se.optgroup = se.option, se.tbody = se.tfoot = se.colgroup = se.caption = se.thead, se.th = se.td;
	var ce, ue, de = /<|&#?\w+;/;

	function pe(e, t, n, i, o) {
		for (var r, s, a, l, c, u, d = t.createDocumentFragment(), p = [], f = 0, h = e.length; f < h; f++) if ((r = e[f]) || 0 === r) if ("object" === S.type(r)) S.merge(p, r.nodeType ? [r] : r); else if (de.test(r)) {
			for (s = s || d.appendChild(t.createElement("div")), a = (oe.exec(r) || ["", ""])[1].toLowerCase(), l = se[a] || se._default, s.innerHTML = l[1] + S.htmlPrefilter(r) + l[2], u = l[0]; u--;) s = s.lastChild;
			S.merge(p, s.childNodes), (s = d.firstChild).textContent = ""
		} else p.push(t.createTextNode(r));
		for (d.textContent = "", f = 0; r = p[f++];) if (i && -1 < S.inArray(r, i)) o && o.push(r); else if (c = S.contains(r.ownerDocument, r), s = ae(d.appendChild(r), "script"), c && le(s), n) for (u = 0; r = s[u++];) re.test(r.type || "") && n.push(r);
		return d
	}

	ce = C.createDocumentFragment().appendChild(C.createElement("div")), (ue = C.createElement("input")).setAttribute("type", "radio"), ue.setAttribute("checked", "checked"), ue.setAttribute("name", "t"), ce.appendChild(ue), v.checkClone = ce.cloneNode(!0).cloneNode(!0).lastChild.checked, ce.innerHTML = "<textarea>x</textarea>", v.noCloneChecked = !!ce.cloneNode(!0).lastChild.defaultValue;
	var fe = C.documentElement, he = /^key/, ge = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
		ve = /^([^.]*)(?:\.(.+)|)/;

	function me() {
		return !0
	}

	function ye() {
		return !1
	}

	function be() {
		try {
			return C.activeElement
		} catch (e) {
		}
	}

	function xe(e, t, n, i, o, r) {
		var s, a;
		if ("object" == typeof t) {
			for (a in "string" != typeof n && (i = i || n, n = void 0), t) xe(e, a, n, i, t[a], r);
			return e
		}
		if (null == i && null == o ? (o = n, i = n = void 0) : null == o && ("string" == typeof n ? (o = i, i = void 0) : (o = i, i = n, n = void 0)), !1 === o) o = ye; else if (!o) return e;
		return 1 === r && (s = o, (o = function (e) {
			return S().off(e), s.apply(this, arguments)
		}).guid = s.guid || (s.guid = S.guid++)), e.each(function () {
			S.event.add(this, t, o, i, n)
		})
	}

	S.event = {
		global: {}, add: function (t, e, n, i, o) {
			var r, s, a, l, c, u, d, p, f, h, g, v = B.get(t);
			if (v) for (n.handler && (n = (r = n).handler, o = r.selector), o && S.find.matchesSelector(fe, o), n.guid || (n.guid = S.guid++), (l = v.events) || (l = v.events = {}), (s = v.handle) || (s = v.handle = function (e) {
				return void 0 !== S && S.event.triggered !== e.type ? S.event.dispatch.apply(t, arguments) : void 0
			}), c = (e = (e || "").match(q) || [""]).length; c--;) f = g = (a = ve.exec(e[c]) || [])[1], h = (a[2] || "").split(".").sort(), f && (d = S.event.special[f] || {}, f = (o ? d.delegateType : d.bindType) || f, d = S.event.special[f] || {}, u = S.extend({
				type: f,
				origType: g,
				data: i,
				handler: n,
				guid: n.guid,
				selector: o,
				needsContext: o && S.expr.match.needsContext.test(o),
				namespace: h.join(".")
			}, r), (p = l[f]) || ((p = l[f] = []).delegateCount = 0, d.setup && !1 !== d.setup.call(t, i, h, s) || t.addEventListener && t.addEventListener(f, s)), d.add && (d.add.call(t, u), u.handler.guid || (u.handler.guid = n.guid)), o ? p.splice(p.delegateCount++, 0, u) : p.push(u), S.event.global[f] = !0)
		}, remove: function (e, t, n, i, o) {
			var r, s, a, l, c, u, d, p, f, h, g, v = B.hasData(e) && B.get(e);
			if (v && (l = v.events)) {
				for (c = (t = (t || "").match(q) || [""]).length; c--;) if (f = g = (a = ve.exec(t[c]) || [])[1], h = (a[2] || "").split(".").sort(), f) {
					for (d = S.event.special[f] || {}, p = l[f = (i ? d.delegateType : d.bindType) || f] || [], a = a[2] && new RegExp("(^|\\.)" + h.join("\\.(?:.*\\.|)") + "(\\.|$)"), s = r = p.length; r--;) u = p[r], !o && g !== u.origType || n && n.guid !== u.guid || a && !a.test(u.namespace) || i && i !== u.selector && ("**" !== i || !u.selector) || (p.splice(r, 1), u.selector && p.delegateCount--, d.remove && d.remove.call(e, u));
					s && !p.length && (d.teardown && !1 !== d.teardown.call(e, h, v.handle) || S.removeEvent(e, f, v.handle), delete l[f])
				} else for (f in l) S.event.remove(e, f + t[c], n, i, !0);
				S.isEmptyObject(l) && B.remove(e, "handle events")
			}
		}, dispatch: function (e) {
			var t, n, i, o, r, s, a = S.event.fix(e), l = new Array(arguments.length),
				c = (B.get(this, "events") || {})[a.type] || [], u = S.event.special[a.type] || {};
			for (l[0] = a, t = 1; t < arguments.length; t++) l[t] = arguments[t];
			if (a.delegateTarget = this, !u.preDispatch || !1 !== u.preDispatch.call(this, a)) {
				for (s = S.event.handlers.call(this, a, c), t = 0; (o = s[t++]) && !a.isPropagationStopped();) for (a.currentTarget = o.elem, n = 0; (r = o.handlers[n++]) && !a.isImmediatePropagationStopped();) a.rnamespace && !a.rnamespace.test(r.namespace) || (a.handleObj = r, a.data = r.data, void 0 !== (i = ((S.event.special[r.origType] || {}).handle || r.handler).apply(o.elem, l)) && !1 === (a.result = i) && (a.preventDefault(), a.stopPropagation()));
				return u.postDispatch && u.postDispatch.call(this, a), a.result
			}
		}, handlers: function (e, t) {
			var n, i, o, r, s, a = [], l = t.delegateCount, c = e.target;
			if (l && c.nodeType && !("click" === e.type && 1 <= e.button)) for (; c !== this; c = c.parentNode || this) if (1 === c.nodeType && ("click" !== e.type || !0 !== c.disabled)) {
				for (r = [], s = {}, n = 0; n < l; n++) void 0 === s[o = (i = t[n]).selector + " "] && (s[o] = i.needsContext ? -1 < S(o, this).index(c) : S.find(o, this, null, [c]).length), s[o] && r.push(i);
				r.length && a.push({elem: c, handlers: r})
			}
			return c = this, l < t.length && a.push({elem: c, handlers: t.slice(l)}), a
		}, addProp: function (t, e) {
			Object.defineProperty(S.Event.prototype, t, {
				enumerable: !0,
				configurable: !0,
				get: S.isFunction(e) ? function () {
					if (this.originalEvent) return e(this.originalEvent)
				} : function () {
					if (this.originalEvent) return this.originalEvent[t]
				},
				set: function (e) {
					Object.defineProperty(this, t, {enumerable: !0, configurable: !0, writable: !0, value: e})
				}
			})
		}, fix: function (e) {
			return e[S.expando] ? e : new S.Event(e)
		}, special: {
			load: {noBubble: !0}, focus: {
				trigger: function () {
					if (this !== be() && this.focus) return this.focus(), !1
				}, delegateType: "focusin"
			}, blur: {
				trigger: function () {
					if (this === be() && this.blur) return this.blur(), !1
				}, delegateType: "focusout"
			}, click: {
				trigger: function () {
					if ("checkbox" === this.type && this.click && S.nodeName(this, "input")) return this.click(), !1
				}, _default: function (e) {
					return S.nodeName(e.target, "a")
				}
			}, beforeunload: {
				postDispatch: function (e) {
					void 0 !== e.result && e.originalEvent && (e.originalEvent.returnValue = e.result)
				}
			}
		}
	}, S.removeEvent = function (e, t, n) {
		e.removeEventListener && e.removeEventListener(t, n)
	}, S.Event = function (e, t) {
		return this instanceof S.Event ? (e && e.type ? (this.originalEvent = e, this.type = e.type, this.isDefaultPrevented = e.defaultPrevented || void 0 === e.defaultPrevented && !1 === e.returnValue ? me : ye, this.target = e.target && 3 === e.target.nodeType ? e.target.parentNode : e.target, this.currentTarget = e.currentTarget, this.relatedTarget = e.relatedTarget) : this.type = e, t && S.extend(this, t), this.timeStamp = e && e.timeStamp || S.now(), void (this[S.expando] = !0)) : new S.Event(e, t)
	}, S.Event.prototype = {
		constructor: S.Event,
		isDefaultPrevented: ye,
		isPropagationStopped: ye,
		isImmediatePropagationStopped: ye,
		isSimulated: !1,
		preventDefault: function () {
			var e = this.originalEvent;
			this.isDefaultPrevented = me, e && !this.isSimulated && e.preventDefault()
		},
		stopPropagation: function () {
			var e = this.originalEvent;
			this.isPropagationStopped = me, e && !this.isSimulated && e.stopPropagation()
		},
		stopImmediatePropagation: function () {
			var e = this.originalEvent;
			this.isImmediatePropagationStopped = me, e && !this.isSimulated && e.stopImmediatePropagation(), this.stopPropagation()
		}
	}, S.each({
		altKey: !0,
		bubbles: !0,
		cancelable: !0,
		changedTouches: !0,
		ctrlKey: !0,
		detail: !0,
		eventPhase: !0,
		metaKey: !0,
		pageX: !0,
		pageY: !0,
		shiftKey: !0,
		view: !0,
		char: !0,
		charCode: !0,
		key: !0,
		keyCode: !0,
		button: !0,
		buttons: !0,
		clientX: !0,
		clientY: !0,
		offsetX: !0,
		offsetY: !0,
		pointerId: !0,
		pointerType: !0,
		screenX: !0,
		screenY: !0,
		targetTouches: !0,
		toElement: !0,
		touches: !0,
		which: function (e) {
			var t = e.button;
			return null == e.which && he.test(e.type) ? null != e.charCode ? e.charCode : e.keyCode : !e.which && void 0 !== t && ge.test(e.type) ? 1 & t ? 1 : 2 & t ? 3 : 4 & t ? 2 : 0 : e.which
		}
	}, S.event.addProp), S.each({
		mouseenter: "mouseover",
		mouseleave: "mouseout",
		pointerenter: "pointerover",
		pointerleave: "pointerout"
	}, function (e, o) {
		S.event.special[e] = {
			delegateType: o, bindType: o, handle: function (e) {
				var t, n = e.relatedTarget, i = e.handleObj;
				return n && (n === this || S.contains(this, n)) || (e.type = i.origType, t = i.handler.apply(this, arguments), e.type = o), t
			}
		}
	}), S.fn.extend({
		on: function (e, t, n, i) {
			return xe(this, e, t, n, i)
		}, one: function (e, t, n, i) {
			return xe(this, e, t, n, i, 1)
		}, off: function (e, t, n) {
			var i, o;
			if (e && e.preventDefault && e.handleObj) return i = e.handleObj, S(e.delegateTarget).off(i.namespace ? i.origType + "." + i.namespace : i.origType, i.selector, i.handler), this;
			if ("object" != typeof e) return !1 !== t && "function" != typeof t || (n = t, t = void 0), !1 === n && (n = ye), this.each(function () {
				S.event.remove(this, e, n, t)
			});
			for (o in e) this.off(o, t, e[o]);
			return this
		}
	});
	var we = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,
		ke = /<script|<style|<link/i, Te = /checked\s*(?:[^=]|=\s*.checked.)/i, Ce = /^true\/(.*)/,
		Se = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;

	function $e(e, t) {
		return S.nodeName(e, "table") && S.nodeName(11 !== t.nodeType ? t : t.firstChild, "tr") && e.getElementsByTagName("tbody")[0] || e
	}

	function Ee(e) {
		return e.type = (null !== e.getAttribute("type")) + "/" + e.type, e
	}

	function Ae(e) {
		var t = Ce.exec(e.type);
		return t ? e.type = t[1] : e.removeAttribute("type"), e
	}

	function De(e, t) {
		var n, i, o, r, s, a, l, c;
		if (1 === t.nodeType) {
			if (B.hasData(e) && (r = B.access(e), s = B.set(t, r), c = r.events)) for (o in delete s.handle, s.events = {}, c) for (n = 0, i = c[o].length; n < i; n++) S.event.add(t, o, c[o][n]);
			U.hasData(e) && (a = U.access(e), l = S.extend({}, a), U.set(t, l))
		}
	}

	function je(n, i, o, r) {
		i = g.apply([], i);
		var e, t, s, a, l, c, u = 0, d = n.length, p = d - 1, f = i[0], h = S.isFunction(f);
		if (h || 1 < d && "string" == typeof f && !v.checkClone && Te.test(f)) return n.each(function (e) {
			var t = n.eq(e);
			h && (i[0] = f.call(this, e, t.html())), je(t, i, o, r)
		});
		if (d && (t = (e = pe(i, n[0].ownerDocument, !1, n, r)).firstChild, 1 === e.childNodes.length && (e = t), t || r)) {
			for (a = (s = S.map(ae(e, "script"), Ee)).length; u < d; u++) l = e, u !== p && (l = S.clone(l, !0, !0), a && S.merge(s, ae(l, "script"))), o.call(n[u], l, u);
			if (a) for (c = s[s.length - 1].ownerDocument, S.map(s, Ae), u = 0; u < a; u++) l = s[u], re.test(l.type || "") && !B.access(l, "globalEval") && S.contains(c, l) && (l.src ? S._evalUrl && S._evalUrl(l.src) : m(l.textContent.replace(Se, ""), c))
		}
		return n
	}

	function Ne(e, t, n) {
		for (var i, o = t ? S.filter(t, e) : e, r = 0; null != (i = o[r]); r++) n || 1 !== i.nodeType || S.cleanData(ae(i)), i.parentNode && (n && S.contains(i.ownerDocument, i) && le(ae(i, "script")), i.parentNode.removeChild(i));
		return e
	}

	S.extend({
		htmlPrefilter: function (e) {
			return e.replace(we, "<$1></$2>")
		}, clone: function (e, t, n) {
			var i, o, r, s, a, l, c, u = e.cloneNode(!0), d = S.contains(e.ownerDocument, e);
			if (!(v.noCloneChecked || 1 !== e.nodeType && 11 !== e.nodeType || S.isXMLDoc(e))) for (s = ae(u), i = 0, o = (r = ae(e)).length; i < o; i++) a = r[i], l = s[i], void 0, "input" === (c = l.nodeName.toLowerCase()) && ie.test(a.type) ? l.checked = a.checked : "input" !== c && "textarea" !== c || (l.defaultValue = a.defaultValue);
			if (t) if (n) for (r = r || ae(e), s = s || ae(u), i = 0, o = r.length; i < o; i++) De(r[i], s[i]); else De(e, u);
			return 0 < (s = ae(u, "script")).length && le(s, !d && ae(e, "script")), u
		}, cleanData: function (e) {
			for (var t, n, i, o = S.event.special, r = 0; void 0 !== (n = e[r]); r++) if (z(n)) {
				if (t = n[B.expando]) {
					if (t.events) for (i in t.events) o[i] ? S.event.remove(n, i) : S.removeEvent(n, i, t.handle);
					n[B.expando] = void 0
				}
				n[U.expando] && (n[U.expando] = void 0)
			}
		}
	}), S.fn.extend({
		detach: function (e) {
			return Ne(this, e, !0)
		}, remove: function (e) {
			return Ne(this, e)
		}, text: function (e) {
			return R(this, function (e) {
				return void 0 === e ? S.text(this) : this.empty().each(function () {
					1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || (this.textContent = e)
				})
			}, null, e, arguments.length)
		}, append: function () {
			return je(this, arguments, function (e) {
				1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || $e(this, e).appendChild(e)
			})
		}, prepend: function () {
			return je(this, arguments, function (e) {
				if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
					var t = $e(this, e);
					t.insertBefore(e, t.firstChild)
				}
			})
		}, before: function () {
			return je(this, arguments, function (e) {
				this.parentNode && this.parentNode.insertBefore(e, this)
			})
		}, after: function () {
			return je(this, arguments, function (e) {
				this.parentNode && this.parentNode.insertBefore(e, this.nextSibling)
			})
		}, empty: function () {
			for (var e, t = 0; null != (e = this[t]); t++) 1 === e.nodeType && (S.cleanData(ae(e, !1)), e.textContent = "");
			return this
		}, clone: function (e, t) {
			return e = null != e && e, t = null == t ? e : t, this.map(function () {
				return S.clone(this, e, t)
			})
		}, html: function (e) {
			return R(this, function (e) {
				var t = this[0] || {}, n = 0, i = this.length;
				if (void 0 === e && 1 === t.nodeType) return t.innerHTML;
				if ("string" == typeof e && !ke.test(e) && !se[(oe.exec(e) || ["", ""])[1].toLowerCase()]) {
					e = S.htmlPrefilter(e);
					try {
						for (; n < i; n++) 1 === (t = this[n] || {}).nodeType && (S.cleanData(ae(t, !1)), t.innerHTML = e);
						t = 0
					} catch (e) {
					}
				}
				t && this.empty().append(e)
			}, null, e, arguments.length)
		}, replaceWith: function () {
			var n = [];
			return je(this, arguments, function (e) {
				var t = this.parentNode;
				S.inArray(this, n) < 0 && (S.cleanData(ae(this)), t && t.replaceChild(e, this))
			}, n)
		}
	}), S.each({
		appendTo: "append",
		prependTo: "prepend",
		insertBefore: "before",
		insertAfter: "after",
		replaceAll: "replaceWith"
	}, function (e, s) {
		S.fn[e] = function (e) {
			for (var t, n = [], i = S(e), o = i.length - 1, r = 0; r <= o; r++) t = r === o ? this : this.clone(!0), S(i[r])[s](t), l.apply(n, t.get());
			return this.pushStack(n)
		}
	});
	var Le, Oe, qe, He, Ie, Pe, Fe = /^margin/, _e = new RegExp("^(" + G + ")(?!px)[a-z%]+$", "i"), Me = function (e) {
		var t = e.ownerDocument.defaultView;
		return t && t.opener || (t = T), t.getComputedStyle(e)
	};

	function ze() {
		if (Pe) {
			Pe.style.cssText = "box-sizing:border-box;position:relative;display:block;margin:auto;border:1px;padding:1px;top:1%;width:50%", Pe.innerHTML = "", fe.appendChild(Ie);
			var e = T.getComputedStyle(Pe);
			Le = "1%" !== e.top, He = "2px" === e.marginLeft, Oe = "4px" === e.width, Pe.style.marginRight = "50%", qe = "4px" === e.marginRight, fe.removeChild(Ie), Pe = null
		}
	}

	function Re(e, t, n) {
		var i, o, r, s, a = e.style;
		return (n = n || Me(e)) && ("" !== (s = n.getPropertyValue(t) || n[t]) || S.contains(e.ownerDocument, e) || (s = S.style(e, t)), !v.pixelMarginRight() && _e.test(s) && Fe.test(t) && (i = a.width, o = a.minWidth, r = a.maxWidth, a.minWidth = a.maxWidth = a.width = s, s = n.width, a.width = i, a.minWidth = o, a.maxWidth = r)), void 0 !== s ? s + "" : s
	}

	function We(e, t) {
		return {
			get: function () {
				return e() ? void delete this.get : (this.get = t).apply(this, arguments)
			}
		}
	}

	Ie = C.createElement("div"), (Pe = C.createElement("div")).style && (Pe.style.backgroundClip = "content-box", Pe.cloneNode(!0).style.backgroundClip = "", v.clearCloneStyle = "content-box" === Pe.style.backgroundClip, Ie.style.cssText = "border:0;width:8px;height:0;top:0;left:-9999px;padding:0;margin-top:1px;position:absolute", Ie.appendChild(Pe), S.extend(v, {
		pixelPosition: function () {
			return ze(), Le
		}, boxSizingReliable: function () {
			return ze(), Oe
		}, pixelMarginRight: function () {
			return ze(), qe
		}, reliableMarginLeft: function () {
			return ze(), He
		}
	}));
	var Be = /^(none|table(?!-c[ea]).+)/, Ue = {position: "absolute", visibility: "hidden", display: "block"},
		Xe = {letterSpacing: "0", fontWeight: "400"}, Ke = ["Webkit", "Moz", "ms"], Ve = C.createElement("div").style;

	function Ye(e) {
		if (e in Ve) return e;
		for (var t = e[0].toUpperCase() + e.slice(1), n = Ke.length; n--;) if ((e = Ke[n] + t) in Ve) return e
	}

	function Ge(e, t, n) {
		var i = J.exec(t);
		return i ? Math.max(0, i[2] - (n || 0)) + (i[3] || "px") : t
	}

	function Je(e, t, n, i, o) {
		var r, s = 0;
		for (r = n === (i ? "border" : "content") ? 4 : "width" === t ? 1 : 0; r < 4; r += 2) "margin" === n && (s += S.css(e, n + Q[r], !0, o)), i ? ("content" === n && (s -= S.css(e, "padding" + Q[r], !0, o)), "margin" !== n && (s -= S.css(e, "border" + Q[r] + "Width", !0, o))) : (s += S.css(e, "padding" + Q[r], !0, o), "padding" !== n && (s += S.css(e, "border" + Q[r] + "Width", !0, o)));
		return s
	}

	function Qe(e, t, n) {
		var i, o = !0, r = Me(e), s = "border-box" === S.css(e, "boxSizing", !1, r);
		if (e.getClientRects().length && (i = e.getBoundingClientRect()[t]), i <= 0 || null == i) {
			if (((i = Re(e, t, r)) < 0 || null == i) && (i = e.style[t]), _e.test(i)) return i;
			o = s && (v.boxSizingReliable() || i === e.style[t]), i = parseFloat(i) || 0
		}
		return i + Je(e, t, n || (s ? "border" : "content"), o, r) + "px"
	}

	function Ze(e, t, n, i, o) {
		return new Ze.prototype.init(e, t, n, i, o)
	}

	S.extend({
		cssHooks: {
			opacity: {
				get: function (e, t) {
					if (t) {
						var n = Re(e, "opacity");
						return "" === n ? "1" : n
					}
				}
			}
		},
		cssNumber: {
			animationIterationCount: !0,
			columnCount: !0,
			fillOpacity: !0,
			flexGrow: !0,
			flexShrink: !0,
			fontWeight: !0,
			lineHeight: !0,
			opacity: !0,
			order: !0,
			orphans: !0,
			widows: !0,
			zIndex: !0,
			zoom: !0
		},
		cssProps: {float: "cssFloat"},
		style: function (e, t, n, i) {
			if (e && 3 !== e.nodeType && 8 !== e.nodeType && e.style) {
				var o, r, s, a = S.camelCase(t), l = e.style;
				return t = S.cssProps[a] || (S.cssProps[a] = Ye(a) || a), s = S.cssHooks[t] || S.cssHooks[a], void 0 === n ? s && "get" in s && void 0 !== (o = s.get(e, !1, i)) ? o : l[t] : ("string" === (r = typeof n) && (o = J.exec(n)) && o[1] && (n = ee(e, t, o), r = "number"), void (null != n && n == n && ("number" === r && (n += o && o[3] || (S.cssNumber[a] ? "" : "px")), v.clearCloneStyle || "" !== n || 0 !== t.indexOf("background") || (l[t] = "inherit"), s && "set" in s && void 0 === (n = s.set(e, n, i)) || (l[t] = n))))
			}
		},
		css: function (e, t, n, i) {
			var o, r, s, a = S.camelCase(t);
			return t = S.cssProps[a] || (S.cssProps[a] = Ye(a) || a), (s = S.cssHooks[t] || S.cssHooks[a]) && "get" in s && (o = s.get(e, !0, n)), void 0 === o && (o = Re(e, t, i)), "normal" === o && t in Xe && (o = Xe[t]), "" === n || n ? (r = parseFloat(o), !0 === n || isFinite(r) ? r || 0 : o) : o
		}
	}), S.each(["height", "width"], function (e, s) {
		S.cssHooks[s] = {
			get: function (e, t, n) {
				if (t) return !Be.test(S.css(e, "display")) || e.getClientRects().length && e.getBoundingClientRect().width ? Qe(e, s, n) : Y(e, Ue, function () {
					return Qe(e, s, n)
				})
			}, set: function (e, t, n) {
				var i, o = n && Me(e), r = n && Je(e, s, n, "border-box" === S.css(e, "boxSizing", !1, o), o);
				return r && (i = J.exec(t)) && "px" !== (i[3] || "px") && (e.style[s] = t, t = S.css(e, s)), Ge(0, t, r)
			}
		}
	}), S.cssHooks.marginLeft = We(v.reliableMarginLeft, function (e, t) {
		if (t) return (parseFloat(Re(e, "marginLeft")) || e.getBoundingClientRect().left - Y(e, {marginLeft: 0}, function () {
			return e.getBoundingClientRect().left
		})) + "px"
	}), S.each({margin: "", padding: "", border: "Width"}, function (o, r) {
		S.cssHooks[o + r] = {
			expand: function (e) {
				for (var t = 0, n = {}, i = "string" == typeof e ? e.split(" ") : [e]; t < 4; t++) n[o + Q[t] + r] = i[t] || i[t - 2] || i[0];
				return n
			}
		}, Fe.test(o) || (S.cssHooks[o + r].set = Ge)
	}), S.fn.extend({
		css: function (e, t) {
			return R(this, function (e, t, n) {
				var i, o, r = {}, s = 0;
				if (S.isArray(t)) {
					for (i = Me(e), o = t.length; s < o; s++) r[t[s]] = S.css(e, t[s], !1, i);
					return r
				}
				return void 0 !== n ? S.style(e, t, n) : S.css(e, t)
			}, e, t, 1 < arguments.length)
		}
	}), ((S.Tween = Ze).prototype = {
		constructor: Ze, init: function (e, t, n, i, o, r) {
			this.elem = e, this.prop = n, this.easing = o || S.easing._default, this.options = t, this.start = this.now = this.cur(), this.end = i, this.unit = r || (S.cssNumber[n] ? "" : "px")
		}, cur: function () {
			var e = Ze.propHooks[this.prop];
			return e && e.get ? e.get(this) : Ze.propHooks._default.get(this)
		}, run: function (e) {
			var t, n = Ze.propHooks[this.prop];
			return this.options.duration ? this.pos = t = S.easing[this.easing](e, this.options.duration * e, 0, 1, this.options.duration) : this.pos = t = e, this.now = (this.end - this.start) * t + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), n && n.set ? n.set(this) : Ze.propHooks._default.set(this), this
		}
	}).init.prototype = Ze.prototype, (Ze.propHooks = {
		_default: {
			get: function (e) {
				var t;
				return 1 !== e.elem.nodeType || null != e.elem[e.prop] && null == e.elem.style[e.prop] ? e.elem[e.prop] : (t = S.css(e.elem, e.prop, "")) && "auto" !== t ? t : 0
			}, set: function (e) {
				S.fx.step[e.prop] ? S.fx.step[e.prop](e) : 1 !== e.elem.nodeType || null == e.elem.style[S.cssProps[e.prop]] && !S.cssHooks[e.prop] ? e.elem[e.prop] = e.now : S.style(e.elem, e.prop, e.now + e.unit)
			}
		}
	}).scrollTop = Ze.propHooks.scrollLeft = {
		set: function (e) {
			e.elem.nodeType && e.elem.parentNode && (e.elem[e.prop] = e.now)
		}
	}, S.easing = {
		linear: function (e) {
			return e
		}, swing: function (e) {
			return .5 - Math.cos(e * Math.PI) / 2
		}, _default: "swing"
	}, S.fx = Ze.prototype.init, S.fx.step = {};
	var et, tt, nt, it, ot = /^(?:toggle|show|hide)$/, rt = /queueHooks$/;

	function st() {
		tt && (T.requestAnimationFrame(st), S.fx.tick())
	}

	function at() {
		return T.setTimeout(function () {
			et = void 0
		}), et = S.now()
	}

	function lt(e, t) {
		var n, i = 0, o = {height: e};
		for (t = t ? 1 : 0; i < 4; i += 2 - t) o["margin" + (n = Q[i])] = o["padding" + n] = e;
		return t && (o.opacity = o.width = e), o
	}

	function ct(e, t, n) {
		for (var i, o = (ut.tweeners[t] || []).concat(ut.tweeners["*"]), r = 0, s = o.length; r < s; r++) if (i = o[r].call(n, t, e)) return i
	}

	function ut(r, e, t) {
		var n, s, i = 0, o = ut.prefilters.length, a = S.Deferred().always(function () {
			delete l.elem
		}), l = function () {
			if (s) return !1;
			for (var e = et || at(), t = Math.max(0, c.startTime + c.duration - e), n = 1 - (t / c.duration || 0), i = 0, o = c.tweens.length; i < o; i++) c.tweens[i].run(n);
			return a.notifyWith(r, [c, n, t]), n < 1 && o ? t : (a.resolveWith(r, [c]), !1)
		}, c = a.promise({
			elem: r,
			props: S.extend({}, e),
			opts: S.extend(!0, {specialEasing: {}, easing: S.easing._default}, t),
			originalProperties: e,
			originalOptions: t,
			startTime: et || at(),
			duration: t.duration,
			tweens: [],
			createTween: function (e, t) {
				var n = S.Tween(r, c.opts, e, t, c.opts.specialEasing[e] || c.opts.easing);
				return c.tweens.push(n), n
			},
			stop: function (e) {
				var t = 0, n = e ? c.tweens.length : 0;
				if (s) return this;
				for (s = !0; t < n; t++) c.tweens[t].run(1);
				return e ? (a.notifyWith(r, [c, 1, 0]), a.resolveWith(r, [c, e])) : a.rejectWith(r, [c, e]), this
			}
		}), u = c.props;
		for (function (e, t) {
			var n, i, o, r, s;
			for (n in e) if (o = t[i = S.camelCase(n)], r = e[n], S.isArray(r) && (o = r[1], r = e[n] = r[0]), n !== i && (e[i] = r, delete e[n]), (s = S.cssHooks[i]) && "expand" in s) for (n in r = s.expand(r), delete e[i], r) n in e || (e[n] = r[n], t[n] = o); else t[i] = o
		}(u, c.opts.specialEasing); i < o; i++) if (n = ut.prefilters[i].call(c, r, u, c.opts)) return S.isFunction(n.stop) && (S._queueHooks(c.elem, c.opts.queue).stop = S.proxy(n.stop, n)), n;
		return S.map(u, ct, c), S.isFunction(c.opts.start) && c.opts.start.call(r, c), S.fx.timer(S.extend(l, {
			elem: r,
			anim: c,
			queue: c.opts.queue
		})), c.progress(c.opts.progress).done(c.opts.done, c.opts.complete).fail(c.opts.fail).always(c.opts.always)
	}

	S.Animation = S.extend(ut, {
		tweeners: {
			"*": [function (e, t) {
				var n = this.createTween(e, t);
				return ee(n.elem, e, J.exec(t), n), n
			}]
		}, tweener: function (e, t) {
			for (var n, i = 0, o = (e = S.isFunction(e) ? (t = e, ["*"]) : e.match(q)).length; i < o; i++) n = e[i], ut.tweeners[n] = ut.tweeners[n] || [], ut.tweeners[n].unshift(t)
		}, prefilters: [function (e, t, n) {
			var i, o, r, s, a, l, c, u, d = "width" in t || "height" in t, p = this, f = {}, h = e.style,
				g = e.nodeType && Z(e), v = B.get(e, "fxshow");
			for (i in n.queue || (null == (s = S._queueHooks(e, "fx")).unqueued && (s.unqueued = 0, a = s.empty.fire, s.empty.fire = function () {
				s.unqueued || a()
			}), s.unqueued++, p.always(function () {
				p.always(function () {
					s.unqueued--, S.queue(e, "fx").length || s.empty.fire()
				})
			})), t) if (o = t[i], ot.test(o)) {
				if (delete t[i], r = r || "toggle" === o, o === (g ? "hide" : "show")) {
					if ("show" !== o || !v || void 0 === v[i]) continue;
					g = !0
				}
				f[i] = v && v[i] || S.style(e, i)
			}
			if ((l = !S.isEmptyObject(t)) || !S.isEmptyObject(f)) for (i in d && 1 === e.nodeType && (n.overflow = [h.overflow, h.overflowX, h.overflowY], null == (c = v && v.display) && (c = B.get(e, "display")), "none" === (u = S.css(e, "display")) && (c ? u = c : (ne([e], !0), c = e.style.display || c, u = S.css(e, "display"), ne([e]))), ("inline" === u || "inline-block" === u && null != c) && "none" === S.css(e, "float") && (l || (p.done(function () {
				h.display = c
			}), null == c && (u = h.display, c = "none" === u ? "" : u)), h.display = "inline-block")), n.overflow && (h.overflow = "hidden", p.always(function () {
				h.overflow = n.overflow[0], h.overflowX = n.overflow[1], h.overflowY = n.overflow[2]
			})), l = !1, f) l || (v ? "hidden" in v && (g = v.hidden) : v = B.access(e, "fxshow", {display: c}), r && (v.hidden = !g), g && ne([e], !0), p.done(function () {
				for (i in g || ne([e]), B.remove(e, "fxshow"), f) S.style(e, i, f[i])
			})), l = ct(g ? v[i] : 0, i, p), i in v || (v[i] = l.start, g && (l.end = l.start, l.start = 0))
		}], prefilter: function (e, t) {
			t ? ut.prefilters.unshift(e) : ut.prefilters.push(e)
		}
	}), S.speed = function (e, t, n) {
		var i = e && "object" == typeof e ? S.extend({}, e) : {
			complete: n || !n && t || S.isFunction(e) && e,
			duration: e,
			easing: n && t || t && !S.isFunction(t) && t
		};
		return S.fx.off || C.hidden ? i.duration = 0 : "number" != typeof i.duration && (i.duration in S.fx.speeds ? i.duration = S.fx.speeds[i.duration] : i.duration = S.fx.speeds._default), null != i.queue && !0 !== i.queue || (i.queue = "fx"), i.old = i.complete, i.complete = function () {
			S.isFunction(i.old) && i.old.call(this), i.queue && S.dequeue(this, i.queue)
		}, i
	}, S.fn.extend({
		fadeTo: function (e, t, n, i) {
			return this.filter(Z).css("opacity", 0).show().end().animate({opacity: t}, e, n, i)
		}, animate: function (t, e, n, i) {
			function o() {
				var e = ut(this, S.extend({}, t), s);
				(r || B.get(this, "finish")) && e.stop(!0)
			}

			var r = S.isEmptyObject(t), s = S.speed(e, n, i);
			return o.finish = o, r || !1 === s.queue ? this.each(o) : this.queue(s.queue, o)
		}, stop: function (o, e, r) {
			function s(e) {
				var t = e.stop;
				delete e.stop, t(r)
			}

			return "string" != typeof o && (r = e, e = o, o = void 0), e && !1 !== o && this.queue(o || "fx", []), this.each(function () {
				var e = !0, t = null != o && o + "queueHooks", n = S.timers, i = B.get(this);
				if (t) i[t] && i[t].stop && s(i[t]); else for (t in i) i[t] && i[t].stop && rt.test(t) && s(i[t]);
				for (t = n.length; t--;) n[t].elem !== this || null != o && n[t].queue !== o || (n[t].anim.stop(r), e = !1, n.splice(t, 1));
				!e && r || S.dequeue(this, o)
			})
		}, finish: function (s) {
			return !1 !== s && (s = s || "fx"), this.each(function () {
				var e, t = B.get(this), n = t[s + "queue"], i = t[s + "queueHooks"], o = S.timers, r = n ? n.length : 0;
				for (t.finish = !0, S.queue(this, s, []), i && i.stop && i.stop.call(this, !0), e = o.length; e--;) o[e].elem === this && o[e].queue === s && (o[e].anim.stop(!0), o.splice(e, 1));
				for (e = 0; e < r; e++) n[e] && n[e].finish && n[e].finish.call(this);
				delete t.finish
			})
		}
	}), S.each(["toggle", "show", "hide"], function (e, i) {
		var o = S.fn[i];
		S.fn[i] = function (e, t, n) {
			return null == e || "boolean" == typeof e ? o.apply(this, arguments) : this.animate(lt(i, !0), e, t, n)
		}
	}), S.each({
		slideDown: lt("show"),
		slideUp: lt("hide"),
		slideToggle: lt("toggle"),
		fadeIn: {opacity: "show"},
		fadeOut: {opacity: "hide"},
		fadeToggle: {opacity: "toggle"}
	}, function (e, i) {
		S.fn[e] = function (e, t, n) {
			return this.animate(i, e, t, n)
		}
	}), S.timers = [], S.fx.tick = function () {
		var e, t = 0, n = S.timers;
		for (et = S.now(); t < n.length; t++) (e = n[t])() || n[t] !== e || n.splice(t--, 1);
		n.length || S.fx.stop(), et = void 0
	}, S.fx.timer = function (e) {
		S.timers.push(e), e() ? S.fx.start() : S.timers.pop()
	}, S.fx.interval = 13, S.fx.start = function () {
		tt = tt || (T.requestAnimationFrame ? T.requestAnimationFrame(st) : T.setInterval(S.fx.tick, S.fx.interval))
	}, S.fx.stop = function () {
		T.cancelAnimationFrame ? T.cancelAnimationFrame(tt) : T.clearInterval(tt), tt = null
	}, S.fx.speeds = {slow: 600, fast: 200, _default: 400}, S.fn.delay = function (i, e) {
		return i = S.fx && S.fx.speeds[i] || i, e = e || "fx", this.queue(e, function (e, t) {
			var n = T.setTimeout(e, i);
			t.stop = function () {
				T.clearTimeout(n)
			}
		})
	}, nt = C.createElement("input"), it = C.createElement("select").appendChild(C.createElement("option")), nt.type = "checkbox", v.checkOn = "" !== nt.value, v.optSelected = it.selected, (nt = C.createElement("input")).value = "t", nt.type = "radio", v.radioValue = "t" === nt.value;
	var dt, pt = S.expr.attrHandle;
	S.fn.extend({
		attr: function (e, t) {
			return R(this, S.attr, e, t, 1 < arguments.length)
		}, removeAttr: function (e) {
			return this.each(function () {
				S.removeAttr(this, e)
			})
		}
	}), S.extend({
		attr: function (e, t, n) {
			var i, o, r = e.nodeType;
			if (3 !== r && 8 !== r && 2 !== r) return void 0 === e.getAttribute ? S.prop(e, t, n) : (1 === r && S.isXMLDoc(e) || (o = S.attrHooks[t.toLowerCase()] || (S.expr.match.bool.test(t) ? dt : void 0)), void 0 !== n ? null === n ? void S.removeAttr(e, t) : o && "set" in o && void 0 !== (i = o.set(e, n, t)) ? i : (e.setAttribute(t, n + ""), n) : o && "get" in o && null !== (i = o.get(e, t)) ? i : null == (i = S.find.attr(e, t)) ? void 0 : i)
		}, attrHooks: {
			type: {
				set: function (e, t) {
					if (!v.radioValue && "radio" === t && S.nodeName(e, "input")) {
						var n = e.value;
						return e.setAttribute("type", t), n && (e.value = n), t
					}
				}
			}
		}, removeAttr: function (e, t) {
			var n, i = 0, o = t && t.match(q);
			if (o && 1 === e.nodeType) for (; n = o[i++];) e.removeAttribute(n)
		}
	}), dt = {
		set: function (e, t, n) {
			return !1 === t ? S.removeAttr(e, n) : e.setAttribute(n, n), n
		}
	}, S.each(S.expr.match.bool.source.match(/\w+/g), function (e, t) {
		var s = pt[t] || S.find.attr;
		pt[t] = function (e, t, n) {
			var i, o, r = t.toLowerCase();
			return n || (o = pt[r], pt[r] = i, i = null != s(e, t, n) ? r : null, pt[r] = o), i
		}
	});
	var ft = /^(?:input|select|textarea|button)$/i, ht = /^(?:a|area)$/i;

	function gt(e) {
		return (e.match(q) || []).join(" ")
	}

	function vt(e) {
		return e.getAttribute && e.getAttribute("class") || ""
	}

	S.fn.extend({
		prop: function (e, t) {
			return R(this, S.prop, e, t, 1 < arguments.length)
		}, removeProp: function (e) {
			return this.each(function () {
				delete this[S.propFix[e] || e]
			})
		}
	}), S.extend({
		prop: function (e, t, n) {
			var i, o, r = e.nodeType;
			if (3 !== r && 8 !== r && 2 !== r) return 1 === r && S.isXMLDoc(e) || (t = S.propFix[t] || t, o = S.propHooks[t]), void 0 !== n ? o && "set" in o && void 0 !== (i = o.set(e, n, t)) ? i : e[t] = n : o && "get" in o && null !== (i = o.get(e, t)) ? i : e[t]
		}, propHooks: {
			tabIndex: {
				get: function (e) {
					var t = S.find.attr(e, "tabindex");
					return t ? parseInt(t, 10) : ft.test(e.nodeName) || ht.test(e.nodeName) && e.href ? 0 : -1
				}
			}
		}, propFix: {for: "htmlFor", class: "className"}
	}), v.optSelected || (S.propHooks.selected = {
		get: function (e) {
			var t = e.parentNode;
			return t && t.parentNode && t.parentNode.selectedIndex, null
		}, set: function (e) {
			var t = e.parentNode;
			t && (t.selectedIndex, t.parentNode && t.parentNode.selectedIndex)
		}
	}), S.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function () {
		S.propFix[this.toLowerCase()] = this
	}), S.fn.extend({
		addClass: function (t) {
			var e, n, i, o, r, s, a, l = 0;
			if (S.isFunction(t)) return this.each(function (e) {
				S(this).addClass(t.call(this, e, vt(this)))
			});
			if ("string" == typeof t && t) for (e = t.match(q) || []; n = this[l++];) if (o = vt(n), i = 1 === n.nodeType && " " + gt(o) + " ") {
				for (s = 0; r = e[s++];) i.indexOf(" " + r + " ") < 0 && (i += r + " ");
				o !== (a = gt(i)) && n.setAttribute("class", a)
			}
			return this
		}, removeClass: function (t) {
			var e, n, i, o, r, s, a, l = 0;
			if (S.isFunction(t)) return this.each(function (e) {
				S(this).removeClass(t.call(this, e, vt(this)))
			});
			if (!arguments.length) return this.attr("class", "");
			if ("string" == typeof t && t) for (e = t.match(q) || []; n = this[l++];) if (o = vt(n), i = 1 === n.nodeType && " " + gt(o) + " ") {
				for (s = 0; r = e[s++];) for (; -1 < i.indexOf(" " + r + " ");) i = i.replace(" " + r + " ", " ");
				o !== (a = gt(i)) && n.setAttribute("class", a)
			}
			return this
		}, toggleClass: function (o, t) {
			var r = typeof o;
			return "boolean" == typeof t && "string" == r ? t ? this.addClass(o) : this.removeClass(o) : S.isFunction(o) ? this.each(function (e) {
				S(this).toggleClass(o.call(this, e, vt(this), t), t)
			}) : this.each(function () {
				var e, t, n, i;
				if ("string" == r) for (t = 0, n = S(this), i = o.match(q) || []; e = i[t++];) n.hasClass(e) ? n.removeClass(e) : n.addClass(e); else void 0 !== o && "boolean" != r || ((e = vt(this)) && B.set(this, "__className__", e), this.setAttribute && this.setAttribute("class", e || !1 === o ? "" : B.get(this, "__className__") || ""))
			})
		}, hasClass: function (e) {
			var t, n, i = 0;
			for (t = " " + e + " "; n = this[i++];) if (1 === n.nodeType && -1 < (" " + gt(vt(n)) + " ").indexOf(t)) return !0;
			return !1
		}
	});
	var mt = /\r/g;
	S.fn.extend({
		val: function (n) {
			var i, e, o, t = this[0];
			return arguments.length ? (o = S.isFunction(n), this.each(function (e) {
				var t;
				1 === this.nodeType && (null == (t = o ? n.call(this, e, S(this).val()) : n) ? t = "" : "number" == typeof t ? t += "" : S.isArray(t) && (t = S.map(t, function (e) {
					return null == e ? "" : e + ""
				})), (i = S.valHooks[this.type] || S.valHooks[this.nodeName.toLowerCase()]) && "set" in i && void 0 !== i.set(this, t, "value") || (this.value = t))
			})) : t ? (i = S.valHooks[t.type] || S.valHooks[t.nodeName.toLowerCase()]) && "get" in i && void 0 !== (e = i.get(t, "value")) ? e : "string" == typeof (e = t.value) ? e.replace(mt, "") : null == e ? "" : e : void 0
		}
	}), S.extend({
		valHooks: {
			option: {
				get: function (e) {
					var t = S.find.attr(e, "value");
					return null != t ? t : gt(S.text(e))
				}
			}, select: {
				get: function (e) {
					var t, n, i, o = e.options, r = e.selectedIndex, s = "select-one" === e.type, a = s ? null : [],
						l = s ? r + 1 : o.length;
					for (i = r < 0 ? l : s ? r : 0; i < l; i++) if (((n = o[i]).selected || i === r) && !n.disabled && (!n.parentNode.disabled || !S.nodeName(n.parentNode, "optgroup"))) {
						if (t = S(n).val(), s) return t;
						a.push(t)
					}
					return a
				}, set: function (e, t) {
					for (var n, i, o = e.options, r = S.makeArray(t), s = o.length; s--;) ((i = o[s]).selected = -1 < S.inArray(S.valHooks.option.get(i), r)) && (n = !0);
					return n || (e.selectedIndex = -1), r
				}
			}
		}
	}), S.each(["radio", "checkbox"], function () {
		S.valHooks[this] = {
			set: function (e, t) {
				if (S.isArray(t)) return e.checked = -1 < S.inArray(S(e).val(), t)
			}
		}, v.checkOn || (S.valHooks[this].get = function (e) {
			return null === e.getAttribute("value") ? "on" : e.value
		})
	});
	var yt = /^(?:focusinfocus|focusoutblur)$/;
	S.extend(S.event, {
		trigger: function (e, t, n, i) {
			var o, r, s, a, l, c, u, d = [n || C], p = h.call(e, "type") ? e.type : e,
				f = h.call(e, "namespace") ? e.namespace.split(".") : [];
			if (r = s = n = n || C, 3 !== n.nodeType && 8 !== n.nodeType && !yt.test(p + S.event.triggered) && (-1 < p.indexOf(".") && (p = (f = p.split(".")).shift(), f.sort()), l = p.indexOf(":") < 0 && "on" + p, (e = e[S.expando] ? e : new S.Event(p, "object" == typeof e && e)).isTrigger = i ? 2 : 3, e.namespace = f.join("."), e.rnamespace = e.namespace ? new RegExp("(^|\\.)" + f.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, e.result = void 0, e.target || (e.target = n), t = null == t ? [e] : S.makeArray(t, [e]), u = S.event.special[p] || {}, i || !u.trigger || !1 !== u.trigger.apply(n, t))) {
				if (!i && !u.noBubble && !S.isWindow(n)) {
					for (a = u.delegateType || p, yt.test(a + p) || (r = r.parentNode); r; r = r.parentNode) d.push(r), s = r;
					s === (n.ownerDocument || C) && d.push(s.defaultView || s.parentWindow || T)
				}
				for (o = 0; (r = d[o++]) && !e.isPropagationStopped();) e.type = 1 < o ? a : u.bindType || p, (c = (B.get(r, "events") || {})[e.type] && B.get(r, "handle")) && c.apply(r, t), (c = l && r[l]) && c.apply && z(r) && (e.result = c.apply(r, t), !1 === e.result && e.preventDefault());
				return e.type = p, i || e.isDefaultPrevented() || u._default && !1 !== u._default.apply(d.pop(), t) || !z(n) || l && S.isFunction(n[p]) && !S.isWindow(n) && ((s = n[l]) && (n[l] = null), n[S.event.triggered = p](), S.event.triggered = void 0, s && (n[l] = s)), e.result
			}
		}, simulate: function (e, t, n) {
			var i = S.extend(new S.Event, n, {type: e, isSimulated: !0});
			S.event.trigger(i, null, t)
		}
	}), S.fn.extend({
		trigger: function (e, t) {
			return this.each(function () {
				S.event.trigger(e, t, this)
			})
		}, triggerHandler: function (e, t) {
			var n = this[0];
			if (n) return S.event.trigger(e, t, n, !0)
		}
	}), S.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "), function (e, n) {
		S.fn[n] = function (e, t) {
			return 0 < arguments.length ? this.on(n, null, e, t) : this.trigger(n)
		}
	}), S.fn.extend({
		hover: function (e, t) {
			return this.mouseenter(e).mouseleave(t || e)
		}
	}), v.focusin = "onfocusin" in T, v.focusin || S.each({focus: "focusin", blur: "focusout"}, function (n, i) {
		function o(e) {
			S.event.simulate(i, e.target, S.event.fix(e))
		}

		S.event.special[i] = {
			setup: function () {
				var e = this.ownerDocument || this, t = B.access(e, i);
				t || e.addEventListener(n, o, !0), B.access(e, i, (t || 0) + 1)
			}, teardown: function () {
				var e = this.ownerDocument || this, t = B.access(e, i) - 1;
				t ? B.access(e, i, t) : (e.removeEventListener(n, o, !0), B.remove(e, i))
			}
		}
	});
	var bt = T.location, xt = S.now(), wt = /\?/;
	S.parseXML = function (e) {
		var t;
		if (!e || "string" != typeof e) return null;
		try {
			t = (new T.DOMParser).parseFromString(e, "text/xml")
		} catch (e) {
			t = void 0
		}
		return t && !t.getElementsByTagName("parsererror").length || S.error("Invalid XML: " + e), t
	};
	var kt = /\[\]$/, Tt = /\r?\n/g, Ct = /^(?:submit|button|image|reset|file)$/i,
		St = /^(?:input|select|textarea|keygen)/i;

	function $t(n, e, i, o) {
		var t;
		if (S.isArray(e)) S.each(e, function (e, t) {
			i || kt.test(n) ? o(n, t) : $t(n + "[" + ("object" == typeof t && null != t ? e : "") + "]", t, i, o)
		}); else if (i || "object" !== S.type(e)) o(n, e); else for (t in e) $t(n + "[" + t + "]", e[t], i, o)
	}

	S.param = function (e, t) {
		function n(e, t) {
			var n = S.isFunction(t) ? t() : t;
			o[o.length] = encodeURIComponent(e) + "=" + encodeURIComponent(null == n ? "" : n)
		}

		var i, o = [];
		if (S.isArray(e) || e.jquery && !S.isPlainObject(e)) S.each(e, function () {
			n(this.name, this.value)
		}); else for (i in e) $t(i, e[i], t, n);
		return o.join("&")
	}, S.fn.extend({
		serialize: function () {
			return S.param(this.serializeArray())
		}, serializeArray: function () {
			return this.map(function () {
				var e = S.prop(this, "elements");
				return e ? S.makeArray(e) : this
			}).filter(function () {
				var e = this.type;
				return this.name && !S(this).is(":disabled") && St.test(this.nodeName) && !Ct.test(e) && (this.checked || !ie.test(e))
			}).map(function (e, t) {
				var n = S(this).val();
				return null == n ? null : S.isArray(n) ? S.map(n, function (e) {
					return {name: t.name, value: e.replace(Tt, "\r\n")}
				}) : {name: t.name, value: n.replace(Tt, "\r\n")}
			}).get()
		}
	});
	var Et = /%20/g, At = /#.*$/, Dt = /([?&])_=[^&]*/, jt = /^(.*?):[ \t]*([^\r\n]*)$/gm, Nt = /^(?:GET|HEAD)$/,
		Lt = /^\/\//, Ot = {}, qt = {}, Ht = "*/".concat("*"), It = C.createElement("a");

	function Pt(r) {
		return function (e, t) {
			"string" != typeof e && (t = e, e = "*");
			var n, i = 0, o = e.toLowerCase().match(q) || [];
			if (S.isFunction(t)) for (; n = o[i++];) "+" === n[0] ? (n = n.slice(1) || "*", (r[n] = r[n] || []).unshift(t)) : (r[n] = r[n] || []).push(t)
		}
	}

	function Ft(t, o, r, s) {
		var a = {}, l = t === qt;

		function c(e) {
			var i;
			return a[e] = !0, S.each(t[e] || [], function (e, t) {
				var n = t(o, r, s);
				return "string" != typeof n || l || a[n] ? l ? !(i = n) : void 0 : (o.dataTypes.unshift(n), c(n), !1)
			}), i
		}

		return c(o.dataTypes[0]) || !a["*"] && c("*")
	}

	function _t(e, t) {
		var n, i, o = S.ajaxSettings.flatOptions || {};
		for (n in t) void 0 !== t[n] && ((o[n] ? e : i = i || {})[n] = t[n]);
		return i && S.extend(!0, e, i), e
	}

	It.href = bt.href, S.extend({
		active: 0,
		lastModified: {},
		etag: {},
		ajaxSettings: {
			url: bt.href,
			type: "GET",
			isLocal: /^(?:about|app|app-storage|.+-extension|file|res|widget):$/.test(bt.protocol),
			global: !0,
			processData: !0,
			async: !0,
			contentType: "application/x-www-form-urlencoded; charset=UTF-8",
			accepts: {
				"*": Ht,
				text: "text/plain",
				html: "text/html",
				xml: "application/xml, text/xml",
				json: "application/json, text/javascript"
			},
			contents: {xml: /\bxml\b/, html: /\bhtml/, json: /\bjson\b/},
			responseFields: {xml: "responseXML", text: "responseText", json: "responseJSON"},
			converters: {"* text": String, "text html": !0, "text json": JSON.parse, "text xml": S.parseXML},
			flatOptions: {url: !0, context: !0}
		},
		ajaxSetup: function (e, t) {
			return t ? _t(_t(e, S.ajaxSettings), t) : _t(S.ajaxSettings, e)
		},
		ajaxPrefilter: Pt(Ot),
		ajaxTransport: Pt(qt),
		ajax: function (e, t) {
			"object" == typeof e && (t = e, e = void 0), t = t || {};
			var u, d, p, n, f, i, h, g, o, r, v = S.ajaxSetup({}, t), m = v.context || v,
				y = v.context && (m.nodeType || m.jquery) ? S(m) : S.event, b = S.Deferred(),
				x = S.Callbacks("once memory"), w = v.statusCode || {}, s = {}, a = {}, l = "canceled", k = {
					readyState: 0, getResponseHeader: function (e) {
						var t;
						if (h) {
							if (!n) for (n = {}; t = jt.exec(p);) n[t[1].toLowerCase()] = t[2];
							t = n[e.toLowerCase()]
						}
						return null == t ? null : t
					}, getAllResponseHeaders: function () {
						return h ? p : null
					}, setRequestHeader: function (e, t) {
						return null == h && (e = a[e.toLowerCase()] = a[e.toLowerCase()] || e, s[e] = t), this
					}, overrideMimeType: function (e) {
						return null == h && (v.mimeType = e), this
					}, statusCode: function (e) {
						var t;
						if (e) if (h) k.always(e[k.status]); else for (t in e) w[t] = [w[t], e[t]];
						return this
					}, abort: function (e) {
						var t = e || l;
						return u && u.abort(t), c(0, t), this
					}
				};
			if (b.promise(k), v.url = ((e || v.url || bt.href) + "").replace(Lt, bt.protocol + "//"), v.type = t.method || t.type || v.method || v.type, v.dataTypes = (v.dataType || "*").toLowerCase().match(q) || [""], null == v.crossDomain) {
				i = C.createElement("a");
				try {
					i.href = v.url, i.href = i.href, v.crossDomain = It.protocol + "//" + It.host != i.protocol + "//" + i.host
				} catch (e) {
					v.crossDomain = !0
				}
			}
			if (v.data && v.processData && "string" != typeof v.data && (v.data = S.param(v.data, v.traditional)), Ft(Ot, v, t, k), h) return k;
			for (o in (g = S.event && v.global) && 0 == S.active++ && S.event.trigger("ajaxStart"), v.type = v.type.toUpperCase(), v.hasContent = !Nt.test(v.type), d = v.url.replace(At, ""), v.hasContent ? v.data && v.processData && 0 === (v.contentType || "").indexOf("application/x-www-form-urlencoded") && (v.data = v.data.replace(Et, "+")) : (r = v.url.slice(d.length), v.data && (d += (wt.test(d) ? "&" : "?") + v.data, delete v.data), !1 === v.cache && (d = d.replace(Dt, "$1"), r = (wt.test(d) ? "&" : "?") + "_=" + xt++ + r), v.url = d + r), v.ifModified && (S.lastModified[d] && k.setRequestHeader("If-Modified-Since", S.lastModified[d]), S.etag[d] && k.setRequestHeader("If-None-Match", S.etag[d])), (v.data && v.hasContent && !1 !== v.contentType || t.contentType) && k.setRequestHeader("Content-Type", v.contentType), k.setRequestHeader("Accept", v.dataTypes[0] && v.accepts[v.dataTypes[0]] ? v.accepts[v.dataTypes[0]] + ("*" !== v.dataTypes[0] ? ", " + Ht + "; q=0.01" : "") : v.accepts["*"]), v.headers) k.setRequestHeader(o, v.headers[o]);
			if (v.beforeSend && (!1 === v.beforeSend.call(m, k, v) || h)) return k.abort();
			if (l = "abort", x.add(v.complete), k.done(v.success), k.fail(v.error), u = Ft(qt, v, t, k)) {
				if (k.readyState = 1, g && y.trigger("ajaxSend", [k, v]), h) return k;
				v.async && 0 < v.timeout && (f = T.setTimeout(function () {
					k.abort("timeout")
				}, v.timeout));
				try {
					h = !1, u.send(s, c)
				} catch (e) {
					if (h) throw e;
					c(-1, e)
				}
			} else c(-1, "No Transport");

			function c(e, t, n, i) {
				var o, r, s, a, l, c = t;
				h || (h = !0, f && T.clearTimeout(f), u = void 0, p = i || "", k.readyState = 0 < e ? 4 : 0, o = 200 <= e && e < 300 || 304 === e, n && (a = function (e, t, n) {
					for (var i, o, r, s, a = e.contents, l = e.dataTypes; "*" === l[0];) l.shift(), void 0 === i && (i = e.mimeType || t.getResponseHeader("Content-Type"));
					if (i) for (o in a) if (a[o] && a[o].test(i)) {
						l.unshift(o);
						break
					}
					if (l[0] in n) r = l[0]; else {
						for (o in n) {
							if (!l[0] || e.converters[o + " " + l[0]]) {
								r = o;
								break
							}
							s = s || o
						}
						r = r || s
					}
					if (r) return r !== l[0] && l.unshift(r), n[r]
				}(v, k, n)), a = function (e, t, n, i) {
					var o, r, s, a, l, c = {}, u = e.dataTypes.slice();
					if (u[1]) for (s in e.converters) c[s.toLowerCase()] = e.converters[s];
					for (r = u.shift(); r;) if (e.responseFields[r] && (n[e.responseFields[r]] = t), !l && i && e.dataFilter && (t = e.dataFilter(t, e.dataType)), l = r, r = u.shift()) if ("*" === r) r = l; else if ("*" !== l && l !== r) {
						if (!(s = c[l + " " + r] || c["* " + r])) for (o in c) if ((a = o.split(" "))[1] === r && (s = c[l + " " + a[0]] || c["* " + a[0]])) {
							!0 === s ? s = c[o] : !0 !== c[o] && (r = a[0], u.unshift(a[1]));
							break
						}
						if (!0 !== s) if (s && e.throws) t = s(t); else try {
							t = s(t)
						} catch (e) {
							return {state: "parsererror", error: s ? e : "No conversion from " + l + " to " + r}
						}
					}
					return {state: "success", data: t}
				}(v, a, k, o), o ? (v.ifModified && ((l = k.getResponseHeader("Last-Modified")) && (S.lastModified[d] = l), (l = k.getResponseHeader("etag")) && (S.etag[d] = l)), 204 === e || "HEAD" === v.type ? c = "nocontent" : 304 === e ? c = "notmodified" : (c = a.state, r = a.data, o = !(s = a.error))) : (s = c, !e && c || (c = "error", e < 0 && (e = 0))), k.status = e, k.statusText = (t || c) + "", o ? b.resolveWith(m, [r, c, k]) : b.rejectWith(m, [k, c, s]), k.statusCode(w), w = void 0, g && y.trigger(o ? "ajaxSuccess" : "ajaxError", [k, v, o ? r : s]), x.fireWith(m, [k, c]), g && (y.trigger("ajaxComplete", [k, v]), --S.active || S.event.trigger("ajaxStop")))
			}

			return k
		},
		getJSON: function (e, t, n) {
			return S.get(e, t, n, "json")
		},
		getScript: function (e, t) {
			return S.get(e, void 0, t, "script")
		}
	}), S.each(["get", "post"], function (e, o) {
		S[o] = function (e, t, n, i) {
			return S.isFunction(t) && (i = i || n, n = t, t = void 0), S.ajax(S.extend({
				url: e,
				type: o,
				dataType: i,
				data: t,
				success: n
			}, S.isPlainObject(e) && e))
		}
	}), S._evalUrl = function (e) {
		return S.ajax({url: e, type: "GET", dataType: "script", cache: !0, async: !1, global: !1, throws: !0})
	}, S.fn.extend({
		wrapAll: function (e) {
			var t;
			return this[0] && (S.isFunction(e) && (e = e.call(this[0])), t = S(e, this[0].ownerDocument).eq(0).clone(!0), this[0].parentNode && t.insertBefore(this[0]), t.map(function () {
				for (var e = this; e.firstElementChild;) e = e.firstElementChild;
				return e
			}).append(this)), this
		}, wrapInner: function (n) {
			return S.isFunction(n) ? this.each(function (e) {
				S(this).wrapInner(n.call(this, e))
			}) : this.each(function () {
				var e = S(this), t = e.contents();
				t.length ? t.wrapAll(n) : e.append(n)
			})
		}, wrap: function (t) {
			var n = S.isFunction(t);
			return this.each(function (e) {
				S(this).wrapAll(n ? t.call(this, e) : t)
			})
		}, unwrap: function (e) {
			return this.parent(e).not("body").each(function () {
				S(this).replaceWith(this.childNodes)
			}), this
		}
	}), S.expr.pseudos.hidden = function (e) {
		return !S.expr.pseudos.visible(e)
	}, S.expr.pseudos.visible = function (e) {
		return !!(e.offsetWidth || e.offsetHeight || e.getClientRects().length)
	}, S.ajaxSettings.xhr = function () {
		try {
			return new T.XMLHttpRequest
		} catch (e) {
		}
	};
	var Mt = {0: 200, 1223: 204}, zt = S.ajaxSettings.xhr();
	v.cors = !!zt && "withCredentials" in zt, v.ajax = zt = !!zt, S.ajaxTransport(function (o) {
		var r, s;
		if (v.cors || zt && !o.crossDomain) return {
			send: function (e, t) {
				var n, i = o.xhr();
				if (i.open(o.type, o.url, o.async, o.username, o.password), o.xhrFields) for (n in o.xhrFields) i[n] = o.xhrFields[n];
				for (n in o.mimeType && i.overrideMimeType && i.overrideMimeType(o.mimeType), o.crossDomain || e["X-Requested-With"] || (e["X-Requested-With"] = "XMLHttpRequest"), e) i.setRequestHeader(n, e[n]);
				r = function (e) {
					return function () {
						r && (r = s = i.onload = i.onerror = i.onabort = i.onreadystatechange = null, "abort" === e ? i.abort() : "error" === e ? "number" != typeof i.status ? t(0, "error") : t(i.status, i.statusText) : t(Mt[i.status] || i.status, i.statusText, "text" !== (i.responseType || "text") || "string" != typeof i.responseText ? {binary: i.response} : {text: i.responseText}, i.getAllResponseHeaders()))
					}
				}, i.onload = r(), s = i.onerror = r("error"), void 0 !== i.onabort ? i.onabort = s : i.onreadystatechange = function () {
					4 === i.readyState && T.setTimeout(function () {
						r && s()
					})
				}, r = r("abort");
				try {
					i.send(o.hasContent && o.data || null)
				} catch (e) {
					if (r) throw e
				}
			}, abort: function () {
				r && r()
			}
		}
	}), S.ajaxPrefilter(function (e) {
		e.crossDomain && (e.contents.script = !1)
	}), S.ajaxSetup({
		accepts: {script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"},
		contents: {script: /\b(?:java|ecma)script\b/},
		converters: {
			"text script": function (e) {
				return S.globalEval(e), e
			}
		}
	}), S.ajaxPrefilter("script", function (e) {
		void 0 === e.cache && (e.cache = !1), e.crossDomain && (e.type = "GET")
	}), S.ajaxTransport("script", function (n) {
		var i, o;
		if (n.crossDomain) return {
			send: function (e, t) {
				i = S("<script>").prop({charset: n.scriptCharset, src: n.url}).on("load error", o = function (e) {
					i.remove(), o = null, e && t("error" === e.type ? 404 : 200, e.type)
				}), C.head.appendChild(i[0])
			}, abort: function () {
				o && o()
			}
		}
	});
	var Rt, Wt = [], Bt = /(=)\?(?=&|$)|\?\?/;

	function Ut(e) {
		return S.isWindow(e) ? e : 9 === e.nodeType && e.defaultView
	}

	S.ajaxSetup({
		jsonp: "callback", jsonpCallback: function () {
			var e = Wt.pop() || S.expando + "_" + xt++;
			return this[e] = !0, e
		}
	}), S.ajaxPrefilter("json jsonp", function (e, t, n) {
		var i, o, r,
			s = !1 !== e.jsonp && (Bt.test(e.url) ? "url" : "string" == typeof e.data && 0 === (e.contentType || "").indexOf("application/x-www-form-urlencoded") && Bt.test(e.data) && "data");
		if (s || "jsonp" === e.dataTypes[0]) return i = e.jsonpCallback = S.isFunction(e.jsonpCallback) ? e.jsonpCallback() : e.jsonpCallback, s ? e[s] = e[s].replace(Bt, "$1" + i) : !1 !== e.jsonp && (e.url += (wt.test(e.url) ? "&" : "?") + e.jsonp + "=" + i), e.converters["script json"] = function () {
			return r || S.error(i + " was not called"), r[0]
		}, e.dataTypes[0] = "json", o = T[i], T[i] = function () {
			r = arguments
		}, n.always(function () {
			void 0 === o ? S(T).removeProp(i) : T[i] = o, e[i] && (e.jsonpCallback = t.jsonpCallback, Wt.push(i)), r && S.isFunction(o) && o(r[0]), r = o = void 0
		}), "script"
	}), v.createHTMLDocument = ((Rt = C.implementation.createHTMLDocument("").body).innerHTML = "<form></form><form></form>", 2 === Rt.childNodes.length), S.parseHTML = function (e, t, n) {
		return "string" != typeof e ? [] : ("boolean" == typeof t && (n = t, t = !1), t || (v.createHTMLDocument ? ((i = (t = C.implementation.createHTMLDocument("")).createElement("base")).href = C.location.href, t.head.appendChild(i)) : t = C), r = !n && [], (o = $.exec(e)) ? [t.createElement(o[1])] : (o = pe([e], t, r), r && r.length && S(r).remove(), S.merge([], o.childNodes)));
		var i, o, r
	}, S.fn.load = function (e, t, n) {
		var i, o, r, s = this, a = e.indexOf(" ");
		return -1 < a && (i = gt(e.slice(a)), e = e.slice(0, a)), S.isFunction(t) ? (n = t, t = void 0) : t && "object" == typeof t && (o = "POST"), 0 < s.length && S.ajax({
			url: e,
			type: o || "GET",
			dataType: "html",
			data: t
		}).done(function (e) {
			r = arguments, s.html(i ? S("<div>").append(S.parseHTML(e)).find(i) : e)
		}).always(n && function (e, t) {
			s.each(function () {
				n.apply(this, r || [e.responseText, t, e])
			})
		}), this
	}, S.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function (e, t) {
		S.fn[t] = function (e) {
			return this.on(t, e)
		}
	}), S.expr.pseudos.animated = function (t) {
		return S.grep(S.timers, function (e) {
			return t === e.elem
		}).length
	}, S.offset = {
		setOffset: function (e, t, n) {
			var i, o, r, s, a, l, c = S.css(e, "position"), u = S(e), d = {};
			"static" === c && (e.style.position = "relative"), a = u.offset(), r = S.css(e, "top"), l = S.css(e, "left"), o = ("absolute" === c || "fixed" === c) && -1 < (r + l).indexOf("auto") ? (s = (i = u.position()).top, i.left) : (s = parseFloat(r) || 0, parseFloat(l) || 0), S.isFunction(t) && (t = t.call(e, n, S.extend({}, a))), null != t.top && (d.top = t.top - a.top + s), null != t.left && (d.left = t.left - a.left + o), "using" in t ? t.using.call(e, d) : u.css(d)
		}
	}, S.fn.extend({
		offset: function (t) {
			if (arguments.length) return void 0 === t ? this : this.each(function (e) {
				S.offset.setOffset(this, t, e)
			});
			var e, n, i, o, r = this[0];
			return r ? r.getClientRects().length ? (i = r.getBoundingClientRect()).width || i.height ? (n = Ut(o = r.ownerDocument), e = o.documentElement, {
				top: i.top + n.pageYOffset - e.clientTop,
				left: i.left + n.pageXOffset - e.clientLeft
			}) : i : {top: 0, left: 0} : void 0
		}, position: function () {
			if (this[0]) {
				var e, t, n = this[0], i = {top: 0, left: 0};
				return "fixed" === S.css(n, "position") ? t = n.getBoundingClientRect() : (e = this.offsetParent(), t = this.offset(), S.nodeName(e[0], "html") || (i = e.offset()), i = {
					top: i.top + S.css(e[0], "borderTopWidth", !0),
					left: i.left + S.css(e[0], "borderLeftWidth", !0)
				}), {top: t.top - i.top - S.css(n, "marginTop", !0), left: t.left - i.left - S.css(n, "marginLeft", !0)}
			}
		}, offsetParent: function () {
			return this.map(function () {
				for (var e = this.offsetParent; e && "static" === S.css(e, "position");) e = e.offsetParent;
				return e || fe
			})
		}
	}), S.each({scrollLeft: "pageXOffset", scrollTop: "pageYOffset"}, function (t, o) {
		var r = "pageYOffset" === o;
		S.fn[t] = function (e) {
			return R(this, function (e, t, n) {
				var i = Ut(e);
				return void 0 === n ? i ? i[o] : e[t] : void (i ? i.scrollTo(r ? i.pageXOffset : n, r ? n : i.pageYOffset) : e[t] = n)
			}, t, e, arguments.length)
		}
	}), S.each(["top", "left"], function (e, n) {
		S.cssHooks[n] = We(v.pixelPosition, function (e, t) {
			if (t) return t = Re(e, n), _e.test(t) ? S(e).position()[n] + "px" : t
		})
	}), S.each({Height: "height", Width: "width"}, function (s, a) {
		S.each({padding: "inner" + s, content: a, "": "outer" + s}, function (i, r) {
			S.fn[r] = function (e, t) {
				var n = arguments.length && (i || "boolean" != typeof e),
					o = i || (!0 === e || !0 === t ? "margin" : "border");
				return R(this, function (e, t, n) {
					var i;
					return S.isWindow(e) ? 0 === r.indexOf("outer") ? e["inner" + s] : e.document.documentElement["client" + s] : 9 === e.nodeType ? (i = e.documentElement, Math.max(e.body["scroll" + s], i["scroll" + s], e.body["offset" + s], i["offset" + s], i["client" + s])) : void 0 === n ? S.css(e, t, o) : S.style(e, t, n, o)
				}, a, n ? e : void 0, n)
			}
		})
	}), S.fn.extend({
		bind: function (e, t, n) {
			return this.on(e, null, t, n)
		}, unbind: function (e, t) {
			return this.off(e, null, t)
		}, delegate: function (e, t, n, i) {
			return this.on(t, e, n, i)
		}, undelegate: function (e, t, n) {
			return 1 === arguments.length ? this.off(e, "**") : this.off(t, e || "**", n)
		}
	}), S.parseJSON = JSON.parse, "function" == typeof define && define.amd && define("jquery", [], function () {
		return S
	});
	var Xt = T.jQuery, Kt = T.$;
	return S.noConflict = function (e) {
		return T.$ === S && (T.$ = Kt), e && T.jQuery === S && (T.jQuery = Xt), S
	}, e || (T.jQuery = T.$ = S), S
}), function (e, t) {
	"use strict";
	"object" == typeof module && "object" == typeof module.exports ? module.exports = e.document ? t(e, !0) : function (e) {
		if (!e.document) throw new Error("jQuery requires a window with a document");
		return t(e)
	} : t(e)
}("undefined" != typeof window ? window : this, function (T, e) {
	"use strict";

	function g(e, t) {
		var n = (t = t || V).createElement("script");
		n.text = e, t.head.appendChild(n).parentNode.removeChild(n)
	}

	function a(e) {
		var t = !!e && "length" in e && e.length, n = se.type(e);
		return "function" !== n && !se.isWindow(e) && ("array" === n || 0 === t || "number" == typeof t && 0 < t && t - 1 in e)
	}

	function c(e, t) {
		return e.nodeName && e.nodeName.toLowerCase() === t.toLowerCase()
	}

	function t(e, n, i) {
		return se.isFunction(n) ? se.grep(e, function (e, t) {
			return !!n.call(e, t, e) !== i
		}) : n.nodeType ? se.grep(e, function (e) {
			return e === n !== i
		}) : "string" != typeof n ? se.grep(e, function (e) {
			return -1 < Z.call(n, e) !== i
		}) : ge.test(n) ? se.filter(n, e, i) : (n = se.filter(n, e), se.grep(e, function (e) {
			return -1 < Z.call(n, e) !== i && 1 === e.nodeType
		}))
	}

	function n(e, t) {
		for (; (e = e[t]) && 1 !== e.nodeType;) ;
		return e
	}

	function u(e) {
		return e
	}

	function d(e) {
		throw e
	}

	function l(e, t, n, i) {
		var o;
		try {
			e && se.isFunction(o = e.promise) ? o.call(e).done(t).fail(n) : e && se.isFunction(o = e.then) ? o.call(e, t, n) : t.apply(void 0, [e].slice(i))
		} catch (e) {
			n.apply(void 0, [e])
		}
	}

	function i() {
		V.removeEventListener("DOMContentLoaded", i), T.removeEventListener("load", i), se.ready()
	}

	function o() {
		this.expando = se.expando + o.uid++
	}

	function p(e, t, n) {
		var i;
		if (void 0 === n && 1 === e.nodeType) if (i = "data-" + t.replace(Ae, "-$&").toLowerCase(), "string" == typeof (n = e.getAttribute(i))) {
			try {
				n = function (e) {
					return "true" === e || "false" !== e && ("null" === e ? null : e === +e + "" ? +e : Ee.test(e) ? JSON.parse(e) : e)
				}(n)
			} catch (e) {
			}
			$e.set(e, t, n)
		} else n = void 0;
		return n
	}

	function f(e, t, n, i) {
		var o, r = 1, s = 20, a = i ? function () {
				return i.cur()
			} : function () {
				return se.css(e, t, "")
			}, l = a(), c = n && n[3] || (se.cssNumber[t] ? "" : "px"),
			u = (se.cssNumber[t] || "px" !== c && +l) && Ne.exec(se.css(e, t));
		if (u && u[3] !== c) for (c = c || u[3], n = n || [], u = +l || 1; u /= r = r || ".5", se.style(e, t, u + c), r !== (r = a() / l) && 1 !== r && --s;) ;
		return n && (u = +u || +l || 0, o = n[1] ? u + (n[1] + 1) * n[2] : +n[2], i && (i.unit = c, i.start = u, i.end = o)), o
	}

	function m(e, t) {
		for (var n, i, o = [], r = 0, s = e.length; r < s; r++) (i = e[r]).style && (n = i.style.display, t ? ("none" === n && (o[r] = Se.get(i, "display") || null, o[r] || (i.style.display = "")), "" === i.style.display && Oe(i) && (o[r] = (d = c = l = void 0, c = (a = i).ownerDocument, u = a.nodeName, (d = qe[u]) || (l = c.body.appendChild(c.createElement(u)), d = se.css(l, "display"), l.parentNode.removeChild(l), "none" === d && (d = "block"), qe[u] = d)))) : "none" !== n && (o[r] = "none", Se.set(i, "display", n)));
		var a, l, c, u, d;
		for (r = 0; r < s; r++) null != o[r] && (e[r].style.display = o[r]);
		return e
	}

	function v(e, t) {
		var n;
		return n = void 0 !== e.getElementsByTagName ? e.getElementsByTagName(t || "*") : void 0 !== e.querySelectorAll ? e.querySelectorAll(t || "*") : [], void 0 === t || t && c(e, t) ? se.merge([e], n) : n
	}

	function y(e, t) {
		for (var n = 0, i = e.length; n < i; n++) Se.set(e[n], "globalEval", !t || Se.get(t[n], "globalEval"))
	}

	function b(e, t, n, i, o) {
		for (var r, s, a, l, c, u, d = t.createDocumentFragment(), p = [], f = 0, h = e.length; f < h; f++) if ((r = e[f]) || 0 === r) if ("object" === se.type(r)) se.merge(p, r.nodeType ? [r] : r); else if (ze.test(r)) {
			for (s = s || d.appendChild(t.createElement("div")), a = (Ie.exec(r) || ["", ""])[1].toLowerCase(), l = Fe[a] || Fe._default, s.innerHTML = l[1] + se.htmlPrefilter(r) + l[2], u = l[0]; u--;) s = s.lastChild;
			se.merge(p, s.childNodes), (s = d.firstChild).textContent = ""
		} else p.push(t.createTextNode(r));
		for (d.textContent = "", f = 0; r = p[f++];) if (i && -1 < se.inArray(r, i)) o && o.push(r); else if (c = se.contains(r.ownerDocument, r), s = v(d.appendChild(r), "script"), c && y(s), n) for (u = 0; r = s[u++];) Pe.test(r.type || "") && n.push(r);
		return d
	}

	function r() {
		return !0
	}

	function h() {
		return !1
	}

	function s() {
		try {
			return V.activeElement
		} catch (e) {
		}
	}

	function x(e, t, n, i, o, r) {
		var s, a;
		if ("object" == typeof t) {
			for (a in "string" != typeof n && (i = i || n, n = void 0), t) x(e, a, n, i, t[a], r);
			return e
		}
		if (null == i && null == o ? (o = n, i = n = void 0) : null == o && ("string" == typeof n ? (o = i, i = void 0) : (o = i, i = n, n = void 0)), !1 === o) o = h; else if (!o) return e;
		return 1 === r && (s = o, (o = function (e) {
			return se().off(e), s.apply(this, arguments)
		}).guid = s.guid || (s.guid = se.guid++)), e.each(function () {
			se.event.add(this, t, o, i, n)
		})
	}

	function w(e, t) {
		return c(e, "table") && c(11 !== t.nodeType ? t : t.firstChild, "tr") && se(">tbody", e)[0] || e
	}

	function k(e) {
		return e.type = (null !== e.getAttribute("type")) + "/" + e.type, e
	}

	function C(e) {
		var t = Ye.exec(e.type);
		return t ? e.type = t[1] : e.removeAttribute("type"), e
	}

	function S(e, t) {
		var n, i, o, r, s, a, l, c;
		if (1 === t.nodeType) {
			if (Se.hasData(e) && (r = Se.access(e), s = Se.set(t, r), c = r.events)) for (o in delete s.handle, s.events = {}, c) for (n = 0, i = c[o].length; n < i; n++) se.event.add(t, o, c[o][n]);
			$e.hasData(e) && (a = $e.access(e), l = se.extend({}, a), $e.set(t, l))
		}
	}

	function $(n, i, o, r) {
		i = J.apply([], i);
		var e, t, s, a, l, c, u = 0, d = n.length, p = d - 1, f = i[0], h = se.isFunction(f);
		if (h || 1 < d && "string" == typeof f && !re.checkClone && Ve.test(f)) return n.each(function (e) {
			var t = n.eq(e);
			h && (i[0] = f.call(this, e, t.html())), $(t, i, o, r)
		});
		if (d && (t = (e = b(i, n[0].ownerDocument, !1, n, r)).firstChild, 1 === e.childNodes.length && (e = t), t || r)) {
			for (a = (s = se.map(v(e, "script"), k)).length; u < d; u++) l = e, u !== p && (l = se.clone(l, !0, !0), a && se.merge(s, v(l, "script"))), o.call(n[u], l, u);
			if (a) for (c = s[s.length - 1].ownerDocument, se.map(s, C), u = 0; u < a; u++) l = s[u], Pe.test(l.type || "") && !Se.access(l, "globalEval") && se.contains(c, l) && (l.src ? se._evalUrl && se._evalUrl(l.src) : g(l.textContent.replace(Ge, ""), c))
		}
		return n
	}

	function E(e, t, n) {
		for (var i, o = t ? se.filter(t, e) : e, r = 0; null != (i = o[r]); r++) n || 1 !== i.nodeType || se.cleanData(v(i)), i.parentNode && (n && se.contains(i.ownerDocument, i) && y(v(i, "script")), i.parentNode.removeChild(i));
		return e
	}

	function A(e, t, n) {
		var i, o, r, s, a = e.style;
		return (n = n || rt(e)) && ("" !== (s = n.getPropertyValue(t) || n[t]) || se.contains(e.ownerDocument, e) || (s = se.style(e, t)), !re.pixelMarginRight() && ot.test(s) && it.test(t) && (i = a.width, o = a.minWidth, r = a.maxWidth, a.minWidth = a.maxWidth = a.width = s, s = n.width, a.width = i, a.minWidth = o, a.maxWidth = r)), void 0 !== s ? s + "" : s
	}

	function D(e, t) {
		return {
			get: function () {
				return e() ? void delete this.get : (this.get = t).apply(this, arguments)
			}
		}
	}

	function j(e) {
		var t = se.cssProps[e];
		return t = t || (se.cssProps[e] = function (e) {
			if (e in pt) return e;
			for (var t = e[0].toUpperCase() + e.slice(1), n = dt.length; n--;) if ((e = dt[n] + t) in pt) return e
		}(e) || e)
	}

	function N(e, t, n) {
		var i = Ne.exec(t);
		return i ? Math.max(0, i[2] - (n || 0)) + (i[3] || "px") : t
	}

	function L(e, t, n, i, o) {
		var r, s = 0;
		for (r = n === (i ? "border" : "content") ? 4 : "width" === t ? 1 : 0; r < 4; r += 2) "margin" === n && (s += se.css(e, n + Le[r], !0, o)), i ? ("content" === n && (s -= se.css(e, "padding" + Le[r], !0, o)), "margin" !== n && (s -= se.css(e, "border" + Le[r] + "Width", !0, o))) : (s += se.css(e, "padding" + Le[r], !0, o), "padding" !== n && (s += se.css(e, "border" + Le[r] + "Width", !0, o)));
		return s
	}

	function O(e, t, n) {
		var i, o = rt(e), r = A(e, t, o), s = "border-box" === se.css(e, "boxSizing", !1, o);
		return ot.test(r) ? r : (i = s && (re.boxSizingReliable() || r === e.style[t]), "auto" === r && (r = e["offset" + t[0].toUpperCase() + t.slice(1)]), (r = parseFloat(r) || 0) + L(e, t, n || (s ? "border" : "content"), i, o) + "px")
	}

	function q(e, t, n, i, o) {
		return new q.prototype.init(e, t, n, i, o)
	}

	function H() {
		ht && (!1 === V.hidden && T.requestAnimationFrame ? T.requestAnimationFrame(H) : T.setTimeout(H, se.fx.interval), se.fx.tick())
	}

	function I() {
		return T.setTimeout(function () {
			ft = void 0
		}), ft = se.now()
	}

	function P(e, t) {
		var n, i = 0, o = {height: e};
		for (t = t ? 1 : 0; i < 4; i += 2 - t) o["margin" + (n = Le[i])] = o["padding" + n] = e;
		return t && (o.opacity = o.width = e), o
	}

	function F(e, t, n) {
		for (var i, o = (_.tweeners[t] || []).concat(_.tweeners["*"]), r = 0, s = o.length; r < s; r++) if (i = o[r].call(n, t, e)) return i
	}

	function _(r, e, t) {
		var n, s, i = 0, o = _.prefilters.length, a = se.Deferred().always(function () {
			delete l.elem
		}), l = function () {
			if (s) return !1;
			for (var e = ft || I(), t = Math.max(0, c.startTime + c.duration - e), n = 1 - (t / c.duration || 0), i = 0, o = c.tweens.length; i < o; i++) c.tweens[i].run(n);
			return a.notifyWith(r, [c, n, t]), n < 1 && o ? t : (o || a.notifyWith(r, [c, 1, 0]), a.resolveWith(r, [c]), !1)
		}, c = a.promise({
			elem: r,
			props: se.extend({}, e),
			opts: se.extend(!0, {specialEasing: {}, easing: se.easing._default}, t),
			originalProperties: e,
			originalOptions: t,
			startTime: ft || I(),
			duration: t.duration,
			tweens: [],
			createTween: function (e, t) {
				var n = se.Tween(r, c.opts, e, t, c.opts.specialEasing[e] || c.opts.easing);
				return c.tweens.push(n), n
			},
			stop: function (e) {
				var t = 0, n = e ? c.tweens.length : 0;
				if (s) return this;
				for (s = !0; t < n; t++) c.tweens[t].run(1);
				return e ? (a.notifyWith(r, [c, 1, 0]), a.resolveWith(r, [c, e])) : a.rejectWith(r, [c, e]), this
			}
		}), u = c.props;
		for (function (e, t) {
			var n, i, o, r, s;
			for (n in e) if (o = t[i = se.camelCase(n)], r = e[n], Array.isArray(r) && (o = r[1], r = e[n] = r[0]), n !== i && (e[i] = r, delete e[n]), (s = se.cssHooks[i]) && "expand" in s) for (n in r = s.expand(r), delete e[i], r) n in e || (e[n] = r[n], t[n] = o); else t[i] = o
		}(u, c.opts.specialEasing); i < o; i++) if (n = _.prefilters[i].call(c, r, u, c.opts)) return se.isFunction(n.stop) && (se._queueHooks(c.elem, c.opts.queue).stop = se.proxy(n.stop, n)), n;
		return se.map(u, F, c), se.isFunction(c.opts.start) && c.opts.start.call(r, c), c.progress(c.opts.progress).done(c.opts.done, c.opts.complete).fail(c.opts.fail).always(c.opts.always), se.fx.timer(se.extend(l, {
			elem: r,
			anim: c,
			queue: c.opts.queue
		})), c
	}

	function M(e) {
		return (e.match(xe) || []).join(" ")
	}

	function z(e) {
		return e.getAttribute && e.getAttribute("class") || ""
	}

	function R(n, e, i, o) {
		var t;
		if (Array.isArray(e)) se.each(e, function (e, t) {
			i || At.test(n) ? o(n, t) : R(n + "[" + ("object" == typeof t && null != t ? e : "") + "]", t, i, o)
		}); else if (i || "object" !== se.type(e)) o(n, e); else for (t in e) R(n + "[" + t + "]", e[t], i, o)
	}

	function W(r) {
		return function (e, t) {
			"string" != typeof e && (t = e, e = "*");
			var n, i = 0, o = e.toLowerCase().match(xe) || [];
			if (se.isFunction(t)) for (; n = o[i++];) "+" === n[0] ? (n = n.slice(1) || "*", (r[n] = r[n] || []).unshift(t)) : (r[n] = r[n] || []).push(t)
		}
	}

	function B(t, o, r, s) {
		function a(e) {
			var i;
			return l[e] = !0, se.each(t[e] || [], function (e, t) {
				var n = t(o, r, s);
				return "string" != typeof n || c || l[n] ? c ? !(i = n) : void 0 : (o.dataTypes.unshift(n), a(n), !1)
			}), i
		}

		var l = {}, c = t === _t;
		return a(o.dataTypes[0]) || !l["*"] && a("*")
	}

	function U(e, t) {
		var n, i, o = se.ajaxSettings.flatOptions || {};
		for (n in t) void 0 !== t[n] && ((o[n] ? e : i = i || {})[n] = t[n]);
		return i && se.extend(!0, e, i), e
	}

	function X(e, t) {
		return t.toUpperCase()
	}

	var K = [], V = T.document, Y = Object.getPrototypeOf, G = K.slice, J = K.concat, Q = K.push, Z = K.indexOf,
		ee = {}, te = ee.toString, ne = ee.hasOwnProperty, ie = ne.toString, oe = ie.call(Object), re = {},
		se = function (e, t) {
			return new se.fn.init(e, t)
		}, ae = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, le = /^-ms-/, ce = /-([a-z])/g;
	se.fn = se.prototype = {
		jquery: "3.2.1", constructor: se, length: 0, toArray: function () {
			return G.call(this)
		}, get: function (e) {
			return null == e ? G.call(this) : e < 0 ? this[e + this.length] : this[e]
		}, pushStack: function (e) {
			var t = se.merge(this.constructor(), e);
			return t.prevObject = this, t
		}, each: function (e) {
			return se.each(this, e)
		}, map: function (n) {
			return this.pushStack(se.map(this, function (e, t) {
				return n.call(e, t, e)
			}))
		}, slice: function () {
			return this.pushStack(G.apply(this, arguments))
		}, first: function () {
			return this.eq(0)
		}, last: function () {
			return this.eq(-1)
		}, eq: function (e) {
			var t = this.length, n = +e + (e < 0 ? t : 0);
			return this.pushStack(0 <= n && n < t ? [this[n]] : [])
		}, end: function () {
			return this.prevObject || this.constructor()
		}, push: Q, sort: K.sort, splice: K.splice
	}, se.extend = se.fn.extend = function () {
		var e, t, n, i, o, r, s = arguments[0] || {}, a = 1, l = arguments.length, c = !1;
		for ("boolean" == typeof s && (c = s, s = arguments[a] || {}, a++), "object" == typeof s || se.isFunction(s) || (s = {}), a === l && (s = this, a--); a < l; a++) if (null != (e = arguments[a])) for (t in e) n = s[t], s !== (i = e[t]) && (c && i && (se.isPlainObject(i) || (o = Array.isArray(i))) ? (r = o ? (o = !1, n && Array.isArray(n) ? n : []) : n && se.isPlainObject(n) ? n : {}, s[t] = se.extend(c, r, i)) : void 0 !== i && (s[t] = i));
		return s
	}, se.extend({
		expando: "jQuery" + ("3.2.1" + Math.random()).replace(/\D/g, ""), isReady: !0, error: function (e) {
			throw new Error(e)
		}, noop: function () {
		}, isFunction: function (e) {
			return "function" === se.type(e)
		}, isWindow: function (e) {
			return null != e && e === e.window
		}, isNumeric: function (e) {
			var t = se.type(e);
			return ("number" === t || "string" === t) && !isNaN(e - parseFloat(e))
		}, isPlainObject: function (e) {
			var t, n;
			return !(!e || "[object Object]" !== te.call(e) || (t = Y(e)) && ("function" != typeof (n = ne.call(t, "constructor") && t.constructor) || ie.call(n) !== oe))
		}, isEmptyObject: function (e) {
			var t;
			for (t in e) return !1;
			return !0
		}, type: function (e) {
			return null == e ? e + "" : "object" == typeof e || "function" == typeof e ? ee[te.call(e)] || "object" : typeof e
		}, globalEval: function (e) {
			g(e)
		}, camelCase: function (e) {
			return e.replace(le, "ms-").replace(ce, X)
		}, each: function (e, t) {
			var n, i = 0;
			if (a(e)) for (n = e.length; i < n && !1 !== t.call(e[i], i, e[i]); i++) ; else for (i in e) if (!1 === t.call(e[i], i, e[i])) break;
			return e
		}, trim: function (e) {
			return null == e ? "" : (e + "").replace(ae, "")
		}, makeArray: function (e, t) {
			var n = t || [];
			return null != e && (a(Object(e)) ? se.merge(n, "string" == typeof e ? [e] : e) : Q.call(n, e)), n
		}, inArray: function (e, t, n) {
			return null == t ? -1 : Z.call(t, e, n)
		}, merge: function (e, t) {
			for (var n = +t.length, i = 0, o = e.length; i < n; i++) e[o++] = t[i];
			return e.length = o, e
		}, grep: function (e, t, n) {
			for (var i = [], o = 0, r = e.length, s = !n; o < r; o++) !t(e[o], o) != s && i.push(e[o]);
			return i
		}, map: function (e, t, n) {
			var i, o, r = 0, s = [];
			if (a(e)) for (i = e.length; r < i; r++) null != (o = t(e[r], r, n)) && s.push(o); else for (r in e) null != (o = t(e[r], r, n)) && s.push(o);
			return J.apply([], s)
		}, guid: 1, proxy: function (e, t) {
			var n, i, o;
			if ("string" == typeof t && (n = e[t], t = e, e = n), se.isFunction(e)) return i = G.call(arguments, 2), (o = function () {
				return e.apply(t || this, i.concat(G.call(arguments)))
			}).guid = e.guid = e.guid || se.guid++, o
		}, now: Date.now, support: re
	}), "function" == typeof Symbol && (se.fn[Symbol.iterator] = K[Symbol.iterator]), se.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), function (e, t) {
		ee["[object " + t + "]"] = t.toLowerCase()
	});
	var ue = function (n) {
		function x(e, t, n, i) {
			var o, r, s, a, l, c, u, d = t && t.ownerDocument, p = t ? t.nodeType : 9;
			if (n = n || [], "string" != typeof e || !e || 1 !== p && 9 !== p && 11 !== p) return n;
			if (!i && ((t ? t.ownerDocument || t : R) !== q && O(t), t = t || q, I)) {
				if (11 !== p && (l = ye.exec(e))) if (o = l[1]) {
					if (9 === p) {
						if (!(s = t.getElementById(o))) return n;
						if (s.id === o) return n.push(s), n
					} else if (d && (s = d.getElementById(o)) && M(t, s) && s.id === o) return n.push(s), n
				} else {
					if (l[2]) return Z.apply(n, t.getElementsByTagName(e)), n;
					if ((o = l[3]) && T.getElementsByClassName && t.getElementsByClassName) return Z.apply(n, t.getElementsByClassName(o)), n
				}
				if (T.qsa && !K[e + " "] && (!P || !P.test(e))) {
					if (1 !== p) d = t, u = e; else if ("object" !== t.nodeName.toLowerCase()) {
						for ((a = t.getAttribute("id")) ? a = a.replace(we, ke) : t.setAttribute("id", a = z), r = (c = E(e)).length; r--;) c[r] = "#" + a + " " + h(c[r]);
						u = c.join(","), d = be.test(e) && f(t.parentNode) || t
					}
					if (u) try {
						return Z.apply(n, d.querySelectorAll(u)), n
					} catch (e) {
					} finally {
						a === z && t.removeAttribute("id")
					}
				}
			}
			return D(e.replace(le, "$1"), t, n, i)
		}

		function e() {
			var i = [];
			return function e(t, n) {
				return i.push(t + " ") > C.cacheLength && delete e[i.shift()], e[t + " "] = n
			}
		}

		function l(e) {
			return e[z] = !0, e
		}

		function o(e) {
			var t = q.createElement("fieldset");
			try {
				return !!e(t)
			} catch (e) {
				return !1
			} finally {
				t.parentNode && t.parentNode.removeChild(t), t = null
			}
		}

		function t(e, t) {
			for (var n = e.split("|"), i = n.length; i--;) C.attrHandle[n[i]] = t
		}

		function c(e, t) {
			var n = t && e, i = n && 1 === e.nodeType && 1 === t.nodeType && e.sourceIndex - t.sourceIndex;
			if (i) return i;
			if (n) for (; n = n.nextSibling;) if (n === t) return -1;
			return e ? 1 : -1
		}

		function i(t) {
			return function (e) {
				return "input" === e.nodeName.toLowerCase() && e.type === t
			}
		}

		function r(n) {
			return function (e) {
				var t = e.nodeName.toLowerCase();
				return ("input" === t || "button" === t) && e.type === n
			}
		}

		function s(t) {
			return function (e) {
				return "form" in e ? e.parentNode && !1 === e.disabled ? "label" in e ? "label" in e.parentNode ? e.parentNode.disabled === t : e.disabled === t : e.isDisabled === t || e.isDisabled !== !t && Te(e) === t : e.disabled === t : "label" in e && e.disabled === t
			}
		}

		function a(s) {
			return l(function (r) {
				return r = +r, l(function (e, t) {
					for (var n, i = s([], e.length, r), o = i.length; o--;) e[n = i[o]] && (e[n] = !(t[n] = e[n]))
				})
			})
		}

		function f(e) {
			return e && void 0 !== e.getElementsByTagName && e
		}

		function u() {
		}

		function h(e) {
			for (var t = 0, n = e.length, i = ""; t < n; t++) i += e[t].value;
			return i
		}

		function d(a, e, t) {
			var l = e.dir, c = e.next, u = c || l, d = t && "parentNode" === u, p = B++;
			return e.first ? function (e, t, n) {
				for (; e = e[l];) if (1 === e.nodeType || d) return a(e, t, n);
				return !1
			} : function (e, t, n) {
				var i, o, r, s = [W, p];
				if (n) {
					for (; e = e[l];) if ((1 === e.nodeType || d) && a(e, t, n)) return !0
				} else for (; e = e[l];) if (1 === e.nodeType || d) if (o = (r = e[z] || (e[z] = {}))[e.uniqueID] || (r[e.uniqueID] = {}), c && c === e.nodeName.toLowerCase()) e = e[l] || e; else {
					if ((i = o[u]) && i[0] === W && i[1] === p) return s[2] = i[2];
					if ((o[u] = s)[2] = a(e, t, n)) return !0
				}
				return !1
			}
		}

		function p(o) {
			return 1 < o.length ? function (e, t, n) {
				for (var i = o.length; i--;) if (!o[i](e, t, n)) return !1;
				return !0
			} : o[0]
		}

		function w(e, t, n, i, o) {
			for (var r, s = [], a = 0, l = e.length, c = null != t; a < l; a++) (r = e[a]) && (n && !n(r, i, o) || (s.push(r), c && t.push(a)));
			return s
		}

		function y(f, h, g, v, m, e) {
			return v && !v[z] && (v = y(v)), m && !m[z] && (m = y(m, e)), l(function (e, t, n, i) {
				var o, r, s, a = [], l = [], c = t.length, u = e || function (e, t, n) {
						for (var i = 0, o = t.length; i < o; i++) x(e, t[i], n);
						return n
					}(h || "*", n.nodeType ? [n] : n, []), d = !f || !e && h ? u : w(u, a, f, n, i),
					p = g ? m || (e ? f : c || v) ? [] : t : d;
				if (g && g(d, p, n, i), v) for (o = w(p, l), v(o, [], n, i), r = o.length; r--;) (s = o[r]) && (p[l[r]] = !(d[l[r]] = s));
				if (e) {
					if (m || f) {
						if (m) {
							for (o = [], r = p.length; r--;) (s = p[r]) && o.push(d[r] = s);
							m(null, p = [], o, i)
						}
						for (r = p.length; r--;) (s = p[r]) && -1 < (o = m ? te(e, s) : a[r]) && (e[o] = !(t[o] = s))
					}
				} else p = w(p === t ? p.splice(c, p.length) : p), m ? m(null, t, p, i) : Z.apply(t, p)
			})
		}

		function g(e) {
			for (var o, t, n, i = e.length, r = C.relative[e[0].type], s = r || C.relative[" "], a = r ? 1 : 0, l = d(function (e) {
				return e === o
			}, s, !0), c = d(function (e) {
				return -1 < te(o, e)
			}, s, !0), u = [function (e, t, n) {
				var i = !r && (n || t !== j) || ((o = t).nodeType ? l(e, t, n) : c(e, t, n));
				return o = null, i
			}]; a < i; a++) if (t = C.relative[e[a].type]) u = [d(p(u), t)]; else {
				if ((t = C.filter[e[a].type].apply(null, e[a].matches))[z]) {
					for (n = ++a; n < i && !C.relative[e[n].type]; n++) ;
					return y(1 < a && p(u), 1 < a && h(e.slice(0, a - 1).concat({value: " " === e[a - 2].type ? "*" : ""})).replace(le, "$1"), t, a < n && g(e.slice(a, n)), n < i && g(e = e.slice(n)), n < i && h(e))
				}
				u.push(t)
			}
			return p(u)
		}

		function v(v, m) {
			function e(e, t, n, i, o) {
				var r, s, a, l = 0, c = "0", u = e && [], d = [], p = j, f = e || b && C.find.TAG("*", o),
					h = W += null == p ? 1 : Math.random() || .1, g = f.length;
				for (o && (j = t === q || t || o); c !== g && null != (r = f[c]); c++) {
					if (b && r) {
						for (s = 0, t || r.ownerDocument === q || (O(r), n = !I); a = v[s++];) if (a(r, t || q, n)) {
							i.push(r);
							break
						}
						o && (W = h)
					}
					y && ((r = !a && r) && l--, e && u.push(r))
				}
				if (l += c, y && c !== l) {
					for (s = 0; a = m[s++];) a(u, d, t, n);
					if (e) {
						if (0 < l) for (; c--;) u[c] || d[c] || (d[c] = J.call(i));
						d = w(d)
					}
					Z.apply(i, d), o && !e && 0 < d.length && 1 < l + m.length && x.uniqueSort(i)
				}
				return o && (W = h, j = p), u
			}

			var y = 0 < m.length, b = 0 < v.length;
			return y ? l(e) : e
		}

		function m(e, t, n) {
			var i = "0x" + t - 65536;
			return i != i || n ? t : i < 0 ? String.fromCharCode(65536 + i) : String.fromCharCode(i >> 10 | 55296, 1023 & i | 56320)
		}

		function b() {
			O()
		}

		var k, T, C, S, $, E, A, D, j, N, L, O, q, H, I, P, F, _, M, z = "sizzle" + 1 * new Date, R = n.document, W = 0,
			B = 0, U = e(), X = e(), K = e(), V = function (e, t) {
				return e === t && (L = !0), 0
			}, Y = {}.hasOwnProperty, G = [], J = G.pop, Q = G.push, Z = G.push, ee = G.slice, te = function (e, t) {
				for (var n = 0, i = e.length; n < i; n++) if (e[n] === t) return n;
				return -1
			},
			ne = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
			ie = "[\\x20\\t\\r\\n\\f]", oe = "(?:\\\\.|[\\w-]|[^\0-\\xa0])+",
			re = "\\[" + ie + "*(" + oe + ")(?:" + ie + "*([*^$|!~]?=)" + ie + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + oe + "))|)" + ie + "*\\]",
			se = ":(" + oe + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + re + ")*)|.*)\\)|)",
			ae = new RegExp(ie + "+", "g"), le = new RegExp("^" + ie + "+|((?:^|[^\\\\])(?:\\\\.)*)" + ie + "+$", "g"),
			ce = new RegExp("^" + ie + "*," + ie + "*"), ue = new RegExp("^" + ie + "*([>+~]|" + ie + ")" + ie + "*"),
			de = new RegExp("=" + ie + "*([^\\]'\"]*?)" + ie + "*\\]", "g"), pe = new RegExp(se),
			fe = new RegExp("^" + oe + "$"), he = {
				ID: new RegExp("^#(" + oe + ")"),
				CLASS: new RegExp("^\\.(" + oe + ")"),
				TAG: new RegExp("^(" + oe + "|[*])"),
				ATTR: new RegExp("^" + re),
				PSEUDO: new RegExp("^" + se),
				CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + ie + "*(even|odd|(([+-]|)(\\d*)n|)" + ie + "*(?:([+-]|)" + ie + "*(\\d+)|))" + ie + "*\\)|)", "i"),
				bool: new RegExp("^(?:" + ne + ")$", "i"),
				needsContext: new RegExp("^" + ie + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + ie + "*((?:-\\d)?\\d*)" + ie + "*\\)|)(?=[^-]|$)", "i")
			}, ge = /^(?:input|select|textarea|button)$/i, ve = /^h\d$/i, me = /^[^{]+\{\s*\[native \w/,
			ye = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/, be = /[+~]/,
			xe = new RegExp("\\\\([\\da-f]{1,6}" + ie + "?|(" + ie + ")|.)", "ig"),
			we = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g, ke = function (e, t) {
				return t ? "\0" === e ? "�" : e.slice(0, -1) + "\\" + e.charCodeAt(e.length - 1).toString(16) + " " : "\\" + e
			}, Te = d(function (e) {
				return !0 === e.disabled && ("form" in e || "label" in e)
			}, {dir: "parentNode", next: "legend"});
		try {
			Z.apply(G = ee.call(R.childNodes), R.childNodes), G[R.childNodes.length].nodeType
		} catch (n) {
			Z = {
				apply: G.length ? function (e, t) {
					Q.apply(e, ee.call(t))
				} : function (e, t) {
					for (var n = e.length, i = 0; e[n++] = t[i++];) ;
					e.length = n - 1
				}
			}
		}
		for (k in T = x.support = {}, $ = x.isXML = function (e) {
			var t = e && (e.ownerDocument || e).documentElement;
			return !!t && "HTML" !== t.nodeName
		}, O = x.setDocument = function (e) {
			var t, n, i = e ? e.ownerDocument || e : R;
			return i !== q && 9 === i.nodeType && i.documentElement && (H = (q = i).documentElement, I = !$(q), R !== q && (n = q.defaultView) && n.top !== n && (n.addEventListener ? n.addEventListener("unload", b, !1) : n.attachEvent && n.attachEvent("onunload", b)), T.attributes = o(function (e) {
				return e.className = "i", !e.getAttribute("className")
			}), T.getElementsByTagName = o(function (e) {
				return e.appendChild(q.createComment("")), !e.getElementsByTagName("*").length
			}), T.getElementsByClassName = me.test(q.getElementsByClassName), T.getById = o(function (e) {
				return H.appendChild(e).id = z, !q.getElementsByName || !q.getElementsByName(z).length
			}), T.getById ? (C.filter.ID = function (e) {
				var t = e.replace(xe, m);
				return function (e) {
					return e.getAttribute("id") === t
				}
			}, C.find.ID = function (e, t) {
				if (void 0 !== t.getElementById && I) {
					var n = t.getElementById(e);
					return n ? [n] : []
				}
			}) : (C.filter.ID = function (e) {
				var n = e.replace(xe, m);
				return function (e) {
					var t = void 0 !== e.getAttributeNode && e.getAttributeNode("id");
					return t && t.value === n
				}
			}, C.find.ID = function (e, t) {
				if (void 0 !== t.getElementById && I) {
					var n, i, o, r = t.getElementById(e);
					if (r) {
						if ((n = r.getAttributeNode("id")) && n.value === e) return [r];
						for (o = t.getElementsByName(e), i = 0; r = o[i++];) if ((n = r.getAttributeNode("id")) && n.value === e) return [r]
					}
					return []
				}
			}), C.find.TAG = T.getElementsByTagName ? function (e, t) {
				return void 0 !== t.getElementsByTagName ? t.getElementsByTagName(e) : T.qsa ? t.querySelectorAll(e) : void 0
			} : function (e, t) {
				var n, i = [], o = 0, r = t.getElementsByTagName(e);
				if ("*" !== e) return r;
				for (; n = r[o++];) 1 === n.nodeType && i.push(n);
				return i
			}, C.find.CLASS = T.getElementsByClassName && function (e, t) {
				if (void 0 !== t.getElementsByClassName && I) return t.getElementsByClassName(e)
			}, F = [], P = [], (T.qsa = me.test(q.querySelectorAll)) && (o(function (e) {
				H.appendChild(e).innerHTML = "<a id='" + z + "'></a><select id='" + z + "-\r\\' msallowcapture=''><option selected=''></option></select>", e.querySelectorAll("[msallowcapture^='']").length && P.push("[*^$]=" + ie + "*(?:''|\"\")"), e.querySelectorAll("[selected]").length || P.push("\\[" + ie + "*(?:value|" + ne + ")"), e.querySelectorAll("[id~=" + z + "-]").length || P.push("~="), e.querySelectorAll(":checked").length || P.push(":checked"), e.querySelectorAll("a#" + z + "+*").length || P.push(".#.+[+~]")
			}), o(function (e) {
				e.innerHTML = "<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";
				var t = q.createElement("input");
				t.setAttribute("type", "hidden"), e.appendChild(t).setAttribute("name", "D"), e.querySelectorAll("[name=d]").length && P.push("name" + ie + "*[*^$|!~]?="), 2 !== e.querySelectorAll(":enabled").length && P.push(":enabled", ":disabled"), H.appendChild(e).disabled = !0, 2 !== e.querySelectorAll(":disabled").length && P.push(":enabled", ":disabled"), e.querySelectorAll("*,:x"), P.push(",.*:")
			})), (T.matchesSelector = me.test(_ = H.matches || H.webkitMatchesSelector || H.mozMatchesSelector || H.oMatchesSelector || H.msMatchesSelector)) && o(function (e) {
				T.disconnectedMatch = _.call(e, "*"), _.call(e, "[s!='']:x"), F.push("!=", se)
			}), P = P.length && new RegExp(P.join("|")), F = F.length && new RegExp(F.join("|")), t = me.test(H.compareDocumentPosition), M = t || me.test(H.contains) ? function (e, t) {
				var n = 9 === e.nodeType ? e.documentElement : e, i = t && t.parentNode;
				return e === i || !(!i || 1 !== i.nodeType || !(n.contains ? n.contains(i) : e.compareDocumentPosition && 16 & e.compareDocumentPosition(i)))
			} : function (e, t) {
				if (t) for (; t = t.parentNode;) if (t === e) return !0;
				return !1
			}, V = t ? function (e, t) {
				if (e === t) return L = !0, 0;
				var n = !e.compareDocumentPosition - !t.compareDocumentPosition;
				return n || (1 & (n = (e.ownerDocument || e) === (t.ownerDocument || t) ? e.compareDocumentPosition(t) : 1) || !T.sortDetached && t.compareDocumentPosition(e) === n ? e === q || e.ownerDocument === R && M(R, e) ? -1 : t === q || t.ownerDocument === R && M(R, t) ? 1 : N ? te(N, e) - te(N, t) : 0 : 4 & n ? -1 : 1)
			} : function (e, t) {
				if (e === t) return L = !0, 0;
				var n, i = 0, o = e.parentNode, r = t.parentNode, s = [e], a = [t];
				if (!o || !r) return e === q ? -1 : t === q ? 1 : o ? -1 : r ? 1 : N ? te(N, e) - te(N, t) : 0;
				if (o === r) return c(e, t);
				for (n = e; n = n.parentNode;) s.unshift(n);
				for (n = t; n = n.parentNode;) a.unshift(n);
				for (; s[i] === a[i];) i++;
				return i ? c(s[i], a[i]) : s[i] === R ? -1 : a[i] === R ? 1 : 0
			}), q
		}, x.matches = function (e, t) {
			return x(e, null, null, t)
		}, x.matchesSelector = function (e, t) {
			if ((e.ownerDocument || e) !== q && O(e), t = t.replace(de, "='$1']"), T.matchesSelector && I && !K[t + " "] && (!F || !F.test(t)) && (!P || !P.test(t))) try {
				var n = _.call(e, t);
				if (n || T.disconnectedMatch || e.document && 11 !== e.document.nodeType) return n
			} catch (e) {
			}
			return 0 < x(t, q, null, [e]).length
		}, x.contains = function (e, t) {
			return (e.ownerDocument || e) !== q && O(e), M(e, t)
		}, x.attr = function (e, t) {
			(e.ownerDocument || e) !== q && O(e);
			var n = C.attrHandle[t.toLowerCase()],
				i = n && Y.call(C.attrHandle, t.toLowerCase()) ? n(e, t, !I) : void 0;
			return void 0 !== i ? i : T.attributes || !I ? e.getAttribute(t) : (i = e.getAttributeNode(t)) && i.specified ? i.value : null
		}, x.escape = function (e) {
			return (e + "").replace(we, ke)
		}, x.error = function (e) {
			throw new Error("Syntax error, unrecognized expression: " + e)
		}, x.uniqueSort = function (e) {
			var t, n = [], i = 0, o = 0;
			if (L = !T.detectDuplicates, N = !T.sortStable && e.slice(0), e.sort(V), L) {
				for (; t = e[o++];) t === e[o] && (i = n.push(o));
				for (; i--;) e.splice(n[i], 1)
			}
			return N = null, e
		}, S = x.getText = function (e) {
			var t, n = "", i = 0, o = e.nodeType;
			if (o) {
				if (1 === o || 9 === o || 11 === o) {
					if ("string" == typeof e.textContent) return e.textContent;
					for (e = e.firstChild; e; e = e.nextSibling) n += S(e)
				} else if (3 === o || 4 === o) return e.nodeValue
			} else for (; t = e[i++];) n += S(t);
			return n
		}, (C = x.selectors = {
			cacheLength: 50,
			createPseudo: l,
			match: he,
			attrHandle: {},
			find: {},
			relative: {
				">": {dir: "parentNode", first: !0},
				" ": {dir: "parentNode"},
				"+": {dir: "previousSibling", first: !0},
				"~": {dir: "previousSibling"}
			},
			preFilter: {
				ATTR: function (e) {
					return e[1] = e[1].replace(xe, m), e[3] = (e[3] || e[4] || e[5] || "").replace(xe, m), "~=" === e[2] && (e[3] = " " + e[3] + " "), e.slice(0, 4)
				}, CHILD: function (e) {
					return e[1] = e[1].toLowerCase(), "nth" === e[1].slice(0, 3) ? (e[3] || x.error(e[0]), e[4] = +(e[4] ? e[5] + (e[6] || 1) : 2 * ("even" === e[3] || "odd" === e[3])), e[5] = +(e[7] + e[8] || "odd" === e[3])) : e[3] && x.error(e[0]), e
				}, PSEUDO: function (e) {
					var t, n = !e[6] && e[2];
					return he.CHILD.test(e[0]) ? null : (e[3] ? e[2] = e[4] || e[5] || "" : n && pe.test(n) && (t = E(n, !0)) && (t = n.indexOf(")", n.length - t) - n.length) && (e[0] = e[0].slice(0, t), e[2] = n.slice(0, t)), e.slice(0, 3))
				}
			},
			filter: {
				TAG: function (e) {
					var t = e.replace(xe, m).toLowerCase();
					return "*" === e ? function () {
						return !0
					} : function (e) {
						return e.nodeName && e.nodeName.toLowerCase() === t
					}
				}, CLASS: function (e) {
					var t = U[e + " "];
					return t || (t = new RegExp("(^|" + ie + ")" + e + "(" + ie + "|$)")) && U(e, function (e) {
						return t.test("string" == typeof e.className && e.className || void 0 !== e.getAttribute && e.getAttribute("class") || "")
					})
				}, ATTR: function (n, i, o) {
					return function (e) {
						var t = x.attr(e, n);
						return null == t ? "!=" === i : !i || (t += "", "=" === i ? t === o : "!=" === i ? t !== o : "^=" === i ? o && 0 === t.indexOf(o) : "*=" === i ? o && -1 < t.indexOf(o) : "$=" === i ? o && t.slice(-o.length) === o : "~=" === i ? -1 < (" " + t.replace(ae, " ") + " ").indexOf(o) : "|=" === i && (t === o || t.slice(0, o.length + 1) === o + "-"))
					}
				}, CHILD: function (h, e, t, g, v) {
					var m = "nth" !== h.slice(0, 3), y = "last" !== h.slice(-4), b = "of-type" === e;
					return 1 === g && 0 === v ? function (e) {
						return !!e.parentNode
					} : function (e, t, n) {
						var i, o, r, s, a, l, c = m != y ? "nextSibling" : "previousSibling", u = e.parentNode,
							d = b && e.nodeName.toLowerCase(), p = !n && !b, f = !1;
						if (u) {
							if (m) {
								for (; c;) {
									for (s = e; s = s[c];) if (b ? s.nodeName.toLowerCase() === d : 1 === s.nodeType) return !1;
									l = c = "only" === h && !l && "nextSibling"
								}
								return !0
							}
							if (l = [y ? u.firstChild : u.lastChild], y && p) {
								for (f = (a = (i = (o = (r = (s = u)[z] || (s[z] = {}))[s.uniqueID] || (r[s.uniqueID] = {}))[h] || [])[0] === W && i[1]) && i[2], s = a && u.childNodes[a]; s = ++a && s && s[c] || (f = a = 0) || l.pop();) if (1 === s.nodeType && ++f && s === e) {
									o[h] = [W, a, f];
									break
								}
							} else if (p && (f = a = (i = (o = (r = (s = e)[z] || (s[z] = {}))[s.uniqueID] || (r[s.uniqueID] = {}))[h] || [])[0] === W && i[1]), !1 === f) for (; (s = ++a && s && s[c] || (f = a = 0) || l.pop()) && ((b ? s.nodeName.toLowerCase() !== d : 1 !== s.nodeType) || !++f || (p && ((o = (r = s[z] || (s[z] = {}))[s.uniqueID] || (r[s.uniqueID] = {}))[h] = [W, f]), s !== e));) ;
							return (f -= v) === g || f % g == 0 && 0 <= f / g
						}
					}
				}, PSEUDO: function (e, r) {
					var t, s = C.pseudos[e] || C.setFilters[e.toLowerCase()] || x.error("unsupported pseudo: " + e);
					return s[z] ? s(r) : 1 < s.length ? (t = [e, e, "", r], C.setFilters.hasOwnProperty(e.toLowerCase()) ? l(function (e, t) {
						for (var n, i = s(e, r), o = i.length; o--;) e[n = te(e, i[o])] = !(t[n] = i[o])
					}) : function (e) {
						return s(e, 0, t)
					}) : s
				}
			},
			pseudos: {
				not: l(function (e) {
					var i = [], o = [], a = A(e.replace(le, "$1"));
					return a[z] ? l(function (e, t, n, i) {
						for (var o, r = a(e, null, i, []), s = e.length; s--;) (o = r[s]) && (e[s] = !(t[s] = o))
					}) : function (e, t, n) {
						return i[0] = e, a(i, null, n, o), i[0] = null, !o.pop()
					}
				}), has: l(function (t) {
					return function (e) {
						return 0 < x(t, e).length
					}
				}), contains: l(function (t) {
					return t = t.replace(xe, m), function (e) {
						return -1 < (e.textContent || e.innerText || S(e)).indexOf(t)
					}
				}), lang: l(function (n) {
					return fe.test(n || "") || x.error("unsupported lang: " + n), n = n.replace(xe, m).toLowerCase(), function (e) {
						var t;
						do {
							if (t = I ? e.lang : e.getAttribute("xml:lang") || e.getAttribute("lang")) return (t = t.toLowerCase()) === n || 0 === t.indexOf(n + "-")
						} while ((e = e.parentNode) && 1 === e.nodeType);
						return !1
					}
				}), target: function (e) {
					var t = n.location && n.location.hash;
					return t && t.slice(1) === e.id
				}, root: function (e) {
					return e === H
				}, focus: function (e) {
					return e === q.activeElement && (!q.hasFocus || q.hasFocus()) && !!(e.type || e.href || ~e.tabIndex)
				}, enabled: s(!1), disabled: s(!0), checked: function (e) {
					var t = e.nodeName.toLowerCase();
					return "input" === t && !!e.checked || "option" === t && !!e.selected
				}, selected: function (e) {
					return e.parentNode && e.parentNode.selectedIndex, !0 === e.selected
				}, empty: function (e) {
					for (e = e.firstChild; e; e = e.nextSibling) if (e.nodeType < 6) return !1;
					return !0
				}, parent: function (e) {
					return !C.pseudos.empty(e)
				}, header: function (e) {
					return ve.test(e.nodeName)
				}, input: function (e) {
					return ge.test(e.nodeName)
				}, button: function (e) {
					var t = e.nodeName.toLowerCase();
					return "input" === t && "button" === e.type || "button" === t
				}, text: function (e) {
					var t;
					return "input" === e.nodeName.toLowerCase() && "text" === e.type && (null == (t = e.getAttribute("type")) || "text" === t.toLowerCase())
				}, first: a(function () {
					return [0]
				}), last: a(function (e, t) {
					return [t - 1]
				}), eq: a(function (e, t, n) {
					return [n < 0 ? n + t : n]
				}), even: a(function (e, t) {
					for (var n = 0; n < t; n += 2) e.push(n);
					return e
				}), odd: a(function (e, t) {
					for (var n = 1; n < t; n += 2) e.push(n);
					return e
				}), lt: a(function (e, t, n) {
					for (var i = n < 0 ? n + t : n; 0 <= --i;) e.push(i);
					return e
				}), gt: a(function (e, t, n) {
					for (var i = n < 0 ? n + t : n; ++i < t;) e.push(i);
					return e
				})
			}
		}).pseudos.nth = C.pseudos.eq, {
			radio: !0,
			checkbox: !0,
			file: !0,
			password: !0,
			image: !0
		}) C.pseudos[k] = i(k);
		for (k in {submit: !0, reset: !0}) C.pseudos[k] = r(k);
		return u.prototype = C.filters = C.pseudos, C.setFilters = new u, E = x.tokenize = function (e, t) {
			var n, i, o, r, s, a, l, c = X[e + " "];
			if (c) return t ? 0 : c.slice(0);
			for (s = e, a = [], l = C.preFilter; s;) {
				for (r in n && !(i = ce.exec(s)) || (i && (s = s.slice(i[0].length) || s), a.push(o = [])), n = !1, (i = ue.exec(s)) && (n = i.shift(), o.push({
					value: n,
					type: i[0].replace(le, " ")
				}), s = s.slice(n.length)), C.filter) !(i = he[r].exec(s)) || l[r] && !(i = l[r](i)) || (n = i.shift(), o.push({
					value: n,
					type: r,
					matches: i
				}), s = s.slice(n.length));
				if (!n) break
			}
			return t ? s.length : s ? x.error(e) : X(e, a).slice(0)
		}, A = x.compile = function (e, t) {
			var n, i = [], o = [], r = K[e + " "];
			if (!r) {
				for (n = (t = t || E(e)).length; n--;) (r = g(t[n]))[z] ? i.push(r) : o.push(r);
				(r = K(e, v(o, i))).selector = e
			}
			return r
		}, D = x.select = function (e, t, n, i) {
			var o, r, s, a, l, c = "function" == typeof e && e, u = !i && E(e = c.selector || e);
			if (n = n || [], 1 === u.length) {
				if (2 < (r = u[0] = u[0].slice(0)).length && "ID" === (s = r[0]).type && 9 === t.nodeType && I && C.relative[r[1].type]) {
					if (!(t = (C.find.ID(s.matches[0].replace(xe, m), t) || [])[0])) return n;
					c && (t = t.parentNode), e = e.slice(r.shift().value.length)
				}
				for (o = he.needsContext.test(e) ? 0 : r.length; o-- && (s = r[o], !C.relative[a = s.type]);) if ((l = C.find[a]) && (i = l(s.matches[0].replace(xe, m), be.test(r[0].type) && f(t.parentNode) || t))) {
					if (r.splice(o, 1), !(e = i.length && h(r))) return Z.apply(n, i), n;
					break
				}
			}
			return (c || A(e, u))(i, t, !I, n, !t || be.test(e) && f(t.parentNode) || t), n
		}, T.sortStable = z.split("").sort(V).join("") === z, T.detectDuplicates = !!L, O(), T.sortDetached = o(function (e) {
			return 1 & e.compareDocumentPosition(q.createElement("fieldset"))
		}), o(function (e) {
			return e.innerHTML = "<a href='#'></a>", "#" === e.firstChild.getAttribute("href")
		}) || t("type|href|height|width", function (e, t, n) {
			if (!n) return e.getAttribute(t, "type" === t.toLowerCase() ? 1 : 2)
		}), T.attributes && o(function (e) {
			return e.innerHTML = "<input/>", e.firstChild.setAttribute("value", ""), "" === e.firstChild.getAttribute("value")
		}) || t("value", function (e, t, n) {
			if (!n && "input" === e.nodeName.toLowerCase()) return e.defaultValue
		}), o(function (e) {
			return null == e.getAttribute("disabled")
		}) || t(ne, function (e, t, n) {
			var i;
			if (!n) return !0 === e[t] ? t.toLowerCase() : (i = e.getAttributeNode(t)) && i.specified ? i.value : null
		}), x
	}(T);
	se.find = ue, se.expr = ue.selectors, se.expr[":"] = se.expr.pseudos, se.uniqueSort = se.unique = ue.uniqueSort, se.text = ue.getText, se.isXMLDoc = ue.isXML, se.contains = ue.contains, se.escapeSelector = ue.escape;

	function de(e, t, n) {
		for (var i = [], o = void 0 !== n; (e = e[t]) && 9 !== e.nodeType;) if (1 === e.nodeType) {
			if (o && se(e).is(n)) break;
			i.push(e)
		}
		return i
	}

	function pe(e, t) {
		for (var n = []; e; e = e.nextSibling) 1 === e.nodeType && e !== t && n.push(e);
		return n
	}

	var fe = se.expr.match.needsContext, he = /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i,
		ge = /^.[^:#\[\.,]*$/;
	se.filter = function (e, t, n) {
		var i = t[0];
		return n && (e = ":not(" + e + ")"), 1 === t.length && 1 === i.nodeType ? se.find.matchesSelector(i, e) ? [i] : [] : se.find.matches(e, se.grep(t, function (e) {
			return 1 === e.nodeType
		}))
	}, se.fn.extend({
		find: function (e) {
			var t, n, i = this.length, o = this;
			if ("string" != typeof e) return this.pushStack(se(e).filter(function () {
				for (t = 0; t < i; t++) if (se.contains(o[t], this)) return !0
			}));
			for (n = this.pushStack([]), t = 0; t < i; t++) se.find(e, o[t], n);
			return 1 < i ? se.uniqueSort(n) : n
		}, filter: function (e) {
			return this.pushStack(t(this, e || [], !1))
		}, not: function (e) {
			return this.pushStack(t(this, e || [], !0))
		}, is: function (e) {
			return !!t(this, "string" == typeof e && fe.test(e) ? se(e) : e || [], !1).length
		}
	});
	var ve, me = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/;
	(se.fn.init = function (e, t, n) {
		var i, o;
		if (!e) return this;
		if (n = n || ve, "string" != typeof e) return e.nodeType ? (this[0] = e, this.length = 1, this) : se.isFunction(e) ? void 0 !== n.ready ? n.ready(e) : e(se) : se.makeArray(e, this);
		if (!(i = "<" === e[0] && ">" === e[e.length - 1] && 3 <= e.length ? [null, e, null] : me.exec(e)) || !i[1] && t) return !t || t.jquery ? (t || n).find(e) : this.constructor(t).find(e);
		if (i[1]) {
			if (t = t instanceof se ? t[0] : t, se.merge(this, se.parseHTML(i[1], t && t.nodeType ? t.ownerDocument || t : V, !0)), he.test(i[1]) && se.isPlainObject(t)) for (i in t) se.isFunction(this[i]) ? this[i](t[i]) : this.attr(i, t[i]);
			return this
		}
		return (o = V.getElementById(i[2])) && (this[0] = o, this.length = 1), this
	}).prototype = se.fn, ve = se(V);
	var ye = /^(?:parents|prev(?:Until|All))/, be = {children: !0, contents: !0, next: !0, prev: !0};
	se.fn.extend({
		has: function (e) {
			var t = se(e, this), n = t.length;
			return this.filter(function () {
				for (var e = 0; e < n; e++) if (se.contains(this, t[e])) return !0
			})
		}, closest: function (e, t) {
			var n, i = 0, o = this.length, r = [], s = "string" != typeof e && se(e);
			if (!fe.test(e)) for (; i < o; i++) for (n = this[i]; n && n !== t; n = n.parentNode) if (n.nodeType < 11 && (s ? -1 < s.index(n) : 1 === n.nodeType && se.find.matchesSelector(n, e))) {
				r.push(n);
				break
			}
			return this.pushStack(1 < r.length ? se.uniqueSort(r) : r)
		}, index: function (e) {
			return e ? "string" == typeof e ? Z.call(se(e), this[0]) : Z.call(this, e.jquery ? e[0] : e) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
		}, add: function (e, t) {
			return this.pushStack(se.uniqueSort(se.merge(this.get(), se(e, t))))
		}, addBack: function (e) {
			return this.add(null == e ? this.prevObject : this.prevObject.filter(e))
		}
	}), se.each({
		parent: function (e) {
			var t = e.parentNode;
			return t && 11 !== t.nodeType ? t : null
		}, parents: function (e) {
			return de(e, "parentNode")
		}, parentsUntil: function (e, t, n) {
			return de(e, "parentNode", n)
		}, next: function (e) {
			return n(e, "nextSibling")
		}, prev: function (e) {
			return n(e, "previousSibling")
		}, nextAll: function (e) {
			return de(e, "nextSibling")
		}, prevAll: function (e) {
			return de(e, "previousSibling")
		}, nextUntil: function (e, t, n) {
			return de(e, "nextSibling", n)
		}, prevUntil: function (e, t, n) {
			return de(e, "previousSibling", n)
		}, siblings: function (e) {
			return pe((e.parentNode || {}).firstChild, e)
		}, children: function (e) {
			return pe(e.firstChild)
		}, contents: function (e) {
			return c(e, "iframe") ? e.contentDocument : (c(e, "template") && (e = e.content || e), se.merge([], e.childNodes))
		}
	}, function (i, o) {
		se.fn[i] = function (e, t) {
			var n = se.map(this, o, e);
			return "Until" !== i.slice(-5) && (t = e), t && "string" == typeof t && (n = se.filter(t, n)), 1 < this.length && (be[i] || se.uniqueSort(n), ye.test(i) && n.reverse()), this.pushStack(n)
		}
	});
	var xe = /[^\x20\t\r\n\f]+/g;
	se.Callbacks = function (i) {
		i = "string" == typeof i ? function (e) {
			var n = {};
			return se.each(e.match(xe) || [], function (e, t) {
				n[t] = !0
			}), n
		}(i) : se.extend({}, i);

		function n() {
			for (r = r || i.once, t = o = !0; a.length; l = -1) for (e = a.shift(); ++l < s.length;) !1 === s[l].apply(e[0], e[1]) && i.stopOnFalse && (l = s.length, e = !1);
			i.memory || (e = !1), o = !1, r && (s = e ? [] : "")
		}

		var o, e, t, r, s = [], a = [], l = -1, c = {
			add: function () {
				return s && (e && !o && (l = s.length - 1, a.push(e)), function n(e) {
					se.each(e, function (e, t) {
						se.isFunction(t) ? i.unique && c.has(t) || s.push(t) : t && t.length && "string" !== se.type(t) && n(t)
					})
				}(arguments), e && !o && n()), this
			}, remove: function () {
				return se.each(arguments, function (e, t) {
					for (var n; -1 < (n = se.inArray(t, s, n));) s.splice(n, 1), n <= l && l--
				}), this
			}, has: function (e) {
				return e ? -1 < se.inArray(e, s) : 0 < s.length
			}, empty: function () {
				return s = s && [], this
			}, disable: function () {
				return r = a = [], s = e = "", this
			}, disabled: function () {
				return !s
			}, lock: function () {
				return r = a = [], e || o || (s = e = ""), this
			}, locked: function () {
				return !!r
			}, fireWith: function (e, t) {
				return r || (t = [e, (t = t || []).slice ? t.slice() : t], a.push(t), o || n()), this
			}, fire: function () {
				return c.fireWith(this, arguments), this
			}, fired: function () {
				return !!t
			}
		};
		return c
	}, se.extend({
		Deferred: function (e) {
			var r = [["notify", "progress", se.Callbacks("memory"), se.Callbacks("memory"), 2], ["resolve", "done", se.Callbacks("once memory"), se.Callbacks("once memory"), 0, "resolved"], ["reject", "fail", se.Callbacks("once memory"), se.Callbacks("once memory"), 1, "rejected"]],
				o = "pending", s = {
					state: function () {
						return o
					}, always: function () {
						return a.done(arguments).fail(arguments), this
					}, catch: function (e) {
						return s.then(null, e)
					}, pipe: function () {
						var o = arguments;
						return se.Deferred(function (i) {
							se.each(r, function (e, t) {
								var n = se.isFunction(o[t[4]]) && o[t[4]];
								a[t[1]](function () {
									var e = n && n.apply(this, arguments);
									e && se.isFunction(e.promise) ? e.promise().progress(i.notify).done(i.resolve).fail(i.reject) : i[t[0] + "With"](this, n ? [e] : arguments)
								})
							}), o = null
						}).promise()
					}, then: function (t, n, i) {
						function l(o, r, s, a) {
							return function () {
								function e() {
									var e, t;
									if (!(o < c)) {
										if ((e = s.apply(n, i)) === r.promise()) throw new TypeError("Thenable self-resolution");
										t = e && ("object" == typeof e || "function" == typeof e) && e.then, se.isFunction(t) ? a ? t.call(e, l(c, r, u, a), l(c, r, d, a)) : (c++, t.call(e, l(c, r, u, a), l(c, r, d, a), l(c, r, u, r.notifyWith))) : (s !== u && (n = void 0, i = [e]), (a || r.resolveWith)(n, i))
									}
								}

								var n = this, i = arguments, t = a ? e : function () {
									try {
										e()
									} catch (e) {
										se.Deferred.exceptionHook && se.Deferred.exceptionHook(e, t.stackTrace), c <= o + 1 && (s !== d && (n = void 0, i = [e]), r.rejectWith(n, i))
									}
								};
								o ? t() : (se.Deferred.getStackHook && (t.stackTrace = se.Deferred.getStackHook()), T.setTimeout(t))
							}
						}

						var c = 0;
						return se.Deferred(function (e) {
							r[0][3].add(l(0, e, se.isFunction(i) ? i : u, e.notifyWith)), r[1][3].add(l(0, e, se.isFunction(t) ? t : u)), r[2][3].add(l(0, e, se.isFunction(n) ? n : d))
						}).promise()
					}, promise: function (e) {
						return null != e ? se.extend(e, s) : s
					}
				}, a = {};
			return se.each(r, function (e, t) {
				var n = t[2], i = t[5];
				s[t[1]] = n.add, i && n.add(function () {
					o = i
				}, r[3 - e][2].disable, r[0][2].lock), n.add(t[3].fire), a[t[0]] = function () {
					return a[t[0] + "With"](this === a ? void 0 : this, arguments), this
				}, a[t[0] + "With"] = n.fireWith
			}), s.promise(a), e && e.call(a, a), a
		}, when: function (e) {
			function t(t) {
				return function (e) {
					o[t] = this, r[t] = 1 < arguments.length ? G.call(arguments) : e, --n || s.resolveWith(o, r)
				}
			}

			var n = arguments.length, i = n, o = Array(i), r = G.call(arguments), s = se.Deferred();
			if (n <= 1 && (l(e, s.done(t(i)).resolve, s.reject, !n), "pending" === s.state() || se.isFunction(r[i] && r[i].then))) return s.then();
			for (; i--;) l(r[i], t(i), s.reject);
			return s.promise()
		}
	});
	var we = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;
	se.Deferred.exceptionHook = function (e, t) {
		T.console && T.console.warn && e && we.test(e.name) && T.console.warn("jQuery.Deferred exception: " + e.message, e.stack, t)
	}, se.readyException = function (e) {
		T.setTimeout(function () {
			throw e
		})
	};
	var ke = se.Deferred();
	se.fn.ready = function (e) {
		return ke.then(e).catch(function (e) {
			se.readyException(e)
		}), this
	}, se.extend({
		isReady: !1, readyWait: 1, ready: function (e) {
			(!0 === e ? --se.readyWait : se.isReady) || ((se.isReady = !0) !== e && 0 < --se.readyWait || ke.resolveWith(V, [se]))
		}
	}), se.ready.then = ke.then, "complete" === V.readyState || "loading" !== V.readyState && !V.documentElement.doScroll ? T.setTimeout(se.ready) : (V.addEventListener("DOMContentLoaded", i), T.addEventListener("load", i));

	function Te(e) {
		return 1 === e.nodeType || 9 === e.nodeType || !+e.nodeType
	}

	var Ce = function (e, t, n, i, o, r, s) {
		var a = 0, l = e.length, c = null == n;
		if ("object" === se.type(n)) for (a in o = !0, n) Ce(e, t, a, n[a], !0, r, s); else if (void 0 !== i && (o = !0, se.isFunction(i) || (s = !0), c && (t = s ? (t.call(e, i), null) : (c = t, function (e, t, n) {
			return c.call(se(e), n)
		})), t)) for (; a < l; a++) t(e[a], n, s ? i : i.call(e[a], a, t(e[a], n)));
		return o ? e : c ? t.call(e) : l ? t(e[0], n) : r
	};
	o.uid = 1, o.prototype = {
		cache: function (e) {
			var t = e[this.expando];
			return t || (t = {}, Te(e) && (e.nodeType ? e[this.expando] = t : Object.defineProperty(e, this.expando, {
				value: t,
				configurable: !0
			}))), t
		}, set: function (e, t, n) {
			var i, o = this.cache(e);
			if ("string" == typeof t) o[se.camelCase(t)] = n; else for (i in t) o[se.camelCase(i)] = t[i];
			return o
		}, get: function (e, t) {
			return void 0 === t ? this.cache(e) : e[this.expando] && e[this.expando][se.camelCase(t)]
		}, access: function (e, t, n) {
			return void 0 === t || t && "string" == typeof t && void 0 === n ? this.get(e, t) : (this.set(e, t, n), void 0 !== n ? n : t)
		}, remove: function (e, t) {
			var n, i = e[this.expando];
			if (void 0 !== i) {
				if (void 0 !== t) {
					n = (t = Array.isArray(t) ? t.map(se.camelCase) : (t = se.camelCase(t)) in i ? [t] : t.match(xe) || []).length;
					for (; n--;) delete i[t[n]]
				}
				void 0 !== t && !se.isEmptyObject(i) || (e.nodeType ? e[this.expando] = void 0 : delete e[this.expando])
			}
		}, hasData: function (e) {
			var t = e[this.expando];
			return void 0 !== t && !se.isEmptyObject(t)
		}
	};
	var Se = new o, $e = new o, Ee = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/, Ae = /[A-Z]/g;
	se.extend({
		hasData: function (e) {
			return $e.hasData(e) || Se.hasData(e)
		}, data: function (e, t, n) {
			return $e.access(e, t, n)
		}, removeData: function (e, t) {
			$e.remove(e, t)
		}, _data: function (e, t, n) {
			return Se.access(e, t, n)
		}, _removeData: function (e, t) {
			Se.remove(e, t)
		}
	}), se.fn.extend({
		data: function (n, e) {
			var t, i, o, r = this[0], s = r && r.attributes;
			if (void 0 !== n) return "object" == typeof n ? this.each(function () {
				$e.set(this, n)
			}) : Ce(this, function (e) {
				var t;
				if (r && void 0 === e) {
					if (void 0 !== (t = $e.get(r, n))) return t;
					if (void 0 !== (t = p(r, n))) return t
				} else this.each(function () {
					$e.set(this, n, e)
				})
			}, null, e, 1 < arguments.length, null, !0);
			if (this.length && (o = $e.get(r), 1 === r.nodeType && !Se.get(r, "hasDataAttrs"))) {
				for (t = s.length; t--;) s[t] && (0 === (i = s[t].name).indexOf("data-") && (i = se.camelCase(i.slice(5)), p(r, i, o[i])));
				Se.set(r, "hasDataAttrs", !0)
			}
			return o
		}, removeData: function (e) {
			return this.each(function () {
				$e.remove(this, e)
			})
		}
	}), se.extend({
		queue: function (e, t, n) {
			var i;
			if (e) return t = (t || "fx") + "queue", i = Se.get(e, t), n && (!i || Array.isArray(n) ? i = Se.access(e, t, se.makeArray(n)) : i.push(n)), i || []
		}, dequeue: function (e, t) {
			t = t || "fx";
			var n = se.queue(e, t), i = n.length, o = n.shift(), r = se._queueHooks(e, t);
			"inprogress" === o && (o = n.shift(), i--), o && ("fx" === t && n.unshift("inprogress"), delete r.stop, o.call(e, function () {
				se.dequeue(e, t)
			}, r)), !i && r && r.empty.fire()
		}, _queueHooks: function (e, t) {
			var n = t + "queueHooks";
			return Se.get(e, n) || Se.access(e, n, {
				empty: se.Callbacks("once memory").add(function () {
					Se.remove(e, [t + "queue", n])
				})
			})
		}
	}), se.fn.extend({
		queue: function (t, n) {
			var e = 2;
			return "string" != typeof t && (n = t, t = "fx", e--), arguments.length < e ? se.queue(this[0], t) : void 0 === n ? this : this.each(function () {
				var e = se.queue(this, t, n);
				se._queueHooks(this, t), "fx" === t && "inprogress" !== e[0] && se.dequeue(this, t)
			})
		}, dequeue: function (e) {
			return this.each(function () {
				se.dequeue(this, e)
			})
		}, clearQueue: function (e) {
			return this.queue(e || "fx", [])
		}, promise: function (e, t) {
			function n() {
				--o || r.resolveWith(s, [s])
			}

			var i, o = 1, r = se.Deferred(), s = this, a = this.length;
			for ("string" != typeof e && (t = e, e = void 0), e = e || "fx"; a--;) (i = Se.get(s[a], e + "queueHooks")) && i.empty && (o++, i.empty.add(n));
			return n(), r.promise(t)
		}
	});

	function De(e, t, n, i) {
		var o, r, s = {};
		for (r in t) s[r] = e.style[r], e.style[r] = t[r];
		for (r in o = n.apply(e, i || []), t) e.style[r] = s[r];
		return o
	}

	var je = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source, Ne = new RegExp("^(?:([+-])=|)(" + je + ")([a-z%]*)$", "i"),
		Le = ["Top", "Right", "Bottom", "Left"], Oe = function (e, t) {
			return "none" === (e = t || e).style.display || "" === e.style.display && se.contains(e.ownerDocument, e) && "none" === se.css(e, "display")
		}, qe = {};
	se.fn.extend({
		show: function () {
			return m(this, !0)
		}, hide: function () {
			return m(this)
		}, toggle: function (e) {
			return "boolean" == typeof e ? e ? this.show() : this.hide() : this.each(function () {
				Oe(this) ? se(this).show() : se(this).hide()
			})
		}
	});
	var He = /^(?:checkbox|radio)$/i, Ie = /<([a-z][^\/\0>\x20\t\r\n\f]+)/i, Pe = /^$|\/(?:java|ecma)script/i, Fe = {
		option: [1, "<select multiple='multiple'>", "</select>"],
		thead: [1, "<table>", "</table>"],
		col: [2, "<table><colgroup>", "</colgroup></table>"],
		tr: [2, "<table><tbody>", "</tbody></table>"],
		td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
		_default: [0, "", ""]
	};
	Fe.optgroup = Fe.option, Fe.tbody = Fe.tfoot = Fe.colgroup = Fe.caption = Fe.thead, Fe.th = Fe.td;
	var _e, Me, ze = /<|&#?\w+;/;
	_e = V.createDocumentFragment().appendChild(V.createElement("div")), (Me = V.createElement("input")).setAttribute("type", "radio"), Me.setAttribute("checked", "checked"), Me.setAttribute("name", "t"), _e.appendChild(Me), re.checkClone = _e.cloneNode(!0).cloneNode(!0).lastChild.checked, _e.innerHTML = "<textarea>x</textarea>", re.noCloneChecked = !!_e.cloneNode(!0).lastChild.defaultValue;
	var Re = V.documentElement, We = /^key/, Be = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
		Ue = /^([^.]*)(?:\.(.+)|)/;
	se.event = {
		global: {}, add: function (t, e, n, i, o) {
			var r, s, a, l, c, u, d, p, f, h, g, v = Se.get(t);
			if (v) for (n.handler && (n = (r = n).handler, o = r.selector), o && se.find.matchesSelector(Re, o), n.guid || (n.guid = se.guid++), (l = v.events) || (l = v.events = {}), (s = v.handle) || (s = v.handle = function (e) {
				return void 0 !== se && se.event.triggered !== e.type ? se.event.dispatch.apply(t, arguments) : void 0
			}), c = (e = (e || "").match(xe) || [""]).length; c--;) f = g = (a = Ue.exec(e[c]) || [])[1], h = (a[2] || "").split(".").sort(), f && (d = se.event.special[f] || {}, f = (o ? d.delegateType : d.bindType) || f, d = se.event.special[f] || {}, u = se.extend({
				type: f,
				origType: g,
				data: i,
				handler: n,
				guid: n.guid,
				selector: o,
				needsContext: o && se.expr.match.needsContext.test(o),
				namespace: h.join(".")
			}, r), (p = l[f]) || ((p = l[f] = []).delegateCount = 0, d.setup && !1 !== d.setup.call(t, i, h, s) || t.addEventListener && t.addEventListener(f, s)), d.add && (d.add.call(t, u), u.handler.guid || (u.handler.guid = n.guid)), o ? p.splice(p.delegateCount++, 0, u) : p.push(u), se.event.global[f] = !0)
		}, remove: function (e, t, n, i, o) {
			var r, s, a, l, c, u, d, p, f, h, g, v = Se.hasData(e) && Se.get(e);
			if (v && (l = v.events)) {
				for (c = (t = (t || "").match(xe) || [""]).length; c--;) if (f = g = (a = Ue.exec(t[c]) || [])[1], h = (a[2] || "").split(".").sort(), f) {
					for (d = se.event.special[f] || {}, p = l[f = (i ? d.delegateType : d.bindType) || f] || [], a = a[2] && new RegExp("(^|\\.)" + h.join("\\.(?:.*\\.|)") + "(\\.|$)"), s = r = p.length; r--;) u = p[r], !o && g !== u.origType || n && n.guid !== u.guid || a && !a.test(u.namespace) || i && i !== u.selector && ("**" !== i || !u.selector) || (p.splice(r, 1), u.selector && p.delegateCount--, d.remove && d.remove.call(e, u));
					s && !p.length && (d.teardown && !1 !== d.teardown.call(e, h, v.handle) || se.removeEvent(e, f, v.handle), delete l[f])
				} else for (f in l) se.event.remove(e, f + t[c], n, i, !0);
				se.isEmptyObject(l) && Se.remove(e, "handle events")
			}
		}, dispatch: function (e) {
			var t, n, i, o, r, s, a = se.event.fix(e), l = new Array(arguments.length),
				c = (Se.get(this, "events") || {})[a.type] || [], u = se.event.special[a.type] || {};
			for (l[0] = a, t = 1; t < arguments.length; t++) l[t] = arguments[t];
			if (a.delegateTarget = this, !u.preDispatch || !1 !== u.preDispatch.call(this, a)) {
				for (s = se.event.handlers.call(this, a, c), t = 0; (o = s[t++]) && !a.isPropagationStopped();) for (a.currentTarget = o.elem, n = 0; (r = o.handlers[n++]) && !a.isImmediatePropagationStopped();) a.rnamespace && !a.rnamespace.test(r.namespace) || (a.handleObj = r, a.data = r.data, void 0 !== (i = ((se.event.special[r.origType] || {}).handle || r.handler).apply(o.elem, l)) && !1 === (a.result = i) && (a.preventDefault(), a.stopPropagation()));
				return u.postDispatch && u.postDispatch.call(this, a), a.result
			}
		}, handlers: function (e, t) {
			var n, i, o, r, s, a = [], l = t.delegateCount, c = e.target;
			if (l && c.nodeType && !("click" === e.type && 1 <= e.button)) for (; c !== this; c = c.parentNode || this) if (1 === c.nodeType && ("click" !== e.type || !0 !== c.disabled)) {
				for (r = [], s = {}, n = 0; n < l; n++) void 0 === s[o = (i = t[n]).selector + " "] && (s[o] = i.needsContext ? -1 < se(o, this).index(c) : se.find(o, this, null, [c]).length), s[o] && r.push(i);
				r.length && a.push({elem: c, handlers: r})
			}
			return c = this, l < t.length && a.push({elem: c, handlers: t.slice(l)}), a
		}, addProp: function (t, e) {
			Object.defineProperty(se.Event.prototype, t, {
				enumerable: !0,
				configurable: !0,
				get: se.isFunction(e) ? function () {
					if (this.originalEvent) return e(this.originalEvent)
				} : function () {
					if (this.originalEvent) return this.originalEvent[t]
				},
				set: function (e) {
					Object.defineProperty(this, t, {enumerable: !0, configurable: !0, writable: !0, value: e})
				}
			})
		}, fix: function (e) {
			return e[se.expando] ? e : new se.Event(e)
		}, special: {
			load: {noBubble: !0}, focus: {
				trigger: function () {
					if (this !== s() && this.focus) return this.focus(), !1
				}, delegateType: "focusin"
			}, blur: {
				trigger: function () {
					if (this === s() && this.blur) return this.blur(), !1
				}, delegateType: "focusout"
			}, click: {
				trigger: function () {
					if ("checkbox" === this.type && this.click && c(this, "input")) return this.click(), !1
				}, _default: function (e) {
					return c(e.target, "a")
				}
			}, beforeunload: {
				postDispatch: function (e) {
					void 0 !== e.result && e.originalEvent && (e.originalEvent.returnValue = e.result)
				}
			}
		}
	}, se.removeEvent = function (e, t, n) {
		e.removeEventListener && e.removeEventListener(t, n)
	}, se.Event = function (e, t) {
		if (!(this instanceof se.Event)) return new se.Event(e, t);
		e && e.type ? (this.originalEvent = e, this.type = e.type, this.isDefaultPrevented = e.defaultPrevented || void 0 === e.defaultPrevented && !1 === e.returnValue ? r : h, this.target = e.target && 3 === e.target.nodeType ? e.target.parentNode : e.target, this.currentTarget = e.currentTarget, this.relatedTarget = e.relatedTarget) : this.type = e, t && se.extend(this, t), this.timeStamp = e && e.timeStamp || se.now(), this[se.expando] = !0
	}, se.Event.prototype = {
		constructor: se.Event,
		isDefaultPrevented: h,
		isPropagationStopped: h,
		isImmediatePropagationStopped: h,
		isSimulated: !1,
		preventDefault: function () {
			var e = this.originalEvent;
			this.isDefaultPrevented = r, e && !this.isSimulated && e.preventDefault()
		},
		stopPropagation: function () {
			var e = this.originalEvent;
			this.isPropagationStopped = r, e && !this.isSimulated && e.stopPropagation()
		},
		stopImmediatePropagation: function () {
			var e = this.originalEvent;
			this.isImmediatePropagationStopped = r, e && !this.isSimulated && e.stopImmediatePropagation(), this.stopPropagation()
		}
	}, se.each({
		altKey: !0,
		bubbles: !0,
		cancelable: !0,
		changedTouches: !0,
		ctrlKey: !0,
		detail: !0,
		eventPhase: !0,
		metaKey: !0,
		pageX: !0,
		pageY: !0,
		shiftKey: !0,
		view: !0,
		char: !0,
		charCode: !0,
		key: !0,
		keyCode: !0,
		button: !0,
		buttons: !0,
		clientX: !0,
		clientY: !0,
		offsetX: !0,
		offsetY: !0,
		pointerId: !0,
		pointerType: !0,
		screenX: !0,
		screenY: !0,
		targetTouches: !0,
		toElement: !0,
		touches: !0,
		which: function (e) {
			var t = e.button;
			return null == e.which && We.test(e.type) ? null != e.charCode ? e.charCode : e.keyCode : !e.which && void 0 !== t && Be.test(e.type) ? 1 & t ? 1 : 2 & t ? 3 : 4 & t ? 2 : 0 : e.which
		}
	}, se.event.addProp), se.each({
		mouseenter: "mouseover",
		mouseleave: "mouseout",
		pointerenter: "pointerover",
		pointerleave: "pointerout"
	}, function (e, o) {
		se.event.special[e] = {
			delegateType: o, bindType: o, handle: function (e) {
				var t, n = e.relatedTarget, i = e.handleObj;
				return n && (n === this || se.contains(this, n)) || (e.type = i.origType, t = i.handler.apply(this, arguments), e.type = o), t
			}
		}
	}), se.fn.extend({
		on: function (e, t, n, i) {
			return x(this, e, t, n, i)
		}, one: function (e, t, n, i) {
			return x(this, e, t, n, i, 1)
		}, off: function (e, t, n) {
			var i, o;
			if (e && e.preventDefault && e.handleObj) return i = e.handleObj, se(e.delegateTarget).off(i.namespace ? i.origType + "." + i.namespace : i.origType, i.selector, i.handler), this;
			if ("object" != typeof e) return !1 !== t && "function" != typeof t || (n = t, t = void 0), !1 === n && (n = h), this.each(function () {
				se.event.remove(this, e, n, t)
			});
			for (o in e) this.off(o, t, e[o]);
			return this
		}
	});
	var Xe = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,
		Ke = /<script|<style|<link/i, Ve = /checked\s*(?:[^=]|=\s*.checked.)/i, Ye = /^true\/(.*)/,
		Ge = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;
	se.extend({
		htmlPrefilter: function (e) {
			return e.replace(Xe, "<$1></$2>")
		}, clone: function (e, t, n) {
			var i, o, r, s, a, l, c, u = e.cloneNode(!0), d = se.contains(e.ownerDocument, e);
			if (!(re.noCloneChecked || 1 !== e.nodeType && 11 !== e.nodeType || se.isXMLDoc(e))) for (s = v(u), i = 0, o = (r = v(e)).length; i < o; i++) a = r[i], l = s[i], void 0, "input" === (c = l.nodeName.toLowerCase()) && He.test(a.type) ? l.checked = a.checked : "input" !== c && "textarea" !== c || (l.defaultValue = a.defaultValue);
			if (t) if (n) for (r = r || v(e), s = s || v(u), i = 0, o = r.length; i < o; i++) S(r[i], s[i]); else S(e, u);
			return 0 < (s = v(u, "script")).length && y(s, !d && v(e, "script")), u
		}, cleanData: function (e) {
			for (var t, n, i, o = se.event.special, r = 0; void 0 !== (n = e[r]); r++) if (Te(n)) {
				if (t = n[Se.expando]) {
					if (t.events) for (i in t.events) o[i] ? se.event.remove(n, i) : se.removeEvent(n, i, t.handle);
					n[Se.expando] = void 0
				}
				n[$e.expando] && (n[$e.expando] = void 0)
			}
		}
	}), se.fn.extend({
		detach: function (e) {
			return E(this, e, !0)
		}, remove: function (e) {
			return E(this, e)
		}, text: function (e) {
			return Ce(this, function (e) {
				return void 0 === e ? se.text(this) : this.empty().each(function () {
					1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || (this.textContent = e)
				})
			}, null, e, arguments.length)
		}, append: function () {
			return $(this, arguments, function (e) {
				1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || w(this, e).appendChild(e)
			})
		}, prepend: function () {
			return $(this, arguments, function (e) {
				if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
					var t = w(this, e);
					t.insertBefore(e, t.firstChild)
				}
			})
		}, before: function () {
			return $(this, arguments, function (e) {
				this.parentNode && this.parentNode.insertBefore(e, this)
			})
		}, after: function () {
			return $(this, arguments, function (e) {
				this.parentNode && this.parentNode.insertBefore(e, this.nextSibling)
			})
		}, empty: function () {
			for (var e, t = 0; null != (e = this[t]); t++) 1 === e.nodeType && (se.cleanData(v(e, !1)), e.textContent = "");
			return this
		}, clone: function (e, t) {
			return e = null != e && e, t = null == t ? e : t, this.map(function () {
				return se.clone(this, e, t)
			})
		}, html: function (e) {
			return Ce(this, function (e) {
				var t = this[0] || {}, n = 0, i = this.length;
				if (void 0 === e && 1 === t.nodeType) return t.innerHTML;
				if ("string" == typeof e && !Ke.test(e) && !Fe[(Ie.exec(e) || ["", ""])[1].toLowerCase()]) {
					e = se.htmlPrefilter(e);
					try {
						for (; n < i; n++) 1 === (t = this[n] || {}).nodeType && (se.cleanData(v(t, !1)), t.innerHTML = e);
						t = 0
					} catch (e) {
					}
				}
				t && this.empty().append(e)
			}, null, e, arguments.length)
		}, replaceWith: function () {
			var n = [];
			return $(this, arguments, function (e) {
				var t = this.parentNode;
				se.inArray(this, n) < 0 && (se.cleanData(v(this)), t && t.replaceChild(e, this))
			}, n)
		}
	}), se.each({
		appendTo: "append",
		prependTo: "prepend",
		insertBefore: "before",
		insertAfter: "after",
		replaceAll: "replaceWith"
	}, function (e, s) {
		se.fn[e] = function (e) {
			for (var t, n = [], i = se(e), o = i.length - 1, r = 0; r <= o; r++) t = r === o ? this : this.clone(!0), se(i[r])[s](t), Q.apply(n, t.get());
			return this.pushStack(n)
		}
	});
	var Je, Qe, Ze, et, tt, nt, it = /^margin/, ot = new RegExp("^(" + je + ")(?!px)[a-z%]+$", "i"), rt = function (e) {
		var t = e.ownerDocument.defaultView;
		return t && t.opener || (t = T), t.getComputedStyle(e)
	};

	function st() {
		if (nt) {
			nt.style.cssText = "box-sizing:border-box;position:relative;display:block;margin:auto;border:1px;padding:1px;top:1%;width:50%", nt.innerHTML = "", Re.appendChild(tt);
			var e = T.getComputedStyle(nt);
			Je = "1%" !== e.top, et = "2px" === e.marginLeft, Qe = "4px" === e.width, nt.style.marginRight = "50%", Ze = "4px" === e.marginRight, Re.removeChild(tt), nt = null
		}
	}

	tt = V.createElement("div"), (nt = V.createElement("div")).style && (nt.style.backgroundClip = "content-box", nt.cloneNode(!0).style.backgroundClip = "", re.clearCloneStyle = "content-box" === nt.style.backgroundClip, tt.style.cssText = "border:0;width:8px;height:0;top:0;left:-9999px;padding:0;margin-top:1px;position:absolute", tt.appendChild(nt), se.extend(re, {
		pixelPosition: function () {
			return st(), Je
		}, boxSizingReliable: function () {
			return st(), Qe
		}, pixelMarginRight: function () {
			return st(), Ze
		}, reliableMarginLeft: function () {
			return st(), et
		}
	}));
	var at = /^(none|table(?!-c[ea]).+)/, lt = /^--/,
		ct = {position: "absolute", visibility: "hidden", display: "block"},
		ut = {letterSpacing: "0", fontWeight: "400"}, dt = ["Webkit", "Moz", "ms"], pt = V.createElement("div").style;
	se.extend({
		cssHooks: {
			opacity: {
				get: function (e, t) {
					if (t) {
						var n = A(e, "opacity");
						return "" === n ? "1" : n
					}
				}
			}
		},
		cssNumber: {
			animationIterationCount: !0,
			columnCount: !0,
			fillOpacity: !0,
			flexGrow: !0,
			flexShrink: !0,
			fontWeight: !0,
			lineHeight: !0,
			opacity: !0,
			order: !0,
			orphans: !0,
			widows: !0,
			zIndex: !0,
			zoom: !0
		},
		cssProps: {float: "cssFloat"},
		style: function (e, t, n, i) {
			if (e && 3 !== e.nodeType && 8 !== e.nodeType && e.style) {
				var o, r, s, a = se.camelCase(t), l = lt.test(t), c = e.style;
				if (l || (t = j(a)), s = se.cssHooks[t] || se.cssHooks[a], void 0 === n) return s && "get" in s && void 0 !== (o = s.get(e, !1, i)) ? o : c[t];
				"string" === (r = typeof n) && (o = Ne.exec(n)) && o[1] && (n = f(e, t, o), r = "number"), null != n && n == n && ("number" === r && (n += o && o[3] || (se.cssNumber[a] ? "" : "px")), re.clearCloneStyle || "" !== n || 0 !== t.indexOf("background") || (c[t] = "inherit"), s && "set" in s && void 0 === (n = s.set(e, n, i)) || (l ? c.setProperty(t, n) : c[t] = n))
			}
		},
		css: function (e, t, n, i) {
			var o, r, s, a = se.camelCase(t);
			return lt.test(t) || (t = j(a)), (s = se.cssHooks[t] || se.cssHooks[a]) && "get" in s && (o = s.get(e, !0, n)), void 0 === o && (o = A(e, t, i)), "normal" === o && t in ut && (o = ut[t]), "" === n || n ? (r = parseFloat(o), !0 === n || isFinite(r) ? r || 0 : o) : o
		}
	}), se.each(["height", "width"], function (e, s) {
		se.cssHooks[s] = {
			get: function (e, t, n) {
				if (t) return !at.test(se.css(e, "display")) || e.getClientRects().length && e.getBoundingClientRect().width ? O(e, s, n) : De(e, ct, function () {
					return O(e, s, n)
				})
			}, set: function (e, t, n) {
				var i, o = n && rt(e), r = n && L(e, s, n, "border-box" === se.css(e, "boxSizing", !1, o), o);
				return r && (i = Ne.exec(t)) && "px" !== (i[3] || "px") && (e.style[s] = t, t = se.css(e, s)), N(0, t, r)
			}
		}
	}), se.cssHooks.marginLeft = D(re.reliableMarginLeft, function (e, t) {
		if (t) return (parseFloat(A(e, "marginLeft")) || e.getBoundingClientRect().left - De(e, {marginLeft: 0}, function () {
			return e.getBoundingClientRect().left
		})) + "px"
	}), se.each({margin: "", padding: "", border: "Width"}, function (o, r) {
		se.cssHooks[o + r] = {
			expand: function (e) {
				for (var t = 0, n = {}, i = "string" == typeof e ? e.split(" ") : [e]; t < 4; t++) n[o + Le[t] + r] = i[t] || i[t - 2] || i[0];
				return n
			}
		}, it.test(o) || (se.cssHooks[o + r].set = N)
	}), se.fn.extend({
		css: function (e, t) {
			return Ce(this, function (e, t, n) {
				var i, o, r = {}, s = 0;
				if (Array.isArray(t)) {
					for (i = rt(e), o = t.length; s < o; s++) r[t[s]] = se.css(e, t[s], !1, i);
					return r
				}
				return void 0 !== n ? se.style(e, t, n) : se.css(e, t)
			}, e, t, 1 < arguments.length)
		}
	}), ((se.Tween = q).prototype = {
		constructor: q, init: function (e, t, n, i, o, r) {
			this.elem = e, this.prop = n, this.easing = o || se.easing._default, this.options = t, this.start = this.now = this.cur(), this.end = i, this.unit = r || (se.cssNumber[n] ? "" : "px")
		}, cur: function () {
			var e = q.propHooks[this.prop];
			return e && e.get ? e.get(this) : q.propHooks._default.get(this)
		}, run: function (e) {
			var t, n = q.propHooks[this.prop];
			return this.options.duration ? this.pos = t = se.easing[this.easing](e, this.options.duration * e, 0, 1, this.options.duration) : this.pos = t = e, this.now = (this.end - this.start) * t + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), n && n.set ? n.set(this) : q.propHooks._default.set(this), this
		}
	}).init.prototype = q.prototype, (q.propHooks = {
		_default: {
			get: function (e) {
				var t;
				return 1 !== e.elem.nodeType || null != e.elem[e.prop] && null == e.elem.style[e.prop] ? e.elem[e.prop] : (t = se.css(e.elem, e.prop, "")) && "auto" !== t ? t : 0
			}, set: function (e) {
				se.fx.step[e.prop] ? se.fx.step[e.prop](e) : 1 !== e.elem.nodeType || null == e.elem.style[se.cssProps[e.prop]] && !se.cssHooks[e.prop] ? e.elem[e.prop] = e.now : se.style(e.elem, e.prop, e.now + e.unit)
			}
		}
	}).scrollTop = q.propHooks.scrollLeft = {
		set: function (e) {
			e.elem.nodeType && e.elem.parentNode && (e.elem[e.prop] = e.now)
		}
	}, se.easing = {
		linear: function (e) {
			return e
		}, swing: function (e) {
			return .5 - Math.cos(e * Math.PI) / 2
		}, _default: "swing"
	}, se.fx = q.prototype.init, se.fx.step = {};
	var ft, ht, gt, vt, mt = /^(?:toggle|show|hide)$/, yt = /queueHooks$/;
	se.Animation = se.extend(_, {
		tweeners: {
			"*": [function (e, t) {
				var n = this.createTween(e, t);
				return f(n.elem, e, Ne.exec(t), n), n
			}]
		}, tweener: function (e, t) {
			for (var n, i = 0, o = (e = se.isFunction(e) ? (t = e, ["*"]) : e.match(xe)).length; i < o; i++) n = e[i], _.tweeners[n] = _.tweeners[n] || [], _.tweeners[n].unshift(t)
		}, prefilters: [function (e, t, n) {
			var i, o, r, s, a, l, c, u, d = "width" in t || "height" in t, p = this, f = {}, h = e.style,
				g = e.nodeType && Oe(e), v = Se.get(e, "fxshow");
			for (i in n.queue || (null == (s = se._queueHooks(e, "fx")).unqueued && (s.unqueued = 0, a = s.empty.fire, s.empty.fire = function () {
				s.unqueued || a()
			}), s.unqueued++, p.always(function () {
				p.always(function () {
					s.unqueued--, se.queue(e, "fx").length || s.empty.fire()
				})
			})), t) if (o = t[i], mt.test(o)) {
				if (delete t[i], r = r || "toggle" === o, o === (g ? "hide" : "show")) {
					if ("show" !== o || !v || void 0 === v[i]) continue;
					g = !0
				}
				f[i] = v && v[i] || se.style(e, i)
			}
			if ((l = !se.isEmptyObject(t)) || !se.isEmptyObject(f)) for (i in d && 1 === e.nodeType && (n.overflow = [h.overflow, h.overflowX, h.overflowY], null == (c = v && v.display) && (c = Se.get(e, "display")), "none" === (u = se.css(e, "display")) && (c ? u = c : (m([e], !0), c = e.style.display || c, u = se.css(e, "display"), m([e]))), ("inline" === u || "inline-block" === u && null != c) && "none" === se.css(e, "float") && (l || (p.done(function () {
				h.display = c
			}), null == c && (u = h.display, c = "none" === u ? "" : u)), h.display = "inline-block")), n.overflow && (h.overflow = "hidden", p.always(function () {
				h.overflow = n.overflow[0], h.overflowX = n.overflow[1], h.overflowY = n.overflow[2]
			})), l = !1, f) l || (v ? "hidden" in v && (g = v.hidden) : v = Se.access(e, "fxshow", {display: c}), r && (v.hidden = !g), g && m([e], !0), p.done(function () {
				for (i in g || m([e]), Se.remove(e, "fxshow"), f) se.style(e, i, f[i])
			})), l = F(g ? v[i] : 0, i, p), i in v || (v[i] = l.start, g && (l.end = l.start, l.start = 0))
		}], prefilter: function (e, t) {
			t ? _.prefilters.unshift(e) : _.prefilters.push(e)
		}
	}), se.speed = function (e, t, n) {
		var i = e && "object" == typeof e ? se.extend({}, e) : {
			complete: n || !n && t || se.isFunction(e) && e,
			duration: e,
			easing: n && t || t && !se.isFunction(t) && t
		};
		return se.fx.off ? i.duration = 0 : "number" != typeof i.duration && (i.duration in se.fx.speeds ? i.duration = se.fx.speeds[i.duration] : i.duration = se.fx.speeds._default), null != i.queue && !0 !== i.queue || (i.queue = "fx"), i.old = i.complete, i.complete = function () {
			se.isFunction(i.old) && i.old.call(this), i.queue && se.dequeue(this, i.queue)
		}, i
	}, se.fn.extend({
		fadeTo: function (e, t, n, i) {
			return this.filter(Oe).css("opacity", 0).show().end().animate({opacity: t}, e, n, i)
		}, animate: function (t, e, n, i) {
			function o() {
				var e = _(this, se.extend({}, t), s);
				(r || Se.get(this, "finish")) && e.stop(!0)
			}

			var r = se.isEmptyObject(t), s = se.speed(e, n, i);
			return o.finish = o, r || !1 === s.queue ? this.each(o) : this.queue(s.queue, o)
		}, stop: function (o, e, r) {
			function s(e) {
				var t = e.stop;
				delete e.stop, t(r)
			}

			return "string" != typeof o && (r = e, e = o, o = void 0), e && !1 !== o && this.queue(o || "fx", []), this.each(function () {
				var e = !0, t = null != o && o + "queueHooks", n = se.timers, i = Se.get(this);
				if (t) i[t] && i[t].stop && s(i[t]); else for (t in i) i[t] && i[t].stop && yt.test(t) && s(i[t]);
				for (t = n.length; t--;) n[t].elem !== this || null != o && n[t].queue !== o || (n[t].anim.stop(r), e = !1, n.splice(t, 1));
				!e && r || se.dequeue(this, o)
			})
		}, finish: function (s) {
			return !1 !== s && (s = s || "fx"), this.each(function () {
				var e, t = Se.get(this), n = t[s + "queue"], i = t[s + "queueHooks"], o = se.timers,
					r = n ? n.length : 0;
				for (t.finish = !0, se.queue(this, s, []), i && i.stop && i.stop.call(this, !0), e = o.length; e--;) o[e].elem === this && o[e].queue === s && (o[e].anim.stop(!0), o.splice(e, 1));
				for (e = 0; e < r; e++) n[e] && n[e].finish && n[e].finish.call(this);
				delete t.finish
			})
		}
	}), se.each(["toggle", "show", "hide"], function (e, i) {
		var o = se.fn[i];
		se.fn[i] = function (e, t, n) {
			return null == e || "boolean" == typeof e ? o.apply(this, arguments) : this.animate(P(i, !0), e, t, n)
		}
	}), se.each({
		slideDown: P("show"),
		slideUp: P("hide"),
		slideToggle: P("toggle"),
		fadeIn: {opacity: "show"},
		fadeOut: {opacity: "hide"},
		fadeToggle: {opacity: "toggle"}
	}, function (e, i) {
		se.fn[e] = function (e, t, n) {
			return this.animate(i, e, t, n)
		}
	}), se.timers = [], se.fx.tick = function () {
		var e, t = 0, n = se.timers;
		for (ft = se.now(); t < n.length; t++) (e = n[t])() || n[t] !== e || n.splice(t--, 1);
		n.length || se.fx.stop(), ft = void 0
	}, se.fx.timer = function (e) {
		se.timers.push(e), se.fx.start()
	}, se.fx.interval = 13, se.fx.start = function () {
		ht || (ht = !0, H())
	}, se.fx.stop = function () {
		ht = null
	}, se.fx.speeds = {slow: 600, fast: 200, _default: 400}, se.fn.delay = function (i, e) {
		return i = se.fx && se.fx.speeds[i] || i, e = e || "fx", this.queue(e, function (e, t) {
			var n = T.setTimeout(e, i);
			t.stop = function () {
				T.clearTimeout(n)
			}
		})
	}, gt = V.createElement("input"), vt = V.createElement("select").appendChild(V.createElement("option")), gt.type = "checkbox", re.checkOn = "" !== gt.value, re.optSelected = vt.selected, (gt = V.createElement("input")).value = "t", gt.type = "radio", re.radioValue = "t" === gt.value;
	var bt, xt = se.expr.attrHandle;
	se.fn.extend({
		attr: function (e, t) {
			return Ce(this, se.attr, e, t, 1 < arguments.length)
		}, removeAttr: function (e) {
			return this.each(function () {
				se.removeAttr(this, e)
			})
		}
	}), se.extend({
		attr: function (e, t, n) {
			var i, o, r = e.nodeType;
			if (3 !== r && 8 !== r && 2 !== r) return void 0 === e.getAttribute ? se.prop(e, t, n) : (1 === r && se.isXMLDoc(e) || (o = se.attrHooks[t.toLowerCase()] || (se.expr.match.bool.test(t) ? bt : void 0)), void 0 !== n ? null === n ? void se.removeAttr(e, t) : o && "set" in o && void 0 !== (i = o.set(e, n, t)) ? i : (e.setAttribute(t, n + ""), n) : o && "get" in o && null !== (i = o.get(e, t)) ? i : null == (i = se.find.attr(e, t)) ? void 0 : i)
		}, attrHooks: {
			type: {
				set: function (e, t) {
					if (!re.radioValue && "radio" === t && c(e, "input")) {
						var n = e.value;
						return e.setAttribute("type", t), n && (e.value = n), t
					}
				}
			}
		}, removeAttr: function (e, t) {
			var n, i = 0, o = t && t.match(xe);
			if (o && 1 === e.nodeType) for (; n = o[i++];) e.removeAttribute(n)
		}
	}), bt = {
		set: function (e, t, n) {
			return !1 === t ? se.removeAttr(e, n) : e.setAttribute(n, n), n
		}
	}, se.each(se.expr.match.bool.source.match(/\w+/g), function (e, t) {
		var s = xt[t] || se.find.attr;
		xt[t] = function (e, t, n) {
			var i, o, r = t.toLowerCase();
			return n || (o = xt[r], xt[r] = i, i = null != s(e, t, n) ? r : null, xt[r] = o), i
		}
	});
	var wt = /^(?:input|select|textarea|button)$/i, kt = /^(?:a|area)$/i;
	se.fn.extend({
		prop: function (e, t) {
			return Ce(this, se.prop, e, t, 1 < arguments.length)
		}, removeProp: function (e) {
			return this.each(function () {
				delete this[se.propFix[e] || e]
			})
		}
	}), se.extend({
		prop: function (e, t, n) {
			var i, o, r = e.nodeType;
			if (3 !== r && 8 !== r && 2 !== r) return 1 === r && se.isXMLDoc(e) || (t = se.propFix[t] || t, o = se.propHooks[t]), void 0 !== n ? o && "set" in o && void 0 !== (i = o.set(e, n, t)) ? i : e[t] = n : o && "get" in o && null !== (i = o.get(e, t)) ? i : e[t]
		}, propHooks: {
			tabIndex: {
				get: function (e) {
					var t = se.find.attr(e, "tabindex");
					return t ? parseInt(t, 10) : wt.test(e.nodeName) || kt.test(e.nodeName) && e.href ? 0 : -1
				}
			}
		}, propFix: {for: "htmlFor", class: "className"}
	}), re.optSelected || (se.propHooks.selected = {
		get: function (e) {
			var t = e.parentNode;
			return t && t.parentNode && t.parentNode.selectedIndex, null
		}, set: function (e) {
			var t = e.parentNode;
			t && (t.selectedIndex, t.parentNode && t.parentNode.selectedIndex)
		}
	}), se.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function () {
		se.propFix[this.toLowerCase()] = this
	}), se.fn.extend({
		addClass: function (t) {
			var e, n, i, o, r, s, a, l = 0;
			if (se.isFunction(t)) return this.each(function (e) {
				se(this).addClass(t.call(this, e, z(this)))
			});
			if ("string" == typeof t && t) for (e = t.match(xe) || []; n = this[l++];) if (o = z(n), i = 1 === n.nodeType && " " + M(o) + " ") {
				for (s = 0; r = e[s++];) i.indexOf(" " + r + " ") < 0 && (i += r + " ");
				o !== (a = M(i)) && n.setAttribute("class", a)
			}
			return this
		}, removeClass: function (t) {
			var e, n, i, o, r, s, a, l = 0;
			if (se.isFunction(t)) return this.each(function (e) {
				se(this).removeClass(t.call(this, e, z(this)))
			});
			if (!arguments.length) return this.attr("class", "");
			if ("string" == typeof t && t) for (e = t.match(xe) || []; n = this[l++];) if (o = z(n), i = 1 === n.nodeType && " " + M(o) + " ") {
				for (s = 0; r = e[s++];) for (; -1 < i.indexOf(" " + r + " ");) i = i.replace(" " + r + " ", " ");
				o !== (a = M(i)) && n.setAttribute("class", a)
			}
			return this
		}, toggleClass: function (o, t) {
			var r = typeof o;
			return "boolean" == typeof t && "string" == r ? t ? this.addClass(o) : this.removeClass(o) : se.isFunction(o) ? this.each(function (e) {
				se(this).toggleClass(o.call(this, e, z(this), t), t)
			}) : this.each(function () {
				var e, t, n, i;
				if ("string" == r) for (t = 0, n = se(this), i = o.match(xe) || []; e = i[t++];) n.hasClass(e) ? n.removeClass(e) : n.addClass(e); else void 0 !== o && "boolean" != r || ((e = z(this)) && Se.set(this, "__className__", e), this.setAttribute && this.setAttribute("class", e || !1 === o ? "" : Se.get(this, "__className__") || ""))
			})
		}, hasClass: function (e) {
			var t, n, i = 0;
			for (t = " " + e + " "; n = this[i++];) if (1 === n.nodeType && -1 < (" " + M(z(n)) + " ").indexOf(t)) return !0;
			return !1
		}
	});
	var Tt = /\r/g;
	se.fn.extend({
		val: function (n) {
			var i, e, o, t = this[0];
			return arguments.length ? (o = se.isFunction(n), this.each(function (e) {
				var t;
				1 === this.nodeType && (null == (t = o ? n.call(this, e, se(this).val()) : n) ? t = "" : "number" == typeof t ? t += "" : Array.isArray(t) && (t = se.map(t, function (e) {
					return null == e ? "" : e + ""
				})), (i = se.valHooks[this.type] || se.valHooks[this.nodeName.toLowerCase()]) && "set" in i && void 0 !== i.set(this, t, "value") || (this.value = t))
			})) : t ? (i = se.valHooks[t.type] || se.valHooks[t.nodeName.toLowerCase()]) && "get" in i && void 0 !== (e = i.get(t, "value")) ? e : "string" == typeof (e = t.value) ? e.replace(Tt, "") : null == e ? "" : e : void 0
		}
	}), se.extend({
		valHooks: {
			option: {
				get: function (e) {
					var t = se.find.attr(e, "value");
					return null != t ? t : M(se.text(e))
				}
			}, select: {
				get: function (e) {
					var t, n, i, o = e.options, r = e.selectedIndex, s = "select-one" === e.type, a = s ? null : [],
						l = s ? r + 1 : o.length;
					for (i = r < 0 ? l : s ? r : 0; i < l; i++) if (((n = o[i]).selected || i === r) && !n.disabled && (!n.parentNode.disabled || !c(n.parentNode, "optgroup"))) {
						if (t = se(n).val(), s) return t;
						a.push(t)
					}
					return a
				}, set: function (e, t) {
					for (var n, i, o = e.options, r = se.makeArray(t), s = o.length; s--;) ((i = o[s]).selected = -1 < se.inArray(se.valHooks.option.get(i), r)) && (n = !0);
					return n || (e.selectedIndex = -1), r
				}
			}
		}
	}), se.each(["radio", "checkbox"], function () {
		se.valHooks[this] = {
			set: function (e, t) {
				if (Array.isArray(t)) return e.checked = -1 < se.inArray(se(e).val(), t)
			}
		}, re.checkOn || (se.valHooks[this].get = function (e) {
			return null === e.getAttribute("value") ? "on" : e.value
		})
	});
	var Ct = /^(?:focusinfocus|focusoutblur)$/;
	se.extend(se.event, {
		trigger: function (e, t, n, i) {
			var o, r, s, a, l, c, u, d = [n || V], p = ne.call(e, "type") ? e.type : e,
				f = ne.call(e, "namespace") ? e.namespace.split(".") : [];
			if (r = s = n = n || V, 3 !== n.nodeType && 8 !== n.nodeType && !Ct.test(p + se.event.triggered) && (-1 < p.indexOf(".") && (p = (f = p.split(".")).shift(), f.sort()), l = p.indexOf(":") < 0 && "on" + p, (e = e[se.expando] ? e : new se.Event(p, "object" == typeof e && e)).isTrigger = i ? 2 : 3, e.namespace = f.join("."), e.rnamespace = e.namespace ? new RegExp("(^|\\.)" + f.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, e.result = void 0, e.target || (e.target = n), t = null == t ? [e] : se.makeArray(t, [e]), u = se.event.special[p] || {}, i || !u.trigger || !1 !== u.trigger.apply(n, t))) {
				if (!i && !u.noBubble && !se.isWindow(n)) {
					for (a = u.delegateType || p, Ct.test(a + p) || (r = r.parentNode); r; r = r.parentNode) d.push(r), s = r;
					s === (n.ownerDocument || V) && d.push(s.defaultView || s.parentWindow || T)
				}
				for (o = 0; (r = d[o++]) && !e.isPropagationStopped();) e.type = 1 < o ? a : u.bindType || p, (c = (Se.get(r, "events") || {})[e.type] && Se.get(r, "handle")) && c.apply(r, t), (c = l && r[l]) && c.apply && Te(r) && (e.result = c.apply(r, t), !1 === e.result && e.preventDefault());
				return e.type = p, i || e.isDefaultPrevented() || u._default && !1 !== u._default.apply(d.pop(), t) || !Te(n) || l && se.isFunction(n[p]) && !se.isWindow(n) && ((s = n[l]) && (n[l] = null), n[se.event.triggered = p](), se.event.triggered = void 0, s && (n[l] = s)), e.result
			}
		}, simulate: function (e, t, n) {
			var i = se.extend(new se.Event, n, {type: e, isSimulated: !0});
			se.event.trigger(i, null, t)
		}
	}), se.fn.extend({
		trigger: function (e, t) {
			return this.each(function () {
				se.event.trigger(e, t, this)
			})
		}, triggerHandler: function (e, t) {
			var n = this[0];
			if (n) return se.event.trigger(e, t, n, !0)
		}
	}), se.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "), function (e, n) {
		se.fn[n] = function (e, t) {
			return 0 < arguments.length ? this.on(n, null, e, t) : this.trigger(n)
		}
	}), se.fn.extend({
		hover: function (e, t) {
			return this.mouseenter(e).mouseleave(t || e)
		}
	}), re.focusin = "onfocusin" in T, re.focusin || se.each({focus: "focusin", blur: "focusout"}, function (n, i) {
		function o(e) {
			se.event.simulate(i, e.target, se.event.fix(e))
		}

		se.event.special[i] = {
			setup: function () {
				var e = this.ownerDocument || this, t = Se.access(e, i);
				t || e.addEventListener(n, o, !0), Se.access(e, i, (t || 0) + 1)
			}, teardown: function () {
				var e = this.ownerDocument || this, t = Se.access(e, i) - 1;
				t ? Se.access(e, i, t) : (e.removeEventListener(n, o, !0), Se.remove(e, i))
			}
		}
	});
	var St = T.location, $t = se.now(), Et = /\?/;
	se.parseXML = function (e) {
		var t;
		if (!e || "string" != typeof e) return null;
		try {
			t = (new T.DOMParser).parseFromString(e, "text/xml")
		} catch (e) {
			t = void 0
		}
		return t && !t.getElementsByTagName("parsererror").length || se.error("Invalid XML: " + e), t
	};
	var At = /\[\]$/, Dt = /\r?\n/g, jt = /^(?:submit|button|image|reset|file)$/i,
		Nt = /^(?:input|select|textarea|keygen)/i;
	se.param = function (e, t) {
		function n(e, t) {
			var n = se.isFunction(t) ? t() : t;
			o[o.length] = encodeURIComponent(e) + "=" + encodeURIComponent(null == n ? "" : n)
		}

		var i, o = [];
		if (Array.isArray(e) || e.jquery && !se.isPlainObject(e)) se.each(e, function () {
			n(this.name, this.value)
		}); else for (i in e) R(i, e[i], t, n);
		return o.join("&")
	}, se.fn.extend({
		serialize: function () {
			return se.param(this.serializeArray())
		}, serializeArray: function () {
			return this.map(function () {
				var e = se.prop(this, "elements");
				return e ? se.makeArray(e) : this
			}).filter(function () {
				var e = this.type;
				return this.name && !se(this).is(":disabled") && Nt.test(this.nodeName) && !jt.test(e) && (this.checked || !He.test(e))
			}).map(function (e, t) {
				var n = se(this).val();
				return null == n ? null : Array.isArray(n) ? se.map(n, function (e) {
					return {name: t.name, value: e.replace(Dt, "\r\n")}
				}) : {name: t.name, value: n.replace(Dt, "\r\n")}
			}).get()
		}
	});
	var Lt = /%20/g, Ot = /#.*$/, qt = /([?&])_=[^&]*/, Ht = /^(.*?):[ \t]*([^\r\n]*)$/gm, It = /^(?:GET|HEAD)$/,
		Pt = /^\/\//, Ft = {}, _t = {}, Mt = "*/".concat("*"), zt = V.createElement("a");
	zt.href = St.href, se.extend({
		active: 0,
		lastModified: {},
		etag: {},
		ajaxSettings: {
			url: St.href,
			type: "GET",
			isLocal: /^(?:about|app|app-storage|.+-extension|file|res|widget):$/.test(St.protocol),
			global: !0,
			processData: !0,
			async: !0,
			contentType: "application/x-www-form-urlencoded; charset=UTF-8",
			accepts: {
				"*": Mt,
				text: "text/plain",
				html: "text/html",
				xml: "application/xml, text/xml",
				json: "application/json, text/javascript"
			},
			contents: {xml: /\bxml\b/, html: /\bhtml/, json: /\bjson\b/},
			responseFields: {xml: "responseXML", text: "responseText", json: "responseJSON"},
			converters: {"* text": String, "text html": !0, "text json": JSON.parse, "text xml": se.parseXML},
			flatOptions: {url: !0, context: !0}
		},
		ajaxSetup: function (e, t) {
			return t ? U(U(e, se.ajaxSettings), t) : U(se.ajaxSettings, e)
		},
		ajaxPrefilter: W(Ft),
		ajaxTransport: W(_t),
		ajax: function (e, t) {
			function n(e, t, n, i) {
				var o, r, s, a, l, c = t;
				h || (h = !0, f && T.clearTimeout(f), u = void 0, p = i || "", k.readyState = 0 < e ? 4 : 0, o = 200 <= e && e < 300 || 304 === e, n && (a = function (e, t, n) {
					for (var i, o, r, s, a = e.contents, l = e.dataTypes; "*" === l[0];) l.shift(), void 0 === i && (i = e.mimeType || t.getResponseHeader("Content-Type"));
					if (i) for (o in a) if (a[o] && a[o].test(i)) {
						l.unshift(o);
						break
					}
					if (l[0] in n) r = l[0]; else {
						for (o in n) {
							if (!l[0] || e.converters[o + " " + l[0]]) {
								r = o;
								break
							}
							s = s || o
						}
						r = r || s
					}
					if (r) return r !== l[0] && l.unshift(r), n[r]
				}(v, k, n)), a = function (e, t, n, i) {
					var o, r, s, a, l, c = {}, u = e.dataTypes.slice();
					if (u[1]) for (s in e.converters) c[s.toLowerCase()] = e.converters[s];
					for (r = u.shift(); r;) if (e.responseFields[r] && (n[e.responseFields[r]] = t), !l && i && e.dataFilter && (t = e.dataFilter(t, e.dataType)), l = r, r = u.shift()) if ("*" === r) r = l; else if ("*" !== l && l !== r) {
						if (!(s = c[l + " " + r] || c["* " + r])) for (o in c) if ((a = o.split(" "))[1] === r && (s = c[l + " " + a[0]] || c["* " + a[0]])) {
							!0 === s ? s = c[o] : !0 !== c[o] && (r = a[0], u.unshift(a[1]));
							break
						}
						if (!0 !== s) if (s && e.throws) t = s(t); else try {
							t = s(t)
						} catch (e) {
							return {state: "parsererror", error: s ? e : "No conversion from " + l + " to " + r}
						}
					}
					return {state: "success", data: t}
				}(v, a, k, o), o ? (v.ifModified && ((l = k.getResponseHeader("Last-Modified")) && (se.lastModified[d] = l), (l = k.getResponseHeader("etag")) && (se.etag[d] = l)), 204 === e || "HEAD" === v.type ? c = "nocontent" : 304 === e ? c = "notmodified" : (c = a.state, r = a.data, o = !(s = a.error))) : (s = c, !e && c || (c = "error", e < 0 && (e = 0))), k.status = e, k.statusText = (t || c) + "", o ? b.resolveWith(m, [r, c, k]) : b.rejectWith(m, [k, c, s]), k.statusCode(w), w = void 0, g && y.trigger(o ? "ajaxSuccess" : "ajaxError", [k, v, o ? r : s]), x.fireWith(m, [k, c]), g && (y.trigger("ajaxComplete", [k, v]), --se.active || se.event.trigger("ajaxStop")))
			}

			"object" == typeof e && (t = e, e = void 0), t = t || {};
			var u, d, p, i, f, o, h, g, r, s, v = se.ajaxSetup({}, t), m = v.context || v,
				y = v.context && (m.nodeType || m.jquery) ? se(m) : se.event, b = se.Deferred(),
				x = se.Callbacks("once memory"), w = v.statusCode || {}, a = {}, l = {}, c = "canceled", k = {
					readyState: 0, getResponseHeader: function (e) {
						var t;
						if (h) {
							if (!i) for (i = {}; t = Ht.exec(p);) i[t[1].toLowerCase()] = t[2];
							t = i[e.toLowerCase()]
						}
						return null == t ? null : t
					}, getAllResponseHeaders: function () {
						return h ? p : null
					}, setRequestHeader: function (e, t) {
						return null == h && (e = l[e.toLowerCase()] = l[e.toLowerCase()] || e, a[e] = t), this
					}, overrideMimeType: function (e) {
						return null == h && (v.mimeType = e), this
					}, statusCode: function (e) {
						var t;
						if (e) if (h) k.always(e[k.status]); else for (t in e) w[t] = [w[t], e[t]];
						return this
					}, abort: function (e) {
						var t = e || c;
						return u && u.abort(t), n(0, t), this
					}
				};
			if (b.promise(k), v.url = ((e || v.url || St.href) + "").replace(Pt, St.protocol + "//"), v.type = t.method || t.type || v.method || v.type, v.dataTypes = (v.dataType || "*").toLowerCase().match(xe) || [""], null == v.crossDomain) {
				o = V.createElement("a");
				try {
					o.href = v.url, o.href = o.href, v.crossDomain = zt.protocol + "//" + zt.host != o.protocol + "//" + o.host
				} catch (e) {
					v.crossDomain = !0
				}
			}
			if (v.data && v.processData && "string" != typeof v.data && (v.data = se.param(v.data, v.traditional)), B(Ft, v, t, k), h) return k;
			for (r in (g = se.event && v.global) && 0 == se.active++ && se.event.trigger("ajaxStart"), v.type = v.type.toUpperCase(), v.hasContent = !It.test(v.type), d = v.url.replace(Ot, ""), v.hasContent ? v.data && v.processData && 0 === (v.contentType || "").indexOf("application/x-www-form-urlencoded") && (v.data = v.data.replace(Lt, "+")) : (s = v.url.slice(d.length), v.data && (d += (Et.test(d) ? "&" : "?") + v.data, delete v.data), !1 === v.cache && (d = d.replace(qt, "$1"), s = (Et.test(d) ? "&" : "?") + "_=" + $t++ + s), v.url = d + s), v.ifModified && (se.lastModified[d] && k.setRequestHeader("If-Modified-Since", se.lastModified[d]), se.etag[d] && k.setRequestHeader("If-None-Match", se.etag[d])), (v.data && v.hasContent && !1 !== v.contentType || t.contentType) && k.setRequestHeader("Content-Type", v.contentType), k.setRequestHeader("Accept", v.dataTypes[0] && v.accepts[v.dataTypes[0]] ? v.accepts[v.dataTypes[0]] + ("*" !== v.dataTypes[0] ? ", " + Mt + "; q=0.01" : "") : v.accepts["*"]), v.headers) k.setRequestHeader(r, v.headers[r]);
			if (v.beforeSend && (!1 === v.beforeSend.call(m, k, v) || h)) return k.abort();
			if (c = "abort", x.add(v.complete), k.done(v.success), k.fail(v.error), u = B(_t, v, t, k)) {
				if (k.readyState = 1, g && y.trigger("ajaxSend", [k, v]), h) return k;
				v.async && 0 < v.timeout && (f = T.setTimeout(function () {
					k.abort("timeout")
				}, v.timeout));
				try {
					h = !1, u.send(a, n)
				} catch (e) {
					if (h) throw e;
					n(-1, e)
				}
			} else n(-1, "No Transport");
			return k
		},
		getJSON: function (e, t, n) {
			return se.get(e, t, n, "json")
		},
		getScript: function (e, t) {
			return se.get(e, void 0, t, "script")
		}
	}), se.each(["get", "post"], function (e, o) {
		se[o] = function (e, t, n, i) {
			return se.isFunction(t) && (i = i || n, n = t, t = void 0), se.ajax(se.extend({
				url: e,
				type: o,
				dataType: i,
				data: t,
				success: n
			}, se.isPlainObject(e) && e))
		}
	}), se._evalUrl = function (e) {
		return se.ajax({url: e, type: "GET", dataType: "script", cache: !0, async: !1, global: !1, throws: !0})
	}, se.fn.extend({
		wrapAll: function (e) {
			var t;
			return this[0] && (se.isFunction(e) && (e = e.call(this[0])), t = se(e, this[0].ownerDocument).eq(0).clone(!0), this[0].parentNode && t.insertBefore(this[0]), t.map(function () {
				for (var e = this; e.firstElementChild;) e = e.firstElementChild;
				return e
			}).append(this)), this
		}, wrapInner: function (n) {
			return se.isFunction(n) ? this.each(function (e) {
				se(this).wrapInner(n.call(this, e))
			}) : this.each(function () {
				var e = se(this), t = e.contents();
				t.length ? t.wrapAll(n) : e.append(n)
			})
		}, wrap: function (t) {
			var n = se.isFunction(t);
			return this.each(function (e) {
				se(this).wrapAll(n ? t.call(this, e) : t)
			})
		}, unwrap: function (e) {
			return this.parent(e).not("body").each(function () {
				se(this).replaceWith(this.childNodes)
			}), this
		}
	}), se.expr.pseudos.hidden = function (e) {
		return !se.expr.pseudos.visible(e)
	}, se.expr.pseudos.visible = function (e) {
		return !!(e.offsetWidth || e.offsetHeight || e.getClientRects().length)
	}, se.ajaxSettings.xhr = function () {
		try {
			return new T.XMLHttpRequest
		} catch (e) {
		}
	};
	var Rt = {0: 200, 1223: 204}, Wt = se.ajaxSettings.xhr();
	re.cors = !!Wt && "withCredentials" in Wt, re.ajax = Wt = !!Wt, se.ajaxTransport(function (o) {
		var r, s;
		if (re.cors || Wt && !o.crossDomain) return {
			send: function (e, t) {
				var n, i = o.xhr();
				if (i.open(o.type, o.url, o.async, o.username, o.password), o.xhrFields) for (n in o.xhrFields) i[n] = o.xhrFields[n];
				for (n in o.mimeType && i.overrideMimeType && i.overrideMimeType(o.mimeType), o.crossDomain || e["X-Requested-With"] || (e["X-Requested-With"] = "XMLHttpRequest"), e) i.setRequestHeader(n, e[n]);
				r = function (e) {
					return function () {
						r && (r = s = i.onload = i.onerror = i.onabort = i.onreadystatechange = null, "abort" === e ? i.abort() : "error" === e ? "number" != typeof i.status ? t(0, "error") : t(i.status, i.statusText) : t(Rt[i.status] || i.status, i.statusText, "text" !== (i.responseType || "text") || "string" != typeof i.responseText ? {binary: i.response} : {text: i.responseText}, i.getAllResponseHeaders()))
					}
				}, i.onload = r(), s = i.onerror = r("error"), void 0 !== i.onabort ? i.onabort = s : i.onreadystatechange = function () {
					4 === i.readyState && T.setTimeout(function () {
						r && s()
					})
				}, r = r("abort");
				try {
					i.send(o.hasContent && o.data || null)
				} catch (e) {
					if (r) throw e
				}
			}, abort: function () {
				r && r()
			}
		}
	}), se.ajaxPrefilter(function (e) {
		e.crossDomain && (e.contents.script = !1)
	}), se.ajaxSetup({
		accepts: {script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"},
		contents: {script: /\b(?:java|ecma)script\b/},
		converters: {
			"text script": function (e) {
				return se.globalEval(e), e
			}
		}
	}), se.ajaxPrefilter("script", function (e) {
		void 0 === e.cache && (e.cache = !1), e.crossDomain && (e.type = "GET")
	}), se.ajaxTransport("script", function (n) {
		var i, o;
		if (n.crossDomain) return {
			send: function (e, t) {
				i = se("<script>").prop({charset: n.scriptCharset, src: n.url}).on("load error", o = function (e) {
					i.remove(), o = null, e && t("error" === e.type ? 404 : 200, e.type)
				}), V.head.appendChild(i[0])
			}, abort: function () {
				o && o()
			}
		}
	});
	var Bt, Ut = [], Xt = /(=)\?(?=&|$)|\?\?/;
	se.ajaxSetup({
		jsonp: "callback", jsonpCallback: function () {
			var e = Ut.pop() || se.expando + "_" + $t++;
			return this[e] = !0, e
		}
	}), se.ajaxPrefilter("json jsonp", function (e, t, n) {
		var i, o, r,
			s = !1 !== e.jsonp && (Xt.test(e.url) ? "url" : "string" == typeof e.data && 0 === (e.contentType || "").indexOf("application/x-www-form-urlencoded") && Xt.test(e.data) && "data");
		if (s || "jsonp" === e.dataTypes[0]) return i = e.jsonpCallback = se.isFunction(e.jsonpCallback) ? e.jsonpCallback() : e.jsonpCallback, s ? e[s] = e[s].replace(Xt, "$1" + i) : !1 !== e.jsonp && (e.url += (Et.test(e.url) ? "&" : "?") + e.jsonp + "=" + i), e.converters["script json"] = function () {
			return r || se.error(i + " was not called"), r[0]
		}, e.dataTypes[0] = "json", o = T[i], T[i] = function () {
			r = arguments
		}, n.always(function () {
			void 0 === o ? se(T).removeProp(i) : T[i] = o, e[i] && (e.jsonpCallback = t.jsonpCallback, Ut.push(i)), r && se.isFunction(o) && o(r[0]), r = o = void 0
		}), "script"
	}), re.createHTMLDocument = ((Bt = V.implementation.createHTMLDocument("").body).innerHTML = "<form></form><form></form>", 2 === Bt.childNodes.length), se.parseHTML = function (e, t, n) {
		return "string" != typeof e ? [] : ("boolean" == typeof t && (n = t, t = !1), t || (re.createHTMLDocument ? ((i = (t = V.implementation.createHTMLDocument("")).createElement("base")).href = V.location.href, t.head.appendChild(i)) : t = V), r = !n && [], (o = he.exec(e)) ? [t.createElement(o[1])] : (o = b([e], t, r), r && r.length && se(r).remove(), se.merge([], o.childNodes)));
		var i, o, r
	}, se.fn.load = function (e, t, n) {
		var i, o, r, s = this, a = e.indexOf(" ");
		return -1 < a && (i = M(e.slice(a)), e = e.slice(0, a)), se.isFunction(t) ? (n = t, t = void 0) : t && "object" == typeof t && (o = "POST"), 0 < s.length && se.ajax({
			url: e,
			type: o || "GET",
			dataType: "html",
			data: t
		}).done(function (e) {
			r = arguments, s.html(i ? se("<div>").append(se.parseHTML(e)).find(i) : e)
		}).always(n && function (e, t) {
			s.each(function () {
				n.apply(this, r || [e.responseText, t, e])
			})
		}), this
	}, se.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function (e, t) {
		se.fn[t] = function (e) {
			return this.on(t, e)
		}
	}), se.expr.pseudos.animated = function (t) {
		return se.grep(se.timers, function (e) {
			return t === e.elem
		}).length
	}, se.offset = {
		setOffset: function (e, t, n) {
			var i, o, r, s, a, l, c = se.css(e, "position"), u = se(e), d = {};
			"static" === c && (e.style.position = "relative"), a = u.offset(), r = se.css(e, "top"), l = se.css(e, "left"), o = ("absolute" === c || "fixed" === c) && -1 < (r + l).indexOf("auto") ? (s = (i = u.position()).top, i.left) : (s = parseFloat(r) || 0, parseFloat(l) || 0), se.isFunction(t) && (t = t.call(e, n, se.extend({}, a))), null != t.top && (d.top = t.top - a.top + s), null != t.left && (d.left = t.left - a.left + o), "using" in t ? t.using.call(e, d) : u.css(d)
		}
	}, se.fn.extend({
		offset: function (t) {
			if (arguments.length) return void 0 === t ? this : this.each(function (e) {
				se.offset.setOffset(this, t, e)
			});
			var e, n, i, o, r = this[0];
			return r ? r.getClientRects().length ? (i = r.getBoundingClientRect(), n = (e = r.ownerDocument).documentElement, o = e.defaultView, {
				top: i.top + o.pageYOffset - n.clientTop,
				left: i.left + o.pageXOffset - n.clientLeft
			}) : {top: 0, left: 0} : void 0
		}, position: function () {
			if (this[0]) {
				var e, t, n = this[0], i = {top: 0, left: 0};
				return "fixed" === se.css(n, "position") ? t = n.getBoundingClientRect() : (e = this.offsetParent(), t = this.offset(), c(e[0], "html") || (i = e.offset()), i = {
					top: i.top + se.css(e[0], "borderTopWidth", !0),
					left: i.left + se.css(e[0], "borderLeftWidth", !0)
				}), {
					top: t.top - i.top - se.css(n, "marginTop", !0),
					left: t.left - i.left - se.css(n, "marginLeft", !0)
				}
			}
		}, offsetParent: function () {
			return this.map(function () {
				for (var e = this.offsetParent; e && "static" === se.css(e, "position");) e = e.offsetParent;
				return e || Re
			})
		}
	}), se.each({scrollLeft: "pageXOffset", scrollTop: "pageYOffset"}, function (t, o) {
		var r = "pageYOffset" === o;
		se.fn[t] = function (e) {
			return Ce(this, function (e, t, n) {
				var i;
				if (se.isWindow(e) ? i = e : 9 === e.nodeType && (i = e.defaultView), void 0 === n) return i ? i[o] : e[t];
				i ? i.scrollTo(r ? i.pageXOffset : n, r ? n : i.pageYOffset) : e[t] = n
			}, t, e, arguments.length)
		}
	}), se.each(["top", "left"], function (e, n) {
		se.cssHooks[n] = D(re.pixelPosition, function (e, t) {
			if (t) return t = A(e, n), ot.test(t) ? se(e).position()[n] + "px" : t
		})
	}), se.each({Height: "height", Width: "width"}, function (s, a) {
		se.each({padding: "inner" + s, content: a, "": "outer" + s}, function (i, r) {
			se.fn[r] = function (e, t) {
				var n = arguments.length && (i || "boolean" != typeof e),
					o = i || (!0 === e || !0 === t ? "margin" : "border");
				return Ce(this, function (e, t, n) {
					var i;
					return se.isWindow(e) ? 0 === r.indexOf("outer") ? e["inner" + s] : e.document.documentElement["client" + s] : 9 === e.nodeType ? (i = e.documentElement, Math.max(e.body["scroll" + s], i["scroll" + s], e.body["offset" + s], i["offset" + s], i["client" + s])) : void 0 === n ? se.css(e, t, o) : se.style(e, t, n, o)
				}, a, n ? e : void 0, n)
			}
		})
	}), se.fn.extend({
		bind: function (e, t, n) {
			return this.on(e, null, t, n)
		}, unbind: function (e, t) {
			return this.off(e, null, t)
		}, delegate: function (e, t, n, i) {
			return this.on(t, e, n, i)
		}, undelegate: function (e, t, n) {
			return 1 === arguments.length ? this.off(e, "**") : this.off(t, e || "**", n)
		}
	}), se.holdReady = function (e) {
		e ? se.readyWait++ : se.ready(!0)
	}, se.isArray = Array.isArray, se.parseJSON = JSON.parse, se.nodeName = c, "function" == typeof define && define.amd && define("jquery", [], function () {
		return se
	});
	var Kt = T.jQuery, Vt = T.$;
	return se.noConflict = function (e) {
		return T.$ === se && (T.$ = Vt), e && T.jQuery === se && (T.jQuery = Kt), se
	}, e || (T.jQuery = T.$ = se), se
}), function (e, t) {
	"function" == typeof define && define.amd ? define(["jquery"], t) : "object" == typeof exports ? module.exports = t(require("jquery")) : e.lightbox = t(e.jQuery)
}(this, function (d) {
	function e(e) {
		this.album = [], this.currentImageIndex = void 0, this.init(), this.options = d.extend({}, this.constructor.defaults), this.option(e)
	}

	return e.defaults = {
		albumLabel: "Image %1 of %2",
		alwaysShowNavOnTouchDevices: !1,
		fadeDuration: 600,
		fitImagesInViewport: !0,
		imageFadeDuration: 600,
		positionFromTop: 50,
		resizeDuration: 700,
		showImageNumberLabel: !0,
		wrapAround: !1,
		disableScrolling: !1,
		sanitizeTitle: !1
	}, e.prototype.option = function (e) {
		d.extend(this.options, e)
	}, e.prototype.imageCountLabel = function (e, t) {
		return this.options.albumLabel.replace(/%1/g, e).replace(/%2/g, t)
	}, e.prototype.init = function () {
		var e = this;
		d(document).ready(function () {
			e.enable(), e.build()
		})
	}, e.prototype.enable = function () {
		var t = this;
		d("body").on("click", "a[rel^=lightbox], area[rel^=lightbox], a[data-lightbox], area[data-lightbox]", function (e) {
			return t.start(d(e.currentTarget)), !1
		})
	}, e.prototype.build = function () {
		if (!(0 < d("#lightbox").length)) {
			var t = this;
			d('<div id="lightboxOverlay" class="lightboxOverlay"></div><div id="lightbox" class="lightbox"><div class="lb-outerContainer"><div class="lb-container"><img class="lb-image" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" /><div class="lb-nav"><a class="lb-prev" href="" ></a><a class="lb-next" href="" ></a></div><div class="lb-loader"><a class="lb-cancel"></a></div></div></div><div class="lb-dataContainer"><div class="lb-data"><div class="lb-details"><span class="lb-caption"></span><span class="lb-number"></span></div><div class="lb-closeContainer"><a class="lb-close"></a></div></div></div></div>').appendTo(d("body")), this.$lightbox = d("#lightbox"), this.$overlay = d("#lightboxOverlay"), this.$outerContainer = this.$lightbox.find(".lb-outerContainer"), this.$container = this.$lightbox.find(".lb-container"), this.$image = this.$lightbox.find(".lb-image"), this.$nav = this.$lightbox.find(".lb-nav"), this.containerPadding = {
				top: parseInt(this.$container.css("padding-top"), 10),
				right: parseInt(this.$container.css("padding-right"), 10),
				bottom: parseInt(this.$container.css("padding-bottom"), 10),
				left: parseInt(this.$container.css("padding-left"), 10)
			}, this.imageBorderWidth = {
				top: parseInt(this.$image.css("border-top-width"), 10),
				right: parseInt(this.$image.css("border-right-width"), 10),
				bottom: parseInt(this.$image.css("border-bottom-width"), 10),
				left: parseInt(this.$image.css("border-left-width"), 10)
			}, this.$overlay.hide().on("click", function () {
				return t.end(), !1
			}), this.$lightbox.hide().on("click", function (e) {
				return "lightbox" === d(e.target).attr("id") && t.end(), !1
			}), this.$outerContainer.on("click", function (e) {
				return "lightbox" === d(e.target).attr("id") && t.end(), !1
			}), this.$lightbox.find(".lb-prev").on("click", function () {
				return 0 === t.currentImageIndex ? t.changeImage(t.album.length - 1) : t.changeImage(t.currentImageIndex - 1), !1
			}), this.$lightbox.find(".lb-next").on("click", function () {
				return t.currentImageIndex === t.album.length - 1 ? t.changeImage(0) : t.changeImage(t.currentImageIndex + 1), !1
			}), this.$nav.on("mousedown", function (e) {
				3 === e.which && (t.$nav.css("pointer-events", "none"), t.$lightbox.one("contextmenu", function () {
					setTimeout(function () {
						this.$nav.css("pointer-events", "auto")
					}.bind(t), 0)
				}))
			}), this.$lightbox.find(".lb-loader, .lb-close").on("click", function () {
				return t.end(), !1
			})
		}
	}, e.prototype.start = function (e) {
		function t(e) {
			n.album.push({
				alt: e.attr("data-alt"),
				link: e.attr("href"),
				title: e.attr("data-title") || e.attr("title")
			})
		}

		var n = this, i = d(window);
		i.on("resize", d.proxy(this.sizeOverlay, this)), d("select, object, embed").css({visibility: "hidden"}), this.sizeOverlay(), this.album = [];
		var o, r = 0, s = e.attr("data-lightbox");
		if (s) {
			o = d(e.prop("tagName") + '[data-lightbox="' + s + '"]');
			for (var a = 0; a < o.length; a = ++a) t(d(o[a])), o[a] === e[0] && (r = a)
		} else if ("lightbox" === e.attr("rel")) t(e); else {
			o = d(e.prop("tagName") + '[rel="' + e.attr("rel") + '"]');
			for (var l = 0; l < o.length; l = ++l) t(d(o[l])), o[l] === e[0] && (r = l)
		}
		var c = i.scrollTop() + this.options.positionFromTop, u = i.scrollLeft();
		this.$lightbox.css({
			top: c + "px",
			left: u + "px"
		}).fadeIn(this.options.fadeDuration), this.options.disableScrolling && d("html").addClass("lb-disable-scrolling"), this.changeImage(r)
	}, e.prototype.changeImage = function (s) {
		var a = this;
		this.disableKeyboardNav();
		var l = this.$lightbox.find(".lb-image");
		this.$overlay.fadeIn(this.options.fadeDuration), d(".lb-loader").fadeIn("slow"), this.$lightbox.find(".lb-image, .lb-nav, .lb-prev, .lb-next, .lb-dataContainer, .lb-numbers, .lb-caption").hide(), this.$outerContainer.addClass("animating");
		var c = new Image;
		c.onload = function () {
			var e, t, n, i, o, r;
			l.attr({
				alt: a.album[s].alt,
				src: a.album[s].link
			}), d(c), l.width(c.width), l.height(c.height), a.options.fitImagesInViewport && (r = d(window).width(), o = d(window).height(), i = r - a.containerPadding.left - a.containerPadding.right - a.imageBorderWidth.left - a.imageBorderWidth.right - 20, n = o - a.containerPadding.top - a.containerPadding.bottom - a.imageBorderWidth.top - a.imageBorderWidth.bottom - 120, a.options.maxWidth && a.options.maxWidth < i && (i = a.options.maxWidth), a.options.maxHeight && a.options.maxHeight < i && (n = a.options.maxHeight), (c.width > i || c.height > n) && (c.width / i > c.height / n ? (t = i, e = parseInt(c.height / (c.width / t), 10)) : (e = n, t = parseInt(c.width / (c.height / e), 10)), l.width(t), l.height(e))), a.sizeContainer(l.width(), l.height())
		}, c.src = this.album[s].link, this.currentImageIndex = s
	}, e.prototype.sizeOverlay = function () {
		this.$overlay.width(d(document).width()).height(d(document).height())
	}, e.prototype.sizeContainer = function (e, t) {
		function n() {
			i.$lightbox.find(".lb-dataContainer").width(s), i.$lightbox.find(".lb-prevLink").height(a), i.$lightbox.find(".lb-nextLink").height(a), i.showImage()
		}

		var i = this, o = this.$outerContainer.outerWidth(), r = this.$outerContainer.outerHeight(),
			s = e + this.containerPadding.left + this.containerPadding.right + this.imageBorderWidth.left + this.imageBorderWidth.right,
			a = t + this.containerPadding.top + this.containerPadding.bottom + this.imageBorderWidth.top + this.imageBorderWidth.bottom;
		o !== s || r !== a ? this.$outerContainer.animate({
			width: s,
			height: a
		}, this.options.resizeDuration, "swing", function () {
			n()
		}) : n()
	}, e.prototype.showImage = function () {
		this.$lightbox.find(".lb-loader").stop(!0).hide(), this.$lightbox.find(".lb-image").fadeIn(this.options.imageFadeDuration), this.updateNav(), this.updateDetails(), this.preloadNeighboringImages(), this.enableKeyboardNav()
	}, e.prototype.updateNav = function () {
		var e = !1;
		try {
			document.createEvent("TouchEvent"), e = !!this.options.alwaysShowNavOnTouchDevices
		} catch (e) {
		}
		this.$lightbox.find(".lb-nav").show(), 1 < this.album.length && (this.options.wrapAround ? (e && this.$lightbox.find(".lb-prev, .lb-next").css("opacity", "1"), this.$lightbox.find(".lb-prev, .lb-next").show()) : (0 < this.currentImageIndex && (this.$lightbox.find(".lb-prev").show(), e && this.$lightbox.find(".lb-prev").css("opacity", "1")), this.currentImageIndex < this.album.length - 1 && (this.$lightbox.find(".lb-next").show(), e && this.$lightbox.find(".lb-next").css("opacity", "1"))))
	}, e.prototype.updateDetails = function () {
		var e = this;
		if (void 0 !== this.album[this.currentImageIndex].title && "" !== this.album[this.currentImageIndex].title) {
			var t = this.$lightbox.find(".lb-caption");
			this.options.sanitizeTitle ? t.text(this.album[this.currentImageIndex].title) : t.html(this.album[this.currentImageIndex].title), t.fadeIn("fast").find("a").on("click", function (e) {
				void 0 !== d(this).attr("target") ? window.open(d(this).attr("href"), d(this).attr("target")) : location.href = d(this).attr("href")
			})
		}
		if (1 < this.album.length && this.options.showImageNumberLabel) {
			var n = this.imageCountLabel(this.currentImageIndex + 1, this.album.length);
			this.$lightbox.find(".lb-number").text(n).fadeIn("fast")
		} else this.$lightbox.find(".lb-number").hide();
		this.$outerContainer.removeClass("animating"), this.$lightbox.find(".lb-dataContainer").fadeIn(this.options.resizeDuration, function () {
			return e.sizeOverlay()
		})
	}, e.prototype.preloadNeighboringImages = function () {
		this.album.length > this.currentImageIndex + 1 && ((new Image).src = this.album[this.currentImageIndex + 1].link), 0 < this.currentImageIndex && ((new Image).src = this.album[this.currentImageIndex - 1].link)
	}, e.prototype.enableKeyboardNav = function () {
		d(document).on("keyup.keyboard", d.proxy(this.keyboardAction, this))
	}, e.prototype.disableKeyboardNav = function () {
		d(document).off(".keyboard")
	}, e.prototype.keyboardAction = function (e) {
		var t = e.keyCode, n = String.fromCharCode(t).toLowerCase();
		27 === t || n.match(/x|o|c/) ? this.end() : "p" === n || 37 === t ? 0 !== this.currentImageIndex ? this.changeImage(this.currentImageIndex - 1) : this.options.wrapAround && 1 < this.album.length && this.changeImage(this.album.length - 1) : "n" !== n && 39 !== t || (this.currentImageIndex !== this.album.length - 1 ? this.changeImage(this.currentImageIndex + 1) : this.options.wrapAround && 1 < this.album.length && this.changeImage(0))
	}, e.prototype.end = function () {
		this.disableKeyboardNav(), d(window).off("resize", this.sizeOverlay), this.$lightbox.fadeOut(this.options.fadeDuration), this.$overlay.fadeOut(this.options.fadeDuration), d("select, object, embed").css({visibility: "visible"}), this.options.disableScrolling && d("html").removeClass("lb-disable-scrolling")
	}, new e
}), function (e) {
	"object" == typeof module && "object" == typeof module.exports ? e(require("jquery"), window, document) : e(jQuery, window, document)
}(function (o, e, t, n) {
	function r() {
		return s.length ? s[s.length - 1] : null
	}

	function i() {
		var e, t = !1;
		for (e = s.length - 1; 0 <= e; e--) s[e].$blocker && (s[e].$blocker.toggleClass("current", !t).toggleClass("behind", t), t = !0)
	}

	var s = [];
	o.modal = function (n, e) {
		var i, t;
		if (this.$body = o("body"), this.options = o.extend({}, o.modal.defaults, e), this.options.doFade = !isNaN(parseInt(this.options.fadeDuration, 10)), this.$blocker = null, this.options.closeExisting) for (; o.modal.isActive();) o.modal.close();
		if (s.push(this), n.is("a")) if (t = n.attr("href"), this.anchor = n, /^#/.test(t)) {
			if (this.$elm = o(t), 1 !== this.$elm.length) return null;
			this.$body.append(this.$elm), this.open()
		} else this.$elm = o("<div>"), this.$body.append(this.$elm), i = function (e, t) {
			t.elm.remove()
		}, this.showSpinner(), n.trigger(o.modal.AJAX_SEND), o.get(t).done(function (e) {
			if (o.modal.isActive()) {
				n.trigger(o.modal.AJAX_SUCCESS);
				var t = r();
				t.$elm.empty().append(e).on(o.modal.CLOSE, i), t.hideSpinner(), t.open(), n.trigger(o.modal.AJAX_COMPLETE)
			}
		}).fail(function () {
			n.trigger(o.modal.AJAX_FAIL), r().hideSpinner(), s.pop(), n.trigger(o.modal.AJAX_COMPLETE)
		}); else this.$elm = n, this.anchor = n, this.$body.append(this.$elm), this.open()
	}, o.modal.prototype = {
		constructor: o.modal, open: function () {
			var e = this;
			this.block(), this.anchor.blur(), this.options.doFade ? setTimeout(function () {
				e.show()
			}, this.options.fadeDuration * this.options.fadeDelay) : this.show(), o(t).off("keydown.modal").on("keydown.modal", function (e) {
				var t = r();
				27 === e.which && t.options.escapeClose && t.close()
			}), this.options.clickClose && this.$blocker.click(function (e) {
				e.target === this && o.modal.close()
			})
		}, close: function () {
			s.pop(), this.unblock(), this.hide(), o.modal.isActive() || o(t).off("keydown.modal")
		}, block: function () {
			this.$elm.trigger(o.modal.BEFORE_BLOCK, [this._ctx()]), this.$body.css("overflow", "hidden"), this.$blocker = o('<div class="' + this.options.blockerClass + ' blocker current"></div>').appendTo(this.$body), i(), this.options.doFade && this.$blocker.css("opacity", 0).animate({opacity: 1}, this.options.fadeDuration), this.$elm.trigger(o.modal.BLOCK, [this._ctx()])
		}, unblock: function (e) {
			!e && this.options.doFade ? this.$blocker.fadeOut(this.options.fadeDuration, this.unblock.bind(this, !0)) : (this.$blocker.children().appendTo(this.$body), this.$blocker.remove(), this.$blocker = null, i(), o.modal.isActive() || this.$body.css("overflow", ""))
		}, show: function () {
			this.$elm.trigger(o.modal.BEFORE_OPEN, [this._ctx()]), this.options.showClose && (this.closeButton = o('<a href="#close-modal" rel="modal:close" class="close-modal ' + this.options.closeClass + '">' + this.options.closeText + "</a>"), this.$elm.append(this.closeButton)), this.$elm.addClass(this.options.modalClass).appendTo(this.$blocker), this.options.doFade ? this.$elm.css({
				opacity: 0,
				display: "inline-block"
			}).animate({opacity: 1}, this.options.fadeDuration) : this.$elm.css("display", "inline-block"), this.$elm.trigger(o.modal.OPEN, [this._ctx()])
		}, hide: function () {
			this.$elm.trigger(o.modal.BEFORE_CLOSE, [this._ctx()]), this.closeButton && this.closeButton.remove();
			var e = this;
			this.options.doFade ? this.$elm.fadeOut(this.options.fadeDuration, function () {
				e.$elm.trigger(o.modal.AFTER_CLOSE, [e._ctx()])
			}) : this.$elm.hide(0, function () {
				e.$elm.trigger(o.modal.AFTER_CLOSE, [e._ctx()])
			}), this.$elm.trigger(o.modal.CLOSE, [this._ctx()])
		}, showSpinner: function () {
			this.options.showSpinner && (this.spinner = this.spinner || o('<div class="' + this.options.modalClass + '-spinner"></div>').append(this.options.spinnerHtml), this.$body.append(this.spinner), this.spinner.show())
		}, hideSpinner: function () {
			this.spinner && this.spinner.remove()
		}, _ctx: function () {
			return {elm: this.$elm, $elm: this.$elm, $blocker: this.$blocker, options: this.options}
		}
	}, o.modal.close = function (e) {
		if (o.modal.isActive()) {
			e && e.preventDefault();
			var t = r();
			return t.close(), t.$elm
		}
	}, o.modal.isActive = function () {
		return 0 < s.length
	}, o.modal.getCurrent = r, o.modal.defaults = {
		closeExisting: !0,
		escapeClose: !0,
		clickClose: !0,
		closeText: "Close",
		closeClass: "",
		modalClass: "modal",
		blockerClass: "jquery-modal",
		spinnerHtml: '<div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div>',
		showSpinner: !0,
		showClose: !0,
		fadeDuration: null,
		fadeDelay: 1
	}, o.modal.BEFORE_BLOCK = "modal:before-block", o.modal.BLOCK = "modal:block", o.modal.BEFORE_OPEN = "modal:before-open", o.modal.OPEN = "modal:open", o.modal.BEFORE_CLOSE = "modal:before-close", o.modal.CLOSE = "modal:close", o.modal.AFTER_CLOSE = "modal:after-close", o.modal.AJAX_SEND = "modal:ajax:send", o.modal.AJAX_SUCCESS = "modal:ajax:success", o.modal.AJAX_FAIL = "modal:ajax:fail", o.modal.AJAX_COMPLETE = "modal:ajax:complete", o.fn.modal = function (e) {
		return 1 === this.length && new o.modal(this, e), this
	}, o(t).on("click.modal", 'a[rel~="modal:close"]', o.modal.close), o(t).on("click.modal", 'a[rel~="modal:open"]', function (e) {
		e.preventDefault(), o(this).modal()
	})
}), function (e) {
	"use strict";
	"function" == typeof define && define.amd ? define(["jquery"], e) : "undefined" != typeof exports ? module.exports = e(require("jquery")) : e(jQuery)
}(function (c) {
	"use strict";
	var o, r = window.Slick || {};
	(o = 0, r = function (e, t) {
		var n, i = this;
		i.defaults = {
			accessibility: !0,
			adaptiveHeight: !1,
			appendArrows: c(e),
			appendDots: c(e),
			arrows: !0,
			asNavFor: null,
			prevArrow: '<button class="slick-prev" aria-label="Previous" type="button">Previous</button>',
			nextArrow: '<button class="slick-next" aria-label="Next" type="button">Next</button>',
			autoplay: !1,
			autoplaySpeed: 3e3,
			centerMode: !1,
			centerPadding: "50px",
			cssEase: "ease",
			customPaging: function (e, t) {
				return c('<button type="button" />').text(t + 1)
			},
			dots: !1,
			dotsClass: "slick-dots",
			draggable: !0,
			easing: "linear",
			edgeFriction: .35,
			fade: !1,
			focusOnSelect: !1,
			focusOnChange: !1,
			infinite: !0,
			initialSlide: 0,
			lazyLoad: "ondemand",
			mobileFirst: !1,
			pauseOnHover: !0,
			pauseOnFocus: !0,
			pauseOnDotsHover: !1,
			respondTo: "window",
			responsive: null,
			rows: 1,
			rtl: !1,
			slide: "",
			slidesPerRow: 1,
			slidesToShow: 1,
			slidesToScroll: 1,
			speed: 500,
			swipe: !0,
			swipeToSlide: !1,
			touchMove: !0,
			touchThreshold: 5,
			useCSS: !0,
			useTransform: !0,
			variableWidth: !1,
			vertical: !1,
			verticalSwiping: !1,
			waitForAnimate: !0,
			zIndex: 1e3
		}, i.initials = {
			animating: !1,
			dragging: !1,
			autoPlayTimer: null,
			currentDirection: 0,
			currentLeft: null,
			currentSlide: 0,
			direction: 1,
			$dots: null,
			listWidth: null,
			listHeight: null,
			loadIndex: 0,
			$nextArrow: null,
			$prevArrow: null,
			scrolling: !1,
			slideCount: null,
			slideWidth: null,
			$slideTrack: null,
			$slides: null,
			sliding: !1,
			slideOffset: 0,
			swipeLeft: null,
			swiping: !1,
			$list: null,
			touchObject: {},
			transformsEnabled: !1,
			unslicked: !1
		}, c.extend(i, i.initials), i.activeBreakpoint = null, i.animType = null, i.animProp = null, i.breakpoints = [], i.breakpointSettings = [], i.cssTransitions = !1, i.focussed = !1, i.interrupted = !1, i.hidden = "hidden", i.paused = !0, i.positionProp = null, i.respondTo = null, i.rowCount = 1, i.shouldClick = !0, i.$slider = c(e), i.$slidesCache = null, i.transformType = null, i.transitionType = null, i.visibilityChange = "visibilitychange", i.windowWidth = 0, i.windowTimer = null, n = c(e).data("slick") || {}, i.options = c.extend({}, i.defaults, t, n), i.currentSlide = i.options.initialSlide, i.originalSettings = i.options, void 0 !== document.mozHidden ? (i.hidden = "mozHidden", i.visibilityChange = "mozvisibilitychange") : void 0 !== document.webkitHidden && (i.hidden = "webkitHidden", i.visibilityChange = "webkitvisibilitychange"), i.autoPlay = c.proxy(i.autoPlay, i), i.autoPlayClear = c.proxy(i.autoPlayClear, i), i.autoPlayIterator = c.proxy(i.autoPlayIterator, i), i.changeSlide = c.proxy(i.changeSlide, i), i.clickHandler = c.proxy(i.clickHandler, i), i.selectHandler = c.proxy(i.selectHandler, i), i.setPosition = c.proxy(i.setPosition, i), i.swipeHandler = c.proxy(i.swipeHandler, i), i.dragHandler = c.proxy(i.dragHandler, i), i.keyHandler = c.proxy(i.keyHandler, i), i.instanceUid = o++, i.htmlExpr = /^(?:\s*(<[\w\W]+>)[^>]*)$/, i.registerBreakpoints(), i.init(!0)
	}).prototype.activateADA = function () {
		this.$slideTrack.find(".slick-active").attr({"aria-hidden": "false"}).find("a, input, button, select").attr({tabindex: "0"})
	}, r.prototype.addSlide = r.prototype.slickAdd = function (e, t, n) {
		var i = this;
		if ("boolean" == typeof t) n = t, t = null; else if (t < 0 || t >= i.slideCount) return !1;
		i.unload(), "number" == typeof t ? 0 === t && 0 === i.$slides.length ? c(e).appendTo(i.$slideTrack) : n ? c(e).insertBefore(i.$slides.eq(t)) : c(e).insertAfter(i.$slides.eq(t)) : !0 === n ? c(e).prependTo(i.$slideTrack) : c(e).appendTo(i.$slideTrack), i.$slides = i.$slideTrack.children(this.options.slide), i.$slideTrack.children(this.options.slide).detach(), i.$slideTrack.append(i.$slides), i.$slides.each(function (e, t) {
			c(t).attr("data-slick-index", e)
		}), i.$slidesCache = i.$slides, i.reinit()
	}, r.prototype.animateHeight = function () {
		if (1 === this.options.slidesToShow && !0 === this.options.adaptiveHeight && !1 === this.options.vertical) {
			var e = this.$slides.eq(this.currentSlide).outerHeight(!0);
			this.$list.animate({height: e}, this.options.speed)
		}
	}, r.prototype.animateSlide = function (e, t) {
		var n = {}, i = this;
		i.animateHeight(), !0 === i.options.rtl && !1 === i.options.vertical && (e = -e), !1 === i.transformsEnabled ? !1 === i.options.vertical ? i.$slideTrack.animate({left: e}, i.options.speed, i.options.easing, t) : i.$slideTrack.animate({top: e}, i.options.speed, i.options.easing, t) : !1 === i.cssTransitions ? (!0 === i.options.rtl && (i.currentLeft = -i.currentLeft), c({animStart: i.currentLeft}).animate({animStart: e}, {
			duration: i.options.speed,
			easing: i.options.easing,
			step: function (e) {
				e = Math.ceil(e), !1 === i.options.vertical ? n[i.animType] = "translate(" + e + "px, 0px)" : n[i.animType] = "translate(0px," + e + "px)", i.$slideTrack.css(n)
			},
			complete: function () {
				t && t.call()
			}
		})) : (i.applyTransition(), e = Math.ceil(e), !1 === i.options.vertical ? n[i.animType] = "translate3d(" + e + "px, 0px, 0px)" : n[i.animType] = "translate3d(0px," + e + "px, 0px)", i.$slideTrack.css(n), t && setTimeout(function () {
			i.disableTransition(), t.call()
		}, i.options.speed))
	}, r.prototype.getNavTarget = function () {
		var e = this.options.asNavFor;
		return e && null !== e && (e = c(e).not(this.$slider)), e
	}, r.prototype.asNavFor = function (t) {
		var e = this.getNavTarget();
		null !== e && "object" == typeof e && e.each(function () {
			var e = c(this).slick("getSlick");
			e.unslicked || e.slideHandler(t, !0)
		})
	}, r.prototype.applyTransition = function (e) {
		var t = this, n = {};
		!1 === t.options.fade ? n[t.transitionType] = t.transformType + " " + t.options.speed + "ms " + t.options.cssEase : n[t.transitionType] = "opacity " + t.options.speed + "ms " + t.options.cssEase, !1 === t.options.fade ? t.$slideTrack.css(n) : t.$slides.eq(e).css(n)
	}, r.prototype.autoPlay = function () {
		this.autoPlayClear(), this.slideCount > this.options.slidesToShow && (this.autoPlayTimer = setInterval(this.autoPlayIterator, this.options.autoplaySpeed))
	}, r.prototype.autoPlayClear = function () {
		this.autoPlayTimer && clearInterval(this.autoPlayTimer)
	}, r.prototype.autoPlayIterator = function () {
		var e = this, t = e.currentSlide + e.options.slidesToScroll;
		e.paused || e.interrupted || e.focussed || (!1 === e.options.infinite && (1 === e.direction && e.currentSlide + 1 === e.slideCount - 1 ? e.direction = 0 : 0 === e.direction && (t = e.currentSlide - e.options.slidesToScroll, e.currentSlide - 1 == 0 && (e.direction = 1))), e.slideHandler(t))
	}, r.prototype.buildArrows = function () {
		var e = this;
		!0 === e.options.arrows && (e.$prevArrow = c(e.options.prevArrow).addClass("slick-arrow"), e.$nextArrow = c(e.options.nextArrow).addClass("slick-arrow"), e.slideCount > e.options.slidesToShow ? (e.$prevArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"), e.$nextArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"), e.htmlExpr.test(e.options.prevArrow) && e.$prevArrow.prependTo(e.options.appendArrows), e.htmlExpr.test(e.options.nextArrow) && e.$nextArrow.appendTo(e.options.appendArrows), !0 !== e.options.infinite && e.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true")) : e.$prevArrow.add(e.$nextArrow).addClass("slick-hidden").attr({
			"aria-disabled": "true",
			tabindex: "-1"
		}))
	}, r.prototype.buildDots = function () {
		var e, t;
		if (!0 === this.options.dots) {
			for (this.$slider.addClass("slick-dotted"), t = c("<ul />").addClass(this.options.dotsClass), e = 0; e <= this.getDotCount(); e += 1) t.append(c("<li />").append(this.options.customPaging.call(this, this, e)));
			this.$dots = t.appendTo(this.options.appendDots), this.$dots.find("li").first().addClass("slick-active")
		}
	}, r.prototype.buildOut = function () {
		var e = this;
		e.$slides = e.$slider.children(e.options.slide + ":not(.slick-cloned)").addClass("slick-slide"), e.slideCount = e.$slides.length, e.$slides.each(function (e, t) {
			c(t).attr("data-slick-index", e).data("originalStyling", c(t).attr("style") || "")
		}), e.$slider.addClass("slick-slider"), e.$slideTrack = 0 === e.slideCount ? c('<div class="slick-track"/>').appendTo(e.$slider) : e.$slides.wrapAll('<div class="slick-track"/>').parent(), e.$list = e.$slideTrack.wrap('<div class="slick-list"/>').parent(), e.$slideTrack.css("opacity", 0), !0 !== e.options.centerMode && !0 !== e.options.swipeToSlide || (e.options.slidesToScroll = 1), c("img[data-lazy]", e.$slider).not("[src]").addClass("slick-loading"), e.setupInfinite(), e.buildArrows(), e.buildDots(), e.updateDots(), e.setSlideClasses("number" == typeof e.currentSlide ? e.currentSlide : 0), !0 === e.options.draggable && e.$list.addClass("draggable")
	}, r.prototype.buildRows = function () {
		var e, t, n, i, o, r, s, a = this;
		if (i = document.createDocumentFragment(), r = a.$slider.children(), 1 < a.options.rows) {
			for (s = a.options.slidesPerRow * a.options.rows, o = Math.ceil(r.length / s), e = 0; e < o; e++) {
				var l = document.createElement("div");
				for (t = 0; t < a.options.rows; t++) {
					var c = document.createElement("div");
					for (n = 0; n < a.options.slidesPerRow; n++) {
						var u = e * s + (t * a.options.slidesPerRow + n);
						r.get(u) && c.appendChild(r.get(u))
					}
					l.appendChild(c)
				}
				i.appendChild(l)
			}
			a.$slider.empty().append(i), a.$slider.children().children().children().css({
				width: 100 / a.options.slidesPerRow + "%",
				display: "inline-block"
			})
		}
	}, r.prototype.checkResponsive = function (e, t) {
		var n, i, o, r = this, s = !1, a = r.$slider.width(), l = window.innerWidth || c(window).width();
		if ("window" === r.respondTo ? o = l : "slider" === r.respondTo ? o = a : "min" === r.respondTo && (o = Math.min(l, a)), r.options.responsive && r.options.responsive.length && null !== r.options.responsive) {
			for (n in i = null, r.breakpoints) r.breakpoints.hasOwnProperty(n) && (!1 === r.originalSettings.mobileFirst ? o < r.breakpoints[n] && (i = r.breakpoints[n]) : o > r.breakpoints[n] && (i = r.breakpoints[n]));
			null !== i ? null !== r.activeBreakpoint ? i === r.activeBreakpoint && !t || (r.activeBreakpoint = i, "unslick" === r.breakpointSettings[i] ? r.unslick(i) : (r.options = c.extend({}, r.originalSettings, r.breakpointSettings[i]), !0 === e && (r.currentSlide = r.options.initialSlide), r.refresh(e)), s = i) : (r.activeBreakpoint = i, "unslick" === r.breakpointSettings[i] ? r.unslick(i) : (r.options = c.extend({}, r.originalSettings, r.breakpointSettings[i]), !0 === e && (r.currentSlide = r.options.initialSlide), r.refresh(e)), s = i) : null !== r.activeBreakpoint && (r.activeBreakpoint = null, r.options = r.originalSettings, !0 === e && (r.currentSlide = r.options.initialSlide), r.refresh(e), s = i), e || !1 === s || r.$slider.trigger("breakpoint", [r, s])
		}
	}, r.prototype.changeSlide = function (e, t) {
		var n, i, o = this, r = c(e.currentTarget);
		switch (r.is("a") && e.preventDefault(), r.is("li") || (r = r.closest("li")), n = o.slideCount % o.options.slidesToScroll != 0 ? 0 : (o.slideCount - o.currentSlide) % o.options.slidesToScroll, e.data.message) {
			case"previous":
				i = 0 == n ? o.options.slidesToScroll : o.options.slidesToShow - n, o.slideCount > o.options.slidesToShow && o.slideHandler(o.currentSlide - i, !1, t);
				break;
			case"next":
				i = 0 == n ? o.options.slidesToScroll : n, o.slideCount > o.options.slidesToShow && o.slideHandler(o.currentSlide + i, !1, t);
				break;
			case"index":
				var s = 0 === e.data.index ? 0 : e.data.index || r.index() * o.options.slidesToScroll;
				o.slideHandler(o.checkNavigable(s), !1, t), r.children().trigger("focus");
				break;
			default:
				return
		}
	}, r.prototype.checkNavigable = function (e) {
		var t, n;
		if (n = 0, e > (t = this.getNavigableIndexes())[t.length - 1]) e = t[t.length - 1]; else for (var i in t) {
			if (e < t[i]) {
				e = n;
				break
			}
			n = t[i]
		}
		return e
	}, r.prototype.cleanUpEvents = function () {
		var e = this;
		e.options.dots && null !== e.$dots && (c("li", e.$dots).off("click.slick", e.changeSlide).off("mouseenter.slick", c.proxy(e.interrupt, e, !0)).off("mouseleave.slick", c.proxy(e.interrupt, e, !1)), !0 === e.options.accessibility && e.$dots.off("keydown.slick", e.keyHandler)), e.$slider.off("focus.slick blur.slick"), !0 === e.options.arrows && e.slideCount > e.options.slidesToShow && (e.$prevArrow && e.$prevArrow.off("click.slick", e.changeSlide), e.$nextArrow && e.$nextArrow.off("click.slick", e.changeSlide), !0 === e.options.accessibility && (e.$prevArrow && e.$prevArrow.off("keydown.slick", e.keyHandler), e.$nextArrow && e.$nextArrow.off("keydown.slick", e.keyHandler))), e.$list.off("touchstart.slick mousedown.slick", e.swipeHandler), e.$list.off("touchmove.slick mousemove.slick", e.swipeHandler), e.$list.off("touchend.slick mouseup.slick", e.swipeHandler), e.$list.off("touchcancel.slick mouseleave.slick", e.swipeHandler), e.$list.off("click.slick", e.clickHandler), c(document).off(e.visibilityChange, e.visibility), e.cleanUpSlideEvents(), !0 === e.options.accessibility && e.$list.off("keydown.slick", e.keyHandler), !0 === e.options.focusOnSelect && c(e.$slideTrack).children().off("click.slick", e.selectHandler), c(window).off("orientationchange.slick.slick-" + e.instanceUid, e.orientationChange), c(window).off("resize.slick.slick-" + e.instanceUid, e.resize), c("[draggable!=true]", e.$slideTrack).off("dragstart", e.preventDefault), c(window).off("load.slick.slick-" + e.instanceUid, e.setPosition)
	}, r.prototype.cleanUpSlideEvents = function () {
		this.$list.off("mouseenter.slick", c.proxy(this.interrupt, this, !0)), this.$list.off("mouseleave.slick", c.proxy(this.interrupt, this, !1))
	}, r.prototype.cleanUpRows = function () {
		var e;
		1 < this.options.rows && ((e = this.$slides.children().children()).removeAttr("style"), this.$slider.empty().append(e))
	}, r.prototype.clickHandler = function (e) {
		!1 === this.shouldClick && (e.stopImmediatePropagation(), e.stopPropagation(), e.preventDefault())
	}, r.prototype.destroy = function (e) {
		var t = this;
		t.autoPlayClear(), t.touchObject = {}, t.cleanUpEvents(), c(".slick-cloned", t.$slider).detach(), t.$dots && t.$dots.remove(), t.$prevArrow && t.$prevArrow.length && (t.$prevArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""), t.htmlExpr.test(t.options.prevArrow) && t.$prevArrow.remove()), t.$nextArrow && t.$nextArrow.length && (t.$nextArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""), t.htmlExpr.test(t.options.nextArrow) && t.$nextArrow.remove()), t.$slides && (t.$slides.removeClass("slick-slide slick-active slick-center slick-visible slick-current").removeAttr("aria-hidden").removeAttr("data-slick-index").each(function () {
			c(this).attr("style", c(this).data("originalStyling"))
		}), t.$slideTrack.children(this.options.slide).detach(), t.$slideTrack.detach(), t.$list.detach(), t.$slider.append(t.$slides)), t.cleanUpRows(), t.$slider.removeClass("slick-slider"), t.$slider.removeClass("slick-initialized"), t.$slider.removeClass("slick-dotted"), t.unslicked = !0, e || t.$slider.trigger("destroy", [t])
	}, r.prototype.disableTransition = function (e) {
		var t = {};
		t[this.transitionType] = "", !1 === this.options.fade ? this.$slideTrack.css(t) : this.$slides.eq(e).css(t)
	}, r.prototype.fadeSlide = function (e, t) {
		var n = this;
		!1 === n.cssTransitions ? (n.$slides.eq(e).css({zIndex: n.options.zIndex}), n.$slides.eq(e).animate({opacity: 1}, n.options.speed, n.options.easing, t)) : (n.applyTransition(e), n.$slides.eq(e).css({
			opacity: 1,
			zIndex: n.options.zIndex
		}), t && setTimeout(function () {
			n.disableTransition(e), t.call()
		}, n.options.speed))
	}, r.prototype.fadeSlideOut = function (e) {
		!1 === this.cssTransitions ? this.$slides.eq(e).animate({
			opacity: 0,
			zIndex: this.options.zIndex - 2
		}, this.options.speed, this.options.easing) : (this.applyTransition(e), this.$slides.eq(e).css({
			opacity: 0,
			zIndex: this.options.zIndex - 2
		}))
	}, r.prototype.filterSlides = r.prototype.slickFilter = function (e) {
		null !== e && (this.$slidesCache = this.$slides, this.unload(), this.$slideTrack.children(this.options.slide).detach(), this.$slidesCache.filter(e).appendTo(this.$slideTrack), this.reinit())
	}, r.prototype.focusHandler = function () {
		var n = this;
		n.$slider.off("focus.slick blur.slick").on("focus.slick blur.slick", "*", function (e) {
			e.stopImmediatePropagation();
			var t = c(this);
			setTimeout(function () {
				n.options.pauseOnFocus && (n.focussed = t.is(":focus"), n.autoPlay())
			}, 0)
		})
	}, r.prototype.getCurrent = r.prototype.slickCurrentSlide = function () {
		return this.currentSlide
	}, r.prototype.getDotCount = function () {
		var e = this, t = 0, n = 0, i = 0;
		if (!0 === e.options.infinite) if (e.slideCount <= e.options.slidesToShow) ++i; else for (; t < e.slideCount;) ++i, t = n + e.options.slidesToScroll, n += e.options.slidesToScroll <= e.options.slidesToShow ? e.options.slidesToScroll : e.options.slidesToShow; else if (!0 === e.options.centerMode) i = e.slideCount; else if (e.options.asNavFor) for (; t < e.slideCount;) ++i, t = n + e.options.slidesToScroll, n += e.options.slidesToScroll <= e.options.slidesToShow ? e.options.slidesToScroll : e.options.slidesToShow; else i = 1 + Math.ceil((e.slideCount - e.options.slidesToShow) / e.options.slidesToScroll);
		return i - 1
	}, r.prototype.getLeft = function (e) {
		var t, n, i, o, r = this, s = 0;
		return r.slideOffset = 0, n = r.$slides.first().outerHeight(!0), !0 === r.options.infinite ? (r.slideCount > r.options.slidesToShow && (r.slideOffset = r.slideWidth * r.options.slidesToShow * -1, o = -1, !0 === r.options.vertical && !0 === r.options.centerMode && (2 === r.options.slidesToShow ? o = -1.5 : 1 === r.options.slidesToShow && (o = -2)), s = n * r.options.slidesToShow * o), r.slideCount % r.options.slidesToScroll != 0 && e + r.options.slidesToScroll > r.slideCount && r.slideCount > r.options.slidesToShow && (s = e > r.slideCount ? (r.slideOffset = (r.options.slidesToShow - (e - r.slideCount)) * r.slideWidth * -1, (r.options.slidesToShow - (e - r.slideCount)) * n * -1) : (r.slideOffset = r.slideCount % r.options.slidesToScroll * r.slideWidth * -1, r.slideCount % r.options.slidesToScroll * n * -1))) : e + r.options.slidesToShow > r.slideCount && (r.slideOffset = (e + r.options.slidesToShow - r.slideCount) * r.slideWidth, s = (e + r.options.slidesToShow - r.slideCount) * n), r.slideCount <= r.options.slidesToShow && (s = r.slideOffset = 0), !0 === r.options.centerMode && r.slideCount <= r.options.slidesToShow ? r.slideOffset = r.slideWidth * Math.floor(r.options.slidesToShow) / 2 - r.slideWidth * r.slideCount / 2 : !0 === r.options.centerMode && !0 === r.options.infinite ? r.slideOffset += r.slideWidth * Math.floor(r.options.slidesToShow / 2) - r.slideWidth : !0 === r.options.centerMode && (r.slideOffset = 0, r.slideOffset += r.slideWidth * Math.floor(r.options.slidesToShow / 2)), t = !1 === r.options.vertical ? e * r.slideWidth * -1 + r.slideOffset : e * n * -1 + s, !0 === r.options.variableWidth && (i = r.slideCount <= r.options.slidesToShow || !1 === r.options.infinite ? r.$slideTrack.children(".slick-slide").eq(e) : r.$slideTrack.children(".slick-slide").eq(e + r.options.slidesToShow), t = !0 === r.options.rtl ? i[0] ? -1 * (r.$slideTrack.width() - i[0].offsetLeft - i.width()) : 0 : i[0] ? -1 * i[0].offsetLeft : 0, !0 === r.options.centerMode && (i = r.slideCount <= r.options.slidesToShow || !1 === r.options.infinite ? r.$slideTrack.children(".slick-slide").eq(e) : r.$slideTrack.children(".slick-slide").eq(e + r.options.slidesToShow + 1), t = !0 === r.options.rtl ? i[0] ? -1 * (r.$slideTrack.width() - i[0].offsetLeft - i.width()) : 0 : i[0] ? -1 * i[0].offsetLeft : 0, t += (r.$list.width() - i.outerWidth()) / 2)), t
	}, r.prototype.getOption = r.prototype.slickGetOption = function (e) {
		return this.options[e]
	}, r.prototype.getNavigableIndexes = function () {
		var e, t = this, n = 0, i = 0, o = [];
		for (e = !1 === t.options.infinite ? t.slideCount : (n = -1 * t.options.slidesToScroll, i = -1 * t.options.slidesToScroll, 2 * t.slideCount); n < e;) o.push(n), n = i + t.options.slidesToScroll, i += t.options.slidesToScroll <= t.options.slidesToShow ? t.options.slidesToScroll : t.options.slidesToShow;
		return o
	}, r.prototype.getSlick = function () {
		return this
	}, r.prototype.getSlideCount = function () {
		var n, i, o = this;
		return i = !0 === o.options.centerMode ? o.slideWidth * Math.floor(o.options.slidesToShow / 2) : 0, !0 === o.options.swipeToSlide ? (o.$slideTrack.find(".slick-slide").each(function (e, t) {
			if (t.offsetLeft - i + c(t).outerWidth() / 2 > -1 * o.swipeLeft) return n = t, !1
		}), Math.abs(c(n).attr("data-slick-index") - o.currentSlide) || 1) : o.options.slidesToScroll
	}, r.prototype.goTo = r.prototype.slickGoTo = function (e, t) {
		this.changeSlide({data: {message: "index", index: parseInt(e)}}, t)
	}, r.prototype.init = function (e) {
		var t = this;
		c(t.$slider).hasClass("slick-initialized") || (c(t.$slider).addClass("slick-initialized"), t.buildRows(), t.buildOut(), t.setProps(), t.startLoad(), t.loadSlider(), t.initializeEvents(), t.updateArrows(), t.updateDots(), t.checkResponsive(!0), t.focusHandler()), e && t.$slider.trigger("init", [t]), !0 === t.options.accessibility && t.initADA(), t.options.autoplay && (t.paused = !1, t.autoPlay())
	}, r.prototype.initADA = function () {
		var n = this, i = Math.ceil(n.slideCount / n.options.slidesToShow),
			o = n.getNavigableIndexes().filter(function (e) {
				return 0 <= e && e < n.slideCount
			});
		n.$slides.add(n.$slideTrack.find(".slick-cloned")).attr({
			"aria-hidden": "true",
			tabindex: "-1"
		}).find("a, input, button, select").attr({tabindex: "-1"}), null !== n.$dots && (n.$slides.not(n.$slideTrack.find(".slick-cloned")).each(function (e) {
			var t = o.indexOf(e);
			c(this).attr({
				role: "tabpanel",
				id: "slick-slide" + n.instanceUid + e,
				tabindex: -1
			}), -1 !== t && c(this).attr({"aria-describedby": "slick-slide-control" + n.instanceUid + t})
		}), n.$dots.attr("role", "tablist").find("li").each(function (e) {
			var t = o[e];
			c(this).attr({role: "presentation"}), c(this).find("button").first().attr({
				role: "tab",
				id: "slick-slide-control" + n.instanceUid + e,
				"aria-controls": "slick-slide" + n.instanceUid + t,
				"aria-label": e + 1 + " of " + i,
				"aria-selected": null,
				tabindex: "-1"
			})
		}).eq(n.currentSlide).find("button").attr({"aria-selected": "true", tabindex: "0"}).end());
		for (var e = n.currentSlide, t = e + n.options.slidesToShow; e < t; e++) n.$slides.eq(e).attr("tabindex", 0);
		n.activateADA()
	}, r.prototype.initArrowEvents = function () {
		var e = this;
		!0 === e.options.arrows && e.slideCount > e.options.slidesToShow && (e.$prevArrow.off("click.slick").on("click.slick", {message: "previous"}, e.changeSlide), e.$nextArrow.off("click.slick").on("click.slick", {message: "next"}, e.changeSlide), !0 === e.options.accessibility && (e.$prevArrow.on("keydown.slick", e.keyHandler), e.$nextArrow.on("keydown.slick", e.keyHandler)))
	}, r.prototype.initDotEvents = function () {
		var e = this;
		!0 === e.options.dots && (c("li", e.$dots).on("click.slick", {message: "index"}, e.changeSlide), !0 === e.options.accessibility && e.$dots.on("keydown.slick", e.keyHandler)), !0 === e.options.dots && !0 === e.options.pauseOnDotsHover && c("li", e.$dots).on("mouseenter.slick", c.proxy(e.interrupt, e, !0)).on("mouseleave.slick", c.proxy(e.interrupt, e, !1))
	}, r.prototype.initSlideEvents = function () {
		this.options.pauseOnHover && (this.$list.on("mouseenter.slick", c.proxy(this.interrupt, this, !0)), this.$list.on("mouseleave.slick", c.proxy(this.interrupt, this, !1)))
	}, r.prototype.initializeEvents = function () {
		var e = this;
		e.initArrowEvents(), e.initDotEvents(), e.initSlideEvents(), e.$list.on("touchstart.slick mousedown.slick", {action: "start"}, e.swipeHandler), e.$list.on("touchmove.slick mousemove.slick", {action: "move"}, e.swipeHandler), e.$list.on("touchend.slick mouseup.slick", {action: "end"}, e.swipeHandler), e.$list.on("touchcancel.slick mouseleave.slick", {action: "end"}, e.swipeHandler), e.$list.on("click.slick", e.clickHandler), c(document).on(e.visibilityChange, c.proxy(e.visibility, e)), !0 === e.options.accessibility && e.$list.on("keydown.slick", e.keyHandler), !0 === e.options.focusOnSelect && c(e.$slideTrack).children().on("click.slick", e.selectHandler), c(window).on("orientationchange.slick.slick-" + e.instanceUid, c.proxy(e.orientationChange, e)), c(window).on("resize.slick.slick-" + e.instanceUid, c.proxy(e.resize, e)), c("[draggable!=true]", e.$slideTrack).on("dragstart", e.preventDefault), c(window).on("load.slick.slick-" + e.instanceUid, e.setPosition), c(e.setPosition)
	}, r.prototype.initUI = function () {
		!0 === this.options.arrows && this.slideCount > this.options.slidesToShow && (this.$prevArrow.show(), this.$nextArrow.show()), !0 === this.options.dots && this.slideCount > this.options.slidesToShow && this.$dots.show()
	}, r.prototype.keyHandler = function (e) {
		e.target.tagName.match("TEXTAREA|INPUT|SELECT") || (37 === e.keyCode && !0 === this.options.accessibility ? this.changeSlide({data: {message: !0 === this.options.rtl ? "next" : "previous"}}) : 39 === e.keyCode && !0 === this.options.accessibility && this.changeSlide({data: {message: !0 === this.options.rtl ? "previous" : "next"}}))
	}, r.prototype.lazyLoad = function () {
		function e(e) {
			c("img[data-lazy]", e).each(function () {
				var e = c(this), t = c(this).attr("data-lazy"), n = c(this).attr("data-srcset"),
					i = c(this).attr("data-sizes") || r.$slider.attr("data-sizes"), o = document.createElement("img");
				o.onload = function () {
					e.animate({opacity: 0}, 100, function () {
						n && (e.attr("srcset", n), i && e.attr("sizes", i)), e.attr("src", t).animate({opacity: 1}, 200, function () {
							e.removeAttr("data-lazy data-srcset data-sizes").removeClass("slick-loading")
						}), r.$slider.trigger("lazyLoaded", [r, e, t])
					})
				}, o.onerror = function () {
					e.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"), r.$slider.trigger("lazyLoadError", [r, e, t])
				}, o.src = t
			})
		}

		var t, n, i, r = this;
		if (!0 === r.options.centerMode ? i = !0 === r.options.infinite ? (n = r.currentSlide + (r.options.slidesToShow / 2 + 1)) + r.options.slidesToShow + 2 : (n = Math.max(0, r.currentSlide - (r.options.slidesToShow / 2 + 1)), r.options.slidesToShow / 2 + 1 + 2 + r.currentSlide) : (n = r.options.infinite ? r.options.slidesToShow + r.currentSlide : r.currentSlide, i = Math.ceil(n + r.options.slidesToShow), !0 === r.options.fade && (0 < n && n--, i <= r.slideCount && i++)), t = r.$slider.find(".slick-slide").slice(n, i), "anticipated" === r.options.lazyLoad) for (var o = n - 1, s = i, a = r.$slider.find(".slick-slide"), l = 0; l < r.options.slidesToScroll; l++) o < 0 && (o = r.slideCount - 1), t = (t = t.add(a.eq(o))).add(a.eq(s)), o--, s++;
		e(t), r.slideCount <= r.options.slidesToShow ? e(r.$slider.find(".slick-slide")) : r.currentSlide >= r.slideCount - r.options.slidesToShow ? e(r.$slider.find(".slick-cloned").slice(0, r.options.slidesToShow)) : 0 === r.currentSlide && e(r.$slider.find(".slick-cloned").slice(-1 * r.options.slidesToShow))
	}, r.prototype.loadSlider = function () {
		this.setPosition(), this.$slideTrack.css({opacity: 1}), this.$slider.removeClass("slick-loading"), this.initUI(), "progressive" === this.options.lazyLoad && this.progressiveLazyLoad()
	}, r.prototype.next = r.prototype.slickNext = function () {
		this.changeSlide({data: {message: "next"}})
	}, r.prototype.orientationChange = function () {
		this.checkResponsive(), this.setPosition()
	}, r.prototype.pause = r.prototype.slickPause = function () {
		this.autoPlayClear(), this.paused = !0
	}, r.prototype.play = r.prototype.slickPlay = function () {
		this.autoPlay(), this.options.autoplay = !0, this.paused = !1, this.focussed = !1, this.interrupted = !1
	}, r.prototype.postSlide = function (e) {
		var t = this;
		t.unslicked || (t.$slider.trigger("afterChange", [t, e]), t.animating = !1, t.slideCount > t.options.slidesToShow && t.setPosition(), t.swipeLeft = null, t.options.autoplay && t.autoPlay(), !0 === t.options.accessibility && (t.initADA(), t.options.focusOnChange && c(t.$slides.get(t.currentSlide)).attr("tabindex", 0).focus()))
	}, r.prototype.prev = r.prototype.slickPrev = function () {
		this.changeSlide({data: {message: "previous"}})
	}, r.prototype.preventDefault = function (e) {
		e.preventDefault()
	}, r.prototype.progressiveLazyLoad = function (e) {
		e = e || 1;
		var t, n, i, o, r, s = this, a = c("img[data-lazy]", s.$slider);
		a.length ? (t = a.first(), n = t.attr("data-lazy"), i = t.attr("data-srcset"), o = t.attr("data-sizes") || s.$slider.attr("data-sizes"), (r = document.createElement("img")).onload = function () {
			i && (t.attr("srcset", i), o && t.attr("sizes", o)), t.attr("src", n).removeAttr("data-lazy data-srcset data-sizes").removeClass("slick-loading"), !0 === s.options.adaptiveHeight && s.setPosition(), s.$slider.trigger("lazyLoaded", [s, t, n]), s.progressiveLazyLoad()
		}, r.onerror = function () {
			e < 3 ? setTimeout(function () {
				s.progressiveLazyLoad(e + 1)
			}, 500) : (t.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"), s.$slider.trigger("lazyLoadError", [s, t, n]), s.progressiveLazyLoad())
		}, r.src = n) : s.$slider.trigger("allImagesLoaded", [s])
	}, r.prototype.refresh = function (e) {
		var t, n, i = this;
		n = i.slideCount - i.options.slidesToShow, !i.options.infinite && i.currentSlide > n && (i.currentSlide = n), i.slideCount <= i.options.slidesToShow && (i.currentSlide = 0), t = i.currentSlide, i.destroy(!0), c.extend(i, i.initials, {currentSlide: t}), i.init(), e || i.changeSlide({
			data: {
				message: "index",
				index: t
			}
		}, !1)
	}, r.prototype.registerBreakpoints = function () {
		var e, t, n, i = this, o = i.options.responsive || null;
		if ("array" === c.type(o) && o.length) {
			for (e in i.respondTo = i.options.respondTo || "window", o) if (n = i.breakpoints.length - 1, o.hasOwnProperty(e)) {
				for (t = o[e].breakpoint; 0 <= n;) i.breakpoints[n] && i.breakpoints[n] === t && i.breakpoints.splice(n, 1), n--;
				i.breakpoints.push(t), i.breakpointSettings[t] = o[e].settings
			}
			i.breakpoints.sort(function (e, t) {
				return i.options.mobileFirst ? e - t : t - e
			})
		}
	}, r.prototype.reinit = function () {
		var e = this;
		e.$slides = e.$slideTrack.children(e.options.slide).addClass("slick-slide"), e.slideCount = e.$slides.length, e.currentSlide >= e.slideCount && 0 !== e.currentSlide && (e.currentSlide = e.currentSlide - e.options.slidesToScroll), e.slideCount <= e.options.slidesToShow && (e.currentSlide = 0), e.registerBreakpoints(), e.setProps(), e.setupInfinite(), e.buildArrows(), e.updateArrows(), e.initArrowEvents(), e.buildDots(), e.updateDots(), e.initDotEvents(), e.cleanUpSlideEvents(), e.initSlideEvents(), e.checkResponsive(!1, !0), !0 === e.options.focusOnSelect && c(e.$slideTrack).children().on("click.slick", e.selectHandler), e.setSlideClasses("number" == typeof e.currentSlide ? e.currentSlide : 0), e.setPosition(), e.focusHandler(), e.paused = !e.options.autoplay, e.autoPlay(), e.$slider.trigger("reInit", [e])
	}, r.prototype.resize = function () {
		var e = this;
		c(window).width() !== e.windowWidth && (clearTimeout(e.windowDelay), e.windowDelay = window.setTimeout(function () {
			e.windowWidth = c(window).width(), e.checkResponsive(), e.unslicked || e.setPosition()
		}, 50))
	}, r.prototype.removeSlide = r.prototype.slickRemove = function (e, t, n) {
		var i = this;
		if (e = "boolean" == typeof e ? !0 === (t = e) ? 0 : i.slideCount - 1 : !0 === t ? --e : e, i.slideCount < 1 || e < 0 || e > i.slideCount - 1) return !1;
		i.unload(), !0 === n ? i.$slideTrack.children().remove() : i.$slideTrack.children(this.options.slide).eq(e).remove(), i.$slides = i.$slideTrack.children(this.options.slide), i.$slideTrack.children(this.options.slide).detach(), i.$slideTrack.append(i.$slides), i.$slidesCache = i.$slides, i.reinit()
	}, r.prototype.setCSS = function (e) {
		var t, n, i = this, o = {};
		!0 === i.options.rtl && (e = -e), t = "left" == i.positionProp ? Math.ceil(e) + "px" : "0px", n = "top" == i.positionProp ? Math.ceil(e) + "px" : "0px", o[i.positionProp] = e, !1 === i.transformsEnabled || (!(o = {}) === i.cssTransitions ? o[i.animType] = "translate(" + t + ", " + n + ")" : o[i.animType] = "translate3d(" + t + ", " + n + ", 0px)"), i.$slideTrack.css(o)
	}, r.prototype.setDimensions = function () {
		var e = this;
		!1 === e.options.vertical ? !0 === e.options.centerMode && e.$list.css({padding: "0px " + e.options.centerPadding}) : (e.$list.height(e.$slides.first().outerHeight(!0) * e.options.slidesToShow), !0 === e.options.centerMode && e.$list.css({padding: e.options.centerPadding + " 0px"})), e.listWidth = e.$list.width(), e.listHeight = e.$list.height(), !1 === e.options.vertical && !1 === e.options.variableWidth ? (e.slideWidth = Math.ceil(e.listWidth / e.options.slidesToShow), e.$slideTrack.width(Math.ceil(e.slideWidth * e.$slideTrack.children(".slick-slide").length))) : !0 === e.options.variableWidth ? e.$slideTrack.width(5e3 * e.slideCount) : (e.slideWidth = Math.ceil(e.listWidth), e.$slideTrack.height(Math.ceil(e.$slides.first().outerHeight(!0) * e.$slideTrack.children(".slick-slide").length)));
		var t = e.$slides.first().outerWidth(!0) - e.$slides.first().width();
		!1 === e.options.variableWidth && e.$slideTrack.children(".slick-slide").width(e.slideWidth - t)
	}, r.prototype.setFade = function () {
		var n, i = this;
		i.$slides.each(function (e, t) {
			n = i.slideWidth * e * -1, !0 === i.options.rtl ? c(t).css({
				position: "relative",
				right: n,
				top: 0,
				zIndex: i.options.zIndex - 2,
				opacity: 0
			}) : c(t).css({position: "relative", left: n, top: 0, zIndex: i.options.zIndex - 2, opacity: 0})
		}), i.$slides.eq(i.currentSlide).css({zIndex: i.options.zIndex - 1, opacity: 1})
	}, r.prototype.setHeight = function () {
		if (1 === this.options.slidesToShow && !0 === this.options.adaptiveHeight && !1 === this.options.vertical) {
			var e = this.$slides.eq(this.currentSlide).outerHeight(!0);
			this.$list.css("height", e)
		}
	}, r.prototype.setOption = r.prototype.slickSetOption = function () {
		var e, t, n, i, o, r = this, s = !1;
		if ("object" === c.type(arguments[0]) ? (n = arguments[0], s = arguments[1], o = "multiple") : "string" === c.type(arguments[0]) && (i = arguments[1], s = arguments[2], "responsive" === (n = arguments[0]) && "array" === c.type(arguments[1]) ? o = "responsive" : void 0 !== arguments[1] && (o = "single")), "single" === o) r.options[n] = i; else if ("multiple" === o) c.each(n, function (e, t) {
			r.options[e] = t
		}); else if ("responsive" === o) for (t in i) if ("array" !== c.type(r.options.responsive)) r.options.responsive = [i[t]]; else {
			for (e = r.options.responsive.length - 1; 0 <= e;) r.options.responsive[e].breakpoint === i[t].breakpoint && r.options.responsive.splice(e, 1), e--;
			r.options.responsive.push(i[t])
		}
		s && (r.unload(), r.reinit())
	}, r.prototype.setPosition = function () {
		this.setDimensions(), this.setHeight(), !1 === this.options.fade ? this.setCSS(this.getLeft(this.currentSlide)) : this.setFade(), this.$slider.trigger("setPosition", [this])
	}, r.prototype.setProps = function () {
		var e = this, t = document.body.style;
		e.positionProp = !0 === e.options.vertical ? "top" : "left", "top" === e.positionProp ? e.$slider.addClass("slick-vertical") : e.$slider.removeClass("slick-vertical"), void 0 === t.WebkitTransition && void 0 === t.MozTransition && void 0 === t.msTransition || !0 === e.options.useCSS && (e.cssTransitions = !0), e.options.fade && ("number" == typeof e.options.zIndex ? e.options.zIndex < 3 && (e.options.zIndex = 3) : e.options.zIndex = e.defaults.zIndex), void 0 !== t.OTransform && (e.animType = "OTransform", e.transformType = "-o-transform", e.transitionType = "OTransition", void 0 === t.perspectiveProperty && void 0 === t.webkitPerspective && (e.animType = !1)), void 0 !== t.MozTransform && (e.animType = "MozTransform", e.transformType = "-moz-transform", e.transitionType = "MozTransition", void 0 === t.perspectiveProperty && void 0 === t.MozPerspective && (e.animType = !1)), void 0 !== t.webkitTransform && (e.animType = "webkitTransform", e.transformType = "-webkit-transform", e.transitionType = "webkitTransition", void 0 === t.perspectiveProperty && void 0 === t.webkitPerspective && (e.animType = !1)), void 0 !== t.msTransform && (e.animType = "msTransform", e.transformType = "-ms-transform", e.transitionType = "msTransition", void 0 === t.msTransform && (e.animType = !1)), void 0 !== t.transform && !1 !== e.animType && (e.animType = "transform", e.transformType = "transform", e.transitionType = "transition"), e.transformsEnabled = e.options.useTransform && null !== e.animType && !1 !== e.animType
	}, r.prototype.setSlideClasses = function (e) {
		var t, n, i, o, r = this;
		if (n = r.$slider.find(".slick-slide").removeClass("slick-active slick-center slick-current").attr("aria-hidden", "true"), r.$slides.eq(e).addClass("slick-current"), !0 === r.options.centerMode) {
			var s = r.options.slidesToShow % 2 == 0 ? 1 : 0;
			t = Math.floor(r.options.slidesToShow / 2), !0 === r.options.infinite && (t <= e && e <= r.slideCount - 1 - t ? r.$slides.slice(e - t + s, e + t + 1).addClass("slick-active").attr("aria-hidden", "false") : (i = r.options.slidesToShow + e, n.slice(i - t + 1 + s, i + t + 2).addClass("slick-active").attr("aria-hidden", "false")), 0 === e ? n.eq(n.length - 1 - r.options.slidesToShow).addClass("slick-center") : e === r.slideCount - 1 && n.eq(r.options.slidesToShow).addClass("slick-center")), r.$slides.eq(e).addClass("slick-center")
		} else 0 <= e && e <= r.slideCount - r.options.slidesToShow ? r.$slides.slice(e, e + r.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false") : n.length <= r.options.slidesToShow ? n.addClass("slick-active").attr("aria-hidden", "false") : (o = r.slideCount % r.options.slidesToShow, i = !0 === r.options.infinite ? r.options.slidesToShow + e : e, r.options.slidesToShow == r.options.slidesToScroll && r.slideCount - e < r.options.slidesToShow ? n.slice(i - (r.options.slidesToShow - o), i + o).addClass("slick-active").attr("aria-hidden", "false") : n.slice(i, i + r.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false"));
		"ondemand" !== r.options.lazyLoad && "anticipated" !== r.options.lazyLoad || r.lazyLoad()
	}, r.prototype.setupInfinite = function () {
		var e, t, n, i = this;
		if (!0 === i.options.fade && (i.options.centerMode = !1), !0 === i.options.infinite && !1 === i.options.fade && (t = null, i.slideCount > i.options.slidesToShow)) {
			for (n = !0 === i.options.centerMode ? i.options.slidesToShow + 1 : i.options.slidesToShow, e = i.slideCount; e > i.slideCount - n; e -= 1) t = e - 1, c(i.$slides[t]).clone(!0).attr("id", "").attr("data-slick-index", t - i.slideCount).prependTo(i.$slideTrack).addClass("slick-cloned");
			for (e = 0; e < n + i.slideCount; e += 1) t = e, c(i.$slides[t]).clone(!0).attr("id", "").attr("data-slick-index", t + i.slideCount).appendTo(i.$slideTrack).addClass("slick-cloned");
			i.$slideTrack.find(".slick-cloned").find("[id]").each(function () {
				c(this).attr("id", "")
			})
		}
	}, r.prototype.interrupt = function (e) {
		e || this.autoPlay(), this.interrupted = e
	}, r.prototype.selectHandler = function (e) {
		var t = c(e.target).is(".slick-slide") ? c(e.target) : c(e.target).parents(".slick-slide"),
			n = parseInt(t.attr("data-slick-index"));
		n = n || 0, this.slideCount <= this.options.slidesToShow ? this.slideHandler(n, !1, !0) : this.slideHandler(n)
	}, r.prototype.slideHandler = function (e, t, n) {
		var i, o, r, s, a, l = null, c = this;
		if (t = t || !1, !(!0 === c.animating && !0 === c.options.waitForAnimate || !0 === c.options.fade && c.currentSlide === e)) if (!1 === t && c.asNavFor(e), i = e, l = c.getLeft(i), s = c.getLeft(c.currentSlide), c.currentLeft = null === c.swipeLeft ? s : c.swipeLeft, !1 === c.options.infinite && !1 === c.options.centerMode && (e < 0 || e > c.getDotCount() * c.options.slidesToScroll)) !1 === c.options.fade && (i = c.currentSlide, !0 !== n ? c.animateSlide(s, function () {
			c.postSlide(i)
		}) : c.postSlide(i)); else if (!1 === c.options.infinite && !0 === c.options.centerMode && (e < 0 || e > c.slideCount - c.options.slidesToScroll)) !1 === c.options.fade && (i = c.currentSlide, !0 !== n ? c.animateSlide(s, function () {
			c.postSlide(i)
		}) : c.postSlide(i)); else {
			if (c.options.autoplay && clearInterval(c.autoPlayTimer), o = i < 0 ? c.slideCount % c.options.slidesToScroll != 0 ? c.slideCount - c.slideCount % c.options.slidesToScroll : c.slideCount + i : i >= c.slideCount ? c.slideCount % c.options.slidesToScroll != 0 ? 0 : i - c.slideCount : i, c.animating = !0, c.$slider.trigger("beforeChange", [c, c.currentSlide, o]), r = c.currentSlide, c.currentSlide = o, c.setSlideClasses(c.currentSlide), c.options.asNavFor && (a = (a = c.getNavTarget()).slick("getSlick")).slideCount <= a.options.slidesToShow && a.setSlideClasses(c.currentSlide), c.updateDots(), c.updateArrows(), !0 === c.options.fade) return !0 !== n ? (c.fadeSlideOut(r), c.fadeSlide(o, function () {
				c.postSlide(o)
			})) : c.postSlide(o), void c.animateHeight();
			!0 !== n ? c.animateSlide(l, function () {
				c.postSlide(o)
			}) : c.postSlide(o)
		}
	}, r.prototype.startLoad = function () {
		var e = this;
		!0 === e.options.arrows && e.slideCount > e.options.slidesToShow && (e.$prevArrow.hide(), e.$nextArrow.hide()), !0 === e.options.dots && e.slideCount > e.options.slidesToShow && e.$dots.hide(), e.$slider.addClass("slick-loading")
	}, r.prototype.swipeDirection = function () {
		var e, t, n, i;
		return e = this.touchObject.startX - this.touchObject.curX, t = this.touchObject.startY - this.touchObject.curY, n = Math.atan2(t, e), (i = Math.round(180 * n / Math.PI)) < 0 && (i = 360 - Math.abs(i)), i <= 45 && 0 <= i ? !1 === this.options.rtl ? "left" : "right" : i <= 360 && 315 <= i ? !1 === this.options.rtl ? "left" : "right" : 135 <= i && i <= 225 ? !1 === this.options.rtl ? "right" : "left" : !0 === this.options.verticalSwiping ? 35 <= i && i <= 135 ? "down" : "up" : "vertical"
	}, r.prototype.swipeEnd = function (e) {
		var t, n, i = this;
		if (i.dragging = !1, i.swiping = !1, i.scrolling) return i.scrolling = !1;
		if (i.interrupted = !1, i.shouldClick = !(10 < i.touchObject.swipeLength), void 0 === i.touchObject.curX) return !1;
		if (!0 === i.touchObject.edgeHit && i.$slider.trigger("edge", [i, i.swipeDirection()]), i.touchObject.swipeLength >= i.touchObject.minSwipe) {
			switch (n = i.swipeDirection()) {
				case"left":
				case"down":
					t = i.options.swipeToSlide ? i.checkNavigable(i.currentSlide + i.getSlideCount()) : i.currentSlide + i.getSlideCount(), i.currentDirection = 0;
					break;
				case"right":
				case"up":
					t = i.options.swipeToSlide ? i.checkNavigable(i.currentSlide - i.getSlideCount()) : i.currentSlide - i.getSlideCount(), i.currentDirection = 1
			}
			"vertical" != n && (i.slideHandler(t), i.touchObject = {}, i.$slider.trigger("swipe", [i, n]))
		} else i.touchObject.startX !== i.touchObject.curX && (i.slideHandler(i.currentSlide), i.touchObject = {})
	}, r.prototype.swipeHandler = function (e) {
		var t = this;
		if (!(!1 === t.options.swipe || "ontouchend" in document && !1 === t.options.swipe || !1 === t.options.draggable && -1 !== e.type.indexOf("mouse"))) switch (t.touchObject.fingerCount = e.originalEvent && void 0 !== e.originalEvent.touches ? e.originalEvent.touches.length : 1, t.touchObject.minSwipe = t.listWidth / t.options.touchThreshold, !0 === t.options.verticalSwiping && (t.touchObject.minSwipe = t.listHeight / t.options.touchThreshold), e.data.action) {
			case"start":
				t.swipeStart(e);
				break;
			case"move":
				t.swipeMove(e);
				break;
			case"end":
				t.swipeEnd(e)
		}
	}, r.prototype.swipeMove = function (e) {
		var t, n, i, o, r, s, a = this;
		return r = void 0 !== e.originalEvent ? e.originalEvent.touches : null, !(!a.dragging || a.scrolling || r && 1 !== r.length) && (t = a.getLeft(a.currentSlide), a.touchObject.curX = void 0 !== r ? r[0].pageX : e.clientX, a.touchObject.curY = void 0 !== r ? r[0].pageY : e.clientY, a.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(a.touchObject.curX - a.touchObject.startX, 2))), s = Math.round(Math.sqrt(Math.pow(a.touchObject.curY - a.touchObject.startY, 2))), !a.options.verticalSwiping && !a.swiping && 4 < s ? !(a.scrolling = !0) : (!0 === a.options.verticalSwiping && (a.touchObject.swipeLength = s), n = a.swipeDirection(), void 0 !== e.originalEvent && 4 < a.touchObject.swipeLength && (a.swiping = !0, e.preventDefault()), o = (!1 === a.options.rtl ? 1 : -1) * (a.touchObject.curX > a.touchObject.startX ? 1 : -1), !0 === a.options.verticalSwiping && (o = a.touchObject.curY > a.touchObject.startY ? 1 : -1), i = a.touchObject.swipeLength, (a.touchObject.edgeHit = !1) === a.options.infinite && (0 === a.currentSlide && "right" === n || a.currentSlide >= a.getDotCount() && "left" === n) && (i = a.touchObject.swipeLength * a.options.edgeFriction, a.touchObject.edgeHit = !0), !1 === a.options.vertical ? a.swipeLeft = t + i * o : a.swipeLeft = t + i * (a.$list.height() / a.listWidth) * o, !0 === a.options.verticalSwiping && (a.swipeLeft = t + i * o), !0 !== a.options.fade && !1 !== a.options.touchMove && (!0 === a.animating ? (a.swipeLeft = null, !1) : void a.setCSS(a.swipeLeft))))
	}, r.prototype.swipeStart = function (e) {
		var t, n = this;
		if (n.interrupted = !0, 1 !== n.touchObject.fingerCount || n.slideCount <= n.options.slidesToShow) return !(n.touchObject = {});
		void 0 !== e.originalEvent && void 0 !== e.originalEvent.touches && (t = e.originalEvent.touches[0]), n.touchObject.startX = n.touchObject.curX = void 0 !== t ? t.pageX : e.clientX, n.touchObject.startY = n.touchObject.curY = void 0 !== t ? t.pageY : e.clientY, n.dragging = !0
	}, r.prototype.unfilterSlides = r.prototype.slickUnfilter = function () {
		null !== this.$slidesCache && (this.unload(), this.$slideTrack.children(this.options.slide).detach(), this.$slidesCache.appendTo(this.$slideTrack), this.reinit())
	}, r.prototype.unload = function () {
		var e = this;
		c(".slick-cloned", e.$slider).remove(), e.$dots && e.$dots.remove(), e.$prevArrow && e.htmlExpr.test(e.options.prevArrow) && e.$prevArrow.remove(), e.$nextArrow && e.htmlExpr.test(e.options.nextArrow) && e.$nextArrow.remove(), e.$slides.removeClass("slick-slide slick-active slick-visible slick-current").attr("aria-hidden", "true").css("width", "")
	}, r.prototype.unslick = function (e) {
		this.$slider.trigger("unslick", [this, e]), this.destroy()
	}, r.prototype.updateArrows = function () {
		var e = this;
		Math.floor(e.options.slidesToShow / 2), !0 === e.options.arrows && e.slideCount > e.options.slidesToShow && !e.options.infinite && (e.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false"), e.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false"), 0 === e.currentSlide ? (e.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true"), e.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : e.currentSlide >= e.slideCount - e.options.slidesToShow && !1 === e.options.centerMode ? (e.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"), e.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : e.currentSlide >= e.slideCount - 1 && !0 === e.options.centerMode && (e.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"), e.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")))
	}, r.prototype.updateDots = function () {
		null !== this.$dots && (this.$dots.find("li").removeClass("slick-active").end(), this.$dots.find("li").eq(Math.floor(this.currentSlide / this.options.slidesToScroll)).addClass("slick-active"))
	}, r.prototype.visibility = function () {
		this.options.autoplay && (document[this.hidden] ? this.interrupted = !0 : this.interrupted = !1)
	}, c.fn.slick = function () {
		var e, t, n = arguments[0], i = Array.prototype.slice.call(arguments, 1), o = this.length;
		for (e = 0; e < o; e++) if ("object" == typeof n || void 0 === n ? this[e].slick = new r(this[e], n) : t = this[e].slick[n].apply(this[e].slick, i), void 0 !== t) return t;
		return this
	}
}), function (t, n) {
	"function" == typeof define && define.amd ? define(["jquery"], function (e) {
		return n(t, e)
	}) : "object" == typeof module && "object" == typeof module.exports ? module.exports = n(t, require("jquery")) : t.lity = n(t, t.jQuery || t.Zepto)
}("undefined" != typeof window ? window : this, function (e, u) {
	"use strict";

	function d(e) {
		var t = y();
		return D && e.length ? (e.one(D, t.resolve), setTimeout(t.resolve, 500)) : t.resolve(), t.promise()
	}

	function p(e, t, n) {
		if (1 === arguments.length) return u.extend({}, e);
		if ("string" == typeof t) {
			if (void 0 === n) return void 0 === e[t] ? null : e[t];
			e[t] = n
		} else u.extend(e, t);
		return this
	}

	function n(e) {
		for (var t, n = decodeURI(e.split("#")[0]).split("&"), i = {}, o = 0, r = n.length; o < r; o++) n[o] && (i[(t = n[o].split("="))[0]] = t[1]);
		return i
	}

	function i(e, t) {
		return e + (-1 < e.indexOf("?") ? "&" : "?") + u.param(t)
	}

	function o(e, t) {
		var n = e.indexOf("#");
		return -1 === n ? t : (0 < n && (e = e.substr(n)), t + e)
	}

	function t(e, t) {
		function n() {
			r.reject(function (e) {
				return u('<span class="lity-error"/>').append(e)
			}("Failed loading image"))
		}

		var i = t.opener() && t.opener().data("lity-desc") || "Image with no description",
			o = u('<img src="' + e + '" alt="' + i + '"/>'), r = y();
		return o.on("load", function () {
			if (0 === this.naturalWidth) return n();
			r.resolve(o)
		}).on("error", n), r.promise()
	}

	function r(e) {
		return '<div class="lity-iframe-container"><iframe frameborder="0" allowfullscreen allow="autoplay; fullscreen" src="' + e + '"/></div>'
	}

	function f() {
		return v.documentElement.clientHeight ? v.documentElement.clientHeight : Math.round(m.height())
	}

	function h(e) {
		var t = s();
		t && (27 === e.keyCode && t.options("esc") && t.close(), 9 === e.keyCode && function (e, t) {
			var n = t.element().find(c), i = n.index(v.activeElement);
			e.shiftKey && i <= 0 ? (n.get(n.length - 1).focus(), e.preventDefault()) : e.shiftKey || i !== n.length - 1 || (n.get(0).focus(), e.preventDefault())
		}(e, t))
	}

	function g() {
		u.each(x, function (e, t) {
			t.resize()
		})
	}

	function s() {
		return 0 === x.length ? null : x[0]
	}

	function a(e, t, n, i) {
		var o, r, s, a = this, l = !1, c = !1;
		t = u.extend({}, T, t), r = u(t.template), a.element = function () {
			return r
		}, a.opener = function () {
			return n
		}, a.options = u.proxy(p, a, t), a.handlers = u.proxy(p, a, t.handlers), a.resize = function () {
			l && !c && s.css("max-height", f() + "px").trigger("lity:resize", [a])
		}, a.close = function () {
			if (l && !c) {
				c = !0, function (t) {
					t.element().attr(w, "true"), 1 === x.length && (b.removeClass("lity-active"), m.off({
						resize: g,
						keydown: h
					})), ((x = u.grep(x, function (e) {
						return t !== e
					})).length ? x[0].element() : u(".lity-hidden")).removeClass("lity-hidden").each(function () {
						var e = u(this), t = e.data(k);
						t ? e.attr(w, t) : e.removeAttr(w), e.removeData(k)
					})
				}(a);
				var e = y();
				if (i && (v.activeElement === r[0] || u.contains(r[0], v.activeElement))) try {
					i.focus()
				} catch (e) {
				}
				return s.trigger("lity:close", [a]), r.removeClass("lity-opened").addClass("lity-closed"), d(s.add(r)).always(function () {
					s.trigger("lity:remove", [a]), r.remove(), r = void 0, e.resolve()
				}), e.promise()
			}
		}, o = function (n, i, o, e) {
			var r, s = "inline", a = u.extend({}, o);
			return e && a[e] ? (r = a[e](n, i), s = e) : (u.each(["inline", "iframe"], function (e, t) {
				delete a[t], a[t] = o[t]
			}), u.each(a, function (e, t) {
				return !t || !(!t.test || t.test(n, i)) || (!1 !== (r = t(n, i)) ? (s = e, !1) : void 0)
			})), {handler: s, content: r || ""}
		}(e, a, t.handlers, t.handler), r.attr(w, "false").addClass("lity-loading lity-opened lity-" + o.handler).appendTo("body").focus().on("click", "[data-lity-close]", function (e) {
			u(e.target).is("[data-lity-close]") && a.close()
		}).trigger("lity:open", [a]), function (e) {
			1 === x.unshift(e) && (b.addClass("lity-active"), m.on({
				resize: g,
				keydown: h
			})), u("body > *").not(e.element()).addClass("lity-hidden").each(function () {
				var e = u(this);
				void 0 === e.data(k) && e.data(k, e.attr(w) || null)
			}).attr(w, "true")
		}(a), u.when(o.content).always(function (e) {
			s = u(e).css("max-height", f() + "px"), r.find(".lity-loader").each(function () {
				var e = u(this);
				d(e).always(function () {
					e.remove()
				})
			}), r.removeClass("lity-loading").find(".lity-content").empty().append(s), l = !0, s.trigger("lity:ready", [a])
		})
	}

	function l(e, t, n) {
		e.preventDefault ? (e.preventDefault(), e = (n = u(this)).data("lity-target") || n.attr("href") || n.attr("src")) : n = u(n);
		var i = new a(e, u.extend({}, n.data("lity-options") || n.data("lity"), t), n, v.activeElement);
		if (!e.preventDefault) return i
	}

	var v = e.document, m = u(e), y = u.Deferred, b = u("html"), x = [], w = "aria-hidden", k = "lity-" + w,
		c = 'a[href],area[href],input:not([disabled]),select:not([disabled]),textarea:not([disabled]),button:not([disabled]),iframe,object,embed,[contenteditable],[tabindex]:not([tabindex^="-"])',
		T = {
			esc: !0,
			handler: null,
			handlers: {
				image: t, inline: function (e, t) {
					var n, i, o;
					try {
						n = u(e)
					} catch (e) {
						return !1
					}
					return !!n.length && (i = u('<i style="display:none !important"/>'), o = n.hasClass("lity-hide"), t.element().one("lity:remove", function () {
						i.before(n).remove(), o && !n.closest(".lity-content").length && n.addClass("lity-hide")
					}), n.removeClass("lity-hide").after(i))
				}, youtube: function (e) {
					var t = S.exec(e);
					return !!t && r(o(e, i("https://www.youtube" + (t[2] || "") + ".com/embed/" + t[4], u.extend({autoplay: 1}, n(t[5] || "")))))
				}, vimeo: function (e) {
					var t = $.exec(e);
					return !!t && r(o(e, i("https://player.vimeo.com/video/" + t[3], u.extend({autoplay: 1}, n(t[4] || "")))))
				}, googlemaps: function (e) {
					var t = E.exec(e);
					return !!t && r(o(e, i("https://www.google." + t[3] + "/maps?" + t[6], {output: 0 < t[6].indexOf("layer=c") ? "svembed" : "embed"})))
				}, facebookvideo: function (e) {
					var t = A.exec(e);
					return !!t && (0 !== e.indexOf("http") && (e = "https:" + e), r(o(e, i("https://www.facebook.com/plugins/video.php?href=" + e, u.extend({autoplay: 1}, n(t[4] || ""))))))
				}, iframe: r
			},
			template: '<div class="lity" role="dialog" aria-label="Dialog Window (Press escape to close)" tabindex="-1"><div class="lity-wrap" data-lity-close role="document"><div class="lity-loader" aria-hidden="true">Loading...</div><div class="lity-container"><div class="lity-content"></div><button class="lity-close" type="button" aria-label="Close (Press escape to close)" data-lity-close>&times;</button></div></div></div>'
		}, C = /(^data:image\/)|(\.(png|jpe?g|gif|svg|webp|bmp|ico|tiff?)(\?\S*)?$)/i,
		S = /(youtube(-nocookie)?\.com|youtu\.be)\/(watch\?v=|v\/|u\/|embed\/?)?([\w-]{11})(.*)?/i,
		$ = /(vimeo(pro)?.com)\/(?:[^\d]+)?(\d+)\??(.*)?$/,
		E = /((maps|www)\.)?google\.([^\/\?]+)\/?((maps\/?)?\?)(.*)/i,
		A = /(facebook\.com)\/([a-z0-9_-]*)\/videos\/([0-9]*)(.*)?$/i, D = function () {
			var e = v.createElement("div"), t = {
				WebkitTransition: "webkitTransitionEnd",
				MozTransition: "transitionend",
				OTransition: "oTransitionEnd otransitionend",
				transition: "transitionend"
			};
			for (var n in t) if (void 0 !== e.style[n]) return t[n];
			return !1
		}();
	return t.test = function (e) {
		return C.test(e)
	}, l.version = "2.4.0", l.options = u.proxy(p, l, T), l.handlers = u.proxy(p, l, T.handlers), l.current = s, u(v).on("click.lity", "[data-lity]", l), l
});
var _ui = {
	body: $("body"),
	mObserver: window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver,
	observers: $(".observe"),
	cart: $(".header .basket"),
	cartMobile: $("#id-mobile-cart"),
	init: function () {
		_ui.init_components(), _ui.prepareForms(), _ui.resize(), _ui.observe(), _ui.worker(), $(window).resize(_ui.resize), $(window).scroll(_ui.scroll)
	},
	prepareForms: function () {
	},
	resize: function () {
		Modernizr.mq("(max-width: 769px)")
	},
	observe: function () {
		if (0 < _ui.observers.length) {
			var e = new _ui.mObserver(function (e) {
				e.forEach(function (e) {
					"childList" === e.type && (_ui.prepareForms(), _ui.objectfit())
				})
			});
			_ui.observers.each(function () {
				e.observe(this, {childList: !0, characterData: !0, attributes: !0, subtree: !0})
			})
		}
	},
	scroll: function () {
		Modernizr.mq("(min-width: 768px)") && (100 < $(window).scrollTop() ? $(".fixed-header").slideDown() : $(".fixed-header").slideUp())
	},
	init_components: function () {
		$("[data-action='slide']").on("click", function (e) {
			e.preventDefault();
			var t = $(this);
			$(t.data("target")).slideToggle()
		}), $("[data-action='toggle']").on("click", function (e) {
			e.preventDefault();
			var t = $(this), n = $(t.data("target"));
			n && (n.toggle(), n.is(":visible") ? t.addClass("in") : t.removeClass("in"))
		}), $("[data-action='popover']").on("click", function (e) {
			e.preventDefault(), $(this).hasClass("in") ? ($($(this).removeClass("in").data("target")).hide().removeClass("popover active"), $(".popover-overlay").remove()) : $($(this).addClass("in").data("target")).show().addClass("popover active")
		}), _ui.body.on("mouseup", function (t) {
			var e = $("[data-action='popover']");
			0 < e.length && e.each(function () {
				var e = $(this);
				0 === e.has(t.target).length && e.hasClass("in") && $(e.removeClass("in").data("target")).hide().removeClass("popover active")
			})
		}), $("[data-action='accordion']").each(function () {
			$(this).find("[data-target]").each(function () {
				var e = $(this);
				e.hasClass("open") ? $(e.data("target")).show() : $(e.data("target")).hide(), e.click(function () {
					return $(this).toggleClass("open"), $($(this).data("target")).slideToggle(), !1
				})
			})
		}), $("[data-action='tabs']").each(function () {
			$(this).find("[data-action='tab']").each(function () {
				$(this).hasClass("active") ? $($(this).data("target")).show() : $($(this).data("target")).hide(), $(this).click(function () {
					return $(this).parents("[data-action='tabs']").find("[data-action='tab']").each(function () {
						$(this).removeClass("active"), $($(this).data("target")).hide()
					}), $(this).addClass("active"), $($(this).data("target")).addClass("active").show(), !1
				})
			})
		}), $("[data-action='slider']").each(function () {
			$(this).slick({
				adaptiveHeight: !0,
				autoplay: !0,
				autoSpeed: 2e3,
				dots: !1,
				arrows: !0,
				vertical: !!$(this).data("vertical") && $(this).data("vertical"),
				slidesToShow: $(this).data("show") ? $(this).data("show") : 4,
				slidesToScroll: $(this).data("scroll") ? $(this).data("scroll") : 4,
				prevArrow: '<img src="/images/arrL.png" class="slick-prev" />',
				nextArrow: '<img src="/images/arrR.png" class="slick-next" />',
				responsive: [{breakpoint: 1031, settings: {}}, {
					breakpoint: 769,
					settings: {
						slidesToShow: $(this).data("sm-show") ? $(this).data("sm-show") : 2,
						slidesToScroll: $(this).data("sm-scroll") ? $(this).data("sm-scroll") : 2,
						vertical: !!$(this).data("sm-vertical") && $(this).data("sm-vertical")
					}
				}, {
					breakpoint: 481,
					settings: {
						slidesToShow: $(this).data("xs-show") ? $(this).data("xs-show") : 1,
						slidesToScroll: $(this).data("xs-scroll") ? $(this).data("xs-scroll") : 1,
						vertical: !!$(this).data("xs-vertical") && $(this).data("xs-vertical")
					}
				}]
			})
		}), $(".reviews--slider").slick({
			adaptiveHeight: !0,
			autoplay: !0,
			autoSpeed: 5e3,
			dots: !1,
			arrows: !1,
			slidesToShow: 1,
			slidesToScroll: 1
		}), _ui.body.on("click", "[data-action='modal:open']", function () {
			var e = $(this).data("target");
			return this.hash && 2 < this.hash.length && (e = this.hash), e && ($(e).modal(), _ui.body.addClass("blur")), !1
		}), _ui.body.on($.modal.BEFORE_CLOSE, function () {
			_ui.body.removeClass("blur")
		}), $("[data-action='scroll']").on("click", function () {
			var e = $(this).data("target");
			if (this.hash && 2 < this.hash.length && (e = this.hash), !e) return !1;
			var t = $(window).scrollTop(), n = $(e).offset().top, i = n < t ? n + -100 : t + (n - t) + -100;
			return $("html,body").animate({scrollTop: i}, 300), !1
		}), lightbox.option({
			resizeDuration: 200,
			albumLabel: "Рис.: %1 из %2"
		}), $(".js-agree-check").each(function () {
			var e = $(this), t = e.data("target");
			t && $(t).prop("disabled", !e.prop("checked")), e.click(function () {
				var e = $(this), t = e.data("target");
				t && $(t).prop("disabled", !e.prop("checked"))
			})
		})
	},
	loader: function () {
	},
	worker: function () {
		_ui.objectfit(), _ui.body.on("mouseup", function (e) {
			var t = $("#id-mm.active");
			0 === t.has(e.target).length && (t.removeClass("active"), $(".mobile-menu").addClass("hidden"))
		}), $("#id-mm").on("click", function () {
			return $(this).toggleClass("active"), $(".mobile-menu").toggleClass("hidden"), !1
		}), $("#id-mobile-menu-wrap").html("<ul>" + $(".header .top-menu").html() + "</ul>"), _ui.cartMobile.html(_ui.cart.html()).addClass(_ui.cart.attr("class")), $("#id-mobile-phone").html("<ul>" + $(".header .contacts").html() + "</ul>");
		var e = new _ui.mObserver(function (e) {
			e.forEach(function (e) {
				0 < _ui.cartMobile.length && _ui.cartMobile.html(_ui.cart.html()).addClass(_ui.cart.attr("class"))
			})
		});
		_ui.cart.each(function () {
			e.observe(this, {childList: !0, characterData: !0, attributes: !0, subtree: !0})
		});
		var t = $(".catalog");
		sbl = t.find(".sidebar-wrap"), 0 < sbl.length && t.addClass("has-sidebar");
		var n = [];
		//$(".btn-select").on("click", function () {
		$(".btn-select").click(function(){
			$(this).find("ul").slideToggle("fast", function () {
				$(".btn-select").css('opacity','1');
				$(this).is(":hidden") || n.push($(this).parents(".product-select").attr("data-select-id"))
			})
		}),
		_ui.body.on("click", function (e, t) {
			0 < n.length && (n.forEach(function (e, t, n) {
				$('[data-select-id="' + e + '"] ul').hide()
			}), n = [])
		}), $(".product-select").each(function (e, t) {
			var n = "", i = $(t).find("li.selected"), o = $(t).find("li:first-child");
			$(t).attr("data-select-id", e), 0 < i.length ? (n = i.html(), $(t).find(".btn-select-value").html(n)) : 0 < o.length ? (n = o.html(), o.addClass("selected"), $(t).find(".btn-select-value").html(n)) : $(t).hide()
		}), _ui.body.on("click", ".product-select li", function () {
			var e = $(this);
			e.parent().find("li.selected").removeClass("selected"), e.addClass("selected");
			var t = e.html();
			e.parents(".product-select").find(".btn-select-value").html(t), $(".product-select-popup").hide(), p(), f(), 55 == $(this).data("parent") && h()
		}), _ui.body.on("mouseenter", ".product-select li", function () {
			var e = $(this), t = e.find(".popup"), n = e.offset(), i = n.top, o = n.left - 215;
			0 < t.length && $(".product-select-popup").html(t.html()).css({top: i, left: o}).show()
		}), _ui.body.on("mouseleave", ".product-select li", function () {
			$(".product-select-popup").hide()
		});
		var i = window.location.href, o = $(".buy-block").data("product_id"), r = null, s = !1, a = !1,
			l = 0 < $("#id_product_props").val().length ? jQuery.parseJSON($("#id_product_props").val()) : [],
			c = 0 < $("#id_product_offers").val().length ? jQuery.parseJSON($("#id_product_offers").val()) : [], u = [],
			d = $(".buy-block").data("matras");

		function p() {
			$(".selects ul>li.selected").each(function (e, t) {
				var n = $(t).data("value"), i = $(t).data("parent");
				"color" != i && "matras" != i && (u[i] = n), "matras" == i && (s = !0), "color" == i && (a = !0)
			})
		}

		function f() {
			if (r = null, c.forEach(function (e) {
				var t = !1;
				for (var n in l) {
					if (u[n] != e.props[n] && ("" != e.props[n] || 0 != u[n])) {
						t = !1;
						break
					}
					t = !0
				}
				t && (r = e)
			}), null != r) {
				$("#zakaz").attr("disabled", !1).text("✔ КУПИТЬ");
				var e = $.trim(r.PRICE.substr(0, r.PRICE.length - 4));
				$(".price").html(g(e) + "<span>руб</span>").data("price", e.replace(/\s/g, "")), r.OLD_PRICE_VALUE && $("#id_old_price_element").text(r.OLD_PRICE).show()
			} else $("#zakaz").attr("disabled", !0).text("Товара нет"), $("#id_old_price_element").hide();
			a && (function () {
				var e = parseInt($("#colors ul>li.selected .value-dop").text().replace(/\s+/g, "")) || 0,
					t = parseInt($(".buy-block .price").data("price")) || 0, n = $(".price"), i = 0;
				i = e <= 0 ? t : t + Math.round(t * e / 100);
				n.html(g(i) + "<span>руб</span>").data("price", i)
			}(), a = !1), s && (function () {
				var e = parseInt($("#matras ul>li.selected .value-dop").text()) || 0,
					t = parseInt($(".buy-block .price").data("price")) || 0, n = 0;
				n = isNaN(e) || e <= 0 ? parseInt(t) : parseInt(t) + parseInt(e), $(".price").data("price", n).html(g(n) + "<span>руб</span>")
			}(), s = !1)
		}

		function h() {
			var e = u[55], t = $(".buy-block .price").data("price"), n = $(".price"), i = parseInt(t);
			n.html(g(i) + "<span>руб</span>"), $.get("/include/catalog/matras.php", {size: e}, function (e) {
				var t = $("#matras ul").html(e).parent().parent(), n = "", i = $(t).find("li.selected"),
					o = $(t).find("li:first-child");
				$(t).attr("data-select-id", 5), 0 < i.length ? (n = i.html(), $(t).find(".btn-select-value").html(n)) : 0 < o.length ? (n = o.html(), o.addClass("selected"), $(t).find(".btn-select-value").html(n)) : $(t).hide()
			})
		}

		function g(e) {
			e = (e += "").replace(/\s/g, "");
			var t = Number.prototype.toFixed.call(parseFloat(e) || 0, 2).replace(/(\D)/g, ",");
			return t = (t = parseInt(t) + "").replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ")
		}

		p(), f(), d && h(), $("#zakaz").on("click", function (e) {
			e.preventDefault();
			var t = r ? "/include/catalog/basket.php?action=ADD2BASKET&id=" + r.ID : "/include/catalog/basket.php?action=ADD2BASKET&id=" + o;
			t = (t = t + "&color=" + $("#colors ul>li.selected").data("value")) + "&matras=" + $("#matras ul>li.selected").data("value"), $.get(t, function (e) {
				e && ($.get(i, function (e) {
					var t = $(e).find(".cart").html();
					$(".cart").html(t)
				}), $("#id-to-basket-product").text($(".page-title > h1").text()), $("#modal-to-basket").modal("open"))
			})
		});
		var v, m = $(".table-prices table");
		m.wrap('<div class="table-container" />'), (v = m.clone()).find("tbody").remove().end().addClass("fixed").insertBefore(m), m.find("thead").hide(), m.parent().css("paddingTop", m.find("thead").outerHeight()), $(".table-container").on("scroll", function () {
			v.css("top", $(this).scrollTop())
		}), _ui.body.on("click", "#id_show_more", function () {
			return $(this).parent().toggleClass("expand"), !1
		}), setTimeout(function () {
			$.ajax({
				data: {action: "ip"},
				url: "/include/ajax/ip.php",
				type: "GET",
				dataType: "json"
			}).done(function (e) {
				e && e.status && yaCounter45479856.userParams({ip: e.ip})
			})
		}, 15e3), _ui.body.on("click", "a[href^=tel]", function () {
			yaCounter45479856.reachGoal("phone");
			try {
				gtag("event", "event_name", {event_category: "NUKE", event_action: "phone"})
			} catch (e) {
				try {
					ga("send", "event", "NUKE", "phone")
				} catch (e) {
				}
			}
		}), _ui.body.on("click", "#zakaz", function () {
			yaCounter45479856.reachGoal("zakaz");
			try {
				gtag("event", "event_name", {event_category: "NUKE", event_action: "zakaz"})
			} catch (e) {
				try {
					ga("send", "event", "NUKE", "zakaz")
				} catch (e) {
				}
			}
		}), _ui.body.on("click", ".btn.backcall", function () {
			yaCounter45479856.reachGoal("open_backcall");
			try {
				gtag("event", "event_name", {event_category: "NUKE", event_action: "open_backcall"})
			} catch (e) {
				try {
					ga("send", "event", "NUKE", "open_backcall")
				} catch (e) {
				}
			}
		}), _ui.body.on("click", ".btn.one-click-goods", function () {
			yaCounter45479856.reachGoal("open_oneclick");
			try {
				gtag("event", "event_name", {event_category: "NUKE", event_action: "open_oneclick"})
			} catch (e) {
				try {
					ga("send", "event", "NUKE", "open_oneclick")
				} catch (e) {
				}
			}
		}), _ui.body.on("click", ".one-click-goods", function (e) {
			$('form#id_oneclick_form input[name="tovar"]').val($(this).data("tovar"))
		}), _ui.body.on("submit", "form#form-order", function (e) {
			e.preventDefault();
			var t = !0;
			if ($(this).find('input[name="name"]').val().length < 3 ? (t = !1, $(this).find('input[name="name"]').css("border", "1px solid red")) : $(this).find('input[name="name"]').css("border", "1px solid green"), $(this).find('input[name="phone"]').val().length < 5 ? (t = !1, $(this).find('input[name="phone"]').css("border", "1px solid red")) : $(this).find('input[name="phone"]').css("border", "1px solid green"), !t) return !1;
			var n = $(this).serialize();
			$(this);
			$.post("/include/ajax/ajax.php", n, function (e) {
				$.modal.close(), window.location = "/personal/cart/success.php"
			}), yaCounter45479856.reachGoal("order");
			try {
				gtag("event", "event_name", {event_category: "NUKE", event_action: "order"})
			} catch (e) {
				try {
					ga("send", "event", "NUKE", "order")
				} catch (e) {
				}
			}
		}), _ui.body.on("submit", "form#id_oneclick_form", function (e) {
			e.preventDefault();
			var t = !0;
			if ($(this).find('input[name="name"]').val().length < 3 ? (t = !1, $(this).find('input[name="name"]').css("border", "1px solid red")) : $(this).find('input[name="name"]').css("border", "1px solid green"), $(this).find('input[name="phone"]').val().length < 5 ? (t = !1, $(this).find('input[name="phone"]').css("border", "1px solid red")) : $(this).find('input[name="phone"]').css("border", "1px solid green"), $(this).find('input[name="email"]').val().length < 5 ? (t = !1, $(this).find('input[name="email"]').css("border", "1px solid red")) : $(this).find('input[name="email"]').css("border", "1px solid green"), !t) return !1;
			var n = $(this).serialize();
			$(this);
			$.post("/include/ajax/ajax.php", n, function (e) {
				$.modal.close(), alert("Ваша заявка принята")
			}), yaCounter45479856.reachGoal("odin_klik");
			try {
				gtag("event", "event_name", {event_category: "NUKE", event_action: "odin_klik"})
			} catch (e) {
				try {
					ga("send", "event", "NUKE", "odin_klik")
				} catch (e) {
				}
			}
		}), _ui.body.on("submit", "form#id_backcall_form, form#id_user_help_form", function (e) {
			e.preventDefault();
			var t = !0;
			if ($(this).find('input[name="phone"]').val().length < 3 ? (t = !1, $(this).find('input[name="phone"]').css("border", "1px solid red")) : $(this).find('input[name="phone"]').css("border", "1px solid green"), !t) return !1;
			var n = $(this).serialize();
			$(this);
			$.post("/include/ajax/ajax.php", n, function (e) {
				$.modal.close(), alert("Ваша заявка принята")
			}), yaCounter45479856.reachGoal("zakaz_zvonok");
			try {
				gtag("event", "event_name", {event_category: "NUKE", event_action: "zakaz_zvonok"})
			} catch (e) {
				try {
					ga("send", "event", "NUKE", "zakaz_zvonok")
				} catch (e) {
				}
			}
		}), _ui.body.on("click", "#add-to-favorite", function (e) {
			if (window.sidebar && window.sidebar.addPanel) window.sidebar.addPanel(document.title, window.location.href, ""); else if (window.external && "AddFavorite" in window.external) window.external.AddFavorite(location.href, document.title); else {
				if (window.opera && window.print) return this.title = document.title, !0;
				alert("Нажмите " + (-1 !== navigator.userAgent.toLowerCase().indexOf("mac") ? "Command/Cmd" : "CTRL") + " + D для добавления страницы в избранное")
			}
			return !1
		})
	},
	objectfit: function () {
		Modernizr.objectfit ? $(".of-thumb").addClass("object-fit") : $(".of-thumb").each(function () {
			var e = $(this), t = e.find("img").prop("src");
			t && e.css("backgroundImage", "url(" + t + ")").addClass("non-object-fit")
		})
	}
};
window.onload = function () {
	_ui.loader()
}, $(document).ready(function () {
	_ui.init()
});