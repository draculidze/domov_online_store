@extends('layout')
@section('title', 'Страница')
@section('content')

    <aside class="main-top container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="/">Главная</a></li>
                <li>{{ $post->name }}</li>
            </ul>
        </div>
        <div class="page-title">
            <h1>{{ $post->name }}</h1>
        </div>
    </aside>

    <main class="main" role="main">
        <div class="main__inner container">
            {!!  $post->text !!}
        </div>
    </main>
@endsection
