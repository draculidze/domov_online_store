<footer class="footer">
    <div class="container">
        <div class="footer-menu">
            <ul>
                <li>
                    <a href="/about/">О компании</a>
                </li>
                <li>
                    <a href="/catalog/">Каталог</a>
                </li>
                <li>
                    <a href="/otzyvy/">Отзывы</a>
                </li>
                <li>
                    <a href="/colors/">Цвета</a>
                </li>
                <li>
                    <a href="/sertifikaty/">Сертификаты</a>
                </li>
                <li>
                    <a href="/rasprodazha/">Распродажа</a>
                </li>
                <li>
                    <a href="/politika-konfidentsialnosti/">Политика конфиденциальности</a>
                </li>
                <li>
                    <a href="/contacts/">Контакты</a>
                </li>
            </ul>
        </div>

        <div class="footer-contacts">
            <div itemscope itemtype="http://schema.org/Organization">
                <link itemprop="url" href="https://imperiamebeli33.ru/">
                <meta itemprop="name" content="ИмперияМебели">
                <div style="display: none" itemprop="logo" itemscope itemtype="https://schema.org/ImageObject"><img src="{{ asset('img/logo.png') }}" alt="ИмперияМебели" itemprop="image url">
                    <meta itemprop="width" content="700">
                    <meta itemprop="height" content="700">
                </div>
                <!--
                <div style="display: none" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                    <meta itemprop="streetAddress" content="Клязьминская улица, дом 15">
                    <meta itemprop="postalCode" content="105275">
                    <meta itemprop="addressLocality" content="Москва">
                </div>
                -->
                <meta itemprop="telephone" content="8 (920) 927 86 76">
            </div>

            <p>
                <a href="tel:79209278676" class="ya-phone phone_alloka">8 (920) 927 86 76</a><br />
                <a href="mailto:info@imperiamebeli33.ru">info@imperiamebeli33.ru</a>
            </p>
        </div>
    </div>

    <hr>

    <div class="container">
        <p class="footer__copyrights">
            <span>2008 - 2021 © Интернет-магазин "Империя Мебели". Все права защищены.</span>
            <br>Цены на сайте носят ознакомительный характер и не являются публичной офертой (ст.435 ГК РФ)
            <br>Файлы cookie необходимы для корректной работы сайта и полноценного использования его возможностей.
            <br>Работая с этим сайтом, Вы даете свое согласие на использование файлов сookie. <a href="/politika-konfidentsialnosti/">Политика конфиденциальности.</a>
            <br><br>
        </p>
    </div>
</footer>

<div class="totop"><a href="#top"></a></div>
