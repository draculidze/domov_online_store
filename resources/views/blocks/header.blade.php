<header class="header">
    <div class="header-top">
        <div class="container">

            <ul class="top-menu fleft">
                <li>
                    <a href="{{ route('category') }}">Каталог мебели</a>
                </li>
                <li>
                    <a href="/delivery/">Доставка и оплата</a>
                </li>
                <li>
                    <a href="/about/">О нас</a>
                </li>
                <li>
                    <a href="/works/">Наши работы</a>
                </li>
                <li>
                    <a href="/sales/">Распродажи</a>
                </li>
            </ul>

            <div class="basket fright empty">
                <p><a href="{{ route('basket') }}">Корзина пуста</a></p>
                <!--
                <a href="{{ route('login') }}">Войти</a>/<a href="{{ route('register') }}">Регистрация</a>
                -->
            </div>

        </div>
    </div>

    <div class="header-bottom container">
        <div class="header-bottom__inner">
            <div class="logo">
                <p><a href="{{ route('index') }}" title="Мебель из массива сосны и березы">
                        <img src="{{ asset('img/logo.png') }}" alt="Мебель из массива сосны и березы" class="adapt">
                    </a></p>
                <p>Москва и Московская область</p>
            </div>

            <div class="right-side">
                <div class="header-search">
                    <div id="search" class="search-form">
                        <form action="{{ route('search') }}" method="get">
                            <button type="submit" class="btn brown">Поиск...</button>
                            <div class="form-group">
                                <input id="title-search-input" type="text" name="q" value="" size="15" maxlength="50" autocomplete="off" required placeholder="Например, кровать Бали" />
                            </div>
                        </form>
                    </div>

                </div>
            </div>

            <div class="contacts tright">
                <p>
                    <a href="tel:+79209278676" class="ya-phone phone_alloka">8 (920) 927 86 76</a><br />
                    с 9:00 до 23:00</p>
                <a class="btn backcall" href="#callback">Заказать звонок</a>
            </div>

            <!--
            <div class="bottom-menu">
                <ul>
                    <li>
                        <a href="/works/">НАШИ РАБОТЫ</a>
                    </li>
                    <li>
                        <a href="/rasprodazha/">РАСПРОДАЖА</a>
                    </li>
                </ul>
            </div>
            -->

        </div>
    </div>
</header>

<style>.top_block{background: #17d200; color: #fff; font-size: 1.4em;  font-weight: 700;
        text-align: center; padding: 10px 0; text-shadow: 1px 1px 2px #000;
        box-shadow: 1px 1px 9px #130; margin:0 0 15px 0;}
</style>
<!--
<div class="top_block">Мы работаем 23/7. Безопасная доставка по Москве и области!</div>
-->
