@extends('layout')
@section('title', 'Главная')
@section('content')

    <aside class="index-slider container">
        <div class="items" data-action="slider" data-show="1" data-scroll="1" data-sm-show="1" data-sm-scroll="1" data-xs-show="1" data-xs-scroll="1">
            @foreach(\App\Models\Slider::all() as $slide)
                <div class="item"><a href="#" title="{{ $slide->name }}"><img src="myimg/slider/{{ $slide->imgPath }}" alt="{{ $slide->name }}"></a></div>
            @endforeach
        </div>
    </aside>

    <aside class="index-catalog container">
        <div class="items">

            <div class="item">
                <p class="thumb contain">
                    <a href="/catalog/krovati/" title="Кровати">
                        <img src="{{ asset('img/category/bed.jpg') }}" alt="Кровати" title="Кровати">
                    </a>
                </p>
                <p class="name"><a href="/catalog/krovati/" title="Кровати">Кровати</a></p>
            </div>
            <div class="item">
                <p class="thumb contain">
                    <a href="/catalog/buffety/" title="Буфеты">
                        <img src="{{ asset('img/category/buffet.jpg') }}" alt="Буфеты" title="Буфеты">
                    </a>
                </p>
                <p class="name"><a href="/catalog/buffety/" title="Матрасы">Буфеты</a></p>
            </div>
            <div class="item">
                <p class="thumb contain">
                    <a href="/catalog/prihozjie/" title="Прихожие">
                        <img src="{{ asset('img/category/hallways.jpg') }}" alt="Прихожие" title="Прихожие">
                    </a>
                </p>
                <p class="name"><a href="/catalog/spalni/" title="Прихожие">Прихожие</a></p>
            </div>
            <div class="item">
                <p class="thumb contain">
                    <a href="/catalog/shkafy/" title="Шкафы">
                        <img src="{{ asset('img/category/cupboard.jpg') }}" alt="Шкафы" title="Шкафы">
                    </a>
                </p>
                <p class="name"><a href="/catalog/shkafi/" title="Шкафы">Шкафы</a></p>
            </div>
            <div class="item">
                <p class="thumb contain">
                    <a href="/catalog/komody/" title="Комоды">
                        <img src="{{ asset('img/category/commode.jpg') }}" alt="Комоды" title="Комоды">
                    </a>
                </p>
                <p class="name"><a href="/catalog/komody/" title="Комоды">Комоды</a></p>
            </div>
            <div class="item">
                <p class="thumb contain">
                    <a href="/catalog/tumby/" title="Тумбы">
                        <img src="{{ asset('img/category/curbstone.jpg') }}" alt="Тумбы" title="Тумбы">
                    </a>
                </p>
                <p class="name"><a href="/catalog/tumby/" title="Тумбы">Тумбы</a></p>
            </div>
            <div class="item">
                <p class="thumb contain">
                    <a href="/catalog/table/" title="Столы">
                        <img src="{{ asset('img/category/table.jpg') }}" alt="Столы" title="Столы">
                    </a>
                </p>
                <p class="name"><a href="/catalog/table/" title="Столы">Столы</a></p>
            </div>
            <div class="item">
                <p class="thumb contain">
                    <a href="/catalog/mattress/" title="Матрасы">
                        <img src="{{ asset('img/category/matrasy.jpg') }}" alt="Матрасы" title="Матрасы">
                    </a>
                </p>
                <p class="name"><a href="/catalog/mattress/" title="Матрасы">Матрасы</a></p>
            </div>
        </div>
    </aside>

    <aside class="index-tabs">
        <div class="container">

            <div class="tabs" data-action="tabs">
                <div class="buttons">
                    <button class="btn" data-action="tab" data-target="#id_items_lider">Лидеры продаж</button>
                    <button class="btn" data-action="tab" data-target="#id_items_new">Новинки</button>
                    <button class="btn active" data-action="tab" data-target="#id_items_discont">Товары со скидкой</button>
                </div>
                <div class="items_lider" id="id_items_lider">
                    <div class="items grid">
                        @foreach($productsLiders as $product)
                            <div class="item">
                                <p class="thumb of-thumb contain"><img src="{{ asset('myimg/product/' . $product->thumbnail) }}" alt=""></p>
                                <p class="name">{{ $product->name }}</p>
                                <p class="price">{{ $product->getPriceWithKoff() ?: "0"}} руб.</p>
                                <a href="{{ route('product', $product->slug) }}" class="readmore" title="{{ $product->name }}"></a>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="items_new" id="id_items_new">
                    <div class="items grid">
                        @foreach($productsNews as $product)
                            <div class="item">
                                <p class="thumb of-thumb contain"><img src="{{ asset('myimg/product/' . $product->thumbnail) }}" alt=""></p>
                                <p class="name">{{ $product->name }}</p>
                                <p class="price">{{ $product->getPriceWithKoff() ?: "0"}} руб.</p>
                                <a href="{{ route('product', $product->slug) }}" class="readmore" title="{{ $product->name }}"></a>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="items_discont" id="id_items_discont">
                    <div class="items grid">
                        @foreach($productsPrice as $product)
                            <div class="item">
                                <p class="thumb of-thumb contain"><img src="{{ asset('myimg/product/' . $product->thumbnail) }}" alt=""></p>
                                <p class="name">{{ $product->name }}</p>
                                <p class="price">{{ $product->getPriceWithKoff() ?: "0"}} руб.</p>
                                <a href="{{ route('product', $product->slug) }}" class="readmore" title="{{ $product->name }}"></a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>

        </div>
    </aside>

    <aside class="index-advants">
        <div class="container">
            <div class="row">
                <h3>Преимущества заказа мебели<br />на прямую у производителя</h3>
                <div class="items">
                    <div class="item">
                        <div class="thumb"><img src="{{ asset('img/icon-zavod.svg') }}" alt=""></div>
                        <p>Собственное<br /> производство</p>
                    </div>
                    <div class="item">
                        <div class="thumb"><img src="{{ asset('img/icon-order.svg') }}" alt=""></div>
                        <p>Изготовление<br /> под заказ</p>
                    </div>
                    <div class="item">
                        <div class="thumb"><img src="{{ asset('img/icon-price.svg') }}" alt=""></div>
                        <p>Низкие<br /> цены</p>
                    </div>
                    <div class="item">
                        <div class="thumb" style="width: 65%"><img src="{{ asset('img/icon-delivery.svg') }}" alt=""></div>
                        <p>Доставка по<br /> Москве и МО</p>
                    </div>
                    <div class="item">
                        <div class="thumb"><img src="{{ asset('img/icon-guarantee.svg') }}" alt=""></div>
                        <p>Гарантия<br /> до 24 месяцев</p>
                    </div>
                </div>
            </div>
        </div>
    </aside>

    <main class="main" role="main">
        <div class="main__inner container">
            <article class="content">

                <h1>Офицальный сайт фабрики &quot;Империя Мебели&quot; в Москве</h1>

                <p><b>Деревянная мебель из массива</b> является настоящей классикой и вряд ли будет подвержена влиянию моды. Лучшие дизайны интерьеров создаются именно с использованием таких моделей. <b>Мебель от производителя</b> является уникальной и долговечной.</p>

                <p><b>Мебель из массива дерева</b> является непревзойденной классикой, которая создает в доме комфорт и неповторимый уют. Используемый материал имеет ряд достоинств, о которых непременно стоит упомянуть:</p>

                <p>· Дерево является экологически чистым материалом и приносит человеческому организму только пользу;</p>

                <p>· При правильной обработке, древесина прослужит долгое время;</p>

                <p>· Материал имеет отличные теплоизоляционные свойства;</p>

                <p>· Вопреки расхожему мнению о дорогостоящей стоимости, мебель имеет приемлемую цену.</p>

                <p>В последние годы мебель из массива дерева настолько популярна, что предложение на мебельном рынке растет на глазах. Главное в выборе этого элемента интерьера довериться настоящим профессионалам. Эта мебель прослужит долгие годы, если материал обработать правильно, а также производить правильную эксплуатацию. При выборе мебели для Вашего дома, не забывайте и об ее функциональности, ведь в первую очередь она будет нести свое прямое предназначение и поэтому без качественной сборки здесь не обойтись.</p>

                <p> </p>

                <p><b>Купить мебель из массива дерева от производителя</b> можно на нашем сайте. Компания &quot;Империя мебели&quot; направлена на производство мебели из сосны и березы. Мы сами тщательно отбираем сырье для процесса изготовления мебели и соблюдаем всю технологию правильного производства. Мебель для гостиной, спальни, детской или других помещений, выполненная из деревянного массива будет не только радовать Ваш взгляд, но и прослужит всей семье не один десяток лет. Для подтверждения качества нашей продукции, мы предлагаем для Вас гарантию на 24 месяца.</p>
            </article>

        </div>
    </main>

@endsection
