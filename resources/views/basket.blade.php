@extends('layout')
@section('title', 'Корзина')
@section('content')

    <aside class="main-top container">
        <div class="breadcrumbs"><ul><li><a href="/">Главная</a></li><li><a href="/personal/" title="Мой кабинет">Мой кабинет</a></li><li>Моя корзина</li></ul></div>
        <div class="page-title">
            <h1>Корзина</h1>
        </div>
    </aside>

    <main class="main" role="main">
        <div class="main__inner container">
            @csrf
            <div class="cart">
                <div id="basket_form_container">
                    <div class="bx_ordercart ">
                        @isset($order->products)
                            @foreach($order->products as $product)
                                <div class="items">
                                    <div class="item">
                                        <div class="thumb contain of-thumb object-fit">
                                            <img src="{{ asset('myimg/product/' . $product->thumbnail) }}" alt="{{ $product->name }}">
                                        </div>
                                        <a href="{{ route('product', $product->slug) }}"><h2>{{ $product->name }}</h2></a>
                                        <div class="cart-info">
                                            <p>
                                                <span>Цена по умолчанию:</span> {{ $product->pivot->price }}<br>
                                                <span>Количество:</span> {{ $product->pivot->cnt_product }}<br>
                                                @php $sum = $product->pivot->price  @endphp
                                                @foreach(json_decode($product->pivot->params, true) as $key => $value)
                                                    <span>{{ $value['p'] }}:</span> {{ $value['n'] }}<br>
                                                    @php
                                                        if($value['o'] == 1) { // процент
                                                            $sum += $sum * $value['v'] / 100;
                                                        }
                                                        if($value['o'] == 0) // сложение
                                                            $sum += $value['v'];
                                                        if($value['o'] == 2) // вычитание
                                                            $sum -``````````= $value['v'];
                                                    @endphp
                                                @endforeach
                                            </p>
                                        </div>
                                        <div class="cart-dop-info">
                                            <!-- <h5>Дополнительно:</h5>
                                            <p>
                                                <span>Ящик для белья:</span> 2 шт<br>
                                                <span>Подъемный мех:</span> Нет<br>
                                                <span>Матрас:</span> Матрас "Кокос 2"
                                            </p> -->
                                        </div>
                                        <div class="price">{{ $sum }} руб.</div>

                                        <form method="POST" action="{{ route('basket-remove', $product->id) }}">
                                            @csrf
                                            <div class="buttons">
                                                <button type="submit" class="btn close"></button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            @endforeach
                        @endisset

                        <!--<div>
                        <br><br>
                        <button type="submit">Оформить заказ</button>
                    </div>-->

                    </div>
                </div>
            </div>

            <div class="form-order-container order">
                <h3>Оформление заказа</h3>
                <form action="{{ route('basket-confirm') }}" method="POST">
                    @csrf
                    <input type="hidden" name="action" value="order">
                    <div class="form-group">
                        <label>Имя:</label>
                        <input type="text" class="form-control" name="name">
                    </div>
                    <div class="form-group">
                        <label>Телефон:</label>
                        <input type="text" class="form-control" name="phone">
                    </div>
                    <div class="form-group">
                        <label>Адрес доставки:</label>
                        <textarea class="form-control" name="address"></textarea>
                    </div>
                    <label>
                        <input type="checkbox" name="agree" value="" checked="checked" class="js-agree-check" data-target="#id_order_submit"> Согласен с <a href="/politika-konfidentsialnosti/" target="_blank">политикой конфиденциальности</a>
                    </label>

                    <button type="submit" class="btn green order-btn">Оформить заказ</button>
                </form>
            </div>
        </div>
    </main>
@endsection
