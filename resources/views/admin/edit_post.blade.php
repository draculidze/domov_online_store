@extends('admin.layout')
@section('title', 'Редактирование страницы')
@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Редактирование страницы "{{$post->name}}"</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group me-2">
                <a href="{{ route('admin.post.show') }}" class="btn btn-sm btn-outline-secondary">Назад</a>
            </div>
        </div>
    </div>

    <form method="POST" action="{{ route('admin.post.edit', $post->id) }}">
        @csrf
        <div class="mb-3">
            <label for="slug" class="form-label">Ссылка</label>
            <input type="text" name="slug" class="form-control" id="slug" placeholder="Введите ссылку" value="{{ $post->slug }}">
        </div>
        <div class="mb-3">
            <label for="name" class="form-label">Название</label>
            <input type="text" name="name" class="form-control" id="name" placeholder="Введите название" value="{{ $post->name }}">
        </div>
        <div class="mb-3">
            <label for="text" class="form-label">Статья</label>
            <textarea class="form-control" name="text" id="text" rows="3">{{ $post->text }}</textarea>
        </div>
        <div class="mb-3">
            <label for="text" class="form-label">Опубликована</label>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="is_public" id="flexRadioDefault1" value="1"
                       {{ $post->is_public ? "checked" : "" }}>
                <label class="form-check-label" for="flexRadioDefault1">
                    Да
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="is_public" id="flexRadioDefault2" value="0" {{ !$post->is_public ? "checked" : "" }}>
                <label class="form-check-label" for="flexRadioDefault2">
                    Нет
                </label>
            </div>
        </div>

        <button type="submit" class="btn btn-primary">Отправить</button>
    </form>
    <script src="{{ asset('js/nicEdit-latest.js') }}" type="text/javascript"></script>
    <script type="text/javascript">bkLib.onDomLoaded(nicEditors.allTextAreas);</script>
@endsection
