@extends('admin.layout')
@section('title', 'Редактирование товара')
@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Редактирование товара {{$product->name}}</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group me-2">
                <a href="{{ route('admin.product.show') }}" class="btn btn-sm btn-outline-secondary">Назад</a>
            </div>
        </div>
    </div>

    <div>
        <img src="{{ asset('myimg/product/' . $product->thumbnail) }}" style="max-width: 200px; max-height: 200px; border: 1px solid #ced4da; margin-bottom: 20px" alt="">
    </div>

    <form method="POST" action="{{ route('admin.product.add') }}" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="id" value="{{ $product->id }}">
        <div class="mb-3">
            <label for="code" class="form-label">Текстовый код (Артикул)</label>
            <input type="text" name="code" class="form-control" id="code" placeholder="Введите код" value="{{ $product->code }}">
        </div>
        <div class="mb-3">
            <label for="slug" class="form-label">Ссылка</label>
            <input type="text" name="slug" class="form-control" id="slug" placeholder="Введите ссылку" value="{{ $product->slug }}">
        </div>
        <div class="mb-3">
            <label for="name" class="form-label">Название</label>
            <input type="text" name="name" class="form-control" id="name" placeholder="Введите название" value="{{ $product->name }}">
        </div>
        <div class="mb-3">
            <label for="price" class="form-label">Цена</label>
            <input type="text" name="price" class="form-control" id="price" placeholder="Введите цену" value="{{ $product->price }}">
        </div>
        <div class="mb-3">
            <label for="oldprice" class="form-label">Цена</label>
            <input type="text" name="oldprice" class="form-control" id="oldprice" placeholder="Введите старую цену" value="{{ $product->oldprice }}">
        </div>
        <div class="mb-3">
            <label for="text" class="form-label">Текстовое описание</label>
            <textarea class="form-control" name="text" id="text" rows="3">{{ $product->text }}</textarea>
        </div>
        <div class="mb-3">
            <label for="thumbnail" class="form-label">Изображение</label>
            <input type="file" name="thumbnail" class="form-control" id="thumbnail" placeholder="Выберите файл с изображением" value="{{ old('thumbnail') }}">
        </div>
        <div class="mb-3">
            <label for="category_id" class="form-label">Родительская категория</label>
            <select class="form-select" name="category_id" id="category_id" aria-label="Корневая категория">
                <option value="null">Корневая категория</option>
                @foreach(\App\Models\Category::all() as $cat)
                    @if($cat->id == $product->category_id)
                        <option value="{{ $cat->id }}" selected>{{ $cat->name }}</option>
                    @else
                        <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                    @endif
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Отправить</button>
    </form>
    <script src="{{ asset('js/nicEdit-latest.js') }}" type="text/javascript"></script>
    <script type="text/javascript">bkLib.onDomLoaded(nicEditors.allTextAreas);</script>
@endsection
