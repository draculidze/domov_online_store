@extends('admin.layout')
@section('title', 'Товары')
@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Список товаров</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group me-2">
                <a href="{{ route('admin.product.addForm') }}" class="btn btn-sm btn-outline-secondary">Добавить</a>
            </div>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>#</th>
                <th>Изображение</th>
                <th>code</th>
                <th>Ссылка</th>
                <th>Имя</th>
                <th>Текст</th>
                <th>Категория</th>
                <th>Цена</th>
                <th>Старая цена</th>
                <th>Действие</th>
            </tr>
            </thead>
            <tbody>
                @foreach($products as $product)
                    <tr>
                        <td>{{ $product->id }}</td>
                        <td><img src="{{ asset('myimg/product/' . $product->thumbnail) }}" style="max-width: 100px; max-height: 100px;" alt=""></td>
                        <td>{{ $product->code }}</td>
                        <td>{{ $product->slug }}</td>
                        <td>{{ $product->name }}</td>
                        <td>{{ $product->text }}</td>
                        <td>{{ $product->category->name }}</td>
                        <td>{{ $product->price }}</td>
                        <td>{{ $product->oldprice }}</td>
                        <td>
                            <a href="{{ route('admin.product.edit', $product->id) }}">Редактировать</a> |
                            <a href="{{ route('admin.product.delete', $product->id) }}">Удалить</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
