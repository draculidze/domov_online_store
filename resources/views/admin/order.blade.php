@extends('admin.layout')
@section('title', 'Заказы')
@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Список заказов</h1>
    </div>

    <div class="table-responsive">

        <div class="accordion" id="accordionOrders">
            @foreach($orders as $order)
                <div class="accordion-item">
                    <h2 class="accordion-header" id="heading{{ $order->id }}">
                        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapse{{ $order->id }}" aria-expanded="true" aria-controls="collapse{{ $order->id }}">
                            Заказ #{{ $order->id }} от {{ $order->created_at }}
                        </button>
                    </h2>
                    <div id="collapse{{ $order->id }}" class="accordion-collapse collapse show" aria-labelledby="heading{{ $order->id }}" data-bs-parent="#accordionOrders">
                        <div class="accordion-body">
                            <strong>Статус заказа: {{ $order->status == 1 ? 'Новая' : 'В работе' }}.</strong>
                            <p>
                                Имя: {{ $order->name }} <br>
                                Телефон: {{ $order->phone }} <br>
                                Адрес: {{ $order->address }}
                            </p>

                            <h5>Список товаров:</h5>
                            <ul>
                                @foreach($order->products as $product)
                                    <li>{{$product->name}}, цена: {{ $product->price }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
