@extends('admin.layout')
@section('title', 'Редактирование категории')
@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Редактирование категории {{$category->name}}</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group me-2">
                <a href="{{ route('admin.category.show') }}" class="btn btn-sm btn-outline-secondary">Назад</a>
            </div>
        </div>
    </div>

    <form method="POST" action="{{ route('admin.category.add') }}">
        @csrf
        <input type="hidden" name="id" value="{{ $category->id }}">
        <div class="mb-3">
            <label for="code" class="form-label">Текстовый код (Артикул)</label>
            <input type="text" name="code" class="form-control" id="code" placeholder="Введите код" value="{{ $category->code }}">
        </div>
        <div class="mb-3">
            <label for="slug" class="form-label">Ссылка</label>
            <input type="text" name="slug" class="form-control" id="slug" placeholder="Введите ссылку" value="{{ $category->slug }}">
        </div>
        <div class="mb-3">
            <label for="name" class="form-label">Название</label>
            <input type="text" name="name" class="form-control" id="name" placeholder="Введите название" value="{{ $category->name }}">
        </div>
        <div class="mb-3">
            <label for="text" class="form-label">Текстовое описание</label>
            <textarea class="form-control" name="text" id="text" rows="3">{{ $category->text }}</textarea>
        </div>
        <div class="mb-3">
            <label for="thumbnail" class="form-label">Ссылка на изображение</label>
            <input type="text" name="thumbnail" class="form-control" id="thumbnail" placeholder="Введите ссылку на изображение" value="{{ $category->thumbnail }}">
        </div>
        <div class="mb-3">
            <label for="coefficient" class="form-label">Коэффициент</label>
            <input type="text" name="coefficient" class="form-control" id="coefficient" placeholder="Введите коэффициент" value="{{ $category->coefficient }}">
        </div>
        <div class="mb-3">
            <label for="category_id" class="form-label">Родительская категория</label>
            <select class="form-select" name="category_id" id="category_id" aria-label="Корневая категория">
                <option value="null">Корневая категория</option>
                @foreach(\App\Models\Category::all() as $cat)
                    @if($cat->id == $category->category_id)
                        <option value="{{ $cat->id }}" selected>{{ $cat->name }}</option>
                    @else
                        <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                    @endif
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Отправить</button>
    </form>
@endsection
