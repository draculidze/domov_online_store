@extends('admin.layout')
@section('title', 'Редактирование')
@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Редактирование слайда {{$slider->name}}</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group me-2">
                <a href="{{ route('admin.index') }}" class="btn btn-sm btn-outline-secondary">Назад</a>
            </div>
        </div>
    </div>

    <div>
        <img src="{{ asset('myimg/slider/' . $slider->imgPath) }}" style="max-width: 200px; max-height: 200px; border: 1px solid #ced4da; margin-bottom: 20px" alt="">
    </div>

    <form method="POST" action="{{ route('admin.slide.add') }}" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="id" value="{{ $slider->id }}">
        <div class="mb-3">
            <label for="name" class="form-label">Название</label>
            <input type="text" name="name" class="form-control" id="name" placeholder="Введите название" value="{{ $slider->name }}">
        </div>
        <div class="mb-3">
            <label for="imgPath" class="form-label">Изображение</label>
            <input type="file" name="imgPath" class="form-control" id="imgPath" placeholder="Выберите файл с изображением" value="{{ old('imgPath') }}">
        </div>
        <button type="submit" class="btn btn-primary">Отправить</button>
    </form>
@endsection
