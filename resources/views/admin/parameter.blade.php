@extends('admin.layout')
@section('title', 'Параметры')
@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Список параметров</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group me-2">
                <a href="{{ route('admin.parameter.addForm') }}" class="btn btn-sm btn-outline-secondary">Добавить</a>
            </div>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>#</th>
                <th>Имя</th>
                <th>Категория</th>
                <th>Продукт</th>
                <th>Действие</th>
            </tr>
            </thead>
            <tbody>
                @foreach($parameters as $parameter)
                    <tr>
                        <td>{{ $parameter->id }}</td>
                        <td>{{ $parameter->name }}</td>
                        <td>{{ isset($parameter->category->name) ? $parameter->category->name : "" }}</td>
                        <td>{{ isset($parameter->product->name) ? $parameter->product->name : "" }}</td>
                        <td>
                            <a href="{{ route('admin.parameter.edit', $parameter->id) }}">Редактировать</a> |
                            <a href="{{ route('admin.parameter.delete', $parameter->id) }}">Удалить</a> |
                            <a href="{{ route('admin.parameter_item.show', $parameter->id) }}">Элементы параметра</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
