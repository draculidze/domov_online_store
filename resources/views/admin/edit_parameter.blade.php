@extends('admin.layout')
@section('title', 'Редактирование')
@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Редактирование параметра {{ $parameter->name }}</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group me-2">
                <a href="{{ route('admin.parameter.show') }}" class="btn btn-sm btn-outline-secondary">Назад</a>
            </div>
        </div>
    </div>

    <form method="POST" action="{{ route('admin.parameter.add') }}">
        @csrf
        <input type="hidden" name="id" value="{{ $parameter->id }}">
        <div class="mb-3">
            <label for="name" class="form-label">Название</label>
            <input type="text" name="name" class="form-control" id="name" placeholder="Введите название" value="{{ $parameter->name }}">
        </div>
        <div class="mb-3">
            <label for="category_id" class="form-label">Категория</label>
            <select class="form-select" name="category_id" id="category_id" aria-label="Корневая категория">
                <option value="null">Категория</option>
                @foreach(\App\Models\Category::all() as $cat)
                    @if($cat->id == $parameter->category_id)
                        <option value="{{ $cat->id }}" selected>{{ $cat->name }}</option>
                    @else
                        <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                    @endif
                @endforeach
            </select>
        </div>
        <div class="mb-3">
            <label for="category_id" class="form-label">Продукт</label>
            <select class="form-select" name="product_id" id="product_id" aria-label="Корневая категория">
                <option value="null">Продукт</option>
                @foreach(\App\Models\Product::all() as $prod)
                    @if($prod->id == $parameter->category_id)
                        <option value="{{ $prod->id }}" selected>{{ $prod->name }}</option>
                    @else
                        <option value="{{ $prod->id }}">{{ $prod->name }}</option>
                    @endif
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Отправить</button>
    </form>
@endsection
