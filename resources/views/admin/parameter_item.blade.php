@extends('admin.layout')
@section('title', 'Параметры')
@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Список элементов параметра "{{ $parameter->name }}"</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group me-2">
                <a href="{{ route('admin.parameter_item.addForm', $parameter->id) }}" class="btn btn-sm btn-outline-secondary">Добавить</a>
            </div>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>#</th>
                <th>Имя</th>
                <th>Параметр</th>
                <th>Изображение</th>
                <th>Операция</th>
                <th>Значение (% или руб)</th>
                <th>Действие</th>
            </tr>
            </thead>
            <tbody>
                @isset($items)
                    @foreach($items as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->parameter->name }}</td>
                            <td>
                                @if($item->img)
                                    {{ $item->img }} <br>
                                    <img style="width: 50px; height: 50px" src="{{ substr($item->img, 0, 4) == 'http' ? $item->img : asset('myimg/params/' . $item->img) }}" alt="">
                                @endif
                            </td>
                            <td>{{ $item->OperationText() }}</td>
                            <td>{{ $item->operation == 2 ? "-" : "+" }} {{ $item->value ? $item->value : 0 }} {{ $item->operation === 1 ? "%" : " руб." }}</td>
                            <td>
                                <a href="{{ route('admin.parameter_item.edit', $item->id) }}">Редактировать</a> |
                                <a href="{{ route('admin.parameter_item.delete', $item->id) }}">Удалить</a>
                            </td>
                        </tr>
                    @endforeach
                @endisset
            </tbody>
        </table>
    </div>
@endsection
