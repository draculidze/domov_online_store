@extends('admin.layout')
@section('title', 'Страницы')
@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Список страниц</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group me-2">
                <a href="{{ route('admin.post.addForm') }}" class="btn btn-sm btn-outline-secondary">Добавить</a>
            </div>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>#</th>
                <th>Ссылка</th>
                <th>Имя</th>
                <th>Текст</th>
                <th>Опубликована</th>
                <th>Действие</th>
            </tr>
            </thead>
            <tbody>
                @foreach($posts as $post)
                    <tr>
                        <td>{{ $post->id }}</td>
                        <td>{{ $post->slug }}</td>
                        <td>{{ $post->name }}</td>
                        <td>{{ $post->text }}</td>
                        <td>{{ $post->is_public === 0 ? 'Нет' : 'Да'}}</td>
                        <td>
                            <a href="{{ route('admin.post.editForm', $post->id) }}">Редактировать</a> |
                            <a href="{{ route('admin.post.delete', $post->id) }}">Удалить</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
