@extends('admin.layout')
@section('title', 'Слайды')
@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Добавление слайда</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group me-2">
                <a href="{{ route('admin.index') }}" class="btn btn-sm btn-outline-secondary">Назад</a>
            </div>
        </div>
    </div>

    <form enctype="multipart/form-data" method="POST" action="{{ route('admin.slide.add') }}">
        @csrf
        <div class="mb-3">
            <label for="name" class="form-label">Название</label>
            <input type="text" name="name" class="form-control" id="name" placeholder="Введите название" value="{{ old('name') }}">
        </div>
        <div class="mb-3">
            <label for="imgPath" class="form-label">Изображение</label>
            <input type="file" name="imgPath" class="form-control" id="imgPath" placeholder="Выберите файл с изображением" value="{{ old('imgPath') }}">
        </div>
        <button type="submit" class="btn btn-primary">Сохранить</button>
    </form>
@endsection
