@extends('admin.layout')
@section('title', 'Главная')
@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h3 class="h3">Редактирование слайдера</h3>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group me-2">
                <a href="{{ route('admin.slide.addForm') }}" class="btn btn-sm btn-outline-secondary">Добавить слайд</a>
            </div>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>#</th>
                <th>Имя</th>
                <th>Ссылка на изображение</th>
                <th>Действия</th>
            </tr>
            </thead>
            <tbody>
            @foreach(\App\Http\Controllers\Admin\SliderController::getSlides() as $slide)
                <tr>
                    <td>{{ $slide->id }}</td>
                    <td>{{ $slide->name }}</td>
                    <td>{{ $slide->imgPath }}</td>
                    <td>
                        <a href="{{ route('admin.slide.editForm', $slide->id) }}">Редактировать</a> |
                        <a href="{{ route('admin.slide.delete', $slide->id) }}">Удалить</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
