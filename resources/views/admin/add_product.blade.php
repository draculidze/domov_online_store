@extends('admin.layout')
@section('title', 'Товары')
@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Добавление товара</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group me-2">
                <a href="{{ route('admin.product.show') }}" class="btn btn-sm btn-outline-secondary">Назад</a>
            </div>
        </div>
    </div>

    <form enctype="multipart/form-data" method="POST" action="{{ route('admin.product.add') }}">
        @csrf
        <div class="mb-3">
            <label for="code" class="form-label">Текстовый код (Артикул)</label>
            <input type="text" name="code" class="form-control" id="code" placeholder="Введите код" value="{{ old('code') }}">
        </div>
        <div class="mb-3">
            <label for="slug" class="form-label">Ссылка</label>
            <input type="text" name="slug" class="form-control" id="slug" placeholder="Введите ссылку" value="{{ old('slug') }}">
        </div>
        <div class="mb-3">
            <label for="name" class="form-label">Название</label>
            <input type="text" name="name" class="form-control" id="name" placeholder="Введите название" value="{{ old('name') }}">
        </div>
        <div class="mb-3">
            <label for="text" class="form-label">Текстовое описание</label>
            <textarea class="form-control" name="text" id="text" rows="3">{{ old('text') }}</textarea>
        </div>
        <div class="mb-3">
            <label for="thumbnail" class="form-label">Изображение</label>
            <input type="file" name="thumbnail" class="form-control" id="thumbnail" placeholder="Выберите файл с изображением" value="{{ old('thumbnail') }}">
        </div>
        <div class="mb-3">
            <label for="category_id" class="form-label">Категория товара</label>
            <select class="form-select" name="category_id" id="category_id" aria-label="Выберите категорию">
                @foreach(\App\Models\Category::all() as $cat)
                    <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="mb-3">
            <label for="price" class="form-label">Цена</label>
            <input type="text" name="price" class="form-control" id="price" placeholder="Введите цену товара" value="{{ old('price') }}">
        </div>
        <div class="mb-3">
            <label for="oldprice" class="form-label">Старая цена</label>
            <input type="text" name="oldprice" class="form-control" id="oldprice" placeholder="Введите старую цена" value="{{ old('oldprice') }}">
        </div>
        <button type="submit" class="btn btn-primary">Сохранить</button>
    </form>
@endsection
