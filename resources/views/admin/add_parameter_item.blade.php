@extends('admin.layout')
@section('title', 'Параметры')
@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Добавление элемента параметру "{{ $parameter->name }}"</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group me-2">
                <a href="{{ route('admin.parameter_item.show', $parameter->id) }}" class="btn btn-sm btn-outline-secondary">Назад</a>
            </div>
        </div>
    </div>

    <form method="POST" action="{{ route('admin.parameter_item.add') }}">
        @csrf
        <input type="hidden" name="parameter_id" value="{{ $parameter->id }}">
        <div class="mb-3">
            <label for="name" class="form-label">Название</label>
            <input type="text" name="name" class="form-control" id="name" placeholder="Введите название параметра" value="{{ old('name') }}">
        </div>
        <div class="mb-3">
            <label for="category_id" class="form-label">Параметр: {{ $parameter->name }}</label>
        </div>
        <div class="mb-3">
            <label for="operation" class="form-label">Изменение стоимости от выбранного параметра</label>
            <select class="form-select" name="operation" id="operation" aria-label="Операция">
                <option value="null" selected>Выберите операцию, которая будет происходить при выборе</option>
                <option value="0">Сложение</option>
                <option value="1">Процент</option>
                <option value="2">Вычитание</option>
            </select>
        </div>
        <div class="mb-3">
            <label for="value" class="form-label">Значение</label>
            <input type="text" name="value" class="form-control" id="value" placeholder="Введите название параметра" value="{{ old('value') }}">
        </div>
        <div class="mb-3">
            <label for="img" class="form-label">Изображение</label>
            <input type="text" name="img" class="form-control" id="img" placeholder="Введите имя файла" value="{{ old('img') }}">
        </div>
        <button type="submit" class="btn btn-primary">Сохранить</button>
    </form>
@endsection
