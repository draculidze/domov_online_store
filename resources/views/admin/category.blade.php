@extends('admin.layout')
@section('title', 'Категории')
@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Список категорий</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group me-2">
                <a href="{{ route('admin.category.add') }}" class="btn btn-sm btn-outline-secondary">Добавить</a>
            </div>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>#</th>
                <th>code</th>
                <th>Ссылка</th>
                <th>Имя</th>
                <th>Текст</th>
                <th>Иконка</th>
                <th>Родительская категория</th>
                <th>Коэффициент</th>
                <th>Действие</th>
            </tr>
            </thead>
            <tbody>
                @foreach($categories as $category)
                    <tr>
                        <td>{{ $category->id }}</td>
                        <td>{{ $category->code }}</td>
                        <td>{{ $category->slug }}</td>
                        <td>{{ $category->name }}</td>
                        <td>{{ $category->text }}</td>
                        <td>{{ $category->thumbnail }}</td>
                        <td>{{ !is_null($category->parentCategory) ? $category->parentCategory->name : "" }}</td>
                        <td>{{ $category->coefficient }}</td>
                        <td>
                            <a href="{{ route('admin.category.edit', $category->id) }}">Редактировать</a> |
                            <a href="{{ route('admin.category.delete', $category->id) }}">Удалить</a> |
                            <a href="{{ route('admin.product.show', $category->id) }}">Товары</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
