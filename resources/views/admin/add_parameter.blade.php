@extends('admin.layout')
@section('title', 'Параметры')
@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Добавление параметра</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group me-2">
                <a href="{{ route('admin.parameter.show') }}" class="btn btn-sm btn-outline-secondary">Назад</a>
            </div>
        </div>
    </div>

    <form method="POST" action="{{ route('admin.parameter.add') }}">
        @csrf
        <div class="mb-3">
            <label for="name" class="form-label">Название</label>
            <input type="text" name="name" class="form-control" id="name" placeholder="Введите название параметра" value="{{ old('name') }}">
        </div>
        <div class="mb-3">
            <label for="category_id" class="form-label">Родительская категория</label>
            <select class="form-select" name="category_id" id="category_id" aria-label="Корневая категория">
                <option value="null" selected>Выберите категорию, если необходимо</option>
                @foreach(\App\Models\Category::all() as $cat)
                    <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="mb-3">
            <label for="category_id" class="form-label">Либо продукт, к которому привязать параметр</label>
            <select class="form-select" name="product_id" id="product_id" aria-label="Корневая категория">
                <option value="null" selected>Выберите продукт, для которого будет параметр</option>
                @foreach(\App\Models\Product::all() as $product)
                    <option value="{{ $product->id }}">{{ $product->name }}</option>
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Сохранить</button>
    </form>
@endsection
