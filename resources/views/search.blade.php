@extends('layout')
@section('title', 'Каталог')
@section('content')

    <aside class="main-top container">
        <div class="breadcrumbs"><ul><li><a href="/">Главная</a></li><li>Поиск</li></ul></div>
        <div class="page-title">
            <h1>Найдено товаров: {{ count($products) }}</h1>
        </div>
    </aside>

    <main class="main" role="main">
        <div class="main__inner container">

            <div class="catalog-wrap">
                <div class="category">

                    <div class="category-items">
                        <div class="category-items__list">
                        @foreach($products as $product)
                            <!-- 1 -->
                                <div class="item">
                                    <div class="inner">

                                        @if($product->oldprice > 0)
                                            <div class="labels">
                                                <p class="label red">Распродажа!</p>
                                            </div>
                                        @endif

                                        <div class="thumb of-thumb contain">
                                            <a href="{{ route('product', $product->slug) }}">
                                                <img src="{{ asset('myimg/product/' . $product->thumbnail) }}" alt="{{ $product->name }}" />
                                            </a>
                                        </div>
                                        <p class="name"><a href="{{ route('product', $product->slug) }}">{{ $product->name }}</a></p>
                                        <p class="price">от {{ $product->getPriceWithKoff() }} руб</p>
                                        @if($product->oldprice)
                                            <p class="old-price">{{ $product->oldprice }} руб</p>
                                        @endif
                                        <div class="buttons">
                                            <a href="{{ route('product', $product->slug) }}" class="one-click-goods btn green" >✔ В корзину</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>

                    <br/>
                    <br/>
                    <br/>
                    @if(isset($cat))
                        <div class="category-description content">
                            <h2>Купите {{ $cat->name }} недорого в Москве</h2>
                        </div>
                    @endif
                </div>
            </div>

            <div class="sidebar-wrap">
                <a href="#" class="mobile-nav" data-action="toggle" data-target="#id-sidebar">
                    <span></span>
                    <span></span>
                    <span></span>
                </a>
                <p class="mobile-desc">	&lArr; Каталог товаров</p>

                <div class="sidebar" id="id-sidebar">
                    <div class="module">
                        <div class="catalog-list" data-action="accordion">

                            <!-- Block categories module -->

                            <ul>
                                @foreach($categories as $category)
                                    <li>
                                        <a href="{{ route('category', $category->slug) }}" class="root-item close">{{ $category->name }} <span id="leo-cat-4" style="display:none" class="leo-qty badge pull-right"></span></a>
                                        <ul id="cat-0">
                                            @foreach($category->categories as $ct)
                                                <li><a href=" {{ route('category', $ct->slug) }}">{{ $ct->name }}</a></li>
                                            @endforeach
                                        </ul>
                                    </li>
                                @endforeach
                            </ul>
                            <!-- /Block categories module -->
                        </div>
                    </div>
                    <div class="module">
                        <div class="module__header">⏰ ДОСТАВКА</div>
                        <div class="module__body">
                            Москва до подъезда - <strong>бесплатно</strong><br>
                            За МКАД - <strong>30 руб</strong>/км.<br>
                            По России - по договоренности.
                        </div>
                    </div>
                    <div class="module">
                        <div class="module__header">▲ ПОДЪЕМ В КВАРТИРУ
                        </div>
                        <div class="module__body">
                            Стоимость подъёма обговаривается
                            с курьерами при доставке.
                            Запросите примерный расчет
                            стоимости подъема у менеджера.
                        </div>
                    </div>
                    <div class="module">
                        <div class="module__header">🛠 СБОРКА НА МЕСТЕ
                        </div>
                        <div class="module__body">
                            Специалисты Нашей кампании соберут доставленную Вам мебель.
                            Стоимость сборки Вы можете уточнить у менеджера
                        </div>
                    </div>
                    <div class="module">
                        <div class="module__header">С нами выгодно</div>
                        <div class="module__body">
                            <ol>
                                <li>Доступные цены</li>
                                <li>Распродажи и акции</li>
                                <li>Оплата по факту получения</li>
                                <li>Экологически чистая продукция</li>
                                <li>Гарантия на всё мебель</li>
                                <li>Доставка по москве и области</li>
                                <li>Возможность доставки по России</li>
                                <li>Услуга подъёма и сборки</li>
                                <li>По вашим размерам на заказ</li>
                                <li>Натуральный массив дерева</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
@endsection
