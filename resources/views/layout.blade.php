<!DOCTYPE HTML>
<html class="page" lang="ru-ru" dir="ltr">
<head>

    <title>Империя мебели | @yield('title')</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="format-detection" content="address=no" />

    <!--Основной-->
    <link rel="shortcut icon" href="/bitrix/templates/massiv/images/favicons/favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" href="{{ asset('css/style.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/new_style.css') }}" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="robots" content="index, follow" />
    <meta name="keywords" content="мебель из массива дерева, мебель из массива от производителя, фабрика мебели официальный сайт" />
    <meta name="description" content="✔ В ИмперииМебели33 купить мебель из натурального дерева можно онлайн на официальном сайте фабрики! Описание и цены от производителя представлены в каталоге. Звоните ☎ Тел. в Москве: + 7 (920) 927-86-76. Гарантия - 24 месяца!" />

    <meta property="og:locale" content="ru_RU" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Главная" />
    <meta property="og:description" content="✔ В Империи Мебели мебель из натурального дерева можно онлайн на официальном сайте фабрики! Описание и цены от производителя представлены в каталоге. Звоните ☎ Тел. в Москве: 8 (920) 927-86-76. Гарантия - 24 месяца!" />
    <meta property="og:url" content="https://imperiamebeli33.ru/" />
    <meta property="og:site_name" content="ИмперияМебели" />
    <meta property="og:image" content="{{ asset('img/logo.png') }}" />
    @yield('stylesheets')

    <!--<script src="js/main.js" async></script>-->
</head>


<body>
<noscript>У вас отключен JavaScript. Это пугает.</noscript>
<div id="panel"></div>
<div class="layout-wrap" id="layout-panel">
    <div class="layout">
        @include('blocks.header')
        @if(session()->has('success'))
            <div class="container alert alert-warning">
                <h2>{{ session()->get('success') }}</h2>
            </div>
        @endif
        @if(session()->has('error'))
            <div class="container alert alert-error">
                <h2>{{ session()->get('error') }}</h2>
            </div>
        @endif
        @yield('content')
    </div>
    @include('blocks.footer')

</div>

<div id="modal-to-basket" class="modal small" style="display:none;">
    <div class="modal-header">Товар в корзине!</div>
    <div class="modal-to-basket-body modal-body">
        <p class="tcenter">Товар: <span id="id-to-basket-product"></span>, добавлен в корзину!</p>
        <br>
        <br>

        <div class="buttons">
            <a href="#close-modal" class="btn" rel="modal:close">Продолжить покупки</a>
            <a href="/personal/cart/" class="btn green">Перейти в корзину</a>
        </div>

    </div>
</div>
<div class="product-select-popup hidden"></div>

<script type="text/javascript" src="{{ asset('js/scripts.js') }}" async></script>

@yield('scripts')

<!-- Begin LeadBack code {literal} -->
<script>
    var _emv = _emv || [];
    _emv['campaign'] = '0e98729842c746c20edeee50';

    (function() {
        var em = document.createElement('script'); em.type = 'text/javascript'; em.async = true;
        em.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'leadback.ru/js/leadback.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(em, s);
    })();
</script>
<!-- End LeadBack code {/literal} -->
</body>
</html>
