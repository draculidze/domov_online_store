@extends('layout')
@section('title', 'Товар')
@section('stylesheets')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <style>
        .select2-search__field {
            height: 0px;
        }

        .select2-selection {
            height: 45px;
        }

        .select2-selection img {
            height: 30px;
            width: 30px;
        }

        .select2-container {
            margin-bottom: 15px;
        }

        .select2-container--default .select2-selection--single {
            height: 40px;
        }

        .select2-container--default .select2-selection--single .select2-selection__rendered {
            line-height: 18px;
        }

        span.select-result {
            display: flex;
            align-items: center;
        }

        .select-result img {
            width: 29px;
            height: 29px;
            margin-right: 10px;
        }

        .select2-container--default .select2-selection--single .select2-selection__arrow {
            height: 40px;
            right: 5px;
        }
    </style>
@endsection
@section('content')

    <aside class="main-top container">

        <div class="page-title">
            <h1> {{ $product->name }}</h1>
        </div>
    </aside>

    <main class="main" role="main">
        <div class="main__inner container">

            <div class="product">

                <div class="product-main">

                    <div class="product-images">
                        <div class="product-top-buttons"><a class="btn brown" href="/catalog/">⇐ В каталог</a>
                            <a class="btn green" href="/catalog/{{ $product->category->slug }}/">Все {{ $product->category->name }}</a>
                        </div>

                        <!--Картинки-->
                        <div class="thumb of-thumb contain">
                            <a href="{{ asset('myimg/product/' . $product->thumbnail) }}" data-lightbox="product">
                                <img src="{{ asset('myimg/product/' . $product->thumbnail) }}" alt="{{ $product->name }}">
                            </a>
                        </div>
                        <!--
                        <div class="thumb-list">
                            <a href="img/thumbnail.jpg" class="of-thumb" data-lightbox="product"><img src="img/thumbnail.jpg" alt="Матрас Агат 512"></a>
                        </div>
                        -->
                    </div>

                    <div class="product-info">

                        <div class="product-call">
                            <div class="product-call__icon"><img src="{{ asset('img/call2.png') }}" alt=""></div>
                            <div class="product-call__inner">
                                <div class="product-call__title">Позвоните<br><a href="tel:+79209278676" class="ya-phone phone_alloka">8 (920) 108-23-40</a></div>
                                <div class="product-call__text">и Мы Изготовим Под Ваши Размеры</div>
                            </div>
                        </div>
                        <div class="product-info__inner">
                            <form method="POST" action="{{ route('basket-add', $product) }}">
                                @csrf
                                <input type="hidden" name="startprice" id="startprice" value="{{ $product->price }}">
                                <div class="selects">
                                    @foreach($product->ParametersCategory as $parameter)
                                        <div>
                                            <p>{{ $parameter->name }}</p>
                                            <select class="js-example-basic-single" name="param_{{ $parameter->id }}">
                                                @foreach($parameter->items as $item)
                                                    <option
                                                        value="{{ $item->id }}"
                                                        @if($item->img)
                                                            data-img="{{ substr($item->img, 0, 4) == 'http' ? $item->img : asset('myimg/params/' . $item->img) }}"
                                                        @else
                                                            data-img="null"
                                                        @endif
                                                        data-operation="{{ $item->operation }}"
                                                        data-value="{{ $item->value }}"
                                                        data-value-text="{{ $item->operation == 2 ? "-" : "+" }} {{ $item->value ? $item->value : 0 }} {{ $item->operation === 1 ? "%" : " руб." }}">
                                                        {{ $item->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    @endforeach
                                    @foreach($product->Parameters as $parameter)
                                        <div>
                                            <p>{{ $parameter->name }}</p>
                                            <select class="js-example-basic-single" name="param_{{ $parameter->id }}">
                                                @foreach($parameter->items as $item)
                                                    <option
                                                        value="{{ $item->id }}"
                                                        @if($item->img)
                                                            data-img="{{ substr($item->img, 0, 4) == 'http' ? $item->img : asset('myimg/params/' . $item->img) }}"
                                                        @else
                                                            data-img="null"
                                                        @endif
                                                        data-operation="{{ $item->operation }}"
                                                        data-value="{{ $item->value }}"
                                                        data-value-text="{{ $item->operation == 2 ? "-" : "+" }} {{ $item->value ? $item->value : 0 }} {{ $item->operation === 1 ? "%" : " руб." }}">
                                                        {{ $item->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    @endforeach
                                </div>

                                <div class="buy-block">

                                        @if(!is_null($product->oldprice))
                                            <p class="old-price" id="id_old_price_element">{{$product->oldprice}} руб</p>
                                        @endif
                                        @if(!is_null($product->price))
                                        <p class="price"><span id="endprice">{{ $product->getPriceWithKoff() }}</span> <span>руб</span></p>
                                        @endif
                                        <button type="submit" class="btn brown">✔ КУПИТЬ</button>
                                        <a href="#modal-oneclick" class="one-click one-click-goods" data-action="modal:open">Купить в один клик!</a>
                                </div>
                            </form>
                        </div>
                    </div>


                    <div class="description content">
                        <div class="module">
                            <div class="module__header">⏰ ДОСТАВКА</div>
                            <div class="module__body">
                                Москва до подъезда - <strong>бесплатно</strong>
                                За МКАД - <strong>30 руб./км</strong>
                                По России - по договоренности.
                            </div>
                        </div>
                        <div class="b_mess">
                            <div class="mess_title">Закажите через мессенджер:</div>
                            <div class="mess_link">
                                <div class="mess_link_itm">
                                    <a href="viber://chat?number=+79209278676" class="social__link" target="_blank">
                                        <img class="social-icon viber" alt="viber" src="../images/viber-logo.png">
                                    </a>
                                    <a href="viber://chat?number=+79209278676" class="social__link" target="_blank">Viber</a>
                                </div>
                                <div class="mess_link_itm">
                                    <a href="https://api.whatsapp.com/send?phone=79209278676&amp;text= Здравствуйте!%20Хочу%20заказать%20мебель." target="_blank" class="social__link" onclick="yaCounter45479856.reachGoal('whatsapp');">
                                        <img class="social-icon whatsapp" alt="whatsapp" src="../images/whatsapp-logo.png">
                                    </a>
                                    <a href="https://api.whatsapp.com/send?phone=79209278676&amp;text= Здравствуйте!%20Хочу%20заказать%20мебель." target="_blank" onclick="yaCounter45479856.reachGoal('whatsapp');">Whatsapp</a>
                                </div>
                            </div>
                        </div>
                        <div class="product-utp">
                            <div class="product-utp__list">
                                <div class="product-utp__item">
                                    <div class="product-utp__icon"><img src="{{ asset('images/icons/' . 'product-utp1.png') }}" alt=""></div>
                                    <div class="product-utp__text"><strong>В наличии</strong><br>На складе<br><a href="/rasprodazha/">смотреть</a></div>
                                </div>
                                <div class="product-utp__item">
                                    <div class="product-utp__icon"><img src="{{ asset('images/icons/' . 'product-utp2.png') }}" alt=""></div>
                                    <div class="product-utp__text"><strong>Доставка</strong><br>от 5 дней</div>
                                </div>
                                <div class="product-utp__item">
                                    <div class="product-utp__icon"><img src="{{ asset('images/icons/' . 'product-utp3.png') }}" alt=""></div>
                                    <div class="product-utp__text"><strong>Гарантия</strong><br>12 месяцев</div>
                                </div>
                            </div>
                        </div>

                        <a href="#full-description" class="read-more" data-action="scroll"></a>
                    </div>
                    <div class="b_anons">
                        <div> {!! $product->text !!}</div>

                        <div>
                            <br />
                        </div>

                    </div>

                </div>

                <div class="user-help to-user-content">
                    <div class="user-help__column">
                        <p>Помочь выбрать?
                            <br>БЕСПЛАТНО проконсультируем Вас по телефону.
                            <br> Звоните <span class="phoneAllostat"><a href="tel:+79209278676" class="ya-phone phone_alloka">8 (920) 927 86 76</a></span>!</p>
                    </div>

                    <div class="user-help__column">
                        <p>Оставьте свой телефон и мы Вам перезвоним</p>
                        <br>
                        <br>
                        <form id="id_user_help_form" action="">
                            <div class="inline">
                                <input type="text" id="id_user_help_phone" name="phone" value="" placeholder="Ваш телефон">
                                <button class="btn green">Перезвоните мне</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <aside class="index-advants">
                <div class="container">
                    <div class="row">
                        <h3>Приемущества заказа товара {{ $product->name }}<br />на прямую у производителя</h3>
                        <div class="items grid">
                            <div class="item">
                                <div class="thumb"><img src="{{ asset('images/icons/i1.png') }}" alt=""></div>
                                <p>Собственное<br /> производство</p>
                            </div>
                            <div class="item">
                                <div class="thumb"><img src="{{ asset('images/icons/i2.png') }}" alt=""></div>
                                <p>Изготовление<br /> под заказ</p>
                            </div>
                            <div class="item">
                                <div class="thumb"><img src="{{ asset('images/icons/i3.png') }}" alt=""></div>
                                <p>Низкие<br /> цены</p>
                            </div>
                            <div class="item">
                                <div class="thumb"><img src="{{ asset('images/icons/i4.png') }}" alt=""></div>
                                <p>Доставка по<br /> Москве и МО</p>
                            </div>
                            <div class="item">
                                <div class="thumb"><img src="{{ asset('images/icons/i5.png') }}" alt=""></div>
                                <p>Гарантия<br /> до 24 месяцев</p>
                            </div>
                        </div>
                    </div>
                </div>
            </aside>

        </div>
    </main>

@endsection

<div id="modal-oneclick" class="modal small" style="display:none;">
    <div class="modal-header">Купить в 1 клик!</div>
    <div class="modal-oneclick-body modal-body">
        <p>Заполните поля ниже:</p>
        <form id="id_oneclick_form" action="">
            <fieldset>
                <input type="hidden" name="tovar">
                <input type="text" id="id_oneclick_name" name="name" value="" placeholder="Ваше имя" />
                <input type="text" id="id_oneclick_phone" name="phone" value="" placeholder="Ваш телефон" />
                <input type="text" id="id_oneclick_email" name="email" value="" placeholder="Ваша электронная почта" />
                <textarea name="question" id="id_oneclick_question" cols="30" rows="3" placeholder="Комментарий" ></textarea>
                <label>
                    <input type="checkbox" name="agree" value="" checked="checked" class="js-agree-check" data-target="#id_oneclick_submit2" /> Согласен с <a href="/politika-konfidentsialnosti/" target="_blank">политикой конфиденциальности</a>
                </label>

                <div class="tcenter">
                    <button class="btn green" id="id_oneclick_submit2" rel="modal:close" onsubmit="">ЗАКАЗАТЬ</button>
                </div>
                <input type="hidden" id="id_oneclick_form_action" name="action" value="oneclick" />
                <p>
                    <br>
                    Наш оператор <span>позвонит</span> Вам
                    <span>для уточнения</span> всех деталей</p>
            </fieldset>
        </form>
    </div>
</div>

@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>

        function updatePrice() {
            var startPrice = $('#startprice').val();
            var endPrice = parseFloat(startPrice);
            $('.js-example-basic-single').each(function(index, element) {
                var operation = $(element).find('option:selected').attr('data-operation');
                var value = $(element).find('option:selected').attr('data-value');
                value = value == "" ? 0 : value;

                if(operation == undefined) {

                }
                if(operation == 1) { // процент
                    endPrice = endPrice + (endPrice * parseFloat(value) / 100);
                }
                if(operation == 0) // сложение
                    endPrice += parseFloat(value);
                if(operation == 2) // вычитание
                    endPrice -= parseFloat(value);
                console.log("Operation: " + operation + " , Value: " + value);
            });
            $('#endprice').html(endPrice);
        }

        $(document).ready(function() {
            updatePrice();
        });

        $('.js-example-basic-single').select2({
            templateResult: function (item) {
                var iconUrl = $(item.element).attr('data-img');
                var addPrice = $(item.element).attr('data-value-text');
                if (!iconUrl) {
                    return item.text;
                }
                if(iconUrl != "null") {
                    var $item = $(
                        '<span class="select-result"><img src="' + iconUrl + '" class="img-flag"/><div><span class="text"> ' + item.text + '</span><br><span class="addprice">'+ addPrice +'</span></div></span>'
                    );
                }
                else {
                    var $item = $(
                        '<span class="select-result"><div><span class="text"> ' + item.text + '</span><br><span class="addprice">'+ addPrice +'</span></div></span>'
                    );
                }

                return $item;
            },
            templateSelection: function (item) {
                var iconUrl = $(item.element).attr('data-img');
                var addPrice = $(item.element).attr('data-value-text');
                if (!iconUrl) {
                    return item.text;
                }
                if(iconUrl != "null") {
                    var $item = $(
                        '<span class="select-result"><img src="' + iconUrl + '" class="img-flag"/><div><span class="text"> ' + item.text + '</span><br><span class="addprice">'+ addPrice +'</span></div></span>'
                    );
                }
                else {
                    var $item = $(
                        '<span class="select-result"><div><span class="text"> ' + item.text + '</span><br><span class="addprice">'+ addPrice +'</span></div></span>'
                    );
                }
                return $item;
            }
        });

        $('.js-example-basic-single').change(function () {
            updatePrice();
        })
    </script>
@endsection
