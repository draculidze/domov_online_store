<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index()
    {
        $productsLiders = \App\Models\Product::where('category_id', 3)->limit(8)->get();
        $productsNews = \App\Models\Product::where('category_id', 4)->limit(8)->get();
        $productsPrice = \App\Models\Product::where('category_id', 7)->limit(8)->get();
        return view('index', compact('productsLiders', 'productsNews', 'productsPrice'));
    }
}
