<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index($category = null, $subcategory = null)
    {
        $categories = Category::where('category_id', null)->get();
        if($category == null) {
            $products = Product::limit(30)->get();
            return view('catalog', compact('categories', 'products'));
        } else {
            $cat = Category::where('slug', $subcategory == null ? $category : $subcategory)->first();
            $cats = $cat->categories;
            $ids = array();
            $ids[] = $cat->id;
            foreach ($cats as $ct) {
                $ids[] = $ct->id;
            }
            $products = Product::where('category_id', $ids)->get();
            return view('catalog', compact('categories', 'cat', 'products'));
        }
    }
}
