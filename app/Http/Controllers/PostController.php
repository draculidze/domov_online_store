<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function show($id)
    {
        $post = Post::all()->where('slug', $id)->first();
        if(!$post)
            abort(404);
        return view("post", compact('post'));
    }
}
