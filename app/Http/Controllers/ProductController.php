<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Parameter;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index($id = null)
    {
        $product = Product::where('slug', $id)->first();
        return view('product', compact('product'));
    }

    public function Category()
    {
        return Category::find($this->category_id);
    }
}
