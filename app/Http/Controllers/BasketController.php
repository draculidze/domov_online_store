<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\ParameterItem;
use App\Models\Product;
use Illuminate\Http\Request;

class BasketController extends Controller
{
    public function basket()
    {
        $orderId = session('orderId');
        if (!is_null($orderId)) {
            $order = Order::findOrFail($orderId);
        }
        return view('basket', compact('order'));
    }

    public function basketConfirm(Request $request)
    {
        $orderId = session('orderId');
        if (is_null($orderId)) {
            return redirect()->route('index');
        }
        $order = Order::find($orderId);

        $success = $order->saveOrder($request->name, $request->phone, $request->address);

        if($success)
            session()->flash('success', 'Ваш заказ принят в обработку');
        else
            session()->flash('error', 'Не удалось создать заказ');
        return redirect()->route('index');
    }

    public function basketAdd($productId)
    {
        $orderId = session('orderId');
        if (is_null($orderId)) {
            $order = Order::create();
            session(['orderId' => $order->id]);
        } else {
            $order = Order::find($orderId);
            if(is_null($order)) {
                $order = Order::create();
                session(['orderId' => $order->id]);
            }
        }

        $product = Product::find($productId);
        $params = array();
        foreach($product->ParametersCategory as $parameter) {
            if(isset($_POST['param_' . $parameter->id])) {
                $itemId = $_POST['param_' . $parameter->id];
                $params[$parameter->id]['i'] = $itemId;
                $item = ParameterItem::find($itemId);
                $params[$parameter->id]['v'] = $item->value;
                $params[$parameter->id]['o'] = $item->operation;
                $params[$parameter->id]['n'] = $item->name;
                $params[$parameter->id]['p'] = $parameter->name;
            }

        }

        foreach($product->Parameters as $parameter) {
            if(isset($_POST['param_' . $parameter->id])) {
                $itemId = $_POST['param_' . $parameter->id];
                $params[$parameter->id]['i'] = $itemId;
                $item = ParameterItem::find($itemId);
                $params[$parameter->id]['v'] = $item->value;
                $params[$parameter->id]['o'] = $item->operation;
                $params[$parameter->id]['n'] = $item->name;
                $params[$parameter->id]['p'] = $parameter->name;
            }
        }

        if($order->products->contains($productId)) {
            $pivotRow = $order->products()->where('product_id', $productId)->first()->pivot;
            $pivotRow->price = $product->price;
            $pivotRow->cnt_product++;
            $pivotRow->update();

        } else {
            $order->products()->attach($productId);
            $pivotRow = $order->products()->where('product_id', $productId)->first()->pivot;
            $pivotRow->price = $product->price;
            $pivotRow->params = json_encode($params);
            $pivotRow->update();

        }

        return redirect()->route('basket');
    }

    public function basketRemove($productId)
    {
        $orderId = session('orderId');
        if (is_null($orderId)) {
            return redirect()->route('basket');
        }
        $order = Order::find($orderId);
        $order->products()->detach($productId);
        return redirect()->route('basket');
    }
}
