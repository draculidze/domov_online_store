<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Slider;
use Illuminate\Http\Request;

class SliderController extends Controller
{
    public static function getSlides()
    {
        $slides = Slider::all();
        return $slides;
    }

    public function addForm()
    {
        return view('admin.add_slide');
    }

    public function add(Request $request)
    {
        if(is_null($request->id))
            $slider = new Slider();
        else
            $slider = Slider::find($request->id);

        $slider->name = $request->name;

        if($request->file('imgPath')) {
            $imgName = $request->imgPath->getClientOriginalName();
            $request->file('imgPath')->storeAs('', $imgName, 'userslider');
            $slider->imgPath = $imgName;
        }

        $slider->save();
        return redirect()->route('admin.index')->with('success', 'Слайд добавлен/изменен');
    }

    public function editForm($id)
    {
        $slider = Slider::find($id);
        return view('admin.edit_slide', compact('slider'));
    }

    public function delete($id)
    {
        Slider::destroy($id);
        return redirect()->route('admin.index')->with('success', 'Слайд успешно удален');
    }
}
