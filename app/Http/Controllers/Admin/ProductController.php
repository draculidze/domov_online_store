<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function show($category = null)
    {
        if($category == null)
            $products = Product::all();
        else
            $products = Product::where('category_id', $category)->get();
        return view('admin.product', compact('products'));
    }

    public function addForm()
    {
        return view('admin.add_product');
    }

    public function add(Request $request)
    {
        if(is_null($request->id))
            $product = new Product();
        else
            $product = Product::find($request->id);
        $product->code = $request->code;
        $product->slug = $request->slug;
        $product->name = $request->name;
        $product->text = $request->text;
        //$product->thumbnail = $request->thumbnail;

        if($request->file('thumbnail')) {
            $imgName = $request->thumbnail->getClientOriginalName();
            $request->file('thumbnail')->storeAs('', $imgName);
            $product->thumbnail = $imgName;
        }

        $product->category_id = $request->category_id;
        $product->price = $request->price;
        $product->oldprice = $request->oldprice;
        $product->save();
        return redirect()->route('admin.product.show')->with('success', 'Товар добавлен/изменен');
    }

    public function edit($id)
    {
        $product = Product::find($id);
        return view('admin.edit_product', compact('product'));
    }

    public function delete($id)
    {
        Product::destroy($id);
        return redirect()->route('admin.product.show')->with('success', 'Товар успешно удален');
    }
}
