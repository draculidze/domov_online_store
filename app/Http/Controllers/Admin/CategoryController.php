<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function show()
    {
        $categories = Category::all();
        return view('admin.category', compact('categories'));
    }

    public function addForm()
    {
        return view('admin.add_category');
    }

    public function add(Request $request)
    {
        if(is_null($request->id))
            $category = new Category();
        else
            $category = Category::find($request->id);
        $category->code = $request->code;
        $category->slug = $request->slug;
        $category->name = $request->name;
        $category->text = $request->text;
        $category->thumbnail = $request->thumbnail;
        $category->category_id = $request->category_id == "null" ? null : $request->category_id;
        $category->coefficient = $request->coefficient;
        $category->save();
        return redirect()->route('admin.category.show');
    }

    public function edit($id)
    {
        $category = Category::find($id);
        return view('admin.edit_category', compact('category'));
    }

    public function delete($id)
    {
        Category::destroy($id);
        return redirect()->route('admin.category.show')->with('success', 'Категория успешно удалена');
    }
}
