<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Parameter;
use App\Models\ParameterItem;
use Illuminate\Http\Request;

class ParameterItemController extends Controller
{
    public function show($id)
    {
        $parameter = Parameter::find($id);
        $items = ParameterItem::where("parameter_id", $id)->get();
        return view('admin.parameter_item', compact('items', 'parameter'));
    }

    public function addForm($id)
    {
        $parameter = Parameter::find($id);
        return view('admin.add_parameter_item', compact('parameter'));
    }

    public function add(Request $request)
    {
        if(is_null($request->id))
            $pitem = new ParameterItem();
        else
            $pitem = ParameterItem::find($request->id);
        $pitem->name = $request->name;
        $pitem->parameter_id = $request->parameter_id;
        $pitem->operation = $request->operation == "null" ? null : $request->operation;
        $pitem->value = $request->value;
        $pitem->img = $request->img;
        $pitem->save();
        return redirect()->route('admin.parameter_item.show', $request->parameter_id)->with('success', 'Элемент успешно добавлен к параметру');
    }

    public function edit($id)
    {
        $pitem = ParameterItem::find($id);
        return view('admin.edit_parameter_item', compact('pitem'));
    }

    public function delete($id)
    {
        ParameterItem::destroy($id);
        return redirect()->back()->with('success', 'Параметр успешно удален');
    }
}
