<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function show()
    {
        $posts = Post::all();
        return view('admin.post', compact('posts'));
    }

    public function editForm($id)
    {
        $post = Post::find($id);
        return view('admin.edit_post', compact('post'));
    }

    public function edit(Request $request, $id)
    {
        $post = Post::find($id);
        $post->slug = $request->slug;
        $post->name = $request->name;
        $post->text = $request->text;
        $post->is_public = $request->is_public;
        $post->save();
        return redirect()->route('admin.post.show');
    }

    public function addForm()
    {
        return view('admin.add_post');
    }

    public function add(Request $request)
    {
        $post = new Post();
        $post->slug = $request->slug;
        $post->name = $request->name;
        $post->text = $request->text;
        $post->is_public = $request->is_public;
        $post->save();
        return redirect()->route('admin.post.show');
    }

    public function delete($id)
    {
        Post::destroy($id);
        return redirect()->back()->with('success', 'Пост успешно удален');
    }
}
