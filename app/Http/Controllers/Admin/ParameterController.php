<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Parameter;
use Illuminate\Http\Request;

class ParameterController extends Controller
{
    public function show()
    {
        $parameters = Parameter::all();
        return view('admin.parameter', compact('parameters'));
    }

    public function addForm()
    {
        return view('admin.add_parameter');
    }

    public function add(Request $request)
    {
        if(is_null($request->id))
            $parameter = new Parameter();
        else
            $parameter = Parameter::find($request->id);
        $parameter->name = $request->name;
        $parameter->category_id = $request->category_id == "null" ? null : $request->category_id;
        $parameter->product_id = $request->product_id == "null" ? null : $request->product_id;
        $parameter->save();
        return redirect()->route('admin.parameter.show');
    }

    public function edit($id)
    {
        $parameter = Parameter::find($id);
        return view('admin.edit_parameter', compact('parameter'));
    }

    public function delete($id)
    {
        Parameter::destroy($id);
        return redirect()->route('admin.parameter.show')->with('success', 'Параметр успешно добавлен');
    }
}
