<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function index (Request $request)
    {
        $q = $request->q;
        $products = Product::where('name', 'LIKE', "%{$q}%")->get();
        $categories = Category::where('category_id', null)->get();
        return view('search', compact('products', 'q', 'categories'));
    }
}
