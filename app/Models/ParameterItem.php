<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ParameterItem extends Model
{
    use HasFactory;

    public function Parameter()
    {
        return $this->belongsTo(Parameter::class);
    }

    public function OperationText()
    {
        switch ($this->operation) {
            case 0:
                return "Сложение";
            case 1:
                return "Умножение";
            case 2:
                return "Вычитание";
        }
    }
}
