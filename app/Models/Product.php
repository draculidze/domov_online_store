<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function getPriceForCount() {
        if(!is_null($this->pivot))
            return $this->pivot->cnt_product * $this->price;
        return $this->price;
    }

    public function getPriceWithKoff()
    {
        return ($this->price) * $this->category->coefficient;
    }

    public function ParametersCategory()
    {
        return $this->hasMany(Parameter::class, 'category_id', 'category_id');
    }

    public function Parameters()
    {
        return $this->hasMany(Parameter::class);
    }
}
