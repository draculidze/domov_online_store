<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@index')->name('index');
Route::get('/login/', 'Admin\AdminController@loginForm')->name('login.form');
Route::post('/login/', 'Admin\AdminController@login')->name('login');
Route::get('/register/', 'Admin\AdminController@registerForm')->name('register.form');
Route::post('/register/', 'Admin\AdminController@register')->name('register');

//Route::get('/admin/', 'AdminController@index')->name('admin.index')->middleware('admin');
Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'admin'], function() {
    Route::get('/', 'AdminController@index')->name('admin.index');
    Route::post('/logout', 'AdminController@logout')->name('admin.logout');

    Route::get('/category', 'CategoryController@show')->name('admin.category.show');
    Route::get('/category_edit/{category}', 'CategoryController@edit')->name('admin.category.edit');
    Route::get('/category_add', 'CategoryController@addForm')->name('admin.category.addForm');
    Route::post('/category_add', 'CategoryController@add')->name('admin.category.add');
    Route::get('/category_delete/{category}', 'CategoryController@delete')->name('admin.category.delete');

    Route::get('/product/{category?}', 'ProductController@show')->name('admin.product.show');
    Route::get('/product_edit/{category}', 'ProductController@edit')->name('admin.product.edit');
    Route::get('/product_add', 'ProductController@addForm')->name('admin.product.addForm');
    Route::post('/product_add', 'ProductController@add')->name('admin.product.add');
    Route::get('/product_delete/{category}', 'ProductController@delete')->name('admin.product.delete');

    Route::get('/post', 'PostController@show')->name('admin.post.show');
    Route::get('/post_edit/{post}', 'PostController@editForm')->name('admin.post.editForm');
    Route::post('/post_edit/{post}', 'PostController@edit')->name('admin.post.edit');
    Route::get('/post_add', 'PostController@addForm')->name('admin.post.addForm');
    Route::post('/post_add', 'PostController@add')->name('admin.post.add');
    Route::get('/post_delete/{post}', 'PostController@delete')->name('admin.post.delete');

    Route::get('/order/', 'OrderController@show')->name('admin.order');

    Route::get('/slide_edit/{post}', 'SliderController@editForm')->name('admin.slide.editForm');
    Route::get('/slide_add', 'SliderController@addForm')->name('admin.slide.addForm');
    Route::post('/slide_add', 'SliderController@add')->name('admin.slide.add');
    Route::get('/slide_delete/{post}', 'SliderController@delete')->name('admin.slide.delete');

    Route::get('/parameter', 'ParameterController@show')->name('admin.parameter.show');
    Route::get('/parameter_edit/{parameter}', 'ParameterController@edit')->name('admin.parameter.edit');
    Route::get('/parameter_add', 'ParameterController@addForm')->name('admin.parameter.addForm');
    Route::post('/parameter_add', 'ParameterController@add')->name('admin.parameter.add');
    Route::get('/parameter_delete/{category}', 'ParameterController@delete')->name('admin.parameter.delete');

    Route::get('/parameter_item/{parameter}', 'ParameterItemController@show')->name('admin.parameter_item.show');
    Route::get('/parameter_item_edit/{item}', 'ParameterItemController@edit')->name('admin.parameter_item.edit');
    Route::get('/parameter_item_add/{parameter}', 'ParameterItemController@addForm')->name('admin.parameter_item.addForm');
    Route::post('/parameter_item_add', 'ParameterItemController@add')->name('admin.parameter_item.add');
    Route::get('/parameter_item_delete/{category}', 'ParameterItemController@delete')->name('admin.parameter_item.delete');
});

Route::get('/catalog/{category?}/{subcategory?}', 'CategoryController@index')->name('category');
//Route::get('/catalog/{category?}/{subcategory}', 'CategoryController@index')->name('categorysub');
Route::get('/product/{product}', 'ProductController@index')->name('product');
Route::get('/basket', 'BasketController@basket')->name('basket');
Route::post('/basket/confirm', 'BasketController@basketConfirm')->name('basket-confirm');
Route::post('/basket/add/{id}', 'BasketController@basketAdd')->name('basket-add');
Route::post('/basket/remove/{id}', 'BasketController@basketRemove')->name('basket-remove');
Route::get('/search', 'SearchController@index')->name('search');

Route::get('/{post}', 'PostController@show')->name('post');
