<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [];
        $categories[] = [
            'name' => 'Кровати',
            'slug' => 'krovati',
            'category_id' => null
        ];
        $categories[] = [
            'name' => 'Кровати односпальные',
            'slug' => 'odnospalnye',
            'category_id' => 1
        ];
        $categories[] = [
            'name' => 'Кровати полуторные',
            'slug' => 'polutornye',
            'category_id' => 1
        ];
        DB::table('categories')->insert($categories);
    }
}
